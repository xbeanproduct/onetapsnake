﻿#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

[InitializeOnLoad]
[CustomEditor(typeof(Grid))]
public class GridEditor : Editor {

	public List<string> smallComponentsTag = new List<string>{"Food", "ReverseFood", "Star", "Helmet", "TeleportA", "TeleportB", "StarFragment" };
	public List<string> halfComponentsTag = new List<string>{"StartPoint", "EndPoint", "Rock", "Monster", "MonsterCircle", "Wall"};
	public List<string> quarterComponentsTag = new List<string>{"SmallWall", "SmallRock", "Tussock", "SmallGround"};
    public List<string> freeComponentTag = new List<string>{"GateCorner", "GateCornerRemote"};
	
	Grid grid;
		
//	private bool isCreatedBounder = false;
	GameObject mapComponents;
    GameObject backgroundComponents;
	
	GameObject autoGenBorder;
	// Use this for initialization
	void Start () {
		
	}
	
	void OnEnable() {
		grid = (Grid)target;


		SceneView.onSceneGUIDelegate = GridUpdate;		

		
	}
	
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void CreateBounder() {
		
		if (grid.mapType == Grid.MapType.Normal) {
            if (GameObject.Find("AutoScrollGenBorder")) {
				DestroyImmediate(GameObject.Find("AutoScrollGenBorder"));
                
            }
        
			if (!GameObject.Find("AutoGenBorder")) {
				Debug.Log("Create AutoGenBorder");
				
				autoGenBorder = new GameObject();
				
				autoGenBorder.name = "AutoGenBorder";
				
				if (GameObject.Find("MapComponents")){
					autoGenBorder.transform.parent = GameObject.Find("MapComponents").transform;
				}
				
				int defineColums = 7;
				int defineRows = 11;
				
				// Debug.Log(Camera.main.pixelHeight  +" + " + Camera.main.pixelWidth );
				float width = Camera.main.pixelWidth;
				float height = Camera.main.pixelHeight;
				
				float newGridSize = grid.gridSize / 2;
				
				Object smallWallPref = Resources.Load("Prefabs/MapComponents/Miscellaneous/SmallWall");
				Object wallPref = Resources.Load("Prefabs/MapComponents/Miscellaneous/Wall");
				float leftBound = - defineColums * newGridSize + newGridSize ;
				float rightBound =  defineColums * newGridSize;
				float botBound = - defineRows * newGridSize;
				float topBound = newGridSize + defineRows * newGridSize;
				
				int numberOfColums = Mathf.Abs(Mathf.RoundToInt((rightBound - leftBound)/newGridSize));
				int numberOfRows = Mathf.Abs(Mathf.RoundToInt((topBound - botBound)/ newGridSize));
	
				if (!smallWallPref) {
					return;
				}
				
				if (!wallPref) {
					return;
				}
				
				// GameObject obj = (GameObject)EditorUtility.InstantiatePrefab(prefab);
					// obj.transform.position = new Vector2(0, 0);
				// Generate Top And Bot Border
				for(int i = 0; i <= numberOfColums; i++) {
					GameObject obj = (GameObject)EditorUtility.InstantiatePrefab(wallPref);
					obj.transform.position = new Vector2(leftBound + i * newGridSize, topBound);
					obj.transform.parent = autoGenBorder.transform;
					
					GameObject obj2 = (GameObject)EditorUtility.InstantiatePrefab(wallPref);
					obj2.transform.position = new Vector2(leftBound + i * newGridSize, botBound);
					obj2.transform.parent = autoGenBorder.transform;
				}
				
				// Generate Left And Right Border
				for(int i = 1; i < numberOfRows; i++) {
					GameObject obj = (GameObject)EditorUtility.InstantiatePrefab(smallWallPref);
					obj.transform.position = new Vector2(leftBound, botBound + i * newGridSize);
					obj.transform.parent = autoGenBorder.transform;
					
					GameObject obj2 = (GameObject)EditorUtility.InstantiatePrefab(smallWallPref);
					obj2.transform.position = new Vector2(rightBound, botBound + i * newGridSize);
					obj2.transform.parent = autoGenBorder.transform;
					
					
				}
				
				// Fill Space
				for(int i = 0; i <= defineRows + 1; i++) {
					GameObject wallObj = (GameObject)EditorUtility.InstantiatePrefab(wallPref);
					wallObj.transform.position = new Vector2(leftBound - grid.gridSize/2, botBound + i * grid.gridSize);
					wallObj.transform.parent = autoGenBorder.transform;
					
					GameObject wallObj2 = (GameObject)EditorUtility.InstantiatePrefab(wallPref);
					wallObj2.transform.position = new Vector2(rightBound + grid.gridSize, botBound + i) * grid.gridSize;
					wallObj2.transform.parent = autoGenBorder.transform;
				}
			
			}
			else {
				if (!autoGenBorder) {
					autoGenBorder = GameObject.Find("AutoGenBorder");
				}
				if (autoGenBorder.transform.parent == null) {
					if (GameObject.Find("MapComponents")) {
						autoGenBorder.transform.parent = GameObject.Find("MapComponents").transform;
					}
				}
			}
		}
		else {
			
			if (GameObject.Find("AutoGenBorder")) {
				DestroyImmediate(GameObject.Find("AutoGenBorder"));
                
			}
            
            if (!GameObject.Find("AutoScrollGenBorder")) {
                CreateScrollMap();
            }
                
            
		}
	}
	
    void CreateScrollMap() {
        Debug.Log("Create AutoScrollGenBorder");
				
        autoGenBorder = new GameObject();
        
        autoGenBorder.name = "AutoScrollGenBorder";
        
        if (GameObject.Find("MapComponents")){
            autoGenBorder.transform.parent = GameObject.Find("MapComponents").transform;
        }
        
        int defineColums = 7;
        int defineRows = 11;
        
        // Debug.Log(Camera.main.pixelHeight  +" + " + Camera.main.pixelWidth );
        float width = Camera.main.pixelWidth;
        float height = Camera.main.pixelHeight;
        
        float newGridSize = grid.gridSize / 2;
        
        Object smallWallPref = Resources.Load("Prefabs/MapComponents/Miscellaneous/SmallWall");
        Object wallPref = Resources.Load("Prefabs/MapComponents/Miscellaneous/Wall");
        float leftBound = - defineColums * newGridSize + newGridSize ;
        float rightBound =  defineColums * newGridSize;
        float botBound = - defineRows * newGridSize;
        float topBound = newGridSize + defineRows * newGridSize;
        
        int numberOfColums = Mathf.Abs(Mathf.RoundToInt((rightBound - leftBound)/newGridSize)) * grid.NumberOfMap;
        int numberOfRows = Mathf.Abs(Mathf.RoundToInt((topBound - botBound)/ newGridSize));

        if (!smallWallPref) {
            return;
        }
        
        if (!wallPref) {
            return;
        }
        
        // GameObject obj = (GameObject)EditorUtility.InstantiatePrefab(prefab);
            // obj.transform.position = new Vector2(0, 0);
        // Generate Top And Bot Border
        for(int i = 0; i <= numberOfColums; i++) {
            GameObject obj = (GameObject)EditorUtility.InstantiatePrefab(wallPref);
            obj.transform.position = new Vector2(leftBound + i * newGridSize, topBound);
            obj.transform.parent = autoGenBorder.transform;
            
            GameObject obj2 = (GameObject)EditorUtility.InstantiatePrefab(wallPref);
            obj2.transform.position = new Vector2(leftBound + i * newGridSize, botBound);
            obj2.transform.parent = autoGenBorder.transform;
        }
        
        // Generate Left And Right Border
        for(int i = 1; i < numberOfRows; i++) {
            GameObject obj = (GameObject)EditorUtility.InstantiatePrefab(smallWallPref);
            obj.transform.position = new Vector2(leftBound, botBound + i * newGridSize);
            obj.transform.parent = autoGenBorder.transform;
            
            GameObject obj2 = (GameObject)EditorUtility.InstantiatePrefab(smallWallPref);
            obj2.transform.position = new Vector2(leftBound + (numberOfColums/ grid.NumberOfMap * newGridSize * grid.NumberOfMap), botBound + i * newGridSize);
            obj2.transform.parent = autoGenBorder.transform;
            
            
        }
        
        // Fill Space
        for(int i = 0; i <= defineRows + 1; i++) {
            GameObject wallObj = (GameObject)EditorUtility.InstantiatePrefab(wallPref);
            wallObj.transform.position = new Vector2(leftBound - grid.gridSize/2, botBound + i * grid.gridSize);
            wallObj.transform.parent = autoGenBorder.transform;
            
            GameObject wallObj2 = (GameObject)EditorUtility.InstantiatePrefab(wallPref);
            wallObj2.transform.position = new Vector2(leftBound + (numberOfColums/ grid.NumberOfMap * newGridSize * grid.NumberOfMap) + grid.gridSize, botBound + i) * grid.gridSize;
            wallObj2.transform.parent = autoGenBorder.transform;
        }
    }
	
	void LoadAllResourse() {
		if (!GameObject.Find("GameManager")){
			Debug.Log("Create GameManager");
			EditorUtility.InstantiatePrefab(Resources.Load("Prefabs/MapComponents/GameManager"));
		}
		
        if (!GameObject.Find("Timekeeper")){
            Debug.Log("Create Timekeeper");
            EditorUtility.InstantiatePrefab(Resources.Load("Prefabs/MapComponents/Timekeeper"));
        }
        
		if (!GameObject.Find("MapComponents")) {
			Debug.Log("Create MapComponents");
			
			mapComponents = new GameObject();
			
			mapComponents.name = "MapComponents";
			
		}
		else {
			mapComponents = GameObject.Find("MapComponents");
		}
        
        if (!GameObject.Find("BackgroundComponents")) {
			Debug.Log("Create BackgroundComponents");
			
			backgroundComponents = new GameObject();
			
			backgroundComponents.name = "BackgroundComponents";
			
		}
		else {
			backgroundComponents = GameObject.Find("BackgroundComponents");
		}
		
//		if (GameObject.Find("StartPoint")) {
//			
//			if (!GameObject.Find("SnakeHead")) {
//				Debug.Log("Detected StartPoint. Auto create Snake!!!");
//				EditorUtility.InstantiatePrefab(Resources.Load("Prefabs/Characters/SnakeHolder"));
//				
//				GameObject snake = GameObject.Find("SnakeHead");
//		
//				GameObject.Find("StartPoint").GetComponent<StartPointController>().ReturnObjectPosition(snake);
//			}
//		}
		
		
		
		mapComponents.transform.position = Vector3.zero;
        backgroundComponents.transform.position = Vector3.zero;
	}
	
	private void SettingCamera() {
        if (grid.mapType != Grid.MapType.Scroll) {
            Camera.main.transform.position = new Vector3(0.5f, 0.5f, 0);
        }
        else {
            if (!Camera.main.GetComponent<CameraController>()) {
                Camera.main.gameObject.AddComponent(typeof(CameraController));
            }
            
            Camera.main.GetComponent<CameraController>().numMap = grid.NumberOfMap;
        }
		Camera.main.backgroundColor = Color.white;
		Camera.main.orthographicSize = 6;
		Camera.main.nearClipPlane = -10;
	}
	
	void GridUpdate(SceneView sceneView) {
		if (EditorApplication.currentScene.Contains("MapSelect")) {
			return;	
		}
		
		SettingCamera();
		
		LoadAllResourse();
		
		CreateBounder();
		
		RegridObject();
		
		HandleEvent();
	}
	
	void RegridObject() {
        if (!grid.reGrid) {
            return;
        }
        
		GameObject[] allObjects = UnityEngine.Object.FindObjectsOfType<GameObject>();

		foreach(GameObject obj in allObjects){
            
            
			if (obj.GetComponent<SpriteRenderer>() && !obj.tag.Equals("BackgroundComponent") && !freeComponentTag.Contains(obj.transform.tag)) {
				if (smallComponentsTag.Contains(obj.transform.tag) || 
					obj.transform.name.Equals("SnakeHead")) {
					float newGridSize = grid.gridSize / 5;
					
					Vector2 aligned = new Vector2(Mathf.Floor(obj.transform.position.x/newGridSize)* newGridSize + newGridSize/2.0f,
									Mathf.Floor(obj.transform.position.y/newGridSize)* newGridSize + newGridSize/2.0f);
					obj.transform.position = new Vector3(aligned.x, aligned.y, obj.transform.position.z);
				}
				else 
				if (quarterComponentsTag.Contains(obj.transform.tag)) {
					float newGridSize = grid.gridSize / 2;
					
					Vector2 aligned = new Vector2(Mathf.Floor(obj.transform.position.x/newGridSize)* newGridSize + newGridSize/2.0f,
									Mathf.Floor(obj.transform.position.y/newGridSize)* newGridSize + newGridSize/2.0f);
					obj.transform.position = new Vector3(aligned.x, aligned.y, obj.transform.position.z);
				}
				else
				if (halfComponentsTag.Contains(obj.transform.tag)) {
					float newGridSize = grid.gridSize / 2;
					
					Vector2 aligned = new Vector2(Mathf.Floor(obj.transform.position.x/newGridSize)* newGridSize,
									Mathf.Floor(obj.transform.position.y/newGridSize)* newGridSize);
					obj.transform.position = new Vector3(aligned.x, aligned.y, obj.transform.position.z);
				}
				else {
					Vector2 aligned = new Vector2(Mathf.Floor(obj.transform.position.x/grid.gridSize)*grid.gridSize + grid.gridSize/2.0f,
										Mathf.Floor(obj.transform.position.y/grid.gridSize)*grid.gridSize + grid.gridSize/2.0f);
					obj.transform.position = new Vector3(aligned.x, aligned.y, obj.transform.position.z);
				}
				
				if (obj.transform.parent == null) {
					obj.transform.parent = mapComponents.transform;
				}
			
			}
            else {
                if (obj.tag.Equals("BackgroundComponent")) {
                    if (obj.transform.parent == null) {
                        obj.transform.parent = backgroundComponents.transform;
                    }
                }
            }
			
		}
	}
	
	void HandleEvent() {
		Event e = Event.current;

		Ray r = Camera.current.ScreenPointToRay(new Vector3(e.mousePosition.x, -e.mousePosition.y + Camera.current.pixelHeight));
		Vector3 mousePos = r.origin;
		
		if (e.isKey && e.character.ToString().ToLower() == grid.cloneGameObjectHotKey.ToString().ToLower()) {
			if (Selection.gameObjects.Length == 0) {
				return;
			}
			
			Debug.Log(Selection.gameObjects.Length);
			foreach(GameObject obj in Selection.gameObjects) {
				
				if (obj.GetComponent<SpriteRenderer>() &&
					!obj.name.Equals("SnakeHolder") &&
					!obj.name.Equals("SnakeHead")) {
					Object prefab = EditorUtility.GetPrefabParent(obj);
					if (prefab) {
						GameObject newObj;
						newObj = (GameObject)EditorUtility.InstantiatePrefab(prefab);
						newObj.transform.position = obj.transform.position;
					}
				}
				else {
					Debug.Log("Cannot clone object " + obj.name);
				}

			}
			
		}
	}
	
}

#endif