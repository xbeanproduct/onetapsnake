﻿using UnityEngine;
using System.Collections.Generic;

public enum SoundType
{
	Replace,
	New,
	Only,
	Loop
}

public enum SoundID
{
	// Background musics
	MainMenu,
	MainGame1,
	MainGame2,
	MainGame3,
	MainGame4,

	// Sound effects
	ButtonClose,
	ButtonOpen,
	ButtonChangeScene,
	Wrong,
	Coin,
	StartMove,
	SnakeTurn,
	SnakeDamage,
	SnakeDie,
	GetStar,
	GetFood,
	GetShield,
	BreakShield,
	GetRevert,
	Teleport,
	Explose,
	Crack,
	GateOpen,
	Confirm,
	Success,
	MonsterDie,
	Slow,
	Punch,
	RunFast,

	Raven,
	Whale,
	Bee,
	Star1,
	Star2,
	Star3,

	WinGame,
	LoseGame,

	Count
}

public class SoundManager : Singleton<SoundManager>
{
	[Header("Background Musics")]

	public AudioClip mainMenu;
	public AudioClip mainGame1;
	public AudioClip mainGame2;
	public AudioClip mainGame3;
	public AudioClip mainGame4;

	[Header("Sound Effects")]

	public AudioClip buttonClose;
	public AudioClip buttonOpen;
	public AudioClip changeScene;
	public AudioClip wrong;
	public AudioClip coin;

	public AudioClip snakeTurn;
	public AudioClip snakeDamage;
	public AudioClip snakeDie;
	public AudioClip startMove;
	public AudioClip getStar;
	public AudioClip getFood;
	public AudioClip getShield;
	public AudioClip breakShield;
	public AudioClip getRevert;
	public AudioClip teleport;
	public AudioClip explose;
	public AudioClip crack;
	public AudioClip gateOpen;
	public AudioClip confirm;
	public AudioClip success;
	public AudioClip monsterDie;
	public AudioClip slow;
	public AudioClip punch;
	public AudioClip runFast;

	public AudioClip raven;
	public AudioClip whale;
	public AudioClip bee;
	public AudioClip star1;
	public AudioClip star2;
	public AudioClip star3;

	public AudioClip winGame;
	public AudioClip loseGame;


	[Header("Volume")]

	/// <summary>
	/// The music volume.
	/// </summary>
	[Range(0,1)]
	public float musicVolume = 1f;

	/// <summary>
	/// The sound volume.
	/// </summary>
	[Range(0,1)]
	public float soundVolume = 1f;

	/// <summary>
	/// The audio source to play music.
	/// </summary>
	private AudioSource musicSource;

	/// <summary>
	/// The lookup table for background musics.
	/// </summary>
	private Dictionary<SoundID, AudioClip> musicLookup;

	/// <summary>
	/// The lookup table for sound effects.
	/// </summary>
	private Dictionary<SoundID, AudioSource> soundLookup;

	/// <summary>
	/// True if enable to play background music.
	/// </summary>
	public bool isMusicEnabled = true;

	/// <summary>
	/// True if enable to play sound effect.
	/// </summary>
	public bool isSoundEnabled = true;

	protected override void Awake()
	{
		base.Awake ();
		// Set instance
		_instance = this;

		// Create audio source
		musicSource = gameObject.AddComponent<AudioSource>();
		musicSource.loop = true;
		musicSource.volume = musicVolume;

		// Create loopkup table for background musics
		musicLookup = new Dictionary<SoundID, AudioClip>();

		// Create loopkup table for sound effects
		soundLookup = new Dictionary<SoundID, AudioSource>();

		// Add background musics
		musicLookup.Add(SoundID.MainMenu, mainMenu);
		musicLookup.Add(SoundID.MainGame1, mainGame1);
		musicLookup.Add(SoundID.MainGame2, mainGame2);
		musicLookup.Add(SoundID.MainGame3, mainGame3);
		musicLookup.Add (SoundID.MainGame4, mainGame4);

		// Add sound effects
		soundLookup.Add(SoundID.ButtonClose, AddAudioSource(buttonClose));
		soundLookup.Add(SoundID.ButtonOpen, AddAudioSource(buttonOpen));
		soundLookup.Add (SoundID.ButtonChangeScene, AddAudioSource(changeScene));
		soundLookup.Add (SoundID.Wrong, AddAudioSource(wrong));
		soundLookup.Add (SoundID.Coin, AddAudioSource(coin));

		soundLookup.Add (SoundID.SnakeTurn, AddAudioSource(snakeTurn));
		soundLookup.Add (SoundID.SnakeDamage, AddAudioSource(snakeDamage));
		soundLookup.Add (SoundID.SnakeDie, AddAudioSource(snakeDie));
		soundLookup.Add (SoundID.StartMove, AddAudioSource(startMove));
		soundLookup.Add (SoundID.GetStar, AddAudioSource(getStar));
		soundLookup.Add (SoundID.GetFood, AddAudioSource(getFood));
		soundLookup.Add (SoundID.GetShield, AddAudioSource(getShield));
		soundLookup.Add (SoundID.BreakShield, AddAudioSource(breakShield));
		soundLookup.Add (SoundID.GetRevert, AddAudioSource(getRevert));
		soundLookup.Add (SoundID.Teleport, AddAudioSource(teleport));
		soundLookup.Add (SoundID.Explose, AddAudioSource(explose));
		soundLookup.Add (SoundID.Crack, AddAudioSource(crack));
		soundLookup.Add (SoundID.GateOpen, AddAudioSource(gateOpen));
		soundLookup.Add (SoundID.Confirm, AddAudioSource(confirm));
		soundLookup.Add (SoundID.Success, AddAudioSource(success));
		soundLookup.Add (SoundID.MonsterDie, AddAudioSource(monsterDie));
		soundLookup.Add (SoundID.Slow, AddAudioSource(slow));
		soundLookup.Add (SoundID.Punch, AddAudioSource(punch));
		soundLookup.Add (SoundID.RunFast, AddAudioSource(runFast));

		soundLookup.Add (SoundID.Raven, AddAudioSource(raven));
		soundLookup.Add (SoundID.Whale, AddAudioSource(whale));
		soundLookup.Add (SoundID.Bee, AddAudioSource(bee));
		soundLookup.Add (SoundID.Star1, AddAudioSource(star1));
		soundLookup.Add (SoundID.Star2, AddAudioSource(star2));
		soundLookup.Add (SoundID.Star3, AddAudioSource(star3));

		soundLookup.Add (SoundID.WinGame, AddAudioSource(winGame));
		soundLookup.Add (SoundID.LoseGame, AddAudioSource(loseGame));


		//
		isMusicEnabled = UserData.Instance.BGMOn;
		isSoundEnabled = UserData.Instance.SFXOn;
	}

	public void SetMusicEnable(bool isEnable)
	{
		isMusicEnabled = isEnable;
		if (!isEnable) {
		
			StopMusic ();

		} else {
			PlayRandomMusic (SoundID.MainGame1, SoundID.MainGame2, SoundID.MainGame3, SoundID.MainGame4);

			SoundManager.Instance.PlaySound(SoundID.ButtonOpen);
		}
	}

	public void SetFXEnable(bool isEnable)
	{
		isSoundEnabled = isEnable;

		if (isSoundEnabled) SoundManager.Instance.PlaySound(SoundID.ButtonOpen);
	}

	// Change music volume
	public float MusicVolume
	{
		get
		{
			return musicVolume;
		}

		set
		{
			musicVolume = Mathf.Clamp01(value);

			if (isMusicEnabled)
			{
				musicSource.volume = musicVolume;
			}
		}
	}

	// Change sound volume
	public float SoundVolume
	{
		get
		{
			return soundVolume;
		}

		set
		{
			soundVolume = Mathf.Clamp01(value);

			foreach (AudioSource audioSource in soundLookup.Values)
			{
				audioSource.volume = soundVolume;
			}
		}
	}

	// Enable/Disable background music
	public bool MusicEnabled
	{
		get
		{
			return isMusicEnabled;
		}

		set
		{
			if (isMusicEnabled != value)
			{
				isMusicEnabled = value;

				//				musicSource.enabled = isMusicEnabled;
				musicSource.volume = isMusicEnabled ? musicVolume : 0;
			}
		}
	}

	// Enable/Disable sound effect
	public bool SoundEnabled
	{
		get
		{
			return isSoundEnabled;
		}

		set
		{
			if (isSoundEnabled != value)
			{
				isSoundEnabled = value;

				foreach (AudioSource audioSource in soundLookup.Values)
				{
					audioSource.enabled = isSoundEnabled;
				}
			}
		}
	}

	public bool PlaySound(SoundID soundID, SoundType type = SoundType.Replace, float delay = 0f)
	{
		if (!isSoundEnabled) return false;

		// Get audio source
		AudioSource audioSource = soundLookup[soundID];

		if (audioSource != null)
		{
			if (type == SoundType.Loop)
			{
				audioSource.loop = true;

				if (!audioSource.isPlaying)
				{
					if (delay > 0)
					{
						audioSource.PlayDelayed(delay);
					}
					else
					{
						audioSource.Play();
					}
				}
			}
			else
			{
				audioSource.loop = false;

				if (type == SoundType.Replace)
				{
					if (delay > 0)
					{
						audioSource.PlayDelayed(delay);
					}
					else
					{
						audioSource.Play();
					}
				}
				else if (type == SoundType.New)
				{
					audioSource.PlayOneShot(audioSource.clip);
				}
				else if (type == SoundType.Only)
				{
					if (!audioSource.isPlaying)
					{
						if (delay > 0)
						{
							audioSource.PlayDelayed(delay);
						}
						else
						{
							audioSource.Play();
						}
					}
				}
			}

			return true;
		}

		return false;
	}

	public bool PlayRandomSound(params SoundID[] soundIDs)
	{
		return PlaySound(soundIDs.GetAny());
	}

	public bool PlayMusic(SoundID soundID)
	{
		// Get audio clip
		AudioClip audioClip = musicLookup[soundID];

		if (audioClip != null)
		{
			// Set clip
			musicSource.clip = audioClip;

			// Play music
			musicSource.Play();

			//
			musicSource.volume = isMusicEnabled ? musicVolume : 0;

			return true;
		}

		return false;
	}

	public bool PlayRandomMusic(params SoundID[] musicIDs)
	{
		return PlayMusic(musicIDs.GetAny());
	}

	public void StopSound(SoundID soundID)
	{
		AudioSource audioSource = soundLookup[soundID];

		if (audioSource != null)
		{
			audioSource.Stop();
		}
	}

	public void StopAllSounds()
	{
		foreach (AudioSource audioSource in soundLookup.Values)
		{
			audioSource.Stop();
		}
	}

	public void StopMusic()
	{
		musicSource.Stop();
	}

	public static void PlayButtonClick()
	{
		_instance.PlaySound(SoundID.ButtonClose);
	}

	public static void PlayButtonOpen()
	{
		_instance.PlaySound(SoundID.ButtonOpen);
	}

	AudioSource AddAudioSource(AudioClip clip)
	{
		AudioSource source = gameObject.AddComponent<AudioSource>();
		source.clip = clip;
		source.playOnAwake = false;
		source.volume = soundVolume;

		return source;
	}
}
