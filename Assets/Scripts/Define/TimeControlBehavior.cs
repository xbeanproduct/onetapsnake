﻿using UnityEngine;
using Chronos;

public class TimeControlBehavior : MonoBehaviour {

	public Timeline time
	{
		get
		{
			return GetComponent<Timeline>();
		}
	}
}
