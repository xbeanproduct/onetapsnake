﻿using UnityEngine;
using System.Collections;
using Restifizer;

public class ApiManageErrorHandle : MonoBehaviour, IErrorHandler {

    public bool onRestifizerError(RestifizerError restifizerError) {
        Debug.Log(restifizerError.Message);
        // Debug.Log(restifizerError.Status);
        return true;
    }
}
