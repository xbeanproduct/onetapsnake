﻿using UnityEngine;
using System.Collections;
using GoogleMobileAds.Api;

public class GoogleAds : Singleton<GoogleAds> {

	private InterstitialAd interstitial = null;

	private BannerView banner = null;
	// Use this for initialization
	void Start () {
		LoadBanner ();	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void LoadBanner() {
		string adUnit = "";

		#if UNITY_ANDROID 
		adUnit = Settings.AdsBannerAndroid;
		#elif UNITY_IOS
		adUnit = Settings.AdsBanneriOS;
		#endif

		banner = new BannerView (adUnit, AdSize.SmartBanner, AdPosition.Bottom);
		AdRequest request = new AdRequest.Builder ().Build ();
		banner.LoadAd(request);
		banner.Show ();
	}

	public void HideBanner() {
		banner.Hide ();
	}

	public void ShowBanner() {
		banner.Show ();
	}

	public void RequestInterstitial() {
		string adUnit = "";

		#if UNITY_ANDROID 
		adUnit = Settings.AdsInterstitialAndroid;
		#elif UNITY_IOS
		adUnit = Settings.AdsInterstitialiOS;
		#endif

		interstitial = new InterstitialAd (adUnit);
		AdRequest request = new AdRequest.Builder ().Build ();
		interstitial.LoadAd (request);
	}

	public void ShowInterstitial() {
		if (interstitial != null && interstitial.IsLoaded ()) {
			interstitial.Show ();
		}

		RequestInterstitial ();
	}
}
