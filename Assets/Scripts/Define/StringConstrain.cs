﻿using UnityEngine;
using System.Collections;

public class StringConstrain : MonoBehaviour {

	// Use this for initialization
	public static readonly string[] GameOverMsg = {
        "You are so noob!!!!!!", 
        "Lol, fucking noob!!!!", 
        "You better stop play this game!"
        
        };

	public static readonly string SuggestEnergyTitle = "Oops!";
    public static readonly string[] SuggestEnergyMsg = {
        "Your snake seems to be tired. Do you want get some energy?", 
        "This snake can walk anymore. Get it some energy."
        
	};

	public static readonly string BuyEnergyTitle = "Buy Energy";
	public static readonly string[] BuyEnergyMsg = {
		"Blah blah mua Energy nè... blah blah blah"
	};

	public static readonly string SkinConfirmTitle = "Skin Confirm";
	public static readonly string SkinConfirmMsg = "Purchase this awesome Skin for {0} Coins?";

	public static readonly string ToolkitConfirmTitle = "Toolkit Confirm";
	public static readonly string ToolkitConfirmMsg = "Use this toolkit for {0} Coins?";

	public static readonly string UnlockMapConfirmTitle = "Unlock Chapter {0}";
	public static readonly string UnlockMapConfirmMsg = "Unlock this Chapter for {0} Coins?";

	public static readonly string InsufficientTitle = "Insufficient Scoin";
	public static readonly string ToolkitInsufficientMsg = "You need {0} more Coins to use this toolkit. Add now!";
	public static readonly string SkinInsufficientMsg = "You need {0} more Coins to purchase this awesome Skin. Add now!";
	public static readonly string UnlockMapInsufficientMsg = "You need {0} more Coins to unlock this Chapter. Add now!";


	public static readonly string BuyScoinTitle = "Buy Scoin";
	public static readonly string[] BuyScoinMsg = {
		"Buy some Coins if you wish!"
	};

	public static readonly string ShowAdsRewardTitle = "Congratulation!";

	public static readonly string ShowAdsRewardMsg = "You have earned {0} {1} for watching ads!";

	public static readonly string InviteFriendTitle = "Invite Friends";

	public static readonly string InviteFriendMsg = "Nobody can beat me LOL~";

	public static readonly string InviteRewardMsg = "You have earn {0} {1} for invite {2}.";

	public static readonly string PlayTutorialMsg = "Play the Tutorial again?\nLearn rules and you 'll know how to break them.";

	public static readonly string[] QuitGameTitles = {
		"Really quit?"
	};

	public static readonly string[] QuitGameMsgs = {
		"You are a quitter?"
	};

	public static readonly string[] ReplayGameTitles = {
		"Replay"
	};

	public static readonly string[] ReplayGameMsgs = {
		"Hey loser, replay then become the winner!"
	};

	public static string SkinConfirmMsgFormat(int value) {
		return string.Format (SkinConfirmMsg, value);
	}

	public static string ToolkitConfirmMsgFormat(int value) {
		return string.Format (ToolkitConfirmMsg, value);
	}

	public static string UnlockMapConfirmTitleFormat(int value) {
		return string.Format (UnlockMapConfirmTitle, value);
	}

	public static string UnlockMapConfirmMsgFormat(int value) {
		return string.Format (UnlockMapConfirmMsg, value);
	}

	public static string ToolkitInsufficientMsgFormat(int value) {
		return string.Format (ToolkitInsufficientMsg, value);
	}

	public static string SkinInsufficientMsgFormat(int value) {
		return string.Format (SkinInsufficientMsg, value);
	}

	public static string UnlockMapInsufficientMsgFormat(int value) {
		return string.Format (UnlockMapInsufficientMsg, value);
	}

	public static string ShowAdsRewardMsgFormat(int value, string type) {
		return string.Format (ShowAdsRewardMsg, value, type);
	}

	public static string InviteRewardMsgFormat(int value, string type, string friends) {
		return string.Format (InviteRewardMsg, value, type, friends);
	}
 }
