﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Facebook.Unity;
using Facebook.MiniJSON;
using DG.Tweening;

public class MapSelectionController : MonoBehaviour {

	public Text coinText;

    public ScrollRect mapScrollRect;
    public RectTransform mapContentPanel;
    public Sprite starUI;
    
    public RectTransform avatar;
    public RectTransform username;
    public RectTransform loginFBBtn;
    
	public GameObject unlockChapter2, unlockChapter3;

	public GameObject starMapSelectionEffect;
    
	[SerializeField]
	private GameObject[] _mapsBtnList;
	private int _passedLevels;
    
    public GameObject chapter1UnlockPref, chapter2UnlockPref, chapter3UnlockPref;
    
	public bool isSetAvatar = false;
	// Use this for initialization
	void Start () {
		unlockChapter2.GetComponent<NonCanvasButton> ().Interactable = true;
		unlockChapter3.GetComponent<NonCanvasButton> ().Interactable = true;
		SettingLevelBtn();
        GetFriendHightestLevel();
        
		Resources.UnloadUnusedAssets ();
		GoogleAds.Instance.ShowBanner ();
	}

	// Setting Level Button in Map with Current User Data
	public void SettingLevelBtn() {

		// If this is a first time play, enable Level 1
		if (UserData.Instance.MapsData == null || UserData.Instance.MapsData.Count == 0) {
			_passedLevels = 0;
			_mapsBtnList[0].FindInChildren(Settings.MapButton).GetComponent<NonCanvasButton>().Interactable = true;

			// This mean user must Unlock Perious Chapter First ( In this case is Chapter 2 )
			unlockChapter3.SetActive(false);
		}
		// Else find all Level played and the Hightest Level
		else {

			// Passed Levels
			_passedLevels = UserData.Instance.MapsData.Count;

			// Hightest Level
            int hightestLevel = UserData.Instance.HightestLevel;
            
			// Unlock all Levels below Hightest Level
            for(int i = 0; i < hightestLevel; i++) {
				_mapsBtnList[i].FindInChildren(Settings.MapButton).GetComponent<NonCanvasButton>().Interactable = true;
            }
            

			int currentLevelPlayedStar = 0;
			// Set Information for All Passed Levels
			for(int i = 0; i < _passedLevels; i++) {
                Hashtable mapData = (Hashtable)UserData.Instance.MapsData[i];
                int levelId = int.Parse(mapData[Settings.MapLevelKey].ToString());

                if (levelId - 1 < _mapsBtnList.Length) {
					if (MapManager.Instance.isNewLevelUnlock) {
						if (levelId != MapManager.Instance.currentLevelPlayed) {
							SetStar (_mapsBtnList [levelId - 1].FindInChildren (Settings.MapButton), int.Parse (mapData [Settings.MapStarKey].ToString ()));
						} else {
							currentLevelPlayedStar = int.Parse (mapData [Settings.MapStarKey].ToString ());
						}
					} else {
						if (MapManager.Instance.currentLevelPlayed == levelId) {
							if (int.Parse (mapData [Settings.MapStarKey].ToString ()) > MapManager.Instance.currentLevelStar) {
								AppendStarAnim (_mapsBtnList [levelId - 1].FindInChildren (Settings.MapButton), MapManager.Instance.currentLevelStar, int.Parse (mapData [Settings.MapStarKey].ToString ()));
							} else {
								SetStar (_mapsBtnList [levelId - 1].FindInChildren (Settings.MapButton), int.Parse (mapData [Settings.MapStarKey].ToString ()));
							}

						} else {
							SetStar (_mapsBtnList [levelId - 1].FindInChildren (Settings.MapButton), int.Parse (mapData [Settings.MapStarKey].ToString ()));
						}
					}

                }
			}
            
			// Get current last played Level
			int scrollLevel = MapManager.Instance.currentLevelPlayed;

			// If not, set as hightest Level
			if (scrollLevel == 0) {
				scrollLevel = hightestLevel;
			}

			// Last Played Level position
			if (MapManager.Instance.lastLevelPos != Vector2.zero && scrollLevel > 6) {
				mapContentPanel.anchoredPosition = MapManager.Instance.lastLevelPos;
			} else {
				MapManager.Instance.lastLevelPos = Vector2.zero;
			}
				
			// If Current Hightest Level isn't a Final Level, Scroll to Next level of Hightest Level
            if (hightestLevel < _mapsBtnList.Length) {
				

                if (!MapManager.Instance.isNewLevelUnlock) {
					_mapsBtnList[hightestLevel].FindInChildren(Settings.MapButton).GetComponent<NonCanvasButton>().Interactable = true;
					if (scrollLevel > 6) {
						GoToBtn(_mapsBtnList[scrollLevel].transform);
                    }
                }
                else {
                    // If User Perious passed a Level, play the "Destroy Level Button" Animation
					if (scrollLevel > 6) {
						GoToBtn(_mapsBtnList[scrollLevel].transform);
                    }
					StartCoroutine(UnlockLevel(
						_mapsBtnList[MapManager.Instance.currentLevelPlayed - 1].FindInChildren (Settings.MapButton),
						currentLevelPlayedStar,
						_mapsBtnList[hightestLevel].FindInChildren(Settings.MapButton).transform,
						hightestLevel));
                    
                    MapManager.Instance.isNewLevelUnlock = false;
                }
            }
			// Else go to Final Level
            else {

                GoToBtn(_mapsBtnList[hightestLevel - 1].transform);
            }
            
			// Set button unlock Chapter
			if (hightestLevel >= Settings.Chapter2Level) {
                unlockChapter2.SetActive(false);
            }
			if (hightestLevel >= Settings.Chapter3Level || hightestLevel < Settings.Chapter2Level - 1) {
				unlockChapter3.SetActive (false);
			} else {
				unlockChapter3.SetActive (true);
			}

			MapManager.Instance.currentLevelPlayed = -1;

			// Disable Tutorial
			if (hightestLevel > 2) {
				UserData.Instance.CompleteTutorialPhase1 = true;
				UserData.Instance.WasPlayedTutorial = true;
			}
			if (hightestLevel > 4) {
				UserData.Instance.CompleteTutorialPhase2 = true;
				UserData.Instance.WasPlayedTutorial = true;
			}
			if (hightestLevel > 5) {
				UserData.Instance.CompleteToolkitTutorial = true;
				UserData.Instance.WasPlayedTutorial = true;
			}
			if (hightestLevel > 19) {
				UserData.Instance.CompleteTutorialPhase3 = true;
				UserData.Instance.WasPlayedTutorial = true;
			}
			if (hightestLevel > 25) {
				UserData.Instance.CompleteTutorialPhase4 = true;
				UserData.Instance.WasPlayedTutorial = true;
			}

			UserData.Instance.Save ();
		}
        
        
	}
    
	// Unlock Level Animation when finished a Level
	IEnumerator UnlockLevel(GameObject oldLevel, int starOldLevel, Transform levelBtn, int hightestLevel) {
		if (hightestLevel > 6)
		{
			yield return new WaitForSeconds (1);
		}

		yield return SetStarAnimForPassedLevel (oldLevel, starOldLevel).WaitForCompletion();

		// Enable this level
		levelBtn.GetComponent<NonCanvasButton>().Interactable = true;

		// Set animation depend on Chapter
		if (hightestLevel < Settings.Chapter2Level) {
			Instantiate (chapter1UnlockPref, new Vector3 (levelBtn.position.x, levelBtn.position.y, 0), Quaternion.identity);
		} else if (hightestLevel < Settings.Chapter3Level) {
			Instantiate (chapter2UnlockPref, new Vector3 (levelBtn.position.x, levelBtn.position.y, 0), Quaternion.identity);
		} else {
			Instantiate (chapter3UnlockPref, new Vector3 (levelBtn.position.x, levelBtn.position.y, 0), Quaternion.identity);
		}


		DOVirtual.DelayedCall (1.5f, () => {
			if (MapManager.Instance.currentLevelPlayed == -1) {
				levelBtn.GetComponent<NonCanvasButton>().OnTouchRelease();
			}
		});
	

    }

	private Tween AddScoinAnim(int amount, float delay = 0) {

		Sequence sequence = DOTween.Sequence ();
		int currentScoin = UserData.Instance.EarnScoin;
		int destinationScoin = currentScoin + amount * 10;

		return sequence.Append (coinText.transform.DOBlendableScaleBy (new Vector2 (.5f, .5f), .2f))
			.Append (DOTween.To ((pNewValue) => {
			StatsManager.Instance.SetEarnScoin (Mathf.RoundToInt (pNewValue));
			}, currentScoin, destinationScoin, amount * 0.2f).SetDelay (delay))
			.Append (coinText.transform.DOBlendableScaleBy (new Vector2 (-.5f, -.5f), .2f));

	}

	private Sequence AppendStarAnim(GameObject mapBtn, int oldStar, int newStar) {
		Sequence sequence = DOTween.Sequence ().AppendInterval(1);

		SetStar (mapBtn, oldStar);
		if (oldStar == 1 && newStar == 2) {
			sequence.Append (DOVirtual.DelayedCall (.5f, () => {
				mapBtn.FindInChildren (Settings.MapStar2).GetComponent<SpriteRenderer> ().sprite = starUI;
				mapBtn.FindInChildren (Settings.MapStar2).transform.DOPunchScale (transform.localScale * 1.5f, .5f, 0, 0);
				starMapSelectionEffect.Create(null, mapBtn.FindInChildren (Settings.MapStar2).transform.position);
				SoundManager.Instance.PlaySound(SoundID.Star1);
			}))
				.Join (AddScoinAnim (1));

		} else if (oldStar == 1 && newStar == 3) {
			sequence.Append (DOVirtual.DelayedCall (.5f, () => {
				mapBtn.FindInChildren (Settings.MapStar2).GetComponent<SpriteRenderer> ().sprite = starUI;
				mapBtn.FindInChildren (Settings.MapStar2).transform.DOPunchScale (transform.localScale * 1.5f, .5f, 0, 0);
				starMapSelectionEffect.Create(null, mapBtn.FindInChildren (Settings.MapStar2).transform.position);
				SoundManager.Instance.PlaySound(SoundID.Star1);
			}))
				.Append (DOVirtual.DelayedCall (.5f, () => {
				mapBtn.FindInChildren (Settings.MapStar3).GetComponent<SpriteRenderer> ().sprite = starUI;
				mapBtn.FindInChildren (Settings.MapStar3).transform.DOPunchScale (transform.localScale * 1.5f, .5f, 0, 0);
				starMapSelectionEffect.Create(null, mapBtn.FindInChildren (Settings.MapStar3).transform.position);
					SoundManager.Instance.PlaySound(SoundID.Star2);
			}))
				.Join (AddScoinAnim (2));
		} else if (oldStar == 2 && newStar == 3) {
			sequence.Append (DOVirtual.DelayedCall (.5f, () => {
				mapBtn.FindInChildren (Settings.MapStar3).GetComponent<SpriteRenderer> ().sprite = starUI;
				mapBtn.FindInChildren (Settings.MapStar3).transform.DOPunchScale (transform.localScale * 1.5f, .5f, 0, 0);
				starMapSelectionEffect.Create(null, mapBtn.FindInChildren (Settings.MapStar3).transform.position);
				SoundManager.Instance.PlaySound(SoundID.Star1);
			}))
				.Join (AddScoinAnim (1));
		} else if (oldStar == 0) {
			sequence.Append (SetStarAnimForPassedLevel (mapBtn, newStar));
		}

		return sequence;
	}

	private Sequence SetStarAnimForPassedLevel(GameObject mapBtn, int star) {
		Sequence sequence = DOTween.Sequence ();
		mapBtn.GetComponent<NonCanvasButton>().Interactable = true;
		switch (star) {
		case 1:
			sequence.Append (DOVirtual.DelayedCall (.5f, () => {
				mapBtn.FindInChildren (Settings.MapStar1).GetComponent<SpriteRenderer> ().sprite = starUI;
				mapBtn.FindInChildren (Settings.MapStar1).transform.DOPunchScale(transform.localScale * 1.5f, .5f, 0, 0);
				starMapSelectionEffect.Create(null, mapBtn.FindInChildren (Settings.MapStar1).transform.position);
				SoundManager.Instance.PlaySound(SoundID.Star1);
			}))
				.Join (AddScoinAnim (1));
			break;
		case 2:
			sequence.Append (DOVirtual.DelayedCall (.5f, () => {
				mapBtn.FindInChildren (Settings.MapStar1).GetComponent<SpriteRenderer> ().sprite = starUI;
				mapBtn.FindInChildren (Settings.MapStar1).transform.DOPunchScale (transform.localScale * 1.5f, .5f, 0, 0);
				starMapSelectionEffect.Create(null, mapBtn.FindInChildren (Settings.MapStar1).transform.position);
				SoundManager.Instance.PlaySound(SoundID.Star1);
			}))
				.Append (DOVirtual.DelayedCall (.5f, () => {
				mapBtn.FindInChildren (Settings.MapStar2).GetComponent<SpriteRenderer> ().sprite = starUI;
				mapBtn.FindInChildren (Settings.MapStar2).transform.DOPunchScale (transform.localScale * 1.5f, .5f, 0, 0);
				starMapSelectionEffect.Create(null, mapBtn.FindInChildren (Settings.MapStar1).transform.position);
				SoundManager.Instance.PlaySound(SoundID.Star2);
			}))
				.Join (AddScoinAnim (2));
			break;
		case 3:
			sequence.Append (DOVirtual.DelayedCall (.5f, () => {
				mapBtn.FindInChildren (Settings.MapStar1).GetComponent<SpriteRenderer> ().sprite = starUI;
				mapBtn.FindInChildren (Settings.MapStar1).transform.DOPunchScale(transform.localScale * 1.5f, .5f, 0, 0);
				starMapSelectionEffect.Create(null, mapBtn.FindInChildren (Settings.MapStar1).transform.position);
				SoundManager.Instance.PlaySound(SoundID.Star1);
			}))
				.Append (DOVirtual.DelayedCall (.5f, () => {
				mapBtn.FindInChildren (Settings.MapStar2).GetComponent<SpriteRenderer> ().sprite = starUI;
				mapBtn.FindInChildren (Settings.MapStar2).transform.DOPunchScale(transform.localScale * 1.5f, .5f, 0, 0);
				starMapSelectionEffect.Create(null, mapBtn.FindInChildren (Settings.MapStar2).transform.position);
				SoundManager.Instance.PlaySound(SoundID.Star2);
			}))
				.Append (DOVirtual.DelayedCall (.5f, () => {
				mapBtn.FindInChildren (Settings.MapStar3).GetComponent<SpriteRenderer> ().sprite = starUI;
				mapBtn.FindInChildren (Settings.MapStar3).transform.DOPunchScale(transform.localScale * 1.5f, .5f, 0, 0);
				starMapSelectionEffect.Create(null, mapBtn.FindInChildren (Settings.MapStar3).transform.position);
				SoundManager.Instance.PlaySound(SoundID.Star3);
			}))
				.Join (AddScoinAnim (3));

			break;
		}

//		sequence.AppendInterval (.3f);

		return sequence;
	}
	// Set star of level button
    private void SetStar(GameObject mapBtn, int star) {
		mapBtn.GetComponent<NonCanvasButton>().Interactable = true;
        switch (star) {
            case 1:
                mapBtn.FindInChildren(Settings.MapStar1).GetComponent<SpriteRenderer>().sprite = starUI;
                break;
            case 2:
				mapBtn.FindInChildren(Settings.MapStar1).GetComponent<SpriteRenderer>().sprite = starUI;
				mapBtn.FindInChildren(Settings.MapStar2).GetComponent<SpriteRenderer>().sprite = starUI;
                break;
            case 3:
				mapBtn.FindInChildren(Settings.MapStar1).GetComponent<SpriteRenderer>().sprite = starUI;
				mapBtn.FindInChildren(Settings.MapStar2).GetComponent<SpriteRenderer>().sprite = starUI;
				mapBtn.FindInChildren(Settings.MapStar3).GetComponent<SpriteRenderer>().sprite = starUI;
                break;
        }
    }
    
	// Get current friend position
	public void GetFriendHightestLevel() {
        StartCoroutine(ApiManager.Instance.GetFriendHightestLevel(response => {
            if (response != null && response.Code == Settings.SuccessCode) {
                foreach(ApiFriendHightestLevelHashtable data in response.Data) {
                    if (data.Friends.Count > 0) {
                        if (data.Level - 1 < _mapsBtnList.Length) {
                            SettingFriend(_mapsBtnList[data.Level - 1].FindInChildren(Settings.MapButton), data);
                        }
                    }
                }
            }
        }));
    }
    
	// Set  Friend Avatar
    private void SettingFriend(GameObject mapBtn, ApiFriendHightestLevelHashtable data) {
        switch (data.Friends.Count) {
            case 3:
            StartCoroutine(LoadFriendAvatar(mapBtn.FindInChildren(Settings.MapFriend1), data.Friends[0].FriendAvatar));
            StartCoroutine(LoadFriendAvatar(mapBtn.FindInChildren(Settings.MapFriend2), data.Friends[1].FriendAvatar));
            StartCoroutine(LoadFriendAvatar(mapBtn.FindInChildren(Settings.MapFriend3), data.Friends[2].FriendAvatar));
            break;
            case 2:
            StartCoroutine(LoadFriendAvatar(mapBtn.FindInChildren(Settings.MapFriend1), data.Friends[0].FriendAvatar));
            StartCoroutine(LoadFriendAvatar(mapBtn.FindInChildren(Settings.MapFriend2), data.Friends[1].FriendAvatar));
            break;
            case 1:
            StartCoroutine(LoadFriendAvatar(mapBtn.FindInChildren(Settings.MapFriend1), data.Friends[0].FriendAvatar));
            break;
        }
    }
    
    private IEnumerator LoadFriendAvatar(GameObject friendAvatar, string url) {
        friendAvatar.SetActive(true);
        url = url + "&redirect=false";
        WWW www = new WWW(url);
        yield return www;
        if (www.error == null) {
            Dictionary<string, object> dict = Json.Deserialize(www.text) as Dictionary<string, object>;
            Dictionary<string, object> dataDict = dict["data"] as Dictionary<string,object>;
            string imageUrl = (string)dataDict["url"];
            WWW www2 = new WWW(imageUrl);
            yield return www2;
            if (www2.error == null) {
				friendAvatar.FindInChildren(Settings.FriendAvatarContent).GetComponent<SpriteRenderer>().sprite = www2.texture.ToSprite();
            }
            
            
        }
    }
    


	
	// Update is called once per frame
	void Update () {
		if (UserData.Instance.IsFBLogin) {
            if (loginFBBtn.gameObject.activeInHierarchy) {
                loginFBBtn.gameObject.SetActive(false);
            }
            if (!isSetAvatar) {
                StartCoroutine(LoadImage(UserData.Instance.AvatarUrl));
                if (!string.IsNullOrEmpty(UserData.Instance.Username)) {
                    username.GetComponent<Text>().text = UserData.Instance.Username;
                    username.gameObject.SetActive(true);
                }
            }
        }
        else {
            if (!loginFBBtn.gameObject.activeInHierarchy) {
                loginFBBtn.gameObject.SetActive(true);
            }
            if (username.gameObject.activeInHierarchy) {
                username.gameObject.SetActive(false);
            }
        }
	}

	// Event when select button level
	public void SelectLevel(int level) {
//		if (UserData.Instance.Energy > 0) {
//			PopupManager.Instance.OpenPopupPlayLevel(level);
//
//			SoundManager.Instance.PlaySound (SoundID.ButtonOpen);
//		}
//		else {
//			// Popup Ask friend
//			PopupManager.Instance.OpenPopupSuggestEnergy(false);
//			SoundManager.Instance.PlaySound (SoundID.Wrong);
//		}
		PopupManager.Instance.OpenPopupPlayLevel(level);

		SoundManager.Instance.PlaySound (SoundID.ButtonOpen);

	}

	public void LoginFB() {
		LoginFB (() => {

		});
	}
    
	public void LoginFB(System.Action callback = null) {
		isSetAvatar = false;
        PopupManager.Instance.ShowActivityIndicator();
        
        ApiManager.Instance.LoginSync(response => {
            PopupManager.Instance.HideActivityIndicator();
            
			if (response == null) {
				return;
			}

            if (response.Code != Settings.SuccessCode) {
                Debug.Log(response.Message);
                Debug.Log(response.ResultMessage);
                return;
            }
            else {
                Debug.Log(response.Message);
                SettingLevelBtn();
                GetFriendHightestLevel();
                StatsManager.Instance.UpdateStats();
                
                // username.GetComponent<Text>().text = response.Name;
				callback();
            }
        });
    }
    
	public void AddEnergy() {
		StatsManager.Instance.IncreaseEnergy(1);
		SoundManager.Instance.PlaySound (SoundID.ButtonOpen);
	}
    
    public void ShowScoresLeaderboard() {
        PopupManager.Instance.OpenPopupScoreLeaderboard();
		SoundManager.Instance.PlaySound (SoundID.ButtonOpen);
    }
    
    public void ShowStarsLeaderboard() {
        PopupManager.Instance.OpenPopupStarLeaderboard();
		SoundManager.Instance.PlaySound (SoundID.ButtonOpen);
    }
    
    public void OpenPopupShowSkin() {
        PopupManager.Instance.OpenPopupChangeSkin();
		SoundManager.Instance.PlaySound (SoundID.ButtonOpen);
    }
    
    public void OpenPopupStats() {
        PopupManager.Instance.OpenPopupUserStats();
		SoundManager.Instance.PlaySound (SoundID.ButtonOpen);
    }

	public void OpenPopupSuggestCoin() {
		PopupManager.Instance.OpenPopupSuggestScoin (0, true, InsufficientType.None);
		SoundManager.Instance.PlaySound (SoundID.ButtonOpen);
	}

	public void OpenPopupSuggestEnergy() {
		PopupManager.Instance.OpenPopupSuggestEnergy (true);
		SoundManager.Instance.PlaySound (SoundID.ButtonOpen);
	}
    
    IEnumerator LoadImage(string url) {
        isSetAvatar = true;
        
        url = url + "&redirect=false";
        WWW www = new WWW(url);
        yield return www;
        if (www.error == null) {
            Dictionary<string, object> dict = Json.Deserialize(www.text) as Dictionary<string, object>;
            Dictionary<string, object> dataDict = dict["data"] as Dictionary<string, object>;
            string imageUrl = (string)dataDict["url"];
            WWW www2 = new WWW(imageUrl);
            yield return www2;
            if (www2.error == null) {
                avatar.FindInChildren("Avatar").GetComponent<Image>().sprite = www2.texture.ToSprite();
            }
            
            
        }
    }
    
	// Scroll map to this button
	void GoToBtn(Transform btn) {
//        Vector2 targetPos = (Vector2)mapScrollRect.transform.InverseTransformPoint(mapContentPanel.position)
//			- (Vector2)mapScrollRect.transform.InverseTransformPoint(btn.position);
//		Debug.Log("Y1: " + -btn.position.y / (80.8f / 8100) + 4050);
//		Debug.Log ("Y2: " + ((80.8 / 8100) + 4050));


		Vector2 targetPos = new Vector2(mapContentPanel.anchoredPosition.x, -btn.position.y / (80.8f / 8100) + 4050);


		var newPos = new Vector2(mapContentPanel.anchoredPosition.x, targetPos.y);

		var cameraMovement = GameObject.FindObjectOfType<CameraMovement> ();

		mapContentPanel.DOAnchorPos (newPos, 1).SetEase (Ease.OutSine).OnUpdate(() => {
			if (cameraMovement) {
				cameraMovement.CameraPosUpdate();
			}
		});

		MapManager.Instance.lastLevelPos = newPos;
    }
    
    public void UnlockChapter2() {
		if (!CheckSCoin (Settings.UnlockChapterPrice)) {
			return;
		}

		PopupManager.Instance.OpenPopupNotificationConfirm (StringConstrain.UnlockMapConfirmTitleFormat(2), StringConstrain.UnlockMapConfirmMsgFormat(Settings.UnlockChapterPrice), () => {

			StatsManager.Instance.DecreaseScoin(Settings.UnlockChapterPrice);

			int hightestLevel = Settings.Chapter2Level;

			if (!MapManager.Instance.CheckLevel(hightestLevel)) {
				MapManager.Instance.FinishLevel(0, 0, hightestLevel);

				UserData.Instance.HightestLevel = hightestLevel;

				UserData.Instance.Save();
			}

			SettingLevelBtn();

			Analytics.Instance.LogEvent(Analytics.Purchase, Analytics.PurchaseTypeChapter, string.Format("Chapter {0}", 2.ToString("00")), 1000);
		});

        
       
        
        
    }
    
    public void UnlockChapter3() {
		if (!CheckSCoin (Settings.UnlockChapterPrice)) {
			return;
		}

		PopupManager.Instance.OpenPopupNotificationConfirm (StringConstrain.UnlockMapConfirmTitleFormat(3), StringConstrain.UnlockMapConfirmMsgFormat(Settings.UnlockChapterPrice), () => {

			StatsManager.Instance.DecreaseScoin(Settings.UnlockChapterPrice);

			int hightestLevel = Settings.Chapter3Level;

			if (!MapManager.Instance.CheckLevel(hightestLevel)) {
				MapManager.Instance.FinishLevel(0, 0, hightestLevel);

				UserData.Instance.HightestLevel = hightestLevel;

				UserData.Instance.Save();
			}

			SettingLevelBtn();

			Analytics.Instance.LogEvent(Analytics.Purchase, Analytics.PurchaseTypeChapter, string.Format("Chapter {0}", 3.ToString("00")), 1000);
		});

       
        
        
    }
    
	bool CheckSCoin(int cost) {
		if (UserData.Instance.Scoin < cost) {
			// show popup
			PopupManager.Instance.OpenPopupSuggestScoin(cost - UserData.Instance.Scoin, false, InsufficientType.UnlockMap);
			return false;
		}
		return true;
	}

	public void OpenSetting() {
		PopupManager.Instance.OpenPopupMapSetting ();
	}
	
}
