﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Facebook.Unity;
using Facebook.MiniJSON;
using System.Text;
using System;

public enum RequestObject {
	Coin,
	Energy
}

public class FacebookManager : Singleton<FacebookManager> {

	public static readonly string Coin = "coin";
	public static readonly string Energy = "energy";
    
    public void InitFacebook(System.Action<bool> callback) {
//		#if !UNITY_EDITOR
        FB.Init(() => {
            FB.ActivateApp();
            callback(true);
        });
//		#else
		callback(true);
//		#endif
    }

	public bool IsLogin() {
		return FB.IsLoggedIn;
	}

	public void Login(FacebookDelegate<ILoginResult> callback) {
		FB.LogInWithReadPermissions (Settings.FBPerms, callback);
	}

	public void Share(string contentUrl, string info, string imageUrl, FacebookDelegate<IShareResult> callback) {
		if (FB.IsInitialized) {
			if (IsLogin ()) {
				ShareLink (contentUrl, info, imageUrl, callback);
			} else {
				Login ((lgResult) => {
					if (IsLogin ()) {
						ShareLink(contentUrl, info, imageUrl, callback);
					}
				});
			}
		} else {
			InitFacebook ((result) => {
				if (FB.IsInitialized) {
					if (IsLogin ()) {
						ShareLink (contentUrl, info, imageUrl, callback);
					} else {
						Login ((lgResult) => {
							if (IsLogin ()) {
								ShareLink(contentUrl, info, imageUrl, callback);
							}
						});
					}
				}
			});
		}

	}

	private void ShareLink(string contentUrl, string info, string imageUrl, FacebookDelegate<IShareResult> callback) {
		FB.ShareLink (
			new System.Uri (contentUrl),
			info,
			"Download this game on Appstore and Play store!",
			new System.Uri (imageUrl),
			callback);
	}

	void Request(string title, string message, RequestObject obj) {
		FB.AppRequest(
			message,
			null,
			new List<object> (){ "app_non_users" },
			null,
			null,
			obj == RequestObject.Coin ? Coin : Energy,
			title,
			delegate (IAppRequestResult result) {
				if (string.IsNullOrEmpty(result.Error))
				{
					var dictionary = result.ResultDictionary;

					if (dictionary.ContainsKey("to"))
					{
						var to = dictionary["to"] as List<object>;

						if (to == null)
						{
							string[] tos = dictionary["to"].ToString().Split(new char[] {','}, StringSplitOptions.RemoveEmptyEntries);

							// Coin
							if (obj == RequestObject.Coin)
							{
								for (int i = 0; i < tos.Length; i++)
								{
									FriendData.Instance.AddCoinInvitedFriend(tos[i]);
									Debug.Log("Add - Array: " + tos[i]);
								}
							}
							// Mana
							else
							{
								for (int i = 0; i < tos.Length; i++)
								{
									FriendData.Instance.AddManaInvitedFriend(tos[i]);
								}
							}
						}
						else
						{
							// Coin
							if (obj == RequestObject.Coin)
							{
								for (int i = 0; i < to.Count; i++)
								{
									FriendData.Instance.AddCoinInvitedFriend(to[i].ToString());
									Debug.Log("Add: " + to[i]);
								}
							}
							// Mana
							else
							{
								for (int i = 0; i < to.Count; i++)
								{
									FriendData.Instance.AddManaInvitedFriend(to[i].ToString());
								}
							}
						}

						Log.Debug(FriendData.Instance.ToString());
					}
				}
				else
				{
					Log.Debug("Invite error: " + result.Error);
				}

				//				if (callback != null)
				//				{
				//					callback(result.Error);
				//				}


			}
		);
	}

	public void RequestFriend(string title, string message, RequestObject obj) {

		if (FB.IsInitialized) {
			if (IsLogin ()) {
				Request (title, message, obj);
			} else {
				Login ((lgResult) => {
					if (IsLogin ()) {
						Request (title, message, obj);
					}
				});
			}
		} else {
			InitFacebook ((result) => {
				if (FB.IsInitialized) {
					if (IsLogin ()) {
						Request (title, message, obj);
					} else {
						Login ((lgResult) => {
							if (IsLogin ()) {
								Request (title, message, obj);
							}
						});
					}
				}
			});
		}
        
    }

	public static void GetAppUserName(string uid, Action<string> callback)
	{
		FB.API(string.Format("me?fields=friends.uid({0})", uid), HttpMethod.GET, (result) => {
			if (!string.IsNullOrEmpty(result.Error))
			{
				callback("");
			}
			else
			{
				var dictionary = result.ResultDictionary;

				if (dictionary.ContainsKey("friends"))
				{
					var friends = dictionary["friends"] as Dictionary<string, object>;
					var data = friends["data"] as List<object>;

					if (data.Count == 0)
					{
						callback("");
					}
					else
					{
						var friend = data[0] as Dictionary<string, object>;
						callback(friend["name"].ToString());
					}
				}
				else
				{
					callback("");
				}
			}
		});
	}
}
