﻿using UnityEngine;
using System.Collections;
using UnityEngine.Advertisements;

public class UnityAds : Singleton<UnityAds> {

	private bool _isRewardScoin = false;
	private int _numScoinReward = 0;
	private int _numEnergyReward = 0;

    void ShowRewardedAd()
    {
        
        if (Advertisement.IsReady("rewardedVideo"))
        {
        var options = new ShowOptions { resultCallback = HandleShowResult };
        Advertisement.Show("rewardedVideo", options);
        }
    }

	public void WatchVideoForScoin(int value) {
		_isRewardScoin = true;
		_numEnergyReward = 0;
		_numScoinReward = value;

		ShowRewardedAd ();
	}

//	public void WatchVideoForEnergy(int value) {
//		_isRewardScoin = false;
//		_numEnergyReward = value;
//		_numScoinReward = 0;
//
//		ShowRewardedAd ();
//	}

    private void HandleShowResult(ShowResult result)
    {
        switch (result)
        {
		case ShowResult.Finished:
			Debug.Log ("The ad was successfully shown.");
            
			StatsManager.Instance.IncreaseScoin (_numScoinReward);
			string title = StringConstrain.ShowAdsRewardTitle;
			string message = StringConstrain.ShowAdsRewardMsgFormat (_numScoinReward, "Coin");
			PopupManager.Instance.OpenPopupNotification (title, message);
//			if (_isRewardScoin) {
//				
//
//			} else {
//				string title = StringConstrain.ShowAdsRewardTitle;
//				string message = StringConstrain.ShowAdsRewardMsgFormat (_numEnergyReward, "Energy");
//				PopupManager.Instance.OpenPopupNotification (title, message);
//				StatsManager.Instance.IncreaseEnergy (_numEnergyReward);
//			}
          
            break;
        case ShowResult.Skipped:
            Debug.Log("The ad was skipped before reaching the end.");
            break;
        case ShowResult.Failed:
            Debug.LogError("The ad failed to be shown.");
            break;
        }
    }
    
}
