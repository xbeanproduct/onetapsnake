﻿using UnityEngine;
using System;

public class ReSkinAnimation : MonoBehaviour {

	public string spriteSheetName;

	void Start () {

		var subSprites = Resources.LoadAll<Sprite>("Snake/" + spriteSheetName);

		foreach (var renderer in GetComponentsInChildren<SpriteRenderer>())
		{
			string spriteName = renderer.sprite.name;
			var newSprite = Array.Find(subSprites, item => item.name == spriteName);

			if (newSprite)
				renderer.sprite = newSprite;
		}
	}

	void LateUpdate() {
		if (gameObject.tag.Equals(Settings.SnakeHead)) {
			var subSprites = Resources.LoadAll<Sprite>("Snake/" + spriteSheetName);

			foreach (var renderer in GetComponentsInChildren<SpriteRenderer>())
			{
				string spriteName = renderer.sprite.name;
				var newSprite = Array.Find(subSprites, item => item.name == spriteName);

				if (newSprite)
					renderer.sprite = newSprite;
			}
		}
	}
}
