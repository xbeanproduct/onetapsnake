﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Common : MonoBehaviour {

	// All game variables
	public static int diamond = 0;
	public static int stars = 0;
    
	public static int scores = 0;
	public static int lives = 0;
	public static int maxLife = 3;
	
	// In map variables
	public bool isFreeStyleTutorial = false;

	public bool isGameStart = false;
	public int life = 0;
	public float snakeSpeed = 10;
	public bool isTurnRight = true;
	
	public bool isSnakeGoOut = false;
	public bool isCantMove = false;
    public bool isGamePause = false;
	public bool isGameOver = false;
	public bool isMapCompleted = false;
	public bool isInTussock = false;
	public int teleportCount = 0;
    public bool isTailReachedCorner = false;   
    public bool isFemaleTailReachedCorner = false;   
    public bool isEndPointOpening = false;

	public bool isHoldSprint = false;

	public bool isGameTutorial = false;
	public bool isCurrentMapTutorial = false;
	public bool isShowingTutorial = false;
    
	public bool isGateTurning = false;
    // Toolkit variables
    public bool isImmortal = false;
    public bool isSurvivalSnake = false;
    
	public int star = 0;
    public int starfragments = 0;
	public int score = 0;
	
	public bool isHaveHelmet = false;
	
	public bool isReducedLife = false;

	public int foodEat = 0;
	
	private static Common mInstance;
	public static Common ShareInstance() {
		return mInstance;
	}
	// Use this for initialization
	void Awake () {
		
//		Screen.sleepTimeout = SleepTimeout.NeverSleep;
		if (mInstance == null) {
			mInstance = this;
		}
	}
	
	void Start() {

	}
	
	// Update is called once per frame
	void Update () {


	}
}
