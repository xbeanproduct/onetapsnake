﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;

using System.Collections;
using System;

public class NonCanvasButton : MonoBehaviour {

	[SerializeField]
	private bool _interactable = false;

	public Sprite defaultSprite, highlightSprite, disableSprite;

	public BoxCollider2D bound;


	public UnityEvent onButtonPress;
	public UnityEvent onButtonRelease;

	private bool isTouchedIn = false;

	public bool Interactable {
		get {
			return _interactable;
		}
		set {
			_interactable = value;
			if (_interactable) {
				if (defaultSprite) {
					sprite.sprite = defaultSprite;
				}
			} else {
				if (disableSprite) {
					sprite.sprite = disableSprite;
				}
			}
		}
	}


	private SpriteRenderer sprite;
	// Use this for initialization
	void Awake () {
		sprite = GetComponent<SpriteRenderer> ();

		if (defaultSprite == null) {
			if (sprite.sprite != null) {
				defaultSprite = sprite.sprite;
			}
		}

		Interactable = false;
	}
	
	// Update is called once per frame
	void Update () {


		if (!Interactable) {
			return;
		}

		// Touch input
		if (Input.touchCount > 0)
		{
			// Get touch
			Touch touch = Input.GetTouch(0);

			if (touch.phase == TouchPhase.Began) {
				if (bound.bounds.Contains ((Vector2)GetWorldPosition (touch.position))) {

					OnTouchPressed ();
				}

			} else if (touch.phase == TouchPhase.Ended || touch.phase == TouchPhase.Canceled) {
				if (bound.bounds.Contains ((Vector2)GetWorldPosition (touch.position))) {
					if (isTouchedIn)
						OnTouchRelease ();
				}
				isTouchedIn = false;
			}
		}
		// Mouse input
		else
		{
			if (Input.GetMouseButtonDown(0))
			{
				if (bound.bounds.Contains ((Vector2)GetWorldPosition (Input.mousePosition))) {
					
					OnTouchPressed ();
				}
			}

			else if (Input.GetMouseButtonUp (0)) {
				sprite.sprite = defaultSprite;
				if (bound.bounds.Contains ((Vector2)GetWorldPosition (Input.mousePosition))) {
					if (isTouchedIn)
						OnTouchRelease ();
				}
				isTouchedIn = false;
			}
		}
	
	}

	public void OnTouchPressed()
	{
		if (highlightSprite) {
			sprite.sprite = highlightSprite;
		}
			
		isTouchedIn = true;
//		onButtonPress.Invoke ();
	}

	public void OnTouchRelease() {
		sprite.sprite = defaultSprite;

//		onButtonRelease.Invoke ();
		onButtonPress.Invoke ();
	}


	public void Hello() {
		Debug.Log ("Hello");
	}

	// Get world position of the specified screen position
	Vector3 GetWorldPosition(Vector3 position)
	{
		return Camera.main.ScreenToWorldPoint(position);
	}
}


