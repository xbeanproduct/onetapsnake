﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class Settings
{
	// Monetize
	public static readonly string AdsBannerAndroid = "ca-app-pub-2689619263921185/7967032352";
	public static readonly string AdsInterstitialAndroid = "ca-app-pub-2689619263921185/6490299150";
	public static readonly string AdsBanneriOS = "ca-app-pub-2689619263921185/1780897956";
	public static readonly string AdsInterstitialiOS = "ca-app-pub-2689619263921185/3257631155";


    // Gameplay
    public static readonly int TargetFrameRate = 60;
    public static readonly float SnakeComponentSpace = 0.3f;
    public struct Direction
    {
        public static readonly int Up = 0;
        public static readonly int Down = 180;
        public static readonly int Left = 90;
        public static readonly int Right = 270;
    }
    
    public static readonly Vector2 LargeSegmentCollider = new Vector2(0.3f, 0.3f);
    public static readonly Vector2 SmallSegmentCollider = new Vector2(0.1f, 0.1f);
    public static readonly int RotateRightValue = -90;
    public static readonly int RotateLeftvalue = 90;
    public static readonly int NumStarLimit = 3;
    public static readonly int NumStarFragmentLimit = 9;
    public static readonly float SnakeGoOutTime = 0.5f;
	public static readonly int InitEnergy = 10;
	public static readonly int MaxEnergy 	= 10;
	public static readonly float EnergyDelayTime = 15 * 60f;
    public static readonly float MaxLife = 3;

	public static readonly int Chapter2Level = 25;
	public static readonly int Chapter3Level = 50;


	// Monetize
	public static readonly int CoinByInvite = 50;
	public static readonly int EnergyByInvite = 1;
	public static readonly int UnlockChapterPrice = 1000;

    // Animation
    
        // UI Animation
    public static readonly string ScreenFadeOutTrigger = "FadeOut"; 
    public static readonly string ScreenFadeInTrigger = "FadeIn";
    public static readonly string ShowStarTrigger = "ShowStar";
    public static readonly string ClosePopupTrigger = "ClosePopupTrigger";
    public static readonly string PopupLoadingDisappear = "PopupLoadingDisappear";
       
        
        // Gameplay Animation
    public static readonly string SnakeEatTrigger = "SnakeEatFood";
    public static readonly string BeingDestroyTrigger = "BeingDestroy";
    public static readonly string DestroyEndPointCoverTrigger = "DestroyCover";
    public static readonly string ResetEndPointCoverTrigger = "Reset";
    public static readonly string SnakeDeadTrigger = "SnakeDied";
    public static readonly string SnakeResetTrigger = "SnakeReset";
    
    // GameObject Name
    
        // Gameplay Object
    public static readonly string SnakeHead = "SnakeHead";
    public static readonly string SnakeMidSegment = "MidSegment";
    public static readonly string SnakeLastSegment = "LastSegment";
    public static readonly string StartPointCover = "StartPointCover";
    public static readonly string EndPointCover = "EndPointCover";
    
        // UI Object
    
    public static readonly string LifeUI1 = "LifeUI1";
    public static readonly string LifeUI2 = "LifeUI2";
    public static readonly string LifeUI3 = "LifeUI3";
	public static readonly string LifeInfinity = "LifeInfinity";
    public static readonly string StarUI1 = "StarUI1";
    public static readonly string StarUI2 = "StarUI2";
    public static readonly string StarUI3 = "StarUI3";
    public static readonly string StarFragmentUI1 = "StarFragmentUI1";
    public static readonly string StarFragmentUI2 = "StarFragmentUI2";
    public static readonly string StarFragmentUI3 = "StarFragmentUI3";
    
    public static readonly string CurrentEnergy = "CurrentEnergy";
    public static readonly string EnergyTime = "EnergyTime";
    public static readonly string AllScoreValue = "AllScoreValue";
    public static readonly string AllStarValue = "AllStarValue";
    public static readonly string ScoinValue = "ScoinValue";
    
    public static readonly string MapButton = "MapButton";
    public static readonly string MapStar1 = "Star1";
    public static readonly string MapStar2 = "Star2";
    public static readonly string MapStar3 = "Star3";
    public static readonly string MapFriend1 = "Friend1";
    public static readonly string MapFriend2 = "Friend2";
    public static readonly string MapFriend3 = "Friend3";
    public static readonly string FriendAvatarContent = "FriendAvatarContent";
    
    public static readonly string LoginFBButton = "LoginFB";
    public static readonly string ActivityIndicator = "MainActivityIndicator";
     
    
    // GameObject Tag
    public static readonly string SnakeHolderTag = "SnakeHolder";
    public static readonly string FemaleSnakeHolderTag = "FemaleSnakeHolder";
    public static readonly string StartPointTag = "StartPoint";
    public static readonly string FemaleStartPointTag = "FemaleStartPoint";
    public static readonly string EndPointTag = "EndPoint";
    public static readonly string SnakeTail = "Tail";
    public static readonly string SnakeCorner = "SnakeCorner";   
    public static readonly string FemaleSnakeCorner = "FemaleSnakeCorner";
    public static readonly string StarTag = "Star";
	public static readonly string StarFragmentTag = "StarFragment";
    public static readonly string StarUITag = "StarUI";
	public static readonly string StarFragmentUITag = "StarFragmentUI";
    public static readonly string FoodTag = "Food";
    public static readonly string ReverseFoodTag = "ReverseFood";
    public static readonly string RockTag = "Rock";
    public static readonly string SmallRockTag = "SmallRock";
    public static readonly string HelmetTag = "Helmet";
    public static readonly string TeleportA = "TeleportA";
    public static readonly string TeleportB = "TeleportB";
    public static readonly string MonsterTag = "Monster";
    public static readonly string MonsterCircleTag = "MonsterCircle";
    public static readonly string MonsterPathTag = "MonsterPath";
	public static readonly string MonsterFishTag = "MonsterFish";
    public static readonly string StarEffectTag = "StarEffect";

	public static readonly string GateCornerTag = "GateCorner";
	public static readonly string GateCornerRemoteTag = "GateCornerRemote";
    
	// Scene
	public static readonly string PlayScene = "PlayScene";
	public static readonly string MapSelectScene = "TestNewMapSelect";
	public static readonly string LevelScene = "Level";
	
	// Popup
	public static readonly string PopupSuggestEnergy = "MenuSuggestEnergy";
	public static readonly string PopupFinishLevel = "MenuFinishLevel";
	public static readonly string PopupGameOver = "MenuGameOver";
    public static readonly string PopupChangeSkin = "MenuChangeSkin";
    public static readonly string PopupUserStats = "MenuUserStats";
	public static readonly string PopupNotificationConfirm = "MenuNotificationConfirm";
	public static readonly string PopupToolkitConfirm = "MenuToolkitConfirm";
	public static readonly string PopupToolkitHelp = "MenuToolkitHelp";
	public static readonly string PopupSuggestScoin = "MenuSuggestScoin";
	public static readonly string PopupNotification = "MenuNotification";
	public static readonly string PopupSetting = "MenuSetting";
    public static readonly string TapToPlay = "TapToPlay";
	
	// Map Info
    public static readonly string MapLevelKey = "level_id";
	public static readonly string MapScoreKey = "score";
	public static readonly string MapStarKey = "star";
	
    // Toolkit
    public static readonly int TimeFreezeDuration = 20;
    public static readonly int TimeSlowDuration = 25;
    public static readonly int ImmortalDuration = 20;
    
    
	// items score
	public static readonly int StarScore = 10000;
	public static readonly int FoodScore = 500;
	public static readonly int ReverseFoodScore = 1000;
	public static readonly int LifeScore = 2000;
	
	
	
	// Service
    public static readonly List<string> FBPerms = new List<string>{"user_friends"};
	public static readonly string RequestKey = "FPwk8waFm0f7hzIb1vOyPg==";
    public static readonly int SuccessCode = 200;
    public static readonly int TokenExpiredCode = 330;
    public static readonly int UpdateLevelErrorCode = 210;
    public static readonly int FailCode = 300;
    public static readonly int TimeLoginInterval = 60 * 15;
    
        // Service Parameters
    public static readonly string FBTokenParam = "facebook_token";
    public static readonly string NumEnergyParam = "num_energies";
	public static readonly string NumScoin = "num_scoins";
	public static readonly string NumScoinUsed = "num_scoins_used";
	public static readonly string NumScoinEarn = "num_scoins_earn";
    public static readonly string LevelsParam = "levels";
    public static readonly string PageQuery = "?page=";
    public static readonly string FoodParam = "food";
    public static readonly string ReverseFoodParam = "reverse_food";
    public static readonly string ShieldParam = "shield";
    public static readonly string DiedParam = "died";
    
        // Service Result Key
    public static readonly string CodeKey = "code";
	public static readonly string MessageKey = "message";
	public static readonly string ResultKey = "result";
    public static readonly string AccessTokenKey = "access_token";
    public static readonly string UserNameKey = "name";
    public static readonly string UserAvatarKey = "avatar";
    public static readonly string UserAvatarKeySize50 = "avatar_50";
    public static readonly string UserAvatarKeySize100 = "avatar_100";
    public static readonly string UserAvatarKeySize200 = "avatar_200";
    public static readonly string UserLevelsKey = "levels";
    public static readonly string UserScoreKey = "_score";
    public static readonly string UserStarKey = "_star";
    public static readonly string DataListKey = "data";
    public static readonly string UserEnergiesKey = "energies";
    public static readonly string UserDiamondsKey = "diamonds";
    public static readonly string TotalScoreKey = "total_score";
    public static readonly string TotalStarKey = "total_star";
    public static readonly string FriendListKey = "friends";
    public static readonly string UserRankKey = "rank_id";
    public static readonly string HightestLevelKey = "highest_level";
	public static readonly string ScoinKey = "scoins";
	public static readonly string ScoinEarnKey = "scoins_earn";
	public static readonly string ScoinUsedKey = "scoins_used";
	public static readonly string EnergyKey = "energies";



	// Pooling

	public static readonly string TapEffectPool = "tapEffect";
	public static readonly string FoodEffectPool = "FoodEffect";
	public static readonly string ReverseFoodEffectPool = "ReverseFoodEffect";
	public static readonly string SnakeHitWallPool = "snakeHitWall";
	public static readonly string SnakeHitWaterPool = "WaterDieEffect";
	public static readonly string SnakeHitMonsterPool = "snakeHitMonster";

	public static readonly string StarParticleEffectPool = "StarParticle";
	public static readonly string StarFragmentParticleEffectPool = "StarParticleFragment";
	public static readonly string ShieldParticleEffectPool = "ShieldEffect";
	public static readonly string SnakeSegmentPrefPool = "SnakeSegment";

	public static readonly string Lv1BreakingRockEffectSmallPool = "Lv1_BreakingRock2";
	public static readonly string Lv1BreakingRockEffectBigPool = "Lv1_BreakingRock";
	public static readonly string Lv2BreakingRockEffectSmallPool = "LV2_BoxBreaking1";
	public static readonly string Lv2BreakingRockEffectBigPool = "Lv2_BoxBreaking2";
	public static readonly string SmokeGateEffectPool = "SmokeGate";

	public static readonly string StarHighlightPool = "StarHighlight";
	public static readonly string StarFragmentHighlightPool = "StarFragmentHighlight";
	public static readonly string ReverseHighlightPool = "ReverseHighlight";
	public static readonly string ShieldHighlightPool = "ShieldHighlight";

	public static readonly string KillMonsterEffectPool = "KillMonsterEffect";

	public static readonly string BGCanvasPool = "BGCanvas";
}
