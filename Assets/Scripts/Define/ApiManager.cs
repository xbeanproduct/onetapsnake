﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using Restifizer;
using Facebook.Unity;


public class ApiManager : Singleton<ApiManager> {

	[SerializeField]
	private Restifizer.RestifizerManager restApi;
	
	public bool isServerLogin = false;
	private string _accessToken = "";
	
	private readonly string _loginPath = "sessions";
    private readonly string _loginSyncPath = "login_sync";
	private readonly string _logoutPath = "sessions/abc";
	private readonly string _updateScoin = "scoins";
	private readonly string _updateStats = "stats";
	private readonly string _getUserStats = "";
	private readonly string _updateMapInfo = "levels";
	private readonly string _getFriendStarsLeaderboard = "leaderboard/friend/stars";
	private readonly string _getStarsLeaderboard = "leaderboard/global/stars";
	private readonly string _getFriendScoresLeaderboard = "leaderboard/friend/scores";
	private readonly string _getScoresLeaderboard = "leaderboard/global/scores";
//	private readonly string _getFriendMapLeaderboard = "";
//	private readonly string _getMapLeaderboard = "";
    private readonly string _getFriendHightestLevel = "friend_level_max";
	
    public float lastTimeLogin = 0f;

	private FixedUpdater _invitedFriendUpdater = new FixedUpdater(30.0f);
	private FixedUpdater _checkLoginUpdater = new FixedUpdater(5 * 60);

	// Use this for initialization
    void Start () {

//		Application.targetFrameRate = Settings.TargetFrameRate;
//         #if !UNITY_EDITOR || !UNITY_WEBGL
//         FacebookManager.Instance.InitFacebook(result => {
//            if (result) {
//                Debug.Log("Init FB Successful");
//                if (UserData.Instance.IsFBLogin) {
//                     if (!isServerLogin && Helper.IsOnline()) {
//                         LoginSync(response => {
//                             if (response != null && response.Code == Settings.SuccessCode) {
//                                 Debug.Log("Login Successful");
//                             }
//                         });  
//                     }
//                 }
//            } 
//         });
//         #endif
		_invitedFriendUpdater.Play(UpdateInvitedFriend);
		_checkLoginUpdater.Play (CheckLogin);

	}
	
	// Update is called once per framei
	void Update () {

		//Update Check
		_invitedFriendUpdater.Update();
		_checkLoginUpdater.Update ();
        #if !UNITY_EDITOR || !UNITY_WEBGL
//        if (UserData.Instance.IsFBLogin) {
//            if (!isServerLogin && Helper.IsOnline()) {
//                if (Time.time - lastTimeLogin > Settings.TimeLoginInterval) {
//                    if (FB.IsInitialized) {
//                        lastTimeLogin = Time.time;
//                        LoginSync(response => {
//                            if (response != null && response.Code == Settings.SuccessCode) {
//                                Debug.Log("Login Successful");
//                            }
//                        });  
//                    }
//                    else {
//                        FacebookManager.Instance.InitFacebook(result => {
//                            if (result) {
//                                lastTimeLogin = Time.time;
//                                LoginSync(response => {
//                                    if (response != null && response.Code == Settings.SuccessCode) {
//                                        Debug.Log("Login Successful");
//                                    }
//                                });  
//                                    
//                            } 
//                        });
//                    }
//                }
//            }
//        }
        #endif
	}

	void CheckLogin() {
		_checkLoginUpdater.Stop ();

		if (UserData.Instance.IsFBLogin) {
			if (!isServerLogin && Helper.IsOnline()) {
				if (FB.IsInitialized) {
					LoginSync(response => {
						if (response != null && response.Code == Settings.SuccessCode) {
							Debug.Log("Login Successful");
						}
					});  
				}
				else {
					FacebookManager.Instance.InitFacebook(result => {
						if (result) {
							LoginSync(response => {
								if (response != null && response.Code == Settings.SuccessCode) {
									Debug.Log("Login Successful");
								}
							});  

						} 
					});
				}
			}
		}

		_checkLoginUpdater.Play ();
	}
    
    void OnApplicationQuit() {
        Logout(response => {
           if (response != null && response.Code == Settings.SuccessCode) {
               Debug.Log("Logout Successful");
           } 
        });
    }
    
	void UpdateInvitedFriend()
	{
		_invitedFriendUpdater.Stop();


		if (!FacebookManager.Instance.IsLogin()) {
			_invitedFriendUpdater.Play ();
			return;
		}

		Debug.Log ("checked");

		FriendData.Instance.CheckCoinInvitedFriend((coinName) => {
			if (!string.IsNullOrEmpty(coinName))
			{
				Debug.Log("Add coin");
				// Add coin
//				UserData.Instance.EarnScoin += Settings.CoinByInvite;
				StatsManager.Instance.IncreaseScoin(Settings.CoinByInvite);

//				ShowNotification(string.Format("You get {0} coins by invite {1}", Settings.CoinByInvite, coinName));

//				NotificationManager.OnCoinChanged(UserData.Instance.Coin);

				_invitedFriendUpdater.Play();
			}
			else {
				Debug.Log("No coin name");
			}
		});
	}
	
	#region Validate
    
    public void UpdateToken(string token) {
        this._accessToken = token;
        restApi.ConfigBearerAuth(this._accessToken);    
        UserData.Instance.AccessToken = this._accessToken;
        UserData.Instance.IsFBLogin = true;
        
        UserData.Instance.Save();
    }
    
    public void DeleteToken() {
        this._accessToken = "";
        restApi.ConfigBearerAuth(this._accessToken);
        UserData.Instance.AccessToken = this._accessToken;
        UserData.Instance.IsFBLogin = false;
        
        UserData.Instance.Save();
    }
    
    private void LoginFB(FacebookDelegate<ILoginResult> callback) {
		if (FB.IsInitialized) {
			FB.LogInWithReadPermissions (Settings.FBPerms, callback);
		} else {
			FacebookManager.Instance.InitFacebook (result => {
				if (FB.IsInitialized) {
					FB.LogInWithReadPermissions(Settings.FBPerms, callback);
				}
			});
		}
    }
    
	public void Login(System.Action<ApiLoginResult> callback) {
		// Login FB Get userId
		if (FB.IsLoggedIn) {
            float timeStart = Time.time;
            
            Hashtable param = new Hashtable();
            param.Add(Settings.FBTokenParam, AccessToken.CurrentAccessToken.TokenString);
            restApi.ResourceAt(_loginPath)
            .Post(param, response => {
                Debug.Log("Login time: " + (Time.time - timeStart));
                if (!response.HasError) {
                    callback(new ApiLoginResult(response.Resource));
                }
            });
        }
        else {
            LoginFB(FBCallback => {
                if (FB.IsLoggedIn) {
                    Hashtable param = new Hashtable();
                    param.Add(Settings.FBTokenParam, AccessToken.CurrentAccessToken.TokenString);
                    restApi.ResourceAt(_loginPath)
                    .Post(param, response => {
                        if (!response.HasError) {
                            callback(new ApiLoginResult(response.Resource));
                        }
                    });
                }
            });
        }
	}
    
    public void LoginSync(System.Action<ApiLoginResult> callback) {
        if (FB.IsLoggedIn) {        
            float timeStart = Time.time;
            Debug.Log("FB Logined");
            Debug.Log("Access token: " + AccessToken.CurrentAccessToken.TokenString);
            
            Hashtable param = new Hashtable();
            param.Add(Settings.FBTokenParam, AccessToken.CurrentAccessToken.TokenString);
			param.Add(Settings.NumEnergyParam, int.Parse(UserData.Instance.Energy.ToString()));
			param.Add(Settings.NumScoin, int.Parse((UserData.Instance.EarnScoin - UserData.Instance.UsedScoin).ToString()));
			param.Add(Settings.NumScoinUsed, int.Parse(UserData.Instance.UsedScoin.ToString()));
			param.Add(Settings.NumScoinEarn, int.Parse(UserData.Instance.EarnScoin.ToString()));
            
            if (UserData.Instance.MapsData.Count != 0) {
                param.Add(Settings.LevelsParam, UserData.Instance.MapsData);
            }
            
            
            
            restApi.ResourceAt(_loginSyncPath)
            .Post(param, response => {
                Debug.Log("Login time: " + (Time.time - timeStart));
                if (!response.HasError ) {
                    callback(new ApiLoginResult(response.Resource));
                } 
				else{
					callback(null);
				}
            });
        }
        else {
            LoginFB(FBCallback => {
               if (FB.IsLoggedIn) {
                    Debug.Log("FB Logined");
                    Debug.Log("Access token: " + AccessToken.CurrentAccessToken.TokenString);
                    Hashtable param = new Hashtable();
                    param.Add(Settings.FBTokenParam, AccessToken.CurrentAccessToken.TokenString);
					param.Add(Settings.NumEnergyParam, int.Parse(UserData.Instance.Energy.ToString()));
					param.Add(Settings.NumScoin, int.Parse((UserData.Instance.EarnScoin - UserData.Instance.UsedScoin).ToString()));
					param.Add(Settings.NumScoinUsed, int.Parse(UserData.Instance.UsedScoin.ToString()));
					param.Add(Settings.NumScoinEarn, int.Parse(UserData.Instance.EarnScoin.ToString()));
                    
                    if (UserData.Instance.MapsData.Count != 0) {
                        param.Add(Settings.LevelsParam, UserData.Instance.MapsData);    
                    }
                    
                    restApi.ResourceAt(_loginSyncPath)
                    .Post(param, response => {
                        if (!response.HasError ) {
                            callback(new ApiLoginResult(response.Resource));
                        }
						else {
							callback(null);
						}
                    });
				}else {
					callback(null);
				}
            });
        }
    }
	
	public void Logout(System.Action<ApiLogoutResut> callback) {
        if (!Helper.IsOnline()) {
           callback(null);
           return;
        }
        
		if (!UserData.Instance.IsFBLogin) {
			callback(null);
			return;
		}
        
		restApi.ResourceAt(_logoutPath)
		.WithBearerAuth()
		.Delete(null, response => {
			if (!response.HasError) {
				callback(new ApiLogoutResut(response.Resource));
			}
		});
	}
	#endregion
	
	
	#region SYNC
//	public void UpdateEnergy(System.Action<Hashtable> callback) {
//        if (!Helper.IsOnline()) {
//           callback(null);
//           return;
//        }
//        
//		if (!UserData.Instance.IsFBLogin) {
//			callback(null);
//			return;
//		}
//        
//		Hashtable param = new Hashtable();
//		param.Add(Settings.NumEnergyParam, UserData.Instance.Energy);
//		
//		restApi.ResourceAt(_updateEnergy)
//		.WithBearerAuth()
//		.Post(param, response => {
//			if (!response.HasError) {
//				callback(response.Resource);
//                
//			}
//		});
//	}
	
	public IEnumerator UpdateStats(System.Action<ApiUserStatsResult> callback) {
		if (!UserData.Instance.IsFBLogin) {
			callback(null);
			yield break;
		}

		if (!Helper.IsOnline()) {
			callback(null);
			yield break;
		}
			
		if (!isServerLogin) {
			LoginSync ((result) => {
				if (result != null && result.Code == Settings.SuccessCode) {
					Hashtable param = new Hashtable ();
					param.Add (Settings.NumEnergyParam, int.Parse (UserData.Instance.Energy.ToString ()));
					param.Add (Settings.NumScoin, int.Parse ((UserData.Instance.EarnScoin - UserData.Instance.UsedScoin).ToString ()));
					param.Add (Settings.NumScoinUsed, int.Parse (UserData.Instance.UsedScoin.ToString ()));
					param.Add (Settings.NumScoinEarn, int.Parse (UserData.Instance.EarnScoin.ToString ()));

					restApi.ResourceAt (_updateStats)
						.WithBearerAuth ()
						.Post (param, response => {
							if (!response.HasError) {
								callback (new ApiUserStatsResult (response.Resource));
							}
							else {
								callback(null);
							}
					});
				}
				else {
					callback(null);
				}
			});
		} else {
			Hashtable param = new Hashtable ();
			param.Add (Settings.NumEnergyParam, int.Parse (UserData.Instance.Energy.ToString ()));
			param.Add (Settings.NumScoin, int.Parse ((UserData.Instance.EarnScoin - UserData.Instance.UsedScoin).ToString ()));
			param.Add (Settings.NumScoinUsed, int.Parse (UserData.Instance.UsedScoin.ToString ()));
			param.Add (Settings.NumScoinEarn, int.Parse (UserData.Instance.EarnScoin.ToString ()));
		
			restApi.ResourceAt (_updateStats)
			.WithBearerAuth ()
			.Post (param, response => {
				if (!response.HasError) {
					callback (new ApiUserStatsResult (response.Resource));
				}
					else {
						callback(null);
					}
			});
		}
	}

	
//	public void GetUserStats(System.Action<ApiUserStatsResult> callback) {
//        if (!Helper.IsOnline()) {
//            callback(null);
//            return;
//        }
//        
//		if (!UserData.Instance.IsFBLogin) {
//			callback(null);
//			return;
//		}
//			
//		restApi.ResourceAt(_getUserStats)
//		.WithBearerAuth()
//		.Get(response => {
//			if (!response.HasError) {
//				callback(new ApiUserStatsResult(response.Resource));
//			}
//		});
//	}
	
	public IEnumerator UpdateMapInfo(System.Action<ApiUpdateMapResult> callback) {
        if (!UserData.Instance.IsFBLogin) {
			callback(null);
			yield break;
		}

		if (!Helper.IsOnline ()) {
			callback (null);
			yield break;
		}
        
		if (!isServerLogin) {
			LoginSync ((result) => {
				if (result != null && result.Code == Settings.SuccessCode) {
					Hashtable param = new Hashtable ();
					param.Add (Settings.LevelsParam, UserData.Instance.MapsData);
					param.Add (Settings.FoodParam, UserData.Instance.FoodEat);
					param.Add (Settings.ReverseFoodParam, UserData.Instance.ReverseFoodEat);
					param.Add (Settings.ShieldParam, UserData.Instance.ShieldEat);
					param.Add (Settings.DiedParam, UserData.Instance.Died);

					restApi.ResourceAt (_updateMapInfo)
						.WithBearerAuth ()
						.Post (param, response => {
						if (!response.HasError) {
							callback (new ApiUpdateMapResult (response.Resource));
						}
							else {
								callback(null);
							}
					});
				}
				else {
					callback(null);
				}
			});
		} else {
			Hashtable param = new Hashtable ();
			param.Add (Settings.LevelsParam, UserData.Instance.MapsData);
			param.Add (Settings.FoodParam, UserData.Instance.FoodEat);
			param.Add (Settings.ReverseFoodParam, UserData.Instance.ReverseFoodEat);
			param.Add (Settings.ShieldParam, UserData.Instance.ShieldEat);
			param.Add (Settings.DiedParam, UserData.Instance.Died);
        
			restApi.ResourceAt (_updateMapInfo)
			.WithBearerAuth ()
			.Post (param, response => {
					if (!response.HasError) {
						callback (new ApiUpdateMapResult (response.Resource));
					}
					else {
						callback(null);
					}
				});
		}
	}
	
//	public void GetMapInfos(System.Action<Hashtable> callback) {
//        if (!Helper.IsOnline()) {
//            callback(null);
//            return;
//        }
//        
//		if (!UserData.Instance.IsFBLogin) {
//			callback(null);
//			return;
//		}
//		
//		restApi.ResourceAt(_getMapInfo)
//		.WithBearerAuth()
//		.Get(response => {
//			if (!response.HasError) {
//				callback(response.Resource);
//			}
//		});
//	}
	
	#endregion
	
	#region Non-Level Base Leaderboard
	
	public IEnumerator GetFriendStarsLeaderboard(int page, System.Action<ApiNonLevelLeaderboardStarResult> callback) {
        if (!UserData.Instance.IsFBLogin) {
			callback(null);
			yield break;
		}

		if (!Helper.IsOnline()) {
			callback(null);
			yield break;
		}
        
		if (!isServerLogin) {
			LoginSync ((result) => {
				if (result != null && result.Code == Settings.SuccessCode) {
					string url = _getFriendStarsLeaderboard + Settings.PageQuery + page;

					restApi.ResourceAt (url)
						.WithBearerAuth ()
						.Get (response => {
						if (!response.HasError) {
							callback (new ApiNonLevelLeaderboardStarResult (response.Resource));
						}
							else {
								callback(null);
							}
					});
				}
				else {
					callback(null);
				}
			});
		} else {
			string url = _getFriendStarsLeaderboard + Settings.PageQuery + page;
		
			restApi.ResourceAt (url)
			.WithBearerAuth ()
			.Get (response => {
				if (!response.HasError) {
					callback (new ApiNonLevelLeaderboardStarResult (response.Resource));
				}
					else {
						callback(null);
					}
			});
		}
		
	}
	
	public void GetStarsLeaderboard(int page, System.Action<ApiGlobalNonLevelLeaderboardStarResult> callback) {
        if (!Helper.IsOnline()) {
           callback(null);
           return;
        }
        
		string url = _getStarsLeaderboard + Settings.PageQuery + page;
		
		restApi.ResourceAt(url)
		.Get(response => {
			if (!response.HasError) {
				callback(new ApiGlobalNonLevelLeaderboardStarResult(response.Resource));
			}
				else {
					callback(null);
				}
		});
		
	}
	
	public IEnumerator GetFriendScoresLeaderboard(int page, System.Action<ApiNonLevelLeaderboardScoreResult> callback) {
        if (!UserData.Instance.IsFBLogin) {
			callback(null);
			yield break;
		}

		if (!Helper.IsOnline()) {
			callback(null);
			yield break;
		}
        
		if (!isServerLogin) {
			LoginSync ((result) => {
				if (result != null && result.Code == Settings.SuccessCode) {
					string url = _getFriendScoresLeaderboard + Settings.PageQuery + page;

					restApi.ResourceAt (url)
						.WithBearerAuth ()
						.Get (response => {
						if (!response.HasError) {
							callback (new ApiNonLevelLeaderboardScoreResult (response.Resource));
						}
							else {
								callback(null);
							}
					});
				}
				else {
					callback(null);
				}
			});
		} else {
			string url = _getFriendScoresLeaderboard + Settings.PageQuery + page;
		
			restApi.ResourceAt (url)
			.WithBearerAuth ()
			.Get (response => {
				if (!response.HasError) {
					callback (new ApiNonLevelLeaderboardScoreResult (response.Resource));
				}
					else {
						callback(null);
					}
			});
		}
	}
	
	public void GetScoresLeaderboard(int page, System.Action<ApiGlobalNonLevelLeaderboardScoreResult> callback) {
        if (!Helper.IsOnline()) {
           callback(null);
           return;
        }
        
		string url = _getScoresLeaderboard + Settings.PageQuery + page;
		restApi.ResourceAt(url)
		.Get(response => {
			if (!response.HasError) {
				callback(new ApiGlobalNonLevelLeaderboardScoreResult(response.Resource));
			}
				else {
					callback(null);
				}
		});
		
	}
	
	#endregion
	
	#region Level Base Leaderboard
	
	public IEnumerator GetFriendMapLeaderboard(int page, int level, System.Action<ApiLevelLeaderboardResult> callback) {
        if (!UserData.Instance.IsFBLogin) {
			callback(null);
			yield break;
		}

		if (!Helper.IsOnline()) {
			callback(null);
			yield break;
		}
        
		if (!isServerLogin) {
			LoginSync ((result) => {
				if (result != null && result.Code == Settings.SuccessCode) {
					string url = _getFriendScoresLeaderboard + "/" + level + Settings.PageQuery + page; 

					restApi.ResourceAt (url)
						.WithBearerAuth ()
						.Get (response => {
						if (!response.HasError) {
							callback (new ApiLevelLeaderboardResult (response.Resource));
						} else {
							callback (null);
						}

					});
				}
				else {
					callback(null);
				}
			});
		} else {
			string url = _getFriendScoresLeaderboard + "/" + level + Settings.PageQuery + page; 
		
			restApi.ResourceAt (url)
			.WithBearerAuth ()
			.Get (response => {
				if (!response.HasError) {
					callback (new ApiLevelLeaderboardResult (response.Resource));
				} else {
					callback (null);
				}

			});
		}
	}
	
	public void GetMapLeaderboard(int page, int level, System.Action<ApiGlobalLevelLeaderboardResult> callback) {
        if (!Helper.IsOnline()) {
           callback(null);
           return;
        }
        
		string url = _getScoresLeaderboard + "/" + level + Settings.PageQuery + page;
		
		restApi.ResourceAt(url)
		.Get(response => {
			if (!response.HasError) {
				callback(new ApiGlobalLevelLeaderboardResult(response.Resource));
			}
				else {
					callback(null);
				}
		});
	}
    
    public IEnumerator GetFriendHightestLevel(System.Action<ApiFriendHightestLevelResult> callback) {
        if (!UserData.Instance.IsFBLogin) {
			callback(null);
			yield break;
		}

		if (!Helper.IsOnline()) {
			callback(null);
			yield break;
		}
        
		if (!isServerLogin) {
			LoginSync ((result) => {
				if (result != null && result.Code == Settings.SuccessCode) {
					restApi.ResourceAt (_getFriendHightestLevel)
						.WithBearerAuth ()
						.Get (response => {
						if (!response.HasError) {
							callback (new ApiFriendHightestLevelResult (response.Resource));
						} else {
							callback (null);
						}
					});
				} else {
					callback (null);
				}
			});
		} else {
        
			restApi.ResourceAt (_getFriendHightestLevel)
	        .WithBearerAuth ()
	        .Get (response => {
				if (!response.HasError) {
					callback (new ApiFriendHightestLevelResult (response.Resource));
				}
					else {
						callback(null);
					}
			});
		}
    }
	
	#endregion
	

}


public class ApiResult {	
	public int Code;
	public string Message;
    public string ResultMessage;
	
	
    
    public void GetCodeAndMessage(Hashtable obj) {
        this.Code = (int)obj[Settings.CodeKey];
		this.Message = (string)obj[Settings.MessageKey];
        
        if (this.Code == Settings.TokenExpiredCode) {
            Debug.Log("Server Token expired, relogin...");
            ApiManager.Instance.Login(response => {
                if (response != null && response.Code == Settings.SuccessCode) {
                    Debug.Log("Reloging successful");
                }
            });
        }
    }
    public void UpdateToken(Hashtable obj) {
        ApiManager.Instance.UpdateToken((string)obj[Settings.AccessTokenKey]);
    }
}

public class ApiLoginResult : ApiResult {
	
	public string Name;
	public string Avatar;
    public ArrayList Levels;
    public int HightestLevel;
    
    
	public ApiLoginResult(Hashtable resource) {
        GetCodeAndMessage(resource);
        
        if (this.Code >= Settings.FailCode) {
            return;
        }
        
        Hashtable data = (Hashtable)resource[Settings.ResultKey];
        this.ResultMessage = (string)data[Settings.MessageKey];
        
		this.Name = (string)data[Settings.UserNameKey];
		this.Avatar = (string)data[Settings.UserAvatarKey];
        
        UserData.Instance.AvatarUrl = this.Avatar;
        UserData.Instance.Username = this.Name;
        
        if (this.Code != Settings.UpdateLevelErrorCode) {
            this.Levels = (ArrayList)data[Settings.UserLevelsKey];
			this.HightestLevel = int.Parse(data[Settings.HightestLevelKey].ToString());
            UserData.Instance.MapsData = this.Levels;
            UserData.Instance.HightestLevel = this.HightestLevel;
			UserData.Instance.FoodEat = int.Parse(data[Settings.FoodParam].ToString());
			UserData.Instance.ReverseFoodEat = int.Parse(data[Settings.ReverseFoodParam].ToString());
			UserData.Instance.ShieldEat = int.Parse(data[Settings.ShieldParam].ToString());
			UserData.Instance.Died = int.Parse(data[Settings.DiedParam].ToString());
			UserData.Instance.Scoin = int.Parse (data [Settings.ScoinKey].ToString ());
			UserData.Instance.EarnScoin = int.Parse (data [Settings.ScoinEarnKey].ToString ());
			UserData.Instance.UsedScoin = int.Parse (data [Settings.ScoinUsedKey].ToString ());
			UserData.Instance.Energy = int.Parse (data [Settings.EnergyKey].ToString ());
        }
        
        
        
        
        
        UserData.Instance.Save();
        
		this.UpdateToken(data);
        ApiManager.Instance.isServerLogin = true;
           
	}
}

public class ApiLogoutResut : ApiResult {
	public ApiLogoutResut(Hashtable resource) {
        GetCodeAndMessage(resource);
        
        if (this.Code >= Settings.FailCode) {
            return;
        }
        
        ApiManager.Instance.DeleteToken();
    }
}

public class ApiLeaderboardScore {
    public string Name;
    public string Image;
    public int Score;
    public int Rank;
    
    public ApiLeaderboardScore(Hashtable data) {
        this.Name = (string)data[Settings.UserNameKey];
        this.Image = (string)data[Settings.UserAvatarKey];
        this.Score = int.Parse(data[Settings.UserScoreKey].ToString());
        this.Rank = int.Parse(data[Settings.UserRankKey].ToString());
    }
}

public class ApiLeaderboardLevel {
    public string Name;
    public string Image;
    public int Score;
    public int Rank;
    
    public ApiLeaderboardLevel(Hashtable data) {
        this.Name = (string)data[Settings.UserNameKey];
        this.Image = (string)data[Settings.UserAvatarKeySize100];
        this.Score = int.Parse(data[Settings.UserScoreKey].ToString());
        this.Rank = int.Parse(data[Settings.UserRankKey].ToString());
    }
}

public class ApiLeaderboardStar {
    public string Name;
    public string Image;
    public int Star;
    public int Rank;
    
    public ApiLeaderboardStar(Hashtable data) {
        this.Name = (string)data[Settings.UserNameKey];
        this.Image = (string)data[Settings.UserAvatarKey];
        this.Star = int.Parse(data[Settings.UserStarKey].ToString());
        this.Rank = int.Parse(data[Settings.UserRankKey].ToString());
    }
}

public class ApiGlobalNonLevelLeaderboardScoreResult : ApiResult {
    public List<ApiLeaderboardScore> Data;
    public ApiGlobalNonLevelLeaderboardScoreResult(Hashtable resource) {
        GetCodeAndMessage(resource);
        
        if (this.Code >= Settings.FailCode) {
            return;
        }
        
        Hashtable data = (Hashtable)resource[Settings.ResultKey];
        
        ArrayList list = (ArrayList)data[Settings.DataListKey];
        Data = new List<ApiLeaderboardScore>();        
        foreach(Hashtable obj in list) {
            ApiLeaderboardScore ldboard = new ApiLeaderboardScore(obj);
            Data.Add(ldboard);
        }
    }
}

public class ApiNonLevelLeaderboardScoreResult : ApiResult {
    public List<ApiLeaderboardScore> Data;
    public ApiNonLevelLeaderboardScoreResult(Hashtable resource) {
        GetCodeAndMessage(resource);
        
        if (this.Code >= Settings.FailCode) {
            return;
        }
        
        Hashtable data = (Hashtable)resource[Settings.ResultKey];
        
        ArrayList list = (ArrayList)data[Settings.DataListKey];
        Data = new List<ApiLeaderboardScore>();        
        foreach(Hashtable obj in list) {
            ApiLeaderboardScore ldboard = new ApiLeaderboardScore(obj);
            Data.Add(ldboard);
        }
        this.UpdateToken(data);        
    }
}

public class ApiGlobalNonLevelLeaderboardStarResult : ApiResult {
    public List<ApiLeaderboardStar> Data;
    public ApiGlobalNonLevelLeaderboardStarResult(Hashtable resource) {
        GetCodeAndMessage(resource);
        
        if (this.Code >= Settings.FailCode) {
            return;
        }
        
        Hashtable data = (Hashtable)resource[Settings.ResultKey];
        
        ArrayList list = (ArrayList)data[Settings.DataListKey];
        Data = new List<ApiLeaderboardStar>();        
        foreach(Hashtable obj in list) {
            ApiLeaderboardStar ldboard = new ApiLeaderboardStar(obj);
            Data.Add(ldboard);
        }
    }
}

public class ApiNonLevelLeaderboardStarResult : ApiResult {
    public List<ApiLeaderboardStar> Data;
    public ApiNonLevelLeaderboardStarResult(Hashtable resource) {
        GetCodeAndMessage(resource);
        
        if (this.Code >= Settings.FailCode) {
            return;
        }
        
        Hashtable data = (Hashtable)resource[Settings.ResultKey];
        
        ArrayList list = (ArrayList)data[Settings.DataListKey];
        Data = new List<ApiLeaderboardStar>();        
        foreach(Hashtable obj in list) {
            ApiLeaderboardStar ldboard = new ApiLeaderboardStar(obj);
            Data.Add(ldboard);
        }        
        this.UpdateToken(data);
    }
}

public class ApiGlobalLevelLeaderboardResult : ApiResult {
    public List<ApiLeaderboardLevel> Data;
    public ApiGlobalLevelLeaderboardResult(Hashtable resource) {
        GetCodeAndMessage(resource);
        
        if (this.Code >= Settings.FailCode) {
            return;
        }
        
        Hashtable data = (Hashtable)resource[Settings.ResultKey];
        
        ArrayList list = (ArrayList)data[Settings.DataListKey];
        Data = new List<ApiLeaderboardLevel>();
        foreach(Hashtable obj in list) {
            ApiLeaderboardLevel ldboard = new ApiLeaderboardLevel(obj);
            Data.Add(ldboard);
        }
    }
}

public class ApiLevelLeaderboardResult : ApiResult {
    public List<ApiLeaderboardLevel> Data;
    public ApiLevelLeaderboardResult(Hashtable resource) {
        GetCodeAndMessage(resource);
        
        if (this.Code >= Settings.FailCode) {
            return;
        }
        
        Hashtable data = (Hashtable)resource[Settings.ResultKey];
        
        ArrayList list = (ArrayList)data[Settings.DataListKey];
        Data = new List<ApiLeaderboardLevel>();
        foreach(Hashtable obj in list) {
            ApiLeaderboardLevel ldboard = new ApiLeaderboardLevel(obj);
            Data.Add(ldboard);
        }
        this.UpdateToken(data);
    }
}

public class ApiUserStatsResult : ApiResult {
    
    public ApiUserStatsResult(Hashtable resource) {
        GetCodeAndMessage(resource);
        
        if (this.Code >= Settings.FailCode) {
            return;
        }
        
        Hashtable data = (Hashtable)resource[Settings.ResultKey];
        
		UserData.Instance.Scoin = int.Parse (data [Settings.ScoinKey].ToString ());
		UserData.Instance.EarnScoin = int.Parse (data [Settings.ScoinEarnKey].ToString ());
		UserData.Instance.UsedScoin = int.Parse (data [Settings.ScoinUsedKey].ToString ());
		UserData.Instance.Energy = int.Parse (data [Settings.EnergyKey].ToString ());
		UserData.Instance.Save ();
		StatsManager.Instance.OnEnergyChanged (UserData.Instance.Energy);
        
        this.UpdateToken(data);
    }
}

public class ApiUpdateMapResult : ApiResult {
    
    public ArrayList Levels;
    public ApiUpdateMapResult(Hashtable resource) {
        GetCodeAndMessage(resource);
        
        if (this.Code >= Settings.FailCode) {
           return;
        }
        
        
        Hashtable data = (Hashtable)resource[Settings.ResultKey];
        if (this.Code != Settings.UpdateLevelErrorCode) {
            this.Levels = (ArrayList)data[Settings.UserLevelsKey];
        }
        
        this.UpdateToken(data);
    }
}

public class ApiFriendHightestLevelResult : ApiResult {
    public List<ApiFriendHightestLevelHashtable> Data; 
    public ApiFriendHightestLevelResult(Hashtable resource) {
        GetCodeAndMessage(resource);
        
        if (this.Code >= Settings.FailCode) {
            return;
        }
        
        Hashtable data = (Hashtable)resource[Settings.ResultKey];
        ArrayList list = (ArrayList)data[Settings.DataListKey];
        Data = new List<ApiFriendHightestLevelHashtable>();
        foreach(Hashtable obj in list) {
            ApiFriendHightestLevelHashtable dataHash = new ApiFriendHightestLevelHashtable(obj);
            Data.Add(dataHash);
        }
        this.UpdateToken(data);
    }   
}

public class ApiFriendHightestLevelHashtable {
    public int Level;
    public List<ApiFriendHightestLevel> Friends;
    public ApiFriendHightestLevelHashtable(Hashtable data) {
        this.Level = int.Parse(data[Settings.MapLevelKey].ToString());
        
        ArrayList list = (ArrayList)data[Settings.FriendListKey];
        Friends = new List<ApiFriendHightestLevel>();
        foreach(Hashtable obj in list) {
            ApiFriendHightestLevel friendData = new ApiFriendHightestLevel(obj);
            Friends.Add(friendData);
        }
    }
    
}

public class ApiFriendHightestLevel {
    public string FriendName;
    public string FriendAvatar;
    public int FriendTotalScore;
    public int FriendTotalStar;
    public ApiFriendHightestLevel(Hashtable data) {
        this.FriendName = (string)data[Settings.UserNameKey];
        this.FriendAvatar = (string)data[Settings.UserAvatarKey];
        this.FriendTotalScore = int.Parse(data[Settings.TotalScoreKey].ToString());
        this.FriendTotalStar = int.Parse(data[Settings.TotalStarKey].ToString());
    }
}