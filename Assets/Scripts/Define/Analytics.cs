﻿using UnityEngine;
using System.Collections;

public enum LogLevelType {
	Play,
	Win,
	Lose,
	Quit
}

public class Analytics : Singleton<Analytics> {

	[SerializeField]
	private GoogleAnalyticsV4 analytics;
	// Use this for initialization

	// Category
	public static readonly string Levels = "Levels";
	public static readonly string Purchase = "Purchase";
	public static readonly string Advertisement = "Advertisement";
	public static readonly string Popup = "View Popup";
	public static readonly string Toolkit = "Toolkit";

	// Action
	public static readonly string PurchaseTypeSkin = "Skin";
	public static readonly string PurchaseTypeChapter = "Chapter";
	public static readonly string PurchaseTypeToolkit = "Toolkit";
	public static readonly string AdsTypeUnity = "Unity Ads";
	public static readonly string AdsTypeAdmob = "Admob";

	public static readonly string PopupTypeSuggestEnergy = "Suggest Energy";
	public static readonly string PopupTypeBuyEnergy = "Buy Energy";
	public static readonly string PopupTypeSuggestCoin = "Suggest Coin";
	public static readonly string PopupTypeBuyCoin = "Buy Coin";

	public static readonly string PopupTypeSkin = "Change Skin";
	public static readonly string PopupUserStats = "User Stats";

	public static readonly string ToolkitUseConfirm = "Confirm Use Toolkit";
	public static readonly string ToolkitUseCancel = "Cancel Toolkit Confirm";
	public static readonly string ToolkitViewHelp = "Toolkit View Help";


	public void LogScreen(string screenName) {
		analytics.LogScreen (screenName);
	}

	public void LogEvent(string category, string action, string label, long value) {
		analytics.LogEvent (category, action, label, value);
	}

	public void LogLevel(int level, LogLevelType type) {
//		analytics.LogEvent (Levels, type.ToString (), level.ToString("000"), 1);
		analytics.LogScreen(string.Format("{0}-{1}-{2}", Levels, type.ToString(), level.ToString("000")));
	}
}
