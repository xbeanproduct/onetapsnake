﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using CodeStage.AntiCheat.ObscuredTypes;

[System.Serializable]
public class UserData
{
	private static readonly string UserDataFile = "user.data";

	#region Singleton

	private static UserData _instance;
	
	public static UserData Instance
	{
		get
		{
			if (_instance == null)
			{
				_instance = new UserData();

				if (!Helper.Load<UserData>(UserDataFile, ref _instance))
				{
					_instance.Reset();
				}
			}
			
			return _instance;
		}
	}
	
	#endregion

	// User Id (If login FB)
	private bool _isFBLogin;
    
    // Server Token
    private string _accessToken;
    
    // User Name 
    private string _username;
    
    // User Avatar
    // private Texture _avatar;
    private string _avatarUrl;
	
	// The current Energy
	private ObscuredInt _energy;
    
	// Current Scoin
	private ObscuredInt _scoin;
	
	// Used Scoin
	private ObscuredInt _usedScoin;
    
    // Earn Scoin
	private ObscuredInt _earnScoin;

	// Skin purchased
	private ArrayList _skinPurchaseIndexList;
    
	// Selected Skin index
	private int _selectedSkinIndex;

	// True if BGM on
	private bool _isBGMOn;
	
	// True if SFX on
	private bool _isSFXOn;

	// The Energy's start seconds
	private double _energyStartSeconds;
    
    // Hightest Level
    private int _hightestLevel;
	
	private ArrayList _mapsData;

	private bool _wasPlayedTutorial;
	public bool WasPlayedTutorial {
		get { return _wasPlayedTutorial; }
		set { _wasPlayedTutorial = value; }
	}

	public bool WasShowDied {
		get;
		set;
	}
		

	public bool CompleteBeginTutorial {
		get;
		set;
	}

	public bool CompleteTutorialPhase1 {
		get;
		set;
	}

	public bool CompleteTutorialPhase1_2 {
		get;
		set;
	}

	public bool CompleteTutorialPhase2 {
		get;
		set;
	}

	public bool CompleteTutorialPhase3 {
		get;
		set;
	}

	public bool CompleteTutorialPhase4 {
		get;
		set;
	}

	public bool CompleteTutorialPhase5 {
		get;
		set;
	}

	public bool CompleteToolkitTutorial {
		get;
		set;
	}

    private int _foodEat;
    public int FoodEat {
        get {return _foodEat;}
        set {_foodEat = value;}
    }
    
    private int _reverseFoodEat;
    public int ReverseFoodEat {
        get { return _reverseFoodEat;}
        set {_reverseFoodEat = value;}
    }
    
    private int _shieldEat;
    public int ShieldEat {
        get {return _shieldEat;}
        set {_shieldEat = value;}
    }
    
    private int _died;
    public int Died {
        get {return _died;}
        set {_died = value;}
    }
    

	public bool IsFBLogin
	{
		get {return _isFBLogin;}
		set {_isFBLogin = value;}	
	}
    
    public string AccessToken
    {
        get {return _accessToken;}
        set {_accessToken = value;}
    }
    
    public string Username
    {
        get {return _username;}
        set {_username = value;}
    }
    
    // public Texture Avatar
    // {
    //     get {return _avatar;}
    //     set {_avatar = value;}
    // }
    public string AvatarUrl
    {
        get { return _avatarUrl; }
        set { _avatarUrl = value;}
    }    
	
	public ObscuredInt Energy
	{
		get { return _energy; }
		set { _energy = Mathf.Clamp(value, 0, Settings.MaxEnergy); }
	}
	
	public ObscuredInt Scoin
	{
		get {return _scoin;}
		set {_scoin = value;}
	}
	
	public ObscuredInt UsedScoin
	{
		get {return _usedScoin;}
		set {_usedScoin = value;}
	}
    
	public ObscuredInt EarnScoin
    {
        get {return _earnScoin;}
        set {_earnScoin = value;}
    }

	public ArrayList SkinPurchasedIndexList 
	{
		get { return _skinPurchaseIndexList; }
		set { _skinPurchaseIndexList = value; }
	}

	public int SelectedSkinIndex
	{
		get {return _selectedSkinIndex;}
		set {_selectedSkinIndex = value;}
	}

	public bool BGMOn
	{
		get { return _isBGMOn; }
		set { _isBGMOn = value; }
	}

	public bool SFXOn
	{
		get { return _isSFXOn; }
		set { _isSFXOn = value; }
	}
	
	public ArrayList MapsData 
	{
		get { return _mapsData;}
		set {_mapsData = value;}
	}
    
    public int HightestLevel
    {
        get { return _hightestLevel;}
        set { _hightestLevel = value;}
    }

	public double EnergyStartSeconds
	{
		get { return _energyStartSeconds; }
		set { _energyStartSeconds = value; }
	}
	
	
	public void Reset()
	{
		// Reset Tutorial
		CompleteBeginTutorial = false;
		_wasPlayedTutorial = true;

        // Reset FB Login
        _isFBLogin = false;
        
		// Reset Energy
		_energy = Settings.InitEnergy;

		// Reset BGM
		_isBGMOn = true;

		// Reset SFX
		_isSFXOn = true;

		// Reset Energy's start seconds
		_energyStartSeconds = 0;
		
		// Reset Maps Data
		_mapsData = new ArrayList();

		_skinPurchaseIndexList = new ArrayList(){0};

        
        _accessToken = "";

		_selectedSkinIndex = 0;
        
        _foodEat = 0;
        _reverseFoodEat = 0;
        _shieldEat = 0;

		_scoin = 0;
		_earnScoin = 0;
		_usedScoin = 0;


		// Unlock all tutorial
//		CompleteBeginTutorial = true;
//		_wasPlayedTutorial = true;
//		CompleteTutorialPhase1 = true;
//		CompleteTutorialPhase1_2 = true;
//		CompleteTutorialPhase2 = true;
//		CompleteTutorialPhase3 = true;
//		CompleteTutorialPhase4 = true;
//		CompleteTutorialPhase5 = true;
//		Hashtable mapInfo = new Hashtable();
//
//		mapInfo.Add(Settings.MapLevelKey, 1);
//		mapInfo.Add(Settings.MapScoreKey, 0);
//		mapInfo.Add(Settings.MapStarKey, 0);
//
//		MapsData.Add (mapInfo);
//		HightestLevel = 74;
		_earnScoin = 99999;

	}
	
	public bool Save()
	{
		return Helper.Save<UserData>(this, UserDataFile);
	}
}
