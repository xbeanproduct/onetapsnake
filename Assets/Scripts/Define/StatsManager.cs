﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;

public class StatsManager : Singleton<StatsManager>
{
	// The energy start seconds
	[SerializeField]
	private double _energyStartSeconds;

	// The energy countdown
	[SerializeField]
	private static float _energyCountdown;

	// The energy time
	[SerializeField]
	private float _energyTime;

	//public int currentEnergy;
	//public float localEnergyCooldown;

	[SerializeField] Text currentEnergy;
	[SerializeField] Text energyTimeSecond;
    
    [SerializeField] Text currentStars;
    [SerializeField] Text currentScores;
    [SerializeField] Text currentScoin;
	

	void Start()
	{
		FindUI();
		
		// Get energy start seconds
		_energyStartSeconds = UserData.Instance.EnergyStartSeconds;

		OnEnergyChanged(UserData.Instance.Energy);
	}

	void OnLevelWasLoaded() {
		FindUI();
        UpdateStats();
	}
	
	void FindUI() {
//		if (!currentEnergy) {
//			if (GameObject.Find(Settings.CurrentEnergy)) {
//				currentEnergy = GameObject.Find(Settings.CurrentEnergy).GetComponent<Text>();
//			}
//		}
//		
//		if (!energyTimeSecond) {
//			if (GameObject.Find(Settings.EnergyTime)) {
//				energyTimeSecond = GameObject.Find(Settings.EnergyTime).GetComponent<Text>();
//			}
//		}
        
        if (!currentScores) {
            if (GameObject.Find(Settings.AllScoreValue)) {
                currentScores = GameObject.Find(Settings.AllScoreValue).GetComponent<Text>();
            }
        }
        
        if (!currentStars) {
            if (GameObject.Find(Settings.AllStarValue)) {
                currentStars = GameObject.Find(Settings.AllStarValue).GetComponent<Text>();
            }
        }
        
        if (!currentScoin) {
            if (GameObject.Find(Settings.ScoinValue)) {
                currentScoin = GameObject.Find(Settings.ScoinValue).GetComponent<Text>();
            }
        }
	}
	
	void Update()
	{
		//if (_energyCountdown > 0)
//		{
//			_energyTime += Time.deltaTime;
//
//			// Update Energy countdown each 1 second
//			if (_energyTime >= 1f)
//			{
//				// Reset time
//				_energyTime = 0;
//
//				SetEnergyCountdown(Settings.EnergyDelayTime - (float)(CurrentTime() - _energyStartSeconds));
//			}
//		}
//		
//		if (currentEnergy) {
//			currentEnergy.text = UserData.Instance.Energy.ToString();
//		}
//
//		if (UserData.Instance.Energy >= Settings.MaxEnergy)
//		{
//			if (energyTimeSecond) {
//				energyTimeSecond.text = "FULL";
//			}
//		}
//		else
//		{
//			int minutes = Mathf.FloorToInt(_energyCountdown / 60f);
//			int seconds = Mathf.FloorToInt(_energyCountdown - minutes * 60);
//
//			if (energyTimeSecond) {
//				energyTimeSecond.text = string.Format("{0:00}:{1:00}", minutes, seconds);
//			}
//		}
        
        
        
        
		
	}
    
    public void UpdateStats() {
        int scores = 0;
        int stars = 0;
        for(int i = 0; i < UserData.Instance.MapsData.Count; i++) {
            Hashtable mapData = (Hashtable)UserData.Instance.MapsData[i];
            scores+= int.Parse(mapData[Settings.MapScoreKey].ToString());
            stars+= int.Parse(mapData[Settings.MapStarKey].ToString());
        }
        
        if (currentScores) {
            currentScores.text = scores.ToString();
        }
        
        if (currentStars) {
            currentStars.text = stars.ToString();
        }
        
        if (currentScoin) {
            UserData.Instance.Scoin = UserData.Instance.EarnScoin - UserData.Instance.UsedScoin;
            UserData.Instance.Save();
            currentScoin.text = UserData.Instance.Scoin.ToString();
        }
    }

	void SetEnergyCountdown(float countdown)
	{
		_energyCountdown = countdown;
		
		if (_energyCountdown <= 0)
		{
			
			if (UserData.Instance.Energy < Settings.MaxEnergy)
			{
				do
				{
					// Increase energy
					UserData.Instance.Energy++;
					
					if (UserData.Instance.Energy == Settings.MaxEnergy)
					{
						break;
					}
					else
					{
						_energyStartSeconds += Settings.EnergyDelayTime;
						_energyCountdown    += Settings.EnergyDelayTime;

						UserData.Instance.EnergyStartSeconds = _energyStartSeconds;
					}
				}
				while (_energyCountdown <= 0);
			}
		}
	}

	public void OnEnergyChanged(int energy)
	{
		if (energy < Settings.MaxEnergy)
		{
			// Check if not being countdown energy
			if (_energyCountdown <= 0)
			{
				if (_energyStartSeconds <= 0)
				{
					_energyStartSeconds = CurrentTime();
					UserData.Instance.EnergyStartSeconds = _energyStartSeconds;

					SetEnergyCountdown(Settings.EnergyDelayTime);
				}
				else
				{
					SetEnergyCountdown(Settings.EnergyDelayTime - (float)(CurrentTime() - _energyStartSeconds));
				}
			}
		}
		// Full energy
		else
		{
			// Stop countdown energy
			_energyCountdown = 0;

			_energyStartSeconds = 0;
			UserData.Instance.EnergyStartSeconds = _energyStartSeconds;
		}

		UserData.Instance.Save();

	}

	// Get current time in seconds
	double CurrentTime()
	{
		//return System.DateTime.Now.TimeOfDay.TotalSeconds;
		TimeSpan t = (DateTime.UtcNow - new DateTime(1970, 1, 1));
		return t.TotalSeconds;
	}

	public static float EnergyCountdown
	{
		get
		{
			return _energyCountdown;
		}
	}

	public void DescreaseEnergy()
	{
		if (UserData.Instance.Energy == Settings.MaxEnergy) 
		{
			_energyStartSeconds = CurrentTime();
			UserData.Instance.EnergyStartSeconds = _energyStartSeconds;
		}
		UserData.Instance.Energy--;
		OnEnergyChanged(UserData.Instance.Energy);
		StartCoroutine (ApiManager.Instance.UpdateStats (res => {

		}));
	}

    
    public void IncreaseEnergy(int amount) {
        UserData.Instance.Energy+= amount;
        if (UserData.Instance.Energy > Settings.MaxEnergy) {
            UserData.Instance.Energy = Settings.MaxEnergy;
        }
		OnEnergyChanged(UserData.Instance.Energy);
		StartCoroutine (ApiManager.Instance.UpdateStats (res => {

		}));
    }
    
    public void SetFullEnergy() {
        UserData.Instance.Energy = Settings.MaxEnergy;
        OnEnergyChanged(UserData.Instance.Energy);
		StartCoroutine (ApiManager.Instance.UpdateStats (res => {

		}));
    }

	private void CalculateScoin() {
		UserData.Instance.Scoin = UserData.Instance.EarnScoin - UserData.Instance.UsedScoin;
		UserData.Instance.Save ();
		UpdateStats ();
		StartCoroutine (ApiManager.Instance.UpdateStats (res => {

		}));
	}

	public void IncreaseScoin(int amount) {
		UserData.Instance.EarnScoin += amount;
		CalculateScoin();
	}

	public void DecreaseScoin(int amount) {
		UserData.Instance.UsedScoin += amount;
		CalculateScoin ();

	}

	public void SetEarnScoin(int amount) {
		UserData.Instance.EarnScoin = amount;
		CalculateScoin ();
	}
}
