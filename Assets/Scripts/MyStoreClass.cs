﻿using System;
using UnityEngine;
using UnityEngine.Purchasing;

public class MyStoreClass : MonoBehaviour, IStoreListener {
	static string kProductID100Currency = "com.smutiongame.testinapp.product1";
	static string kProductSmallDiamond = "100Diamond";
	IStoreController m_StoreController;
	void Start() {
		ConfigurationBuilder builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());
		builder.AddProduct(kProductID100Currency, ProductType.Consumable);
		builder.AddProduct(kProductSmallDiamond, ProductType.Consumable);
		UnityPurchasing.Initialize(this,builder);
	}
	public void PurchaseCurrency() {
		if (m_StoreController != null) {
			// Fetch the currency Product reference from Unity Purchasing
			Product product = m_StoreController.products.WithID(kProductID100Currency);
			if (product != null && product.availableToPurchase) {
				m_StoreController.InitiatePurchase(product);
			}
		}
	}

	public void PurchaseSmallDiamond() {
		if (m_StoreController != null) {
			// Fetch the currency Product reference from Unity Purchasing
			Product product = m_StoreController.products.WithID(kProductSmallDiamond);
			if (product != null && product.availableToPurchase) {
				m_StoreController.InitiatePurchase(product);
			}
		}
	}
	public void OnInitialized(IStoreController controller, IExtensionProvider extensions) { 
		m_StoreController = controller; 
	}
	public void OnInitializeFailed(InitializationFailureReason error) {Debug.Log("OnInitializeFailed");}
	public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs e) { 
		if (String.Equals(e.purchasedProduct.definition.id, kProductID100Currency, StringComparison.Ordinal)) {
			Debug.Log("Purchased 100 coins");
		}
		return PurchaseProcessingResult.Complete; 
	}
	public void OnPurchaseFailed(Product item, PurchaseFailureReason r) {Debug.Log("OnPurchaseFailed");}
}