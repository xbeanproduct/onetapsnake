﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;
using Chronos;
using DG.Tweening;

public class GameManager : MonoBehaviour {

    public GameObject snakeGhostPref;
    public GameObject femaleSnake;
    float lastTimeDie = 0;
    private GameObject snakeGhost;
	public StartPointController startPoint;
    public FemaleStartPointController femaleStartPoint;
    public EndPointController endPoint;
    public GameObject startTransitionPref;
    public GameObject starFragmentPref;
    public Sprite starSprite;
	
	public GameObject snakeHead;
	public GameObject snakeHolder;
	public GameObject[] foodList;
	public GameObject[] reverseFoodList;
	public GameObject[] starList;
	public GameObject[] starFragmentList;
    public GameObject[] starEffectList;
	public GameObject[] rockList;
	public GameObject[] smallRockList;
	public GameObject[] helmetList;
	public GameObject[] snakeCornerList;
    public GameObject[] monsterList;
    public GameObject[] monsterCircleList;
    public GameObject[] monsterPathList;
	public GameObject[] monsterFishList;
	public GameObject[] gateCornerList;
	public GameObject[] gateCornerRemoteList;

    GameObject life1, life2, life3, lifeInfinity;
    public Sprite starOutlineSprite;
    public GameObject[] starUIs;
	public GameObject[] starFragmentUIs;

	// Effect
	public GameObject resetEffectPref;

	public static GameManager Instance;
	
	void Awake() {
		if (Instance == null) {
			Instance = this;
		}
        
	}
	// Use this for initialization
	void Start () {
		#if !UNITY_EDITOR 
		GoogleAds.Instance.HideBanner ();
		#endif
		// Find Start Point and End Point
		if (GameObject.FindGameObjectWithTag(Settings.StartPointTag) == null) {
			Debug.LogError("Cannot Find StartPoint Object");
		}
		else {
			startPoint = GameObject.FindGameObjectWithTag(Settings.StartPointTag).GetComponent<StartPointController>();
		}
        
		if (GameObject.FindGameObjectWithTag(Settings.EndPointTag) == null) {
			Debug.LogError("Cannot Find EndPoint Object");
		}
		else {
			endPoint = GameObject.FindGameObjectWithTag(Settings.EndPointTag).GetComponent<EndPointController>();
		}

		// Spawn new snake and set to start position
		GameObject prefab = (GameObject) Instantiate (snakeHolder, new Vector3 (0, 0, 0), Quaternion.identity);
		prefab.FindInChildren(Settings.SnakeHead).GetComponent<ReSkinAnimation> ().spriteSheetName = string.Format ("Skin{0}", UserData.Instance.SelectedSkinIndex);
		prefab.FindInChildren (Settings.SnakeHead).GetComponent<MySnake> ().InitSprintEffect ();
		startPoint.FindSnakeElement();
		startPoint.ReturnPosition();

		// Find Snake Head after Spawn Snake Holder
		if (!snakeHead) {
			snakeHead = GameObject.Find(Settings.SnakeHead);
		}
        
		// Open Fog Animation
        var camPos = Camera.main.transform.position;
        var startTransition = (GameObject)Instantiate(startTransitionPref, camPos, Quaternion.identity);
        
        float height = Camera.main.orthographicSize * 4;
        var cameraSize = new Vector2(Camera.main.aspect * height, height);
        startTransition.GetComponent<InOutSprite>().outerSize = cameraSize;
        startTransition.GetComponent<InOutSprite>().innerPosition = new Vector2(snakeHead.transform.position.x, snakeHead.transform.position.y) - new Vector2(camPos.x, camPos.y);
        
		Sequence blackScreenSequence = DOTween.Sequence ();
		blackScreenSequence.Append (DOTween.To (() => 0, value => startTransition.GetComponent<InOutSprite> ().InnerSize = value, 2, 0.5f));
		blackScreenSequence.Append (DOTween.To (() => 2, value => startTransition.GetComponent<InOutSprite> ().InnerSize = value, cameraSize.y * 2.0f, 2));
		blackScreenSequence.OnComplete (() => {
			Destroy(startTransition);
		});

//        var show = LerpFloatAction.Create(0, 2, .5f, (size) => { startTransition.GetComponent<InOutSprite>().InnerSize = size; });
//		var delay = DelayAction.Create(0.0f);
//		var disappear = LerpFloatAction.Create(2, cameraSize.y * 2.0f, 2f, (size) => { startTransition.GetComponent<InOutSprite>().InnerSize = size; });
//
//		gameObject.Play(SequenceAction.Create(show, /*greet,*/ delay, disappear), () => {
//            Destroy(startTransition);
//        });


		// Find Life UI
//        if (!life1) {
//            if (GameObject.Find(Settings.LifeUI1)) {
//                life1 = GameObject.Find(Settings.LifeUI1);
//            }
//            
//            if (GameObject.Find(Settings.LifeUI2)) {
//                life2 = GameObject.Find(Settings.LifeUI2);
//            }
//            
//            if (GameObject.Find(Settings.LifeUI3)) {
//                life3 = GameObject.Find(Settings.LifeUI3);
//            }
//        }
//
//		if (!lifeInfinity) {
//			if (GameObject.Find (Settings.LifeInfinity)) {
//				lifeInfinity = GameObject.Find (Settings.LifeInfinity);
//			}
//		}
        

		// Find Star UI
        starUIs = GameObject.FindGameObjectsWithTag(Settings.StarUITag);
		starFragmentUIs = GameObject.FindGameObjectsWithTag (Settings.StarFragmentUITag);

		// Find All Component In Current Map
		foodList = GameObject.FindGameObjectsWithTag(Settings.FoodTag);
		reverseFoodList = GameObject.FindGameObjectsWithTag(Settings.ReverseFoodTag);
		starList = GameObject.FindGameObjectsWithTag(Settings.StarTag);
		starFragmentList = GameObject.FindGameObjectsWithTag (Settings.StarFragmentTag);
		rockList = GameObject.FindGameObjectsWithTag(Settings.RockTag);
		smallRockList = GameObject.FindGameObjectsWithTag(Settings.SmallRockTag);
		helmetList = GameObject.FindGameObjectsWithTag(Settings.HelmetTag);
        monsterList = GameObject.FindGameObjectsWithTag(Settings.MonsterTag);
        monsterCircleList = GameObject.FindGameObjectsWithTag(Settings.MonsterCircleTag);
        monsterPathList = GameObject.FindGameObjectsWithTag(Settings.MonsterPathTag);
		monsterFishList = GameObject.FindGameObjectsWithTag (Settings.MonsterFishTag);
		gateCornerList = GameObject.FindGameObjectsWithTag (Settings.GateCornerTag);
		gateCornerRemoteList = GameObject.FindGameObjectsWithTag (Settings.GateCornerRemoteTag);
        
        
        // Show TapToPlay
//		if(!Common.ShareInstance().isFreeStyleTutorial) {
			PopupManager.Instance.ShowTapToPlay ();
//		}

		SoundManager.Instance.PlayRandomMusic (SoundID.MainGame1, SoundID.MainGame2, SoundID.MainGame3, SoundID.MainGame4);

		Analytics.Instance.LogLevel(MapManager.Instance.currentLevelPlayed, LogLevelType.Play);
	}

	void SpawnNewSnake(int skinIndex)
	{
	

	}
    
	// Compile 3 Star Fragment to 1 Golden Star Animation
    public void CompileStarFragment() {
        
		Sequence compileSequence = DOTween.Sequence ();

//        var delay = DelayAction.Create(.5f);
        
        var starFragment1 = GameObject.Find(Settings.StarFragmentUI1);
        var starFragment2 = GameObject.Find(Settings.StarFragmentUI2);
        var starFragment3 = GameObject.Find(Settings.StarFragmentUI3);
        
        starFragment1.GetComponent<Image>().enabled = false;
        starFragment2.GetComponent<Image>().enabled = false;
        starFragment3.GetComponent<Image>().enabled = false;
        
        GameObject starFragMentPref1 = (GameObject)Instantiate(starFragmentPref, Camera.main.ScreenToWorldPoint(starFragment1.transform.position), Quaternion.identity);
        GameObject starFragMentPref2 = (GameObject)Instantiate(starFragmentPref, Camera.main.ScreenToWorldPoint(starFragment2.transform.position), Quaternion.identity);
        GameObject starFragMentPref3 = (GameObject)Instantiate(starFragmentPref, Camera.main.ScreenToWorldPoint(starFragment3.transform.position), Quaternion.identity);
        
		compileSequence.Append (starFragMentPref1.transform.DOShakePosition (0.5f, new Vector2 (0.1f, 0)))
			.Join (starFragMentPref2.transform.DOShakePosition (0.5f, new Vector2 (0.1f, 0)))
			.Join (starFragMentPref3.transform.DOShakePosition (0.5f, new Vector2 (0.1f, 0)));
				
		Vector2 endPoint = Vector3.zero;

		switch (Common.ShareInstance().star) {
		case 1:
			endPoint = Camera.main.ScreenToWorldPoint(GameObject.Find(Settings.StarUI1).transform.position);
			break;
		case 2:
			endPoint = Camera.main.ScreenToWorldPoint(GameObject.Find(Settings.StarUI2).transform.position);
			break;
		case 3:
			endPoint = Camera.main.ScreenToWorldPoint(GameObject.Find(Settings.StarUI3).transform.position);
			break;
		}

		compileSequence.Append (starFragMentPref1.transform.DOMove (endPoint, .5f).SetEase (Ease.OutSine))
			.Join (starFragMentPref2.transform.DOMove (endPoint, .5f).SetEase (Ease.OutSine))
			.Join (starFragMentPref3.transform.DOMove (endPoint, .5f).SetEase (Ease.OutSine))
			.OnComplete (() => {
				Destroy(starFragMentPref1);
				Destroy(starFragMentPref2);
				Destroy(starFragMentPref3);

				switch (Common.ShareInstance().star) {
				case 1:
					GameObject.Find(Settings.StarUI1).GetComponent<Image>().sprite = starSprite;
					break;
				case 2:
					GameObject.Find(Settings.StarUI2).GetComponent<Image>().sprite = starSprite;
					break;
				case 3:
					GameObject.Find(Settings.StarUI3).GetComponent<Image>().sprite = starSprite;
					break;
				}
		});
				    
        
       
    }

	
	// Update is called once per frame
	void Update () {
        if (!Common.ShareInstance().isEndPointOpening) {
            if (Common.ShareInstance().star > 0) {
                OpenEndPointCover();
            }
        }
        
//		if (Common.ShareInstance ().isSurvivalSnake) {
//			if (!lifeInfinity.transform.GetChild(0).gameObject.activeInHierarchy) {
//				life1.Hide ();
//				life2.Hide ();
//				life3.Hide ();
//
//				lifeInfinity.transform.GetChild (0).Show ();
//			}
//		}
	}
    
	// Open EndPoint when current Star > 0
	public void OpenEndPointCover() {
        Common.ShareInstance().isEndPointOpening = true;
                
        GameObject endPointCover = GameObject.FindGameObjectWithTag(Settings.EndPointTag).FindInChildren(Settings.EndPointCover);
        Animator anim = endPointCover.GetComponent<Animator>();
        anim.SetTrigger(Settings.DestroyEndPointCoverTrigger);

		if (endPoint) {
			endPoint.GetComponent<EndPointController> ().PlayOpenFX ();
		}
    }
    
    IEnumerator DestroyEndPointCover(GameObject obj, float delay) {
        yield return new WaitForSeconds(delay);
        obj.SetActive(false);
    }

	// Check Is GameOver or not
	private bool CheckGameover() {
		if (Common.ShareInstance().life == 0) {
			return true;
		}
		return false;
	}

	// Reduce Snake's Life
	public void ReduceLife() {
		if (Common.ShareInstance().isCantMove) {
			return;
		}

		ShakeCamera ();
		
		Common.ShareInstance().isCantMove = true;
		Common.ShareInstance ().teleportCount = 0;
		
        // Add Stats 
        UserData.Instance.Died++;
        UserData.Instance.Save();
        
		StopCornerAnim();
    
		Common.ShareInstance ().life++;

		if (MapManager.Instance.currentLevelPlayed >= 10) {
			if (Common.ShareInstance ().life % 5 == 0) {
				GoogleAds.Instance.ShowInterstitial ();
			}
		}
        
//        snakeGhost = (GameObject)Instantiate(snakeGhostPref, snakeHead.transform.position, Quaternion.identity);
		snakeGhost = ObjectPool.instance.GetObjectForType("DiedSnake", false);
		snakeGhost.transform.position = snakeHead.transform.position;
        snakeHead.GetComponent<Animator>().SetTrigger(Settings.SnakeDeadTrigger);
        

//        CheckLifeUI();
		lastTimeDie = Time.time;

		SoundManager.Instance.PlaySound (SoundID.SnakeDamage);
		SoundManager.Instance.PlaySound(SoundID.Punch);

		PopupManager.Instance.ShowTapToReturn ();
//		if (CheckGameover() && !Common.ShareInstance().isSurvivalSnake) {
//			OnGameOver();
//
//			SoundManager.Instance.PlaySound (SoundID.LoseGame);
//		}
//        else {
//            lastTimeDie = Time.time;
//
//			SoundManager.Instance.PlaySound (SoundID.SnakeDamage);
//
//			PopupManager.Instance.ShowTapToReturn ();
//        }

//		CheckTutorial ();
	}

	void ShakeCamera() {
		Camera camera = Camera.main;
		camera.DOShakePosition (.5f, 0.2f);
	}

	void CheckTutorial() {
		if (UserData.Instance.CompleteTutorialPhase1_2) {
			return;
		}

		if (UserData.Instance.WasShowDied) {
			return;
		}

		if (GameObject.Find("TutorialTriggerDied") == null) {
			return;
		}

		GameObject.Find ("TutorialTriggerDied").GetComponent<Level3TutorialDied> ().Show ();
	}
    
    void StopCornerAnim() {
        snakeCornerList = GameObject.FindGameObjectsWithTag(Settings.SnakeCorner);
        foreach(GameObject obj in snakeCornerList) {
            obj.GetComponent<Animator>().enabled = false;
        }
    }
    
    void ResumeCornerAnim() {
        snakeCornerList = GameObject.FindGameObjectsWithTag(Settings.SnakeCorner);
        foreach(GameObject obj in snakeCornerList) {
            obj.GetComponent<Animator>().enabled = true;
        }
    }

	// Check Current Life And Apply to UI
    void CheckLifeUI() {
        switch(Common.ShareInstance().life) {
			case 2:
			life1.GetComponent<Animator>().SetTrigger("LostLife");
            break;
            case 1:
			life2.GetComponent<Animator>().SetTrigger("LostLife");
            break;
            case 0:
			life3.GetComponent<Animator>().SetTrigger("LostLife");
            break;
        }
    }
    
	// Reset All Component In Current Map
	public void ResetGame() {
        if (Time.time - lastTimeDie < 0.5f) {
            return;
        }

		// Reset Snake
		snakeHead.GetComponent<MySnake>().ResetSnake();
		snakeHead.GetComponent<Animator>().SetTrigger(Settings.SnakeResetTrigger);

		Common.ShareInstance().isTurnRight = true;
		Common.ShareInstance().star = 0;
		Common.ShareInstance ().starfragments = 0;
		Common.ShareInstance().score = 0;
		Common.ShareInstance().isGameStart = false;
		Common.ShareInstance().isCantMove = false;
		Common.ShareInstance().isSnakeGoOut = false;
		Common.ShareInstance().isHaveHelmet = false;
		Common.ShareInstance().isTailReachedCorner = false;
		Common.ShareInstance().isFemaleTailReachedCorner = false;
		Common.ShareInstance().isEndPointOpening = false;
		Common.ShareInstance ().foodEat = 0;
        
        // Show TapToPlay
        PopupManager.Instance.ShowTapToPlay();
        
        // Reset Start End Point
        ResetStartEndPoint();
		
        // Delete Snake Ghost
        if (snakeGhost) {
//            Destroy(snakeGhost);
			ObjectPool.instance.PoolObject(snakeGhost);
            snakeGhost = null;
        }
        
		// SnakeCorner
		foreach(GameObject obj in snakeCornerList) {
			Destroy(obj.gameObject);
		}
		
		// Reset Food
		foreach(GameObject obj in foodList) {
			obj.GetComponent<FoodController>().ResetFood();
		}	
			
		// Reset Reverse Food
		foreach(GameObject obj in reverseFoodList) {
			obj.GetComponent<ReverseFoodController>().ResetFood();
		}
		
		// Reset Star
		foreach(GameObject obj in starList) {
			obj.SetActive(true);
		}

		// Reset Star Fragment
		foreach (GameObject obj in starFragmentList) {
			obj.SetActive (true);
		}

		// Reset Rock
		foreach(GameObject obj in rockList) {
			obj.SetActive(true);
		}
		
		// Reset Small Rock
		foreach(GameObject obj in smallRockList) {
			obj.SetActive(true);
		}
		
		// Reset Helmet
		foreach(GameObject obj in helmetList) {
			obj.SetActive(true);
		}
        
        // Reset Star UI
        foreach(GameObject obj in starUIs) {
            obj.GetComponent<Image>().sprite = starOutlineSprite;
        }

		// Reset Star Fragment UI
		foreach (GameObject obj in starFragmentUIs) {
			obj.GetComponent<Image> ().enabled = false;
		}
        
        // Reset Monster
        foreach(GameObject obj in monsterList) {
            obj.GetComponent<MonsterController>().ResetMonster();
        }
        
        // Reset Monster Circle
        foreach(GameObject obj in monsterCircleList) {
            obj.GetComponent<MonsterCircleController>().ResetMonster();
        }
        
        // Reset Monster Path
        foreach(GameObject obj in monsterPathList) {
            obj.GetComponent<MonsterRectController>().ResetMonster();
        }

		// Reset Monster Fish
		foreach (GameObject obj in monsterFishList) {
			obj.GetComponent<MonsterFishController> ().ResetMonster ();
		}
      
		// Reset Gate Corner
		foreach (GameObject obj in gateCornerList) {
			obj.GetComponent<GateCornerController> ().ResetGate ();
		}

		// Reset Gate Remote
		foreach (GameObject obj in gateCornerRemoteList) {
			obj.GetComponent<GateCornerRemoteController> ().ResetRemote ();
		}

        // Reset Star Effect
        starEffectList = GameObject.FindGameObjectsWithTag(Settings.StarEffectTag);
        foreach(GameObject obj in starEffectList) {
            Destroy(obj);
        }
        
        // Reset Teleport Effect
        GameObject[] teleportA = GameObject.FindGameObjectsWithTag(Settings.TeleportA);
        foreach(GameObject obj in teleportA) {
            obj.GetComponent<TeleportController>().DisableEffect();
        }
        
        GameObject[] teleportB = GameObject.FindGameObjectsWithTag(Settings.TeleportB);
        foreach(GameObject obj in teleportB) {
            obj.GetComponent<TeleportController>().DisableEffect();
        }
        
        
        // Reset Toolkit
        ToolkitManager.Instance.ResetToolkitLife();
		

        
        // Show Effect
		resetEffectPref.Create(null, startPoint.transform.position);

		// Reset Freeze
		if (GameObject.Find ("FreezeEffect")) {
			GameObject.Find ("FreezeEffect").transform.localScale = new Vector2 (2, 2);
		}

		// Reset Immortal
		Common.ShareInstance().isImmortal = false;
		if (GameObject.Find ("ImmortalEffect")) {
			GameObject.Find ("ImmortalEffect").transform.localScale = new Vector2 (2, 2);
		}

		// Reset Slow
		if (GameObject.Find ("SlowEffect")) {
			GameObject.Find ("SlowEffect").transform.localScale = new Vector2 (2, 2);
		}

		PopupManager.Instance.HideTapToReturn ();
	}

	// Reset State of Start Point and EndPoint
    private void ResetStartEndPoint() {
        startPoint.gameObject.FindInChildren(Settings.StartPointCover).SetActive(false);
        startPoint.ReturnPosition();
        
		if (!Common.ShareInstance ().isFreeStyleTutorial) {
			endPoint.gameObject.FindInChildren (Settings.EndPointCover).GetComponent<Animator> ().SetTrigger (Settings.ResetEndPointCoverTrigger);
			endPoint.GetComponent<EndPointController> ().ResetFX ();
		}
    }

	// GameOver
	void OnGameOver() {
		Common.ShareInstance().isGameOver = true;
		PopupManager.Instance.OpenPopupGameOver();

		// Analytics
		Analytics.Instance.LogLevel(MapManager.Instance.currentLevelPlayed, LogLevelType.Lose);
	}

	// Complete A Level
	public void OnMapCompleted() {
        Common.ShareInstance().isMapCompleted = true;
        
        StopCornerAnim();
        
		PopupManager.Instance.OpenPopupFinishLevel();
		
		Common.ShareInstance().score+= Common.ShareInstance().life * Settings.LifeScore;
        
//        if (Common.ShareInstance().life == Settings.MaxLife) {
        StatsManager.Instance.IncreaseEnergy(1);
//        }
        
		
		MapManager.Instance.FinishLevel(Common.ShareInstance().score, Common.ShareInstance().star);


		// Show Ads when level 11
//		if (MapManager.Instance.currentLevelPlayed > 10) {
//			DOVirtual.DelayedCall (2, () => {
//				GoogleAds.Instance.ShowInterstitial ();
//			});
//		}

		SoundManager.Instance.PlaySound (SoundID.WinGame);
        
	}
    
	// Make All Monsters in Current Map Slower
    public void SlowMonster() {
        Clock clock = Timekeeper.instance.Clock("Snake");
		Debug.Log ("ABC");
        
        if (clock.localTimeScale == 1) {
            clock.LerpTimeScale(2f, 0.5f, true);
            // clock.localTimeScale = 0.2f;    
        }
        else{
            // clock.localTimeScale = 1;
            clock.LerpTimeScale(1, 0.5f, true);
        }
        
        
        
    }
}
