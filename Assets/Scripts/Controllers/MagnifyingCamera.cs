﻿using UnityEngine;
using System.Collections;

public class MagnifyingCamera : MonoBehaviour {

    Vector3 mousePos;
	// Use this for initialization
	void Start () {
	    //  GetComponent<Camera>().pixelRect = new Rect(0, 0, 50, 50);
         
	}
	
    public Vector3 getWorldPosition(Vector3 screenPos)
	{
		Vector3 worldPos;
		if(Camera.main.orthographic)
		{
			worldPos = Camera.main.ScreenToWorldPoint (screenPos);
			worldPos.z = Camera.main.transform.position.z;
		}
		else
		{
			worldPos = Camera.main.ScreenToWorldPoint (new Vector3 (screenPos.x, screenPos.y, Camera.main.transform.position.z));
			worldPos.x *= -1;
			worldPos.y *= -1;
		}
		return worldPos;
	}
    
	// Update is called once per frame
	void Update () {
	   // Following lines set the camera's pixelRect and camera position at mouse position
		// GetComponent<Camera>().pixelRect = new Rect (Input.mousePosition.x - 20 / 2.0f, Input.mousePosition.y - 20 / 2.0f, 20, 20);
		// mousePos = getWorldPosition (Input.mousePosition);
        // mousePos.z = -10;
		
	}
}
