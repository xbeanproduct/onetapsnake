﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

public class FemaleInputController : MonoBehaviour {

	public float lastClickTime = 0;
	float timePerClick = .4f;
	float timePerAction = .4f;
	public float lastDoActionTime;
	FemaleSnake snake;
	public bool isWaitToTurn;

    public GameObject touchEffectPref;

	// Use this for initialization
	void Start () {
		snake = GetComponent<FemaleSnake>();
		lastDoActionTime = 0;
		lastClickTime = 0;
		isWaitToTurn = false;
	}
	
	// Update is called once per frame
	void Update () {

		if (isWaitToTurn && !Common.ShareInstance().isCantMove)
		{
			if (Time.time - lastClickTime >= timePerClick && Time.time - lastDoActionTime >= timePerAction) 
			{
				lastClickTime = Time.time;
				SnakeTurn();
				isWaitToTurn = false;
			}
		}

		#if UNITY_EDITOR
		if (Input.GetMouseButtonDown(0)) {
			
			if (EventSystem.current != null && EventSystem.current.IsPointerOverGameObject()) {
				return;
			}
			
			if (!IsPlaying()) {
				return;
			}

			if (Time.time - lastClickTime >= timePerClick && Time.time - lastDoActionTime >= timePerAction) 
			{
				lastClickTime = Time.time;
				SnakeTurn();
			}
			else
			{
				isWaitToTurn = true;
			}
		}

		#elif UNITY_IPHONE || UNITY_ANDROID
		
		if (Input.touchCount > 0) {
			Touch touch = Input.GetTouch(0);
			
			if (EventSystem.current.IsPointerOverGameObject(touch.fingerId)) {
				return;
			}
			
			if (touch.phase == TouchPhase.Began) {
				if(!IsPlaying()) {
					return;
				}
                    
				if (Time.time - lastClickTime >= timePerClick && Time.time - lastDoActionTime >= timePerAction) 
				{
					lastClickTime = Time.time;
					SnakeTurn();
				}
				else
				{
					isWaitToTurn = true;
				}
			}
		}
		#endif
		
		
	}

	void SnakeTurn()
	{
		if (snake.isTurnRight) {
			snake.TurnRight();
		}
		else {
			snake.TurnLeft();
		}
	}
	
	private bool IsPlaying() {
        if (Common.ShareInstance().isGameOver) {
            return false;
        }
        
        if (Common.ShareInstance().isMapCompleted) {
            return false;
        }
        
		if (!snake.isStart) {
            snake.isStart = true;
			return false;
		}
		
		if (Common.ShareInstance().isCantMove) {
			return false;
		}
		
		if (Common.ShareInstance().isInTussock) {
			Common.ShareInstance().isInTussock = false;
			return false;
		}
		
		return true;
	}
	
}
