﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class MapManager : Singleton<MapManager> {

	public int currentLevelPlayed = -1;
	public int currentLevelStar = 0;
    public RectTransform chapter1Image;
	
	private bool _isCurrentLevelFinished = false;

	public Vector2 lastLevelPos = Vector2.zero;
    public bool isNewLevelUnlock = false;
    
	// Use this for initialization
	void Start () {
		//SoundManager.Instance.PlayRandomMusic (SoundID.MainGame1, SoundID.MainGame2, SoundID.MainGame3, SoundID.MainGame4);
	}
	
	// Update is called once per frame
	void Update () {
        
	}
	
	void OnLevelWasLoaded() {
		if (_isCurrentLevelFinished) {
            if (SceneManager.GetActiveScene().name.Equals(Settings.MapSelectScene)) {
                if (isNewLevelUnlock){
                    Debug.Log("Pass a level");
                }
            }
        }
	}

	// Finish Level
	public void FinishLevel(int score, int star) {
		_isCurrentLevelFinished = true;
		Hashtable mapInfo = new Hashtable();
        
        mapInfo.Add(Settings.MapLevelKey, currentLevelPlayed);
		mapInfo.Add(Settings.MapScoreKey, score);
		mapInfo.Add(Settings.MapStarKey, star);
		
        if (UserData.Instance.MapsData.Count > 0) {
            for(int i = 0; i < UserData.Instance.MapsData.Count; i++) {
                if (int.Parse(((Hashtable)UserData.Instance.MapsData[i])[Settings.MapLevelKey].ToString()) == currentLevelPlayed) {
                    if (score > int.Parse(((Hashtable)UserData.Instance.MapsData[i])[Settings.MapScoreKey].ToString())){
                        UserData.Instance.MapsData[i] = mapInfo;
                        UserData.Instance.Save();
                    }
                    UpdateToServer();
                    isNewLevelUnlock = false;
                    return;
                }
            }
        }
        
        UserData.Instance.MapsData.Add(mapInfo);
        if ( UserData.Instance.HightestLevel < currentLevelPlayed) {
            UserData.Instance.HightestLevel = currentLevelPlayed;
            isNewLevelUnlock = true;
        }
        
        UserData.Instance.Save();
        UpdateToServer();
        
		// Log analytics
		Analytics.Instance.LogLevel (currentLevelPlayed, LogLevelType.Win);
	}
    
	// Finish With HightestLevel - Use to Sync
    public void FinishLevel(int score, int star, int level) {
		currentLevelPlayed = level;
		_isCurrentLevelFinished = true;
		Hashtable mapInfo = new Hashtable();
        
        mapInfo.Add(Settings.MapLevelKey, level);
		mapInfo.Add(Settings.MapScoreKey, score);
		mapInfo.Add(Settings.MapStarKey, star);
		
        if (UserData.Instance.MapsData.Count > 0) {
            for(int i = 0; i < UserData.Instance.MapsData.Count; i++) {
                if (int.Parse(((Hashtable)UserData.Instance.MapsData[i])[Settings.MapLevelKey].ToString()) == level) {
                    if (score > int.Parse(((Hashtable)UserData.Instance.MapsData[i])[Settings.MapScoreKey].ToString())){
                        UserData.Instance.MapsData[i] = mapInfo;
                        UserData.Instance.Save();
                    }
                    UpdateToServer();
                    isNewLevelUnlock = false;
                    return;
                }
            }
        }
        
        UserData.Instance.MapsData.Add(mapInfo);
        if ( UserData.Instance.HightestLevel < level) {
            UserData.Instance.HightestLevel = level;
            isNewLevelUnlock = true;
        }
        
        UserData.Instance.Save();
        UpdateToServer();
        
        
	}
    
    public bool CheckLevel(int level) {
        if (UserData.Instance.MapsData.Count > 0) {
            for(int i = 0; i < UserData.Instance.MapsData.Count; i++) {
                if (int.Parse(((Hashtable)UserData.Instance.MapsData[i])[Settings.MapLevelKey].ToString()) == level) {
                    return true;
                }
            }
        }
        
        return false;
    }
    
    public void UpdateToServer() {
        StartCoroutine(ApiManager.Instance.UpdateMapInfo(response => {
            if (response != null && response.Code == Settings.SuccessCode) {
                Debug.Log(response.Message);
                Debug.Log(response.ResultMessage);
                return;
            }
            else {
                Debug.Log("User not login");
            }
        }));
    }
   
}
