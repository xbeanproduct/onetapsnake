﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

public class InputController : MonoBehaviour {

	public float lastClickTime = 0;
	float timePerClick = .4f;
	float timePerAction = .4f;
	public float lastDoActionTime;
	MySnake snake;
	public bool isWaitToTurn;
    public bool isWaitTurnDirection;
    public bool isTurnRightNext = false;

    public GameObject touchEffectPref;

	// Use this for initialization
	void Start () {
		snake = GetComponent<MySnake>();
		lastDoActionTime = 0;
		lastClickTime = 0;
		isWaitToTurn = false;
	}
	
	// Update is called once per frame
	void Update () {

		if (isWaitToTurn && !Common.ShareInstance().isCantMove)
		{
			if (Time.time - lastClickTime >= timePerClick && Time.time - lastDoActionTime >= timePerAction) 
			{
				lastClickTime = Time.time;
				SnakeTurn();
				isWaitToTurn = false;
			}
		}
        
        if (isWaitTurnDirection && !Common.ShareInstance().isCantMove)
		{
			if (Time.time - lastClickTime >= timePerClick && Time.time - lastDoActionTime >= timePerAction) 
			{
				lastClickTime = Time.time;
				Turn();
				isWaitTurnDirection = false;
			}
		}

		#if UNITY_EDITOR
		if (Input.GetMouseButtonDown(0)) {

			if (Common.ShareInstance().isGamePause) {
				return;
			}

			if (EventSystem.current != null && EventSystem.current.IsPointerOverGameObject()) {
				return;
			}
			
			if (!IsPlaying()) {
				return;
			}

//			if (!UserData.Instance.WasPlayedTutorial) {
//				if (Common.ShareInstance().isGameTutorial) {
//					return;
//				}
//				else {
//					Common.ShareInstance().isGameTutorial = true;
//				}
//			}
            
            if (isWaitTurnDirection) {
                return;
            }
            
            // Instance touch effect
//            Instantiate(touchEffectPref, Camera.main.ScreenToWorldPoint(Input.mousePosition), Quaternion.identity);
			ObjectPool.instance.GetObjectForType(Settings.TapEffectPool, false, 3).transform.position = Camera.main.ScreenToWorldPoint(Input.mousePosition);

			if (Time.time - lastClickTime >= timePerClick && Time.time - lastDoActionTime >= timePerAction) 
			{
				lastClickTime = Time.time;
				SnakeTurn();
			}
			else
			{
				isWaitToTurn = true;
			}
		}

		#elif UNITY_IPHONE || UNITY_ANDROID
		
		if (Input.touchCount > 0) {
			
			if (Common.ShareInstance().isGamePause) {
				return;
			}

			foreach(Touch touch in Input.touches) {

				if (!EventSystem.current.IsPointerOverGameObject(touch.fingerId)) {	if (touch.phase == TouchPhase.Began) {
						if(!IsPlaying()) {
							return;
						}

						if (isWaitTurnDirection) {
							return;
						}

//						if (!UserData.Instance.WasPlayedTutorial) {
//							if (Common.ShareInstance().isGameTutorial) {
//								return;
//							}
//							else {
//								Common.ShareInstance().isGameTutorial = true;
//							}
//						}

						// Instance touch effect
//						Instantiate(touchEffectPref, Camera.main.ScreenToWorldPoint(touch.position), Quaternion.identity);
						ObjectPool.instance.GetObjectForType(Settings.TapEffectPool, false, 3).transform.position = Camera.main.ScreenToWorldPoint(touch.position);

						if (Time.time - lastClickTime >= timePerClick && Time.time - lastDoActionTime >= timePerAction) 
						{
							lastClickTime = Time.time;
							SnakeTurn();
						}
						else
						{
							isWaitToTurn = true;
						}
					}
				}

			}
		}
		#endif
		
		
	}
    
    public void TurnDirection(bool isRight) {
        if (Time.time - lastClickTime >= timePerClick && Time.time - lastDoActionTime >= timePerAction) 
        {
            lastClickTime = Time.time;
			if (isRight) {
				snake.TurnRight ();
			} else {
				snake.TurnLeft ();
			}
		
        }
        else
        {
            isWaitTurnDirection = true;
            isTurnRightNext = isRight;
        }
    }
    
    void Turn() {
		if (isTurnRightNext) {
			snake.TurnRight ();
		} else {
			snake.TurnLeft ();
		}
    }

	public void SnakeTurn()
	{
		if (Common.ShareInstance().isTurnRight) {
			snake.TurnRight();
		}
		else {
			snake.TurnLeft();
		}
	}
	
	private bool IsPlaying() {
        if (Common.ShareInstance().isGameOver) {
            return false;
        }
        
        if (Common.ShareInstance().isMapCompleted) {
            return false;
        }
        
		if (!Common.ShareInstance().isGameStart) {
			Common.ShareInstance().isGameStart = true;
            PopupManager.Instance.HideTapToPlay();

			SoundManager.Instance.PlaySound (SoundID.StartMove);
			return false;
		}
		
		if (Common.ShareInstance().isCantMove) {
			if (Common.ShareInstance ().isFreeStyleTutorial) {
				GameManagerTutorial.Instance.ResetGame ();
			} else {
				GameManager.Instance.ResetGame ();
			}

			SoundManager.Instance.PlaySound (SoundID.ButtonChangeScene);
			return false;
		}

		if (Common.ShareInstance ().isGateTurning) {
			return false;
		}
		
		if (Common.ShareInstance().isInTussock) {
			Common.ShareInstance().isInTussock = false;
			return false;
		}
		
		return true;
	}
	
}
