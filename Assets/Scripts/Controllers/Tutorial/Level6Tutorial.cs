﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.EventSystems;
using Chronos;
using DG.Tweening;

public class Level6Tutorial : MonoBehaviour {

	public RectTransform tutorial;
	public RectTransform bg;
	public RectTransform tutorialText;

	public RectTransform scoinText;
	public RectTransform arrow;
	public RectTransform arrowCoin;

	private bool isShowed;
	// Use this for initialization
	void Start () {
		if (!UserData.Instance.CompleteToolkitTutorial) {
			UserData.Instance.WasPlayedTutorial = true;
			Common.ShareInstance ().isGameTutorial = true;
			Common.ShareInstance ().isCurrentMapTutorial = true;

			UserData.Instance.Save ();
		}
	}

	void OnTriggerEnter2D(Collider2D obj) {
		if (!Common.ShareInstance ().isCurrentMapTutorial) {
			return;
		}

		if (isShowed) {
			return;
		}

		if (obj.name.Equals (Settings.SnakeHead)) {

			if (GameObject.FindGameObjectWithTag (Settings.MonsterTag) == null) {
				return;
			}

			Clock clock = Timekeeper.instance.Clock("Root");
			clock.localTimeScale = 0;

			Common.ShareInstance ().isGamePause = true;
			tutorial.Show ();
			Common.ShareInstance ().isGameTutorial = false;
			//Anim
			bg.GetComponent<Image>().DOFade(.85f, .5f);
			tutorialText.GetComponent<Text> ().DOFade (1, .5f)
				.OnComplete (() => {
				DOVirtual.DelayedCall(.5f, () => {
					CountNumber ();
				});
			});

			isShowed = true;

		}
	}

	void CountNumber() {

		Sequence sequence = DOTween.Sequence ();

		int currentCoin = UserData.Instance.EarnScoin;
		int desCoin = currentCoin + 200;

		arrowCoin.Show ();
		sequence.Append (scoinText.DOScale (new Vector2 (1.5f, 1.5f), .5f))
			.Append (DOTween.To ((value) => {
				StatsManager.Instance.SetEarnScoin(Mathf.RoundToInt(value));
			}, currentCoin, desCoin, 2).SetEase(Ease.OutSine))
			.Append (scoinText.DOScale(Vector2.one, .5f)
				.OnComplete(() => {
					arrowCoin.Hide();
				}))
			.OnComplete (() => {
				tutorialText.DOBlendableMoveBy (new Vector3 (-7, 0, 0), .5f).SetEase (Ease.InBack);
				arrow.Show();
			});

		

	}


//	public void Next() {
//		Sequence sequence = DOTween.Sequence ();
//		sequence.Append (tutorialText.DOBlendableMoveBy (new Vector3 (-7, 0, 0), .5f).SetEase (Ease.InBack))
//			.Append (bg.GetComponent<Image> ().DOFade (0, .5f))
//			.OnComplete (() => {
//				Common.ShareInstance ().isGamePause = false;
//				tutorial.Hide ();
//			});
//	}

	// Update is called once per frame
	void Update () {

	}
}
