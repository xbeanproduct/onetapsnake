﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections;
using DG.Tweening;

public class Level2Tutorial : MonoBehaviour {

	public RectTransform tutorial;


	public bool isEnable = false;
	// Use this for initialization
	void Start () {
		if (!UserData.Instance.CompleteTutorialPhase1) {
			UserData.Instance.WasPlayedTutorial = false;
			Common.ShareInstance ().isGameTutorial = true;
			Common.ShareInstance ().isCurrentMapTutorial = true;

			UserData.Instance.Save ();
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (isEnable) {
			#if UNITY_EDITOR
			if (Input.GetMouseButtonDown (0)) {
				if (EventSystem.current != null && EventSystem.current.IsPointerOverGameObject()) {
					return;
				}

				Common.ShareInstance ().isGamePause = false;
				tutorial.Hide ();
				isEnable = false;
			}
			#else
			if (Input.touchCount > 0) {
				if (Input.GetTouch(0).phase == TouchPhase.Began) {
					if (EventSystem.current != null && EventSystem.current.IsPointerOverGameObject(Input.GetTouch(0).fingerId)) {
						return;
					}


					Common.ShareInstance().isGamePause = false;
					tutorial.Hide();
					isEnable = false;
				}
			}
			#endif

		}
	}

	void OnTriggerEnter2D(Collider2D obj) {
		if (UserData.Instance.WasPlayedTutorial) {
			return;
		}

		if (obj.name.Equals (Settings.SnakeHead)) {
			Common.ShareInstance ().isGamePause = true;
			tutorial.Show ();
			//Anim
			Common.ShareInstance ().isGameTutorial = false;
			isEnable = true;
		}
	}
}
