﻿using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using System.Collections;

public class Level3TutorialDied : MonoBehaviour {

	public RectTransform tutorial;
	public RectTransform bg;
	public RectTransform tutorialText;
	public RectTransform nextBtn;

	private bool isShow = false;
	// Use this for initialization
	void Start () {
		if (!UserData.Instance.CompleteTutorialPhase1_2) {
			UserData.Instance.WasPlayedTutorial = true;
			Common.ShareInstance ().isGameTutorial = true;
			Common.ShareInstance ().isCurrentMapTutorial = true;

			UserData.Instance.Save ();
		}
	}

	void OnTriggerEnter2D(Collider2D obj) {
		if (!Common.ShareInstance ().isCurrentMapTutorial) {
			return;
		}

		if (obj.name.Equals (Settings.SnakeHead)) {
			Show ();

		}
	}

	public void Show() {
		if (isShow) {
			return;
		}

		tutorial.Show ();
		Common.ShareInstance ().isGamePause = true;
		Common.ShareInstance ().isGameTutorial = false;
		//Anim
		bg.GetComponent<Image>().DOFade(.85f, .5f);
		tutorialText.GetComponent<Text>().DOFade(1, .5f);
		nextBtn.GetComponent<Image> ().DOFade (1, .5f).SetDelay(0.5f);

		isShow = true;

		if (GameObject.FindGameObjectWithTag ("MenuButton")) {
			GameObject.FindGameObjectWithTag ("MenuButton").GetComponent<Image>().enabled = false;
		}
	}

	public void Next() {
		Sequence sequence = DOTween.Sequence ();
		sequence.Append (tutorialText.DOBlendableMoveBy (new Vector3 (-7, 0, 0), .5f).SetEase (Ease.InBack))
			.Join (nextBtn.DOBlendableMoveBy (new Vector3 (-7, 0, 0), .5f).SetEase (Ease.InBack).SetDelay (0.2f))
			.Append (bg.GetComponent<Image> ().DOFade (0, .5f))
			.OnComplete (() => {
				Common.ShareInstance ().isGamePause = false;
				tutorial.Hide ();

				if (GameObject.FindGameObjectWithTag ("MenuButton")) {
					GameObject.FindGameObjectWithTag ("MenuButton").GetComponent<Image>().enabled = true;
				}

				UserData.Instance.CompleteTutorialPhase1_2 = true;
				UserData.Instance.WasPlayedTutorial = true;

				UserData.Instance.Save();
			});
	}

}
