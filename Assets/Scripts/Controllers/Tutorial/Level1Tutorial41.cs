﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using DG.Tweening;

public class Level1Tutorial41 : MonoBehaviour {

	public RectTransform tutorial31;

	public bool isEnable = false;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

		if (isEnable) {
			#if UNITY_EDITOR
			if (Input.GetMouseButtonDown (0)) {
				if (EventSystem.current != null && EventSystem.current.IsPointerOverGameObject()) {
					return;
				}

				Common.ShareInstance ().isGamePause = false;
				tutorial31.Hide ();
				isEnable = false;
			}
			#else
			if (Input.touchCount > 0) {
				if (Input.GetTouch(0).phase == TouchPhase.Began) {
					if (EventSystem.current != null && EventSystem.current.IsPointerOverGameObject(Input.GetTouch(0).fingerId)) {
						return;
					}

					Common.ShareInstance().isGamePause = false;
					tutorial31.Hide();
					isEnable = false;
				}
			}
			#endif

		}
	}

	void OnTriggerEnter2D(Collider2D obj) {
		if (UserData.Instance.WasPlayedTutorial) {
			return;
		}

		if (obj.name.Equals (Settings.SnakeHead)) {
			Common.ShareInstance ().isGamePause = true;
			tutorial31.Show ();
			//Anim
			Common.ShareInstance ().isGameTutorial = false;
			isEnable = true;
		}
	}
}
