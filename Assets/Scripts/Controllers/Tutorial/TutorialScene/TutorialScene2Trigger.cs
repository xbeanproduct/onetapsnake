﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using DG.Tweening;
using Chronos;

public class TutorialScene2Trigger : MonoBehaviour {

	public GameObject tutorial;
	public GameObject bg;

	public RectTransform tutorialText;
	public RectTransform nextBtn;


	private bool isTriggered = false;
	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {

	}

	void OnTriggerEnter2D(Collider2D obj) {
		if (isTriggered) {
			return;
		}

		if (obj.name.Equals (Settings.SnakeHead)) {
			isTriggered = true;

			Common.ShareInstance ().isGamePause = true;
			tutorial.Show ();

			Common.ShareInstance ().isShowingTutorial = true;
			// Anim
			bg.GetComponent<Image> ().DOFade (.85f, .5f);

			DOVirtual.DelayedCall (1, () => {
				nextBtn.DOBlendableScaleBy(new Vector2(.3f, .3f), .3f).SetLoops(-1, LoopType.Yoyo);
			});

		}
	}

	public void Next() {
		Sequence sequence = DOTween.Sequence ();
		sequence.Append (tutorialText.DOBlendableMoveBy (new Vector3 (7, 0, 0), .5f).SetEase (Ease.InBack))
			.Join (nextBtn.DOBlendableMoveBy (new Vector3 (7, 0, 0), .5f).SetEase (Ease.InBack).SetDelay (0.2f))
			.Append (bg.GetComponent<Image> ().DOFade (0, .5f))
			.OnComplete (() => {
				Common.ShareInstance ().isGamePause = false;
				tutorial.Hide ();
				GameObject.FindObjectOfType<MySnake>().ShowDirection();
				Clock clock = Timekeeper.instance.Clock("Root");
				clock.localTimeScale = 1;
				Common.ShareInstance ().isShowingTutorial = false;
			});

	}
}
