﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using DG.Tweening;
using Chronos;
using UnityEngine.SceneManagement;

public class TutorialScene2Controller : MonoBehaviour {

	public GameObject tutorial;
	public RectTransform tutorialText;
	public RectTransform nextBtn;

	// Tut 2
	public GameObject tutorial2;
	public GameObject tutorialCompoent;
	public GameObject reverseFood;
	public RectTransform tutorialText2;
	public RectTransform nextBtn2;

	// Move on
	public GameObject tutorial3;
	public GameObject arrow;

	// End
	public GameObject tutorial4;

	public RectTransform tapTut;

	// Reverse Highlight
	public GameObject tutorialComp;
	public GameObject tutorial2_1;

	private bool isShowTooltip = false;

	private bool btnClicked = false;


	// Use this for initialization
	void Start () {
		btnClicked = false;

		Common.ShareInstance ().isFreeStyleTutorial = true;
		Common.ShareInstance ().isSurvivalSnake = true;
		UserData.Instance.WasPlayedTutorial = true;
	}
	
	// Update is called once per frame
	void Update () {
		var foods = GameObject.FindGameObjectsWithTag (Settings.FoodTag);
		foreach (var food in foods) {
			if (!food.GetComponent<FoodController> ().isEaten) {
				return;
			}
		}

		if (!Common.ShareInstance ().isEndPointOpening) {
			CompleteTutorialAnim ();
		}
	}

	public void OpenTooltip() {
		if (isShowTooltip) {
			return;
		}

		isShowTooltip = true;

		tutorial.Show ();

		tutorial.FindInChildren ("BG").GetComponent<Image> ().DOFade (.85f, .5f);

		PopupManager.Instance.HideTapToReturn ();
		DOVirtual.DelayedCall (1, () => {
			tutorial.FindInChildren("Btn").transform.DOBlendableScaleBy(new Vector2(.3f, .3f), .3f).SetLoops(-1, LoopType.Yoyo);
		});
	}

	public void ShowReverseTutorial() {
		tutorial2_1.Show ();

		tutorial2_1.FindInChildren("BG").GetComponent<Image> ().DOFade (.85f, .5f);

		DOVirtual.DelayedCall (1, () => {
			tutorial2_1.FindInChildren("Btn").GetComponent<Image>().DOFade(1, .5f);
		});

		DOVirtual.DelayedCall (2, () => {
			tutorial2_1.FindInChildren("Btn").transform.DOBlendableScaleBy(new Vector2(.3f, .3f), .3f).SetLoops(-1, LoopType.Yoyo);
		});

	}

	public void ShowHighlightReverse() {
		Common.ShareInstance ().isGamePause = true;
		tutorialComp.Show ();
	}

//	public void Next() {
//
//		Sequence sequence = DOTween.Sequence ();
//		sequence.Append (tutorialText.DOBlendableMoveBy (new Vector3 (7, 0, 0), .5f).SetEase (Ease.InBack))
//			.Join (nextBtn.DOBlendableMoveBy (new Vector3 (7, 0, 0), .5f).SetEase (Ease.InBack).SetDelay (0.2f))
//			.Append (tutorial.FindInChildren("BG").GetComponent<Image> ().DOFade (0, .5f))
//			.OnComplete (() => {
//				Common.ShareInstance ().isGamePause = false;
//				tutorial.Hide ();
//				GameManagerTutorial.Instance.ResetGame ();
//				PopupManager.Instance.HideTapToPlay ();
//				ShowTutorial2 ();
//			});
//
//	}

	public void Next() {

		Sequence sequence = DOTween.Sequence ();
		sequence.Append (tutorial2_1.FindInChildren("Text").transform.DOBlendableMoveBy (new Vector3 (7, 0, 0), .5f).SetEase (Ease.InBack))
			.Join (tutorial2_1.FindInChildren("Btn").transform.DOBlendableMoveBy (new Vector3 (7, 0, 0), .5f).SetEase (Ease.InBack).SetDelay (0.2f))
			.Append (tutorial2_1.FindInChildren("BG").GetComponent<Image> ().DOFade (0, .5f))
			.OnComplete (() => {
				Common.ShareInstance ().isGamePause = false;
				tutorial2_1.Hide ();
				tutorialComp.Hide();
				Clock clock = Timekeeper.instance.Clock("Root");
				clock.localTimeScale = 1;
				Common.ShareInstance().isShowingTutorial = false;
			});

	}

	void ShowTutorial2() {
		tutorial2.Show ();
		tutorialCompoent.Show ();
		reverseFood.Show ();


		GameManagerTutorial.Instance.reverseFoodList = GameObject.FindGameObjectsWithTag(Settings.ReverseFoodTag);
		tutorial2.FindInChildren ("BG").GetComponent<Image> ().DOFade (.85f, .5f);

		DOVirtual.DelayedCall (3, () => {
			tutorial2.FindInChildren("Btn").transform.DOBlendableScaleBy(new Vector2(.3f, .3f), .3f).SetLoops(-1, LoopType.Yoyo);
		});
	}



	public void Next2() {
		Sequence sequence = DOTween.Sequence ();
		sequence.Append (tutorialText2.DOBlendableMoveBy (new Vector3 (-7, 0, 0), .5f).SetEase (Ease.InBack))
			.Join (nextBtn2.DOBlendableMoveBy (new Vector3 (-7, 0, 0), .5f).SetEase (Ease.InBack).SetDelay (0.2f))
			.Append (tutorial2.FindInChildren("BG").GetComponent<Image> ().DOFade (0, .5f))
			.Join(tapTut.GetComponent<CanvasGroup>().DOFade(0, .5f))
			.OnComplete (() => {
				Common.ShareInstance ().isGamePause = false;
				tutorial2.Hide ();
				tutorialCompoent.Hide();
				PopupManager.Instance.ShowTapToPlay();
			});
	}

 	void CompleteTutorialAnim() {
		tutorial3.Show ();
		arrow.Show ();
		GameObject.FindObjectOfType<GameManagerTutorial> ().OpenEndPointCover ();
	}

	public void OpenEndTutorial() {
		Common.ShareInstance().isGamePause = true;
		Clock clock = Timekeeper.instance.Clock("Snake");
		clock.localTimeScale = 0;

		tutorial3.Hide ();
		arrow.Hide ();
		tutorial4.Show ();

		tutorial4.FindInChildren ("BG").GetComponent<Image> ().DOFade (.85f, .5f);
	}

	public void NextScene() {
		if (btnClicked) {
			return;
		}

		btnClicked = true;
//		FadePanel.Instance.LoadLevel ("Tutorial3");
		FadePanel.Instance.LoadLevel (Settings.MapSelectScene);
		UserData.Instance.CompleteBeginTutorial = true;
		UserData.Instance.Save ();
	}

	public void PlayAgain() {
		if (btnClicked) {
			return;
		}

		btnClicked = true;
		FadePanel.Instance.LoadLevel (SceneManager.GetActiveScene ().name);
	}


}
