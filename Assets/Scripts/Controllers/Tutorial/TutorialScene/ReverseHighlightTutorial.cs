﻿using UnityEngine;
using System.Collections;

public class ReverseHighlightTutorial : MonoBehaviour {

	private bool isShowed = false;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter2D(Collider2D obj) {
		if (obj.name.Equals (Settings.SnakeHead)) {
			if (isShowed) {
				return;
			}
			isShowed = true;

			Common.ShareInstance ().isShowingTutorial = true;
			if (GameObject.FindObjectOfType<TutorialScene2Controller> ()) {
				GameObject.FindObjectOfType<TutorialScene2Controller> ().ShowHighlightReverse ();
			}
		}
	}



}
