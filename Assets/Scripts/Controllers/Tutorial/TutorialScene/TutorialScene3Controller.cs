﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using DG.Tweening;
using UnityEngine.SceneManagement;
using Chronos;

public class TutorialScene3Controller : MonoBehaviour {

	// Anim2
	public GameObject tutorial1;
	public GameObject arrow;

	public GameManagerTutorial gameManager;
	// Anim end
	public GameObject tutorial2;

	private bool btnClicked = false;
	// Use this for initialization

	void Start () {
		//		ShowTextAnim ();
		btnClicked = false;

		Common.ShareInstance ().isFreeStyleTutorial = true;
		Common.ShareInstance ().isSurvivalSnake = true;
		UserData.Instance.WasPlayedTutorial = true;
	}

	// Update is called once per frame
	void Update () {
		var foods = GameObject.FindGameObjectsWithTag (Settings.ReverseFoodTag);
		foreach (var food in foods) {
			if (!food.GetComponent<ReverseFoodController> ().isEaten) {
				return;
			}
		}

		if (!Common.ShareInstance ().isEndPointOpening) {
			CompleteTutorialAnim ();
		}
	}

	void CompleteTutorialAnim() {
		tutorial1.Show ();
		arrow.Show ();
		gameManager.OpenEndPointCover ();
	}

	public void ResetTutorial() {
		tutorial1.Hide ();
		arrow.Hide ();
	}

	public void OpenEndTutorial() {
		Common.ShareInstance().isGamePause = true;
		Clock clock = Timekeeper.instance.Clock("Snake");
		clock.localTimeScale = 0;

		tutorial1.Hide ();
		arrow.Hide ();
		tutorial2.Show ();

		tutorial2.FindInChildren ("BG").GetComponent<Image> ().DOFade (.85f, .5f);
	}

	public void NextScene() {
		if (btnClicked) {
			return;
		}

		btnClicked = true;

		FadePanel.Instance.LoadLevel (Settings.MapSelectScene);
		UserData.Instance.CompleteBeginTutorial = true;
		UserData.Instance.Save ();
	}

	public void PlayAgain() {
		if (btnClicked) {
			return;
		}

		btnClicked = true;

		FadePanel.Instance.LoadLevel (SceneManager.GetActiveScene ().name);
	}
}
