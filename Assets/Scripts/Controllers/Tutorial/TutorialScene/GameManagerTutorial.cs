﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;
using Chronos;
using DG.Tweening;

public class GameManagerTutorial : MonoBehaviour {

	public GameObject snakeGhostPref;
	public GameObject femaleSnake;
	float lastTimeDie = 0;
	private GameObject snakeGhost;
	public StartPointController startPoint;
	public FemaleStartPointController femaleStartPoint;
	public EndPointController endPoint;
	public GameObject startTransitionPref;
	public GameObject starFragmentPref;
	public Sprite starSprite;

	public GameObject snakeHead;
	public GameObject snakeHolder;
	public GameObject[] foodList;
	public GameObject[] reverseFoodList;
	public GameObject[] starList;
	public GameObject[] starFragmentList;
	public GameObject[] starEffectList;
	public GameObject[] rockList;
	public GameObject[] smallRockList;
	public GameObject[] helmetList;
	public GameObject[] snakeCornerList;
	public GameObject[] monsterList;
	public GameObject[] monsterCircleList;
	public GameObject[] monsterPathList;
	public GameObject[] monsterFishList;
	public GameObject[] gateCornerList;

	GameObject life1, life2, life3, lifeInfinity;
	public Sprite starOutlineSprite;
	public GameObject[] starUIs;
	public GameObject[] starFragmentUIs;

	// Effect
	public GameObject resetEffectPref;

	public static GameManagerTutorial Instance;

	void Awake() {
		if (Instance == null) {
			Instance = this;
		}


	}

	// Use this for initialization
	void Start () {
		GoogleAds.Instance.HideBanner ();
		// Find Start Point and End Point
		if (GameObject.FindGameObjectWithTag(Settings.StartPointTag) == null) {
			Debug.LogError("Cannot Find StartPoint Object");
		}
		else {
			startPoint = GameObject.FindGameObjectWithTag(Settings.StartPointTag).GetComponent<StartPointController>();
		}

		if (GameObject.FindGameObjectWithTag(Settings.EndPointTag) == null) {
			Debug.LogError("Cannot Find EndPoint Object");
		}
		else {
			endPoint = GameObject.FindGameObjectWithTag(Settings.EndPointTag).GetComponent<EndPointController>();
		}

		// Spawn new snake and set to start position
		GameObject prefab = (GameObject) Instantiate (snakeHolder, new Vector3 (0, 0, 0), Quaternion.identity);
		prefab.FindInChildren(Settings.SnakeHead).GetComponent<ReSkinAnimation> ().spriteSheetName = string.Format ("Skin{0}", UserData.Instance.SelectedSkinIndex);
		startPoint.FindSnakeElement();
		startPoint.ReturnPosition();

		// Find Snake Head after Spawn Snake Holder
		if (!snakeHead) {
			snakeHead = GameObject.Find(Settings.SnakeHead);
		}

		// Open Fog Animation
		var camPos = Camera.main.transform.position;
		var startTransition = (GameObject)Instantiate(startTransitionPref, camPos, Quaternion.identity);

		float height = Camera.main.orthographicSize * 4;
		var cameraSize = new Vector2(Camera.main.aspect * height, height);
		startTransition.GetComponent<InOutSprite>().outerSize = cameraSize;
		startTransition.GetComponent<InOutSprite>().innerPosition = new Vector2(snakeHead.transform.position.x, snakeHead.transform.position.y) - new Vector2(camPos.x, camPos.y);

		Sequence blackScreenSequence = DOTween.Sequence ();
		blackScreenSequence.Append (DOTween.To (() => 0, value => startTransition.GetComponent<InOutSprite> ().InnerSize = value, 2, 0.5f));
		blackScreenSequence.Append (DOTween.To (() => 2, value => startTransition.GetComponent<InOutSprite> ().InnerSize = value, cameraSize.y * 2.0f, 2));
		blackScreenSequence.OnComplete (() => {
			Destroy(startTransition);
		});

		//        var show = LerpFloatAction.Create(0, 2, .5f, (size) => { startTransition.GetComponent<InOutSprite>().InnerSize = size; });
		//		var delay = DelayAction.Create(0.0f);
		//		var disappear = LerpFloatAction.Create(2, cameraSize.y * 2.0f, 2f, (size) => { startTransition.GetComponent<InOutSprite>().InnerSize = size; });
		//
		//		gameObject.Play(SequenceAction.Create(show, /*greet,*/ delay, disappear), () => {
		//            Destroy(startTransition);
		//        });


		// Find Life UI
		if (!life1) {
			if (GameObject.Find(Settings.LifeUI1)) {
				life1 = GameObject.Find(Settings.LifeUI1);
			}

			if (GameObject.Find(Settings.LifeUI2)) {
				life2 = GameObject.Find(Settings.LifeUI2);
			}

			if (GameObject.Find(Settings.LifeUI3)) {
				life3 = GameObject.Find(Settings.LifeUI3);
			}
		}

		if (!lifeInfinity) {
			if (GameObject.Find (Settings.LifeInfinity)) {
				lifeInfinity = GameObject.Find (Settings.LifeInfinity);
			}
		}


		// Find Star UI
		starUIs = GameObject.FindGameObjectsWithTag(Settings.StarUITag);
		starFragmentUIs = GameObject.FindGameObjectsWithTag (Settings.StarFragmentUITag);

		// Find All Component In Current Map
		foodList = GameObject.FindGameObjectsWithTag(Settings.FoodTag);
		reverseFoodList = GameObject.FindGameObjectsWithTag(Settings.ReverseFoodTag);
		starList = GameObject.FindGameObjectsWithTag(Settings.StarTag);
		starFragmentList = GameObject.FindGameObjectsWithTag (Settings.StarFragmentTag);


		// Show TapToPlay
		//		if(!Common.ShareInstance().isFreeStyleTutorial) {
		PopupManager.Instance.ShowTapToPlay ();
		//		}

		SoundManager.Instance.PlayRandomMusic (SoundID.MainGame1, SoundID.MainGame2, SoundID.MainGame3, SoundID.MainGame4);
	}


	// Update is called once per frame
	void Update () {

//		if (Common.ShareInstance ().isSurvivalSnake) {
//			if (!lifeInfinity.transform.GetChild(0).gameObject.activeInHierarchy) {
//				life1.Hide ();
//				life2.Hide ();
//				life3.Hide ();
//
//				lifeInfinity.transform.GetChild (0).Show ();
//			}
//		}
	}

	// Open EndPoint when current Star > 0
	public void OpenEndPointCover() {
		Common.ShareInstance().isEndPointOpening = true;

		GameObject endPointCover = GameObject.FindGameObjectWithTag(Settings.EndPointTag).FindInChildren(Settings.EndPointCover);
		Animator anim = endPointCover.GetComponent<Animator>();
		anim.SetTrigger(Settings.DestroyEndPointCoverTrigger);
	}

	IEnumerator DestroyEndPointCover(GameObject obj, float delay) {
		yield return new WaitForSeconds(delay);
		obj.SetActive(false);
	}

	// Check Is GameOver or not
	private bool CheckGameover() {
		if (Common.ShareInstance().life == 0) {
			return true;
		}
		return false;
	}

	// Reduce Snake's Life
	public void ReduceLife() {
		if (Common.ShareInstance().isCantMove) {
			return;
		}

		ShakeCamera ();

		PopupManager.Instance.ShowTapToReturn ();

//		if (SceneManager.GetActiveScene ().name.Equals ("Tutorial2")) {
//			GameObject.FindObjectOfType<TutorialScene2Controller> ().OpenTooltip ();
//		}

		Common.ShareInstance().isCantMove = true;
		Common.ShareInstance ().teleportCount = 0;

		if (Common.ShareInstance().life != 0) {		
			Common.ShareInstance().life--;
		}

		snakeGhost = ObjectPool.instance.GetObjectForType("DiedSnake", false);
		snakeGhost.transform.position = snakeHead.transform.position;
		snakeHead.GetComponent<Animator>().SetTrigger(Settings.SnakeDeadTrigger);



//		CheckLifeUI();
//
//		if (CheckGameover() && !Common.ShareInstance().isSurvivalSnake) {
//			OnGameOver();
//
//			SoundManager.Instance.PlaySound (SoundID.LoseGame);
//		}
//		else {
//			lastTimeDie = Time.time;
//
//
//		}
		lastTimeDie = Time.time;
		SoundManager.Instance.PlaySound (SoundID.SnakeDamage);
		SoundManager.Instance.PlaySound(SoundID.Punch);

	}

	void ShakeCamera() {
		Camera camera = Camera.main;
		camera.DOShakePosition (.5f, 0.2f);
	}


	// Check Current Life And Apply to UI
	void CheckLifeUI() {
		switch(Common.ShareInstance().life) {
		case 2:
			life1.GetComponent<Animator>().SetTrigger("LostLife");
			break;
		case 1:
			life2.GetComponent<Animator>().SetTrigger("LostLife");
			break;
		case 0:
			life3.GetComponent<Animator>().SetTrigger("LostLife");
			break;
		}
	}

	// Reset All Component In Current Map
	public void ResetGame() {
		if (Time.time - lastTimeDie < 0.5f) {
			return;
		}

		// Show TapToPlay
		PopupManager.Instance.ShowTapToPlay();

		// Reset Start End Point
		ResetStartEndPoint();

		// Delete Snake Ghost
		if (snakeGhost) {
			Destroy(snakeGhost);
			snakeGhost = null;
		}

		// Reset Food
//		foreach(GameObject obj in foodList) {
//			obj.GetComponent<FoodController>().ResetFood();
//		}	


		// Reset Reverse Food
		foreach(GameObject obj in reverseFoodList) {
			obj.GetComponent<ReverseFoodController>().ResetFood();
		}


		// Reset Snake
		snakeHead.GetComponent<MySnake>().ResetSnake();
		snakeHead.GetComponent<Animator>().SetTrigger(Settings.SnakeResetTrigger);


		Common.ShareInstance().isTurnRight = true;
		Common.ShareInstance().star = 0;
		Common.ShareInstance ().starfragments = 0;
		Common.ShareInstance().score = 0;
		Common.ShareInstance().isGameStart = false;
		Common.ShareInstance().isCantMove = false;
		Common.ShareInstance().isSnakeGoOut = false;
		Common.ShareInstance().isHaveHelmet = false;
		Common.ShareInstance().isTailReachedCorner = false;
		Common.ShareInstance().isFemaleTailReachedCorner = false;
		Common.ShareInstance().isEndPointOpening = false;

		// Show Effect
		resetEffectPref.Create(null, startPoint.transform.position);

		PopupManager.Instance.HideTapToReturn ();
	}

	// Reset State of Start Point and EndPoint
	private void ResetStartEndPoint() {
		startPoint.gameObject.FindInChildren(Settings.StartPointCover).SetActive(false);
		startPoint.ReturnPosition();

		if (Common.ShareInstance ().isFreeStyleTutorial) {
			if (SceneManager.GetActiveScene ().name.Equals ("Tutorial3")) {
				GameObject.Find(Settings.EndPointTag).FindInChildren (Settings.EndPointCover).GetComponent<Animator> ().SetTrigger (Settings.ResetEndPointCoverTrigger);
				GameObject.FindObjectOfType<TutorialScene3Controller> ().ResetTutorial ();
			}
		}
	}
		

	// GameOver
	void OnGameOver() {
		Common.ShareInstance().isGameOver = true;
		PopupManager.Instance.OpenPopupGameOver();

		// Analytics
		Analytics.Instance.LogLevel(MapManager.Instance.currentLevelPlayed, LogLevelType.Lose);
	}


}
