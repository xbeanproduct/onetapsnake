﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class MoveAround : MonoBehaviour {

	public RectTransform head, than0, than1, than2;
	public GameObject handTap;

	private Vector2 top = new Vector2(0, 2);
	private Vector2 right = new Vector2(3, 0);
	private Vector2 bot = new Vector2(0, -2);
	private Vector2 left = new Vector2(-3, 0);

	private Vector3 rotate = new Vector3(0, 0, -90);
	// Use this for initialization
	void Start () {

//		Sequence sequence = DOTween.Sequence ()
//			.Append (head.DOBlendableMoveBy (top, 1))
//			.Append (head.DOBlendableMoveBy (right, 1.5f))
//			.Join (head.DOBlendableRotateBy (rotate, 0))
//			.Append (head.DOBlendableMoveBy (bot, 1))
//			.Join (head.DOBlendableRotateBy (rotate, 0))
//			.Append (head.DOBlendableMoveBy (left, 1.5f))
//			.Join(head.DOBlendableRotateBy(rotate, 0));
				
		head.DOBlendableMoveBy (top, 2).SetEase(Ease.Linear).OnComplete (() => {
			Move (head.transform, true);
		});
		DOVirtual.DelayedCall (1.7f, () => {
			handTap.GetComponent<Animator>().SetTrigger("HandTap");
		});

		than0.DOBlendableMoveBy (top, 2).SetEase(Ease.Linear).SetDelay(.4f).OnComplete (() => {
			Move (than0.transform, false);
		});

		than1.DOBlendableMoveBy (top, 2).SetEase(Ease.Linear).SetDelay(.7f).OnComplete (() => {
			Move (than1.transform, false);
		});

		than2.DOBlendableMoveBy (top, 2).SetEase(Ease.Linear).SetDelay(1f).OnComplete (() => {
			Move (than2.transform, false);
		});

	}
		

	void Move(Transform transform, bool isTouch) {
		DOTween.Sequence ()
			.Append (transform.DOBlendableMoveBy (right, 3f).SetEase(Ease.Linear))
			.Join(DOVirtual.DelayedCall(2.7f, () => {
				if (isTouch) {
					handTap.GetComponent<Animator>().SetTrigger("HandTap");
				}
			}))
			.Join (transform.DOBlendableRotateBy (rotate, 0))
			.Append (transform.DOBlendableMoveBy (bot, 2).SetEase(Ease.Linear))
			.Join(DOVirtual.DelayedCall(1.7f, () => {
				if (isTouch) {
					handTap.GetComponent<Animator>().SetTrigger("HandTap");
				}
			}))
			.Join (transform.DOBlendableRotateBy (rotate, 0))
			.Append (transform.DOBlendableMoveBy (left, 3f).SetEase(Ease.Linear))
			.Join(DOVirtual.DelayedCall(2.7f, () => {
				if (isTouch) {
					handTap.GetComponent<Animator>().SetTrigger("HandTap");
				}
			}))
			.Join (transform.DOBlendableRotateBy (rotate, 0))
			.Append(transform.DOBlendableMoveBy(top, 2).SetEase(Ease.Linear))
			.Join(DOVirtual.DelayedCall(1.7f, () => {
				if (isTouch) {
					handTap.GetComponent<Animator>().SetTrigger("HandTap");
				}
			}))
			.Join(transform.DOBlendableRotateBy(rotate, 0))
			.OnComplete (() => {
//				transform.DOBlendableRotateBy(rotate, 0);
				Move(transform, isTouch);
		});
	}

	void MoveTop(Transform transform) {
		transform.DOBlendableMoveBy (top, 1)
			.OnComplete (() => {
						
				MoveRight(transform);
		});
	}

	void MoveRight(Transform transform) {
		transform.DOBlendableRotateBy (rotate, 0);
		transform.DOBlendableMoveBy (right, 1.5f)
			.OnComplete (() => {
//				transform.DOBlendableRotateBy(rotate, 0);
				MoveBot(transform);
		});
	}

	void MoveBot(Transform transform) {
		transform.DOBlendableRotateBy (rotate, 0);
		transform.DOBlendableMoveBy (bot, 1)
			.OnComplete (() => {
				transform.DOBlendableRotateBy(rotate, 0);
				MoveLeft(transform);
		});
	}

	void MoveLeft(Transform transform) {
		transform.DOBlendableMoveBy (left, 1.5f)
			.OnComplete (() => {
				transform.DOBlendableRotateBy(rotate, 0);
				MoveTop(transform);
		});
	}
	// Update is called once per frame
	void Update () {
	
	}
}
