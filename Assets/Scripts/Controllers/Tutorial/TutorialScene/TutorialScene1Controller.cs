﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections;
using DG.Tweening;
using Chronos;

public class TutorialScene1Controller : MonoBehaviour {

	public GameObject tutorial;
	public RectTransform bg;
	public Text text1, text2, text3;

	public GameManagerTutorial gameManager;

	// Anim2
	public GameObject tutorial2;
	public RectTransform bg2;
	public RectTransform tutorialText;
	public RectTransform nextBtn;

	public GameObject arrow;

	// Anim end
	public GameObject tutorial5;


	private bool btnClicked = false;
	// Use this for initialization

	void Start () {

		btnClicked = false;
//		ShowTextAnim ();
		Common.ShareInstance ().isFreeStyleTutorial = true;
		Common.ShareInstance ().isSurvivalSnake = true;
		UserData.Instance.WasPlayedTutorial = true;

		Analytics.Instance.LogScreen ("Play Tutorial");
	}
	
	// Update is called once per frame
	void Update () {
		var foods = GameObject.FindGameObjectsWithTag (Settings.FoodTag);
		foreach (var food in foods) {
			if (!food.GetComponent<FoodController> ().isEaten) {
				return;
			}
		}

		if (!Common.ShareInstance ().isEndPointOpening) {
			CompleteTutorialAnim ();
		}
	}

	public void ShowTextAnim() {
		Sequence sequence = DOTween.Sequence ();
		sequence.Append (bg.GetComponent<Image> ().DOFade (.85f, .5f));
		sequence.Append (text1.DOFade (1, .5f))
			.Append (text1.transform.DOBlendableMoveBy (new Vector2 (0, 150), 1.5f).SetDelay (1))
			.Append (text2.DOFade (1, .5f))
			.Append (text2.transform.DOBlendableMoveBy (new Vector2 (0, 100), 1.5f).SetDelay (1))
			.Append (text3.DOFade (1, .5f))
			.Append (text3.transform.DOBlendableMoveBy (new Vector2 (0, 50), 1.5f).SetDelay (1));


		sequence.AppendInterval (1)
			.Append (text1.transform.DOBlendableMoveBy (new Vector2 (500, 0), 1))
			.Join (text2.transform.DOBlendableMoveBy (new Vector2 (500, 0), 1).SetDelay(.2f))
			.Join (text3.transform.DOBlendableMoveBy (new Vector2 (500, 0), 1).SetDelay(.4f))
			.Append (bg.GetComponent<Image> ().DOFade (0, 0.5f))
			.OnComplete(() => {
				tutorial.Hide();
				PopupManager.Instance.ShowTapToPlay();

			});
	}

	public void ShowTextAnim2() {
		tutorial2.Show ();
		arrow.Show ();
	}
		

	void CompleteTutorialAnim() {
		ShowTextAnim2 ();
		gameManager.OpenEndPointCover ();
	}

	public void OpenEndTutorial() {
		Common.ShareInstance().isGamePause = true;
		Clock clock = Timekeeper.instance.Clock("Snake");
		clock.localTimeScale = 0;

		tutorial2.Hide ();
		arrow.Hide ();
		tutorial5.Show ();

		tutorial5.FindInChildren ("BG").GetComponent<Image> ().DOFade (.85f, .5f);
	}

	public void NextScene() {
		if (btnClicked) {
			return;
		}

		btnClicked = true;
		FadePanel.Instance.LoadLevel ("Tutorial2");
	}

	public void PlayAgain() {
		if (btnClicked) {
			return;
		}

		btnClicked = true;
		FadePanel.Instance.LoadLevel (SceneManager.GetActiveScene ().name);
	}
}
