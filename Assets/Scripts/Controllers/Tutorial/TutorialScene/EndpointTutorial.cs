﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class EndpointTutorial : MonoBehaviour {

	public enum EndPointDirection {
		Left, Right, Top, Down
	}

	public EndPointDirection direction;
	public GameObject _snake;

	private bool isSnakeRightDirection = false;

	// Use this for initialization
	void Start () {
		if (!_snake) {
			_snake = GameObject.Find(Settings.SnakeHead); 
		}

	}

	// Update is called once per frame
	void Update () {
		if (!_snake) {
			if (GameObject.Find(Settings.SnakeHead)) {
				_snake = GameObject.Find(Settings.SnakeHead);
			} 
		}
		else {
			CheckDirection(_snake);
		}

		if (Common.ShareInstance().star == 0) {
			GetComponent<BoxCollider2D>().offset = new Vector2(0.1f, 0);
			GetComponent<BoxCollider2D>().size = new Vector2(0.5f, .6f);
		}
		else {
			GetComponent<BoxCollider2D>().offset = new Vector2(0, 0);
			GetComponent<BoxCollider2D>().size = new Vector2(0.2f, .6f);
		}
	}


	void OnDrawGizmos() {
		Vector2 toPos = transform.position;
		switch(direction) {
		case EndPointDirection.Left:
			toPos+= new Vector2(-1, 0);
			break;
		case EndPointDirection.Right:
			toPos+= new Vector2(1, 0);
			break;
		case EndPointDirection.Top:
			toPos+= new Vector2(0, 1);
			break;
		case EndPointDirection.Down:
			toPos+= new Vector2(0, -1);
			break;
		default:break;	
		}

		Gizmos.color = Color.yellow;

		Gizmos.DrawLine(transform.position, toPos);
	}

	public void CheckDirection(GameObject obj) {
		float startPointDirection = 0;
		switch(direction) {
		case EndPointDirection.Left: 
			startPointDirection = Settings.Direction.Left;
			break;
		case EndPointDirection.Right:
			startPointDirection = Settings.Direction.Right;
			break;
		case EndPointDirection.Top:
			startPointDirection = Settings.Direction.Up;
			break;
		case EndPointDirection.Down:
			startPointDirection = Settings.Direction.Down;
			break;
		default: break;
		}



		if (Mathf.Approximately(Quaternion.Angle(Quaternion.Euler(0, 0, startPointDirection), obj.transform.rotation), 180)) {
			isSnakeRightDirection = true;
		}
		else {
			isSnakeRightDirection = false;
		}
	}

	void OnTriggerEnter2D(Collider2D obj) {
		if (obj.name.Equals(Settings.SnakeHead)) {
			Debug.Log (GameObject.FindGameObjectsWithTag (Settings.FoodTag).Length);
			var foods = GameObject.FindGameObjectsWithTag (Settings.FoodTag);
			foreach (var food in foods) {
				if (!food.GetComponent<FoodController> ().isEaten) {
					GameManagerTutorial.Instance.ReduceLife();		
					return;
				}
			}

			var reverseFoods = GameObject.FindGameObjectsWithTag (Settings.ReverseFoodTag);
			foreach (var reverseFood in reverseFoods) {
				if (!reverseFood.GetComponent<ReverseFoodController> ().isEaten) {
					GameManagerTutorial.Instance.ReduceLife();	

					return;
				}
			}

			if (SceneManager.GetActiveScene ().name.Equals ("Tutorial1")) {
				GameObject.FindObjectOfType<TutorialScene1Controller> ().OpenEndTutorial ();
			}

			if (SceneManager.GetActiveScene ().name.Equals ("Tutorial2")) {
				GameObject.FindObjectOfType<TutorialScene2Controller> ().OpenEndTutorial ();
			}

			if (SceneManager.GetActiveScene ().name.Equals ("Tutorial3")) {
				GameObject.FindObjectOfType<TutorialScene3Controller> ().OpenEndTutorial ();
			}

		}
	}
}
