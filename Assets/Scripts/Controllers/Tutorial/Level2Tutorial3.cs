﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

public class Level2Tutorial3 : MonoBehaviour {

	public RectTransform tutorial3;
	public bool isEnable;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (isEnable) {
			#if UNITY_EDITOR
			if (Input.GetMouseButtonDown (0)) {
				if (EventSystem.current != null && EventSystem.current.IsPointerOverGameObject()) {
					return;
				}

				Common.ShareInstance ().isGamePause = false;
				tutorial3.Hide ();
				isEnable = false;
			}
			#else
			if (Input.touchCount > 0) {
				if (Input.GetTouch(0).phase == TouchPhase.Began) {
					if (EventSystem.current != null && EventSystem.current.IsPointerOverGameObject(Input.GetTouch(0).fingerId)) {
						return;
					}


					Common.ShareInstance().isGamePause = false;
					tutorial3.Hide();
					isEnable = false;
				}
			}
			#endif

		}
	}

	void OnTriggerEnter2D(Collider2D obj) {
		if (UserData.Instance.WasPlayedTutorial) {
			return;
		}

		if (obj.name.Equals (Settings.SnakeHead)) {
			Common.ShareInstance ().isGamePause = true;
			tutorial3.Show ();
			//Anim
			Common.ShareInstance ().isGameTutorial = false;
			isEnable = true;
		}
	}
}
