﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using DG.Tweening;

public class Level3Tutorial : MonoBehaviour {

	public RectTransform tutorial;
	public RectTransform bg;
	public RectTransform tutorialText;
	public RectTransform nextBtn;

	public GameObject componentTutorial;

	private bool isShowed = false;
	// Use this for initialization
	void Start () {
		if (!UserData.Instance.CompleteTutorialPhase1_2) {
			UserData.Instance.WasPlayedTutorial = true;
			Common.ShareInstance ().isGameTutorial = true;
			Common.ShareInstance ().isCurrentMapTutorial = true;

			UserData.Instance.Save ();
		}
	}
	
	// Update is called once per frame
	void Update () {

	}

	void OnTriggerEnter2D(Collider2D obj) {
		if (!Common.ShareInstance ().isCurrentMapTutorial) {
			return;
		}

		if (isShowed) {
			return;
		}

		if (obj.name.Equals (Settings.SnakeHead)) {
			Common.ShareInstance ().isGamePause = true;
			tutorial.Show ();
			Common.ShareInstance ().isGameTutorial = false;
			//Anim
			bg.GetComponent<Image>().DOFade(.6f, .5f);
			tutorialText.GetComponent<Text>().DOFade(1, .5f);
			nextBtn.GetComponent<Image> ().DOFade (1, .5f).SetDelay(0.5f);

			componentTutorial.Show ();

			isShowed = true;
		}
	}

	public void Next() {
		Sequence sequence = DOTween.Sequence ();
		sequence.Append (tutorialText.DOBlendableMoveBy (new Vector3 (-7, 0, 0), .5f).SetEase (Ease.InBack))
			.Join (nextBtn.DOBlendableMoveBy (new Vector3 (-7, 0, 0), .5f).SetEase (Ease.InBack).SetDelay (0.2f))
			.Append (bg.GetComponent<Image> ().DOFade (0, .5f))
			.OnComplete (() => {
				Common.ShareInstance ().isGamePause = false;
				tutorial.Hide ();
				componentTutorial.Hide();

				tutorialText.GetComponent<Text>().DOFade(0, 0);
				tutorialText.DOBlendableMoveBy (new Vector3 (7, 0, 0), 0);
				nextBtn.DOBlendableMoveBy (new Vector3 (7, 0, 0), 0);
				nextBtn.GetComponent<Image> ().DOFade (0, 0);

				UserData.Instance.CompleteTutorialPhase1_2 = true;
				UserData.Instance.WasPlayedTutorial = true;

				UserData.Instance.Save();
			});
	}
}
