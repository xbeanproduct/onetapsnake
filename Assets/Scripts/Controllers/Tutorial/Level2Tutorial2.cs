﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using DG.Tweening;

public class Level2Tutorial2 : MonoBehaviour {

	public RectTransform tutorial;
	public RectTransform bg;
	public RectTransform tutorialText;
	public RectTransform nextBtn;

	public GameObject reverseTutorial;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

	}

	void OnTriggerEnter2D(Collider2D obj) {
		if (UserData.Instance.WasPlayedTutorial) {
			return;
		}

		if (obj.name.Equals (Settings.SnakeHead)) {
			Common.ShareInstance ().isGamePause = true;
			tutorial.Show ();
			//Anim
			bg.GetComponent<Image>().DOFade(.6f, .5f);
			tutorialText.GetComponent<Text>().DOFade(1, .5f);
			nextBtn.GetComponent<Image> ().DOFade (1, .5f).SetDelay(0.5f);

			reverseTutorial.Show ();
		}
	}

	public void Next() {
		Sequence sequence = DOTween.Sequence ();
		sequence.Append (tutorialText.DOBlendableMoveBy (new Vector3 (-7, 0, 0), .5f).SetEase (Ease.InBack))
			.Join (nextBtn.DOBlendableMoveBy (new Vector3 (-7, 0, 0), .5f).SetEase (Ease.InBack).SetDelay (0.2f))
			.Append (bg.GetComponent<Image> ().DOFade (0, .5f))
			.OnComplete (() => {
				tutorial.Hide ();
				Common.ShareInstance ().isGamePause = false;

				reverseTutorial.Hide ();
			});


	}
}
