﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using DG.Tweening;

public class Level1Tutorial : MonoBehaviour {

	public RectTransform tutorial;
	public RectTransform bg;
	public RectTransform nextBtn;
	public RectTransform tutorialText;

	public GameObject starTut;

	// Use this for initialization
	void Start () {
		if (!UserData.Instance.CompleteTutorialPhase1) {
			UserData.Instance.WasPlayedTutorial = false;
			Common.ShareInstance ().isGameTutorial = true;
			Common.ShareInstance ().isCurrentMapTutorial = true;

			UserData.Instance.Save ();
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter2D(Collider2D obj) {
		if (UserData.Instance.WasPlayedTutorial) {
			return;
		}

		if (obj.name.Equals (Settings.SnakeHead)) {
			Common.ShareInstance ().isGamePause = true;
			tutorial.Show ();
			starTut.Show ();
			// Anim
			bg.GetComponent<Image> ().DOFade (.85f, .5f);
			tutorialText.GetComponent<Text> ().DOFade (1, .5f);
			nextBtn.GetComponent<Image> ().DOFade (1, .5f).SetDelay (0.5f);

			if (GameObject.FindGameObjectWithTag ("MenuButton")) {
				GameObject.FindGameObjectWithTag ("MenuButton").GetComponent<Image> ().enabled = false;
			}
		}
	}

	public void Next() {
		Sequence sequence = DOTween.Sequence ();
		sequence.Append (tutorialText.DOBlendableMoveBy (new Vector3 (7, 0, 0), .5f).SetEase (Ease.InBack))
			.Join (nextBtn.DOBlendableMoveBy (new Vector3 (7, 0, 0), .5f).SetEase (Ease.InBack).SetDelay (0.2f))
			.Append (bg.GetComponent<Image> ().DOFade (0, .5f))
			.OnComplete (() => {
				Common.ShareInstance ().isGamePause = false;
				tutorial.Hide ();
				starTut.Hide ();

				if (GameObject.FindGameObjectWithTag ("MenuButton")) {
					GameObject.FindGameObjectWithTag ("MenuButton").GetComponent<Image>().enabled = true;
				}

				UserData.Instance.CompleteTutorialPhase1 = true;
				UserData.Instance.WasPlayedTutorial = true;
				UserData.Instance.Save();

		});
				
	}
}
