﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using UnityEngine.SceneManagement;
using DG.Tweening;

public class Level3Tutorial3 : MonoBehaviour {

	public RectTransform tutorial;
	public RectTransform bg;
	public RectTransform tutorialText;
	public RectTransform completeBtn, playAgainBtn;

	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {

	}

	void OnTriggerEnter2D(Collider2D obj) {
		if (!Common.ShareInstance ().isCurrentMapTutorial) {
			return;
		}

		if (obj.name.Equals (Settings.SnakeHead)) {
			Common.ShareInstance ().isGamePause = true;
			tutorial.Show ();
			Common.ShareInstance ().isGameTutorial = false;
			//Anim
			bg.GetComponent<Image>().DOFade(.6f, .5f);
			tutorialText.GetComponent<Text>().DOFade(1, .5f);
			completeBtn.GetComponent<Image> ().DOFade (1, .5f).SetDelay(0.5f);
			playAgainBtn.GetComponent<Image> ().DOFade (1, .5f).SetDelay(0.7f);

		}
	}

	public void Complete() {
		Next (() => {
			UserData.Instance.CompleteTutorialPhase1 = true;
			UserData.Instance.WasPlayedTutorial = true;
			UserData.Instance.WasShowDied = true;
			UserData.Instance.Save();
		});
	}

	public void PlayAgain() {
		Next (() => {
			SceneManager.LoadScene(SceneManager.GetActiveScene().name);
		});
	}

	void Next(System.Action callback) {
		Sequence sequence = DOTween.Sequence ();
		sequence.Append (tutorialText.DOBlendableMoveBy (new Vector3 (-7, 0, 0), .5f).SetEase (Ease.InBack))
			.Join (completeBtn.DOBlendableMoveBy (new Vector3 (-7, 0, 0), .5f).SetEase (Ease.InBack).SetDelay (0.2f))
			.Join (playAgainBtn.DOBlendableMoveBy(new Vector3(-7, 0, 0), .5f).SetEase(Ease.InBack).SetDelay(0.4f))
			.Append (bg.GetComponent<Image> ().DOFade (0, .5f))
			.OnComplete (() => {
				Common.ShareInstance().isGamePause = false;
				tutorial.Hide();

				tutorialText.GetComponent<Text>().DOFade(0, 0);
				tutorialText.DOBlendableMoveBy (new Vector3 (7, 0, 0), 0);
				completeBtn.DOBlendableMoveBy (new Vector3 (7, 0, 0), 0);
				completeBtn.GetComponent<Image> ().DOFade (0, 0);
				playAgainBtn.DOBlendableMoveBy (new Vector3 (7, 0, 0), 0);
				playAgainBtn.GetComponent<Image> ().DOFade (0, 0);
				callback();
			});
	}
}
