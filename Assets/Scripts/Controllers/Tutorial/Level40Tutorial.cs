﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using DG.Tweening;

public class Level40Tutorial : MonoBehaviour {

	public RectTransform tutorial;
	public RectTransform bg;
	public RectTransform nextBtn;
	public RectTransform tutorialText;

	public GameObject componentTutorial;
	// Use this for initialization
	void Start () {
		if (!UserData.Instance.CompleteTutorialPhase5) {
			UserData.Instance.WasPlayedTutorial = true;
			Common.ShareInstance ().isGameTutorial = true;
			Common.ShareInstance ().isCurrentMapTutorial = true;

			UserData.Instance.Save ();

		}
	}

	// Update is called once per frame
	void Update () {

	}

	void OnTriggerEnter2D(Collider2D obj) {
		if (!Common.ShareInstance().isCurrentMapTutorial) {
			return;
		}

		if (obj.name.Equals (Settings.SnakeHead)) {
			Common.ShareInstance ().isGamePause = true;
			tutorial.Show ();
			componentTutorial.Show ();
			// Anim
			bg.GetComponent<Image> ().DOFade (.85f, .5f);
			tutorialText.GetComponent<Text> ().DOFade (1, .5f);
			nextBtn.GetComponent<Image> ().DOFade (1, .5f).SetDelay (0.5f);

			if (GameObject.FindGameObjectWithTag ("MenuButton")) {
				GameObject.FindGameObjectWithTag ("MenuButton").GetComponent<Image>().enabled = false;
			}
		}
	}

	public void Next() {
		Sequence sequence = DOTween.Sequence ();
		sequence.Append (tutorialText.DOBlendableMoveBy (new Vector3 (-7, 0, 0), .5f).SetEase (Ease.InBack))
			.Join (nextBtn.DOBlendableMoveBy (new Vector3 (-7, 0, 0), .5f).SetEase (Ease.InBack).SetDelay (0.2f))
			.Append (bg.GetComponent<Image> ().DOFade (0, .5f))
			.OnComplete (() => {
				Common.ShareInstance ().isGamePause = false;
				tutorial.Hide ();
				componentTutorial.Hide ();

				if (GameObject.FindGameObjectWithTag ("MenuButton")) {
					GameObject.FindGameObjectWithTag ("MenuButton").GetComponent<Image>().enabled = true;
				}

				Common.ShareInstance().isCurrentMapTutorial = false;
				UserData.Instance.CompleteTutorialPhase5 = true;
				UserData.Instance.Save();
			});

	}
}
