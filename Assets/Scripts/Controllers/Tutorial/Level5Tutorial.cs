﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.EventSystems;
using Chronos;
using DG.Tweening;

public class Level5Tutorial : MonoBehaviour {

	public RectTransform tutorial;
	public RectTransform bg;
	public RectTransform tutorialText;
	public RectTransform nextBtn;

	public GameObject monsterTutorial;

	private bool isShowed;
	// Use this for initialization
	void Start () {
		if (!UserData.Instance.CompleteTutorialPhase2) {
			UserData.Instance.WasPlayedTutorial = true;
			Common.ShareInstance ().isGameTutorial = true;
			Common.ShareInstance ().isCurrentMapTutorial = true;

			UserData.Instance.Save ();
		}
	}

	void OnTriggerEnter2D(Collider2D obj) {
		if (!Common.ShareInstance ().isCurrentMapTutorial) {
			return;
		}

		if (isShowed) {
			return;
		}

		if (obj.name.Equals (Settings.SnakeHead)) {

			if (GameObject.FindGameObjectWithTag (Settings.MonsterTag) == null) {
				return;
			}

			Clock clock = Timekeeper.instance.Clock("Root");
			clock.localTimeScale = 0;

			Vector2 currentMonsterPos = GameObject.FindGameObjectWithTag (Settings.MonsterTag).transform.position;
			Quaternion currentMonsterRot = GameObject.FindGameObjectWithTag (Settings.MonsterTag).transform.rotation;
			monsterTutorial.transform.position = currentMonsterPos;
			monsterTutorial.transform.rotation = currentMonsterRot;
			monsterTutorial.Show ();

			Common.ShareInstance ().isGamePause = true;
			tutorial.Show ();
			Common.ShareInstance ().isGameTutorial = false;
			//Anim
			bg.GetComponent<Image>().DOFade(.85f, .5f);
			tutorialText.GetComponent<Text>().DOFade(1, .5f);
			nextBtn.GetComponent<Image> ().DOFade (1, .5f).SetDelay(0.5f);

			isShowed = true;

			if (GameObject.FindGameObjectWithTag ("MenuButton")) {
				GameObject.FindGameObjectWithTag ("MenuButton").GetComponent<Image>().enabled = false;
			}
		}
	}

	public void Next() {
		Sequence sequence = DOTween.Sequence ();
		sequence.Append (tutorialText.DOBlendableMoveBy (new Vector3 (-7, 0, 0), .5f).SetEase (Ease.InBack))
			.Join (nextBtn.DOBlendableMoveBy (new Vector3 (-7, 0, 0), .5f).SetEase (Ease.InBack).SetDelay (0.2f))
			.Append (bg.GetComponent<Image> ().DOFade (0, .5f))
			.OnComplete (() => {
				Common.ShareInstance ().isGamePause = false;
				tutorial.Hide ();
				monsterTutorial.Hide();

				if (GameObject.FindGameObjectWithTag ("MenuButton")) {
					GameObject.FindGameObjectWithTag ("MenuButton").GetComponent<Image>().enabled = true;
				}

				Clock clock = Timekeeper.instance.Clock("Root");
				clock.localTimeScale = 1;
			});
	}

	// Update is called once per frame
	void Update () {
		
	}
}
