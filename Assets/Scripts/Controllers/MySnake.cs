﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Chronos;

public class MySnake : MonoBehaviour {

	SnakeSegment segment;
	
    public GameObject snakeHelmet;
	public List<GameObject> childList;
	public int pendingAddChildCount;

	public GameObject snakeDirection;
	
	public GameObject lastChild;

	public GameObject sprintEffect;

	[SerializeField] GameObject snakeHolder;
//	[SerializeField] GameObject snakeCornerPref;
//	[SerializeField] GameObject snakeCornerFlipPref;
	[SerializeField] GameObject snakeSegmentPref;
    
    
    private GameObject midSegment, lastSegment;
    
    private bool isNearFood = false;
	
    public GameObject smokePref;
    public GameObject waterPref;
    public GameObject hitPref;
    
	public SkinEffect skinEffect;

    private ParticleSystem helmetStarPS, helmetRingPS;
	// Use this for initialization
	void Start () {
		
		segment = GetComponent<SnakeSegment>();
		childList = new List<GameObject>();
		
        midSegment = snakeHolder.FindInChildren(Settings.SnakeMidSegment);
        lastSegment = snakeHolder.FindInChildren(Settings.SnakeLastSegment);
		childList.Add(midSegment);
        childList.Add(lastSegment);
		
		if (childList.Count != 0) {
			lastChild = childList[childList.Count - 1];
		}
       
       
        helmetStarPS = snakeHelmet.GetComponent<ParticleSystem>();
        helmetRingPS = snakeHelmet.transform.GetChild(0).GetComponent<ParticleSystem>();

		midSegment.transform.GetChild (0).GetComponent<ReSkinAnimation> ().spriteSheetName = string.Format ("Skin{0}", UserData.Instance.SelectedSkinIndex);
		lastSegment.transform.GetChild (0).GetComponent<ReSkinAnimation> ().spriteSheetName = string.Format ("Skin{0}", UserData.Instance.SelectedSkinIndex);

		snakeSegmentPref.transform.GetChild (0).GetComponent<ReSkinAnimation> ().spriteSheetName = string.Format ("Skin{0}", UserData.Instance.SelectedSkinIndex);
		transform.FindInChildren ("Toungle").GetComponent<ReSkinAnimation> ().spriteSheetName = string.Format ("Skin{0}", UserData.Instance.SelectedSkinIndex);

		RecalculateSegment ();

//		InitSprintEffect ();
	}

	public void InitSprintEffect() {
		int index = 0;
		if (UserData.Instance.SelectedSkinIndex == 4 || UserData.Instance.SelectedSkinIndex == 6 || UserData.Instance.SelectedSkinIndex == 8) {
			index = 1;
		}
		if (UserData.Instance.SelectedSkinIndex == 5 || UserData.Instance.SelectedSkinIndex == 7) {
			index = 2;
		}

		sprintEffect = (GameObject)Instantiate (skinEffect.skinEffectList [index], transform.position, Quaternion.identity);
		sprintEffect.transform.SetParent (transform, true);
		sprintEffect.transform.localRotation = Quaternion.Euler (90, 0, 0);

//		sprintEffect.transform.rotation = Quaternion.Euler (90, 0, 0);

	}
	
	// Update is called once per frame
	void Update () {
        // Tail
//        lastChild.GetComponent<SpriteRenderer>().sprite = tailSprite;
//		Clock clock = Timekeeper.instance.Clock("Monster");
//		if (Common.ShareInstance ().isSnakeGoOut) {
//			
//			clock.localTimeScale = 1;
//		} else {
//			clock.localTimeScale = 0;
//		}
        
        if (Common.ShareInstance().isHaveHelmet) {
           
            if (!helmetStarPS.emission.enabled) {
                var em = helmetStarPS.emission;
                var em1 = helmetRingPS.emission;
                em.enabled = true;
                em1.enabled = true;
            }
          
            
        }
        else {
            if (helmetStarPS.emission.enabled) {
                var em = helmetStarPS.emission;
                var em1 = helmetRingPS.emission;
                em.enabled = false;
                em1.enabled = false;
            }
        }
        
        
        bool isHaveFood = false;
        
        Collider2D[] cols = Physics2D.OverlapCircleAll(transform.position, 0.35f);
        foreach(Collider2D col in cols) {
            if ((col.tag.Equals(Settings.FoodTag) && !col.GetComponent<FoodController>().isEaten)  || 
                (col.tag.Equals(Settings.ReverseFoodTag) && !col.GetComponent<ReverseFoodController>().isEaten) &&
                !Common.ShareInstance().isCantMove) {
                if(!isNearFood) {
                    isNearFood = true;
                    GetComponent<Animator>().SetBool("SnakeSeeFood", isNearFood);
                    isHaveFood = true;
                    
                }
                else {
                    isHaveFood = true;
                }
                break;
            }
        }
        
        if (isNearFood && !isHaveFood) {
            isNearFood = false;
            GetComponent<Animator>().SetBool("SnakeSeeFood", isNearFood);
        }
        
	}

	void RecalculateSegment() {
		int count = childList.Count;
		float maxScale = 1;
		float minScale = 0.7f;
		float scaleAmount = (maxScale - minScale) / count;
		float currentScale = maxScale - scaleAmount;
		Vector2 headCol = GetComponent<BoxCollider2D> ().size;

		foreach (GameObject obj in childList) {
			obj.transform.GetChild(0).SetScale (currentScale);
//			Vector2 objCol = GetComponent<BoxCollider2D> ().size;
//			obj.transform.GetChild(0).GetComponent<BoxCollider2D> ().size = new Vector2 (headCol.x / currentScale, headCol.y / currentScale);
			if (currentScale > minScale) {
				currentScale -= scaleAmount;
			}

//			obj.GetComponent<SnakeSegment> ().snakeComponentSpace = Settings.SnakeComponentSpace - (1 - currentScale) / 3;
//			obj.GetComponent<SnakeSegment> ().FixPosition ();
		}
	}
	
	void FixedUpdate() {
        
	}
	
	public void TurnRight()
	{
		if (childList.Count > 0) {
//			AddCorner(snakeCornerPref);
		}
		segment.OnTurnRight();

		SoundManager.Instance.PlaySound (SoundID.SnakeTurn);
	}
	
	public void TurnLeft()
	{
		if (childList.Count > 0) {
//			AddCorner(snakeCornerFlipPref);
		}
		segment.OnTurnLeft();

		SoundManager.Instance.PlaySound (SoundID.SnakeTurn);
	}
	
	void AddCorner(GameObject pref) {
//		Instantiate(pref, turnPos, transform.rotation);
	}
	
	private Vector3 GetParentDirection(Quaternion parentRotation) {
		if (Mathf.Round(parentRotation.eulerAngles.z) == Settings.Direction.Up) {
			return new Vector2(0, -Settings.SnakeComponentSpace);
		}
		else if (Mathf.Round(parentRotation.eulerAngles.z) == Settings.Direction.Down) {
			return new Vector2(0, Settings.SnakeComponentSpace);
		}
		else if (Mathf.Round(parentRotation.eulerAngles.z) == Settings.Direction.Left) {
			return new Vector2(Settings.SnakeComponentSpace, 0);
		}
		else if (Mathf.Round(parentRotation.eulerAngles.z) == Settings.Direction.Right) {
			return new Vector2(-Settings.SnakeComponentSpace, 0);
		}

		return Vector2.zero;
	}

	public void AddChild() {
        
		GameObject child;
		if (childList.Count == 0) {
//			child = Instantiate(snakeSegmentPref, transform.position + GetParentDirection(transform.rotation), Quaternion.identity) as GameObject;
			child = ObjectPool.instance.GetObjectForType (Settings.SnakeSegmentPrefPool, false);
			child.transform.GetChild (0).GetComponent<ReSkinAnimation> ().spriteSheetName = string.Format ("Skin{0}", UserData.Instance.SelectedSkinIndex);
			child.transform.position = transform.position + GetParentDirection(transform.rotation);
			//child = ChildPool.instance.GetChildAtPos(new Vector2(transform.position.x, transform.position.y) + GetParentDirection(transform.rotation));
			child.transform.rotation = transform.rotation;
		}
		else {
//			child = Instantiate(snakeSegmentPref, lastChild.transform.position + GetParentDirection(lastChild.transform.rotation), Quaternion.identity) as GameObject;
			//child = ChildPool.instance.GetChildAtPos(new Vector2(lastChild.transform.position.x, lastChild.transform.position.y) + GetParentDirection(lastChild.transform.rotation));
			child = ObjectPool.instance.GetObjectForType(Settings.SnakeSegmentPrefPool, false);
			child.transform.GetChild (0).GetComponent<ReSkinAnimation> ().spriteSheetName = string.Format ("Skin{0}", UserData.Instance.SelectedSkinIndex);
			child.transform.position = lastChild.transform.position + GetParentDirection(lastChild.transform.rotation);
			child.transform.rotation = lastChild.transform.rotation;	
		}
		
		
		SnakeSegment childComponent = child.GetComponent<SnakeSegment>();
		childComponent.isTail = true;
		childComponent.isCanTeleport = true;
		if (childList.Count == 0) {
			segment.prevSegment = childComponent;
			childComponent.nextSegment = segment;
		}
		else {
			lastChild.GetComponent<SnakeSegment>().prevSegment = childComponent;
			childComponent.nextSegment = lastChild.GetComponent<SnakeSegment>();
			lastChild.GetComponent<SnakeSegment>().isTail = false;
//			lastChild.transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = segmentSprite;
				
		}

//	     childComponent.FixPosition();
        
		childList.Add(child);
		
		lastChild = child;
		
		child.transform.parent = snakeHolder.transform;

		RecalculateSegment ();

//		if (pendingAddChildCount > 0) {
//			pendingAddChildCount --;
//			StartCoroutine (AddChild ());
//		}
	}
	
	public void ResetSnake() {
		foreach(GameObject child in childList) {
            if (!child.name.Equals(Settings.SnakeMidSegment) && !child.name.Equals(Settings.SnakeLastSegment)){
//                Destroy(child);
				child.GetComponent<SnakeSegment>().ResetSegment();
				ObjectPool.instance.PoolObject(child);
            }
		}


//		midSegment.GetComponent<SnakeSegment> ().ResetSegment ();
//		lastSegment.GetComponent<SnakeSegment> ().ResetSegment ();
//		midSegment.GetComponent<SnakeSegment> ().nextSegment = gameObject.GetComponent<SnakeSegment> ();
//		midSegment.GetComponent<SnakeSegment> ().prevSegment = lastSegment.GetComponent<SnakeSegment> ();
//		lastSegment.GetComponent<SnakeSegment> ().nextSegment = midSegment.GetComponent<SnakeSegment> ();
		lastSegment.GetComponent<SnakeSegment>().prevSegment = null;

		childList.Clear();
        childList.Add(midSegment);
        childList.Add(lastSegment);
//		lastSegment.transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = tailSprite;
        lastSegment.GetComponent<SnakeSegment>().isTail = true;
        
		pendingAddChildCount = 0;

		lastChild = lastSegment;

		HideDirection ();
		GetComponent<InputController>().isWaitToTurn = false;

		RecalculateSegment ();
	}

	public void DestroyHelmet() {
		Common.ShareInstance().isHaveHelmet = false;
	}
    
	private Tween delayCall;

	public void ShowDirection() {
		snakeDirection.Show ();

		DOTween.Kill ("DelayDirection");

		Debug.Log (Common.ShareInstance ().isTurnRight);
		if (Common.ShareInstance ().isTurnRight) {
			snakeDirection.GetComponent<Animator> ().SetTrigger ("TurnRight");
		} else {
			snakeDirection.GetComponent<Animator> ().SetTrigger ("TurnLeft");
		}
			

		DOVirtual.DelayedCall (2, () => {
			snakeDirection.Hide();
		}).SetId("DelayDirection");
	}

	public void HideDirection() {
		snakeDirection.Hide ();
	}

    public void PlaySnakeEffect() {
        GetComponent<Animator>().SetTrigger(Settings.SnakeEatTrigger);
    }
    
    public void AddSmoke() {
//        Instantiate(smokePref, transform.position, Quaternion.identity);
		ObjectPool.instance.GetObjectForType(Settings.SnakeHitWallPool, false, 3).transform.position = transform.position;
    }
    
    public void AddWater() {
//        Instantiate(waterPref, transform.position, Quaternion.identity);
		ObjectPool.instance.GetObjectForType(Settings.SnakeHitWaterPool, false, 3).transform.position = transform.position;
    }
    
    public void AddHit() {
//        Instantiate(hitPref, transform.position, Quaternion.identity);
		ObjectPool.instance.GetObjectForType(Settings.SnakeHitMonsterPool, false, 3).transform.position = transform.position;
    }

	public void ShowSprintEffect() {
//		if (!sprintEffect.activeInHierarchy) {
//			sprintEffect.Show ();
//		}
		if (sprintEffect.GetComponent<ParticleSystem> ().emission.enabled) {
			return;
		}

		var pss = sprintEffect.GetComponentsInChildren<ParticleSystem>();
		foreach (ParticleSystem ps in pss) {
			var emission = ps.emission;
			emission.enabled = true;
		}
	}

	public void HideSprintEffect() {
//		if (sprintEffect.activeInHierarchy) {
//			sprintEffect.Hide ();
//		}
		if (!sprintEffect.GetComponent<ParticleSystem> ().emission.enabled) {
			return;
		}

		var pss = sprintEffect.GetComponentsInChildren<ParticleSystem>();
		foreach (ParticleSystem ps in pss) {
			var emission = ps.emission;
			emission.enabled = false;
		}
	}

	void OnTriggerEnter2D(Collider2D obj) {
		if (obj.tag.Equals(Settings.SnakeTail)) {
			if (Common.ShareInstance ().isFreeStyleTutorial) {
				GameManagerTutorial.Instance.ReduceLife ();
			} else {
				GameManager.Instance.ReduceLife ();
			}
		}
	}
}
