﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class GameManagerSurvival : MonoBehaviour {

	
	public GameObject snakeHead, midSegment, lastSegment;
	public GameObject[] snakeHolderList;
	
	public GameObject[] snakeCornerList;


	
	public static GameManagerSurvival Instance;
	
	void Awake() {
		if (Instance == null) {
			Instance = this;
		}
	}
	// Use this for initialization
	void Start () {

		// Spawn new snake and set to start position
		GameObject prefab = snakeHolderList[UserData.Instance.SelectedSkinIndex];
		GameObject snakeHolder = Instantiate(prefab, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;


		if (!snakeHead) {
			snakeHead = GameObject.Find(Settings.SnakeHead);
            midSegment = GameObject.Find(Settings.SnakeMidSegment);
            lastSegment = GameObject.Find(Settings.SnakeLastSegment);
        }
        
        snakeHead.transform.rotation = Quaternion.AngleAxis(Settings.Direction.Up, Vector3.forward);
		snakeHead.transform.localPosition = new Vector3(0, 0, snakeHead.transform.localPosition.z) + snakeHead.transform.up * 0.3f;
        midSegment.transform.rotation = snakeHead.transform.rotation;
        midSegment.transform.localPosition = snakeHead.transform.position + GetParentDirection(snakeHead.transform.rotation);
        lastSegment.transform.rotation = snakeHead.transform.rotation;
        lastSegment.transform.localPosition = midSegment.transform.position + GetParentDirection(snakeHead.transform.rotation);
        
        // Show TapToPlay
        PopupManager.Instance.ShowTapToPlay();
	}

    private Vector3 GetParentDirection(Quaternion parentRotation) {
		if (Mathf.Round(parentRotation.eulerAngles.z) == Settings.Direction.Up) {
			return new Vector2(0, -Settings.SnakeComponentSpace);
		}
		else if (Mathf.Round(parentRotation.eulerAngles.z) == Settings.Direction.Down) {
			return new Vector2(0, Settings.SnakeComponentSpace);
		}
		else if (Mathf.Round(parentRotation.eulerAngles.z) == Settings.Direction.Left) {
			return new Vector2(Settings.SnakeComponentSpace, 0);
		}
		else if (Mathf.Round(parentRotation.eulerAngles.z) == Settings.Direction.Right) {
			return new Vector2(-Settings.SnakeComponentSpace, 0);
		}
		
		return Vector2.zero;
	}
	
	// Update is called once per frame
	void Update () {
   
	}
    

    void StopCornerAnim() {
        snakeCornerList = GameObject.FindGameObjectsWithTag(Settings.SnakeCorner);
        foreach(GameObject obj in snakeCornerList) {
            obj.GetComponent<Animator>().Stop();
        }
    }

	public void ResetGame() {
        // Show TapToPlay
        PopupManager.Instance.ShowTapToPlay();
       
		
		// SnakeCorner
		foreach(GameObject obj in snakeCornerList) {
			Destroy(obj.gameObject);
		}
		
		// Reset Snake
		snakeHead.GetComponent<MySnake>().ResetSnake();
        // snake.GetComponent<MySnake>().AddChild();
        // snake.GetComponent<MySnake>().AddChild();
		
		Common.ShareInstance().isTurnRight = true;
		Common.ShareInstance().star = 0;
        Common.ShareInstance().score = 0;
		Common.ShareInstance().isGameStart = false;
		Common.ShareInstance().isCantMove = false;
		Common.ShareInstance().isSnakeGoOut = false;
		Common.ShareInstance().isHaveHelmet = false;
        Common.ShareInstance().isTailReachedCorner = false;
        Common.ShareInstance().isEndPointOpening = false;
        
        
	}

	void OnGameOver() {
		Common.ShareInstance().isGameOver = true;
		PopupManager.Instance.OpenPopupGameOver();
	}
	
}
