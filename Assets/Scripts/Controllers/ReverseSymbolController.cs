﻿using UnityEngine;
using System.Collections;

public class ReverseSymbolController : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	   transform.Rotate(0, 0, 5);
       
       if (GetComponent<SpriteRenderer>().enabled) {
           if (Common.ShareInstance().isTurnRight) {
                GetComponent<SpriteRenderer>().enabled = false;   
           }
       }
       else {
           if (!Common.ShareInstance().isTurnRight) {
               GetComponent<SpriteRenderer>().enabled = true;
           }
       }
       
	}
}
