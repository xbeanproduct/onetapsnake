﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Chronos;
using GoogleMobileAds.Api;
using DG.Tweening;

public class GUIController : MonoBehaviour {

    public GameObject BG;   
    public GameObject scoreText;
	public GameObject scoinText;
	public GameObject exitButton;
	public GameObject mapButton;

	public GameObject starHighlight, starfragmentHighlight, reverseHighlight, shieldHighlight;

	public GameObject bgHighlight;

	public Vector3 exitButtonPosition;
	public Vector3 mapButtonPosotion;
	public Vector3 rootButtonPosition;

	public bool isMenuShowed;

	public Sprite menuDefault, menuClicked;
    
	private float actionTime = 0.1f;

	private FixedUpdater _updateScore = new FixedUpdater(0.2f);
	// Use this for initialization
	void Start () {
        if (!scoreText) {
            scoreText = GameObject.Find("ScoreText");
        }

		if (!scoinText) {
			scoinText = GameObject.Find ("ScoinText");
		}

		if (Common.ShareInstance ().isFreeStyleTutorial) {
			return;
		}

		if (exitButton) {
			exitButtonPosition = exitButton.transform.position;
			exitButton.GetComponent<Button> ().interactable = false;
			exitButton.transform.position = rootButtonPosition;
			exitButton.SetAlpha(0f);
		}

		if (mapButton) {
			mapButtonPosotion = mapButton.transform.position;
			mapButton.GetComponent<Button> ().interactable = false;
			mapButton.transform.position = rootButtonPosition;
			mapButton.SetAlpha(0f);

		}

		if (GameObject.Find ("MenuButton")) {
			rootButtonPosition = GameObject.Find("MenuButton").transform.position;
		}

//		exitButtonPosition = Camera.main.ScreenToWorldPoint(exitButton.transform.position);
//		mapButtonPosotion = Camera.main.ScreenToWorldPoint(mapButton.transform.position);
//		rootButtonPosition = Camera.main.ScreenToWorldPoint(GameObject.Find("MenuButton").transform.position);








		isMenuShowed = false;

		_updateScore.UpdateCallback = UpdateScoreWhenSprint;


	}
		

	// Update is called once per frame
	void Update () {
        scoreText.GetComponent<Text>().text = Common.ShareInstance().score.ToString();
		if (scoinText) {
			scoinText.GetComponent<Text> ().text = UserData.Instance.Scoin.ToString ();
		}
			
		#if !UNITY_EDITOR
		if (Input.touchCount == 0) {
			if(Common.ShareInstance ().isHoldSprint) {
				Slower ();
			}
		}
		#endif
	}
	
	//Acton

	public void AddLife() {
		if (Common.ShareInstance().life < 3) {
			Common.ShareInstance().life++;
		}
	}
	
	public void OpenSetting() {
		PopupManager.Instance.OpenPopupSetting ();
	}
    
    //Debug
    public void FinishThisLevel() {
        GameManager.Instance.OnMapCompleted();
    }



	public void OnButtonMenu()	{
		if (Common.ShareInstance ().isGateTurning) {
			return;
		}


		if (isMenuShowed) {

//			if (!UserData.Instance.CompleteTutorialPhase2) {
//				if (GameObject.Find ("ArrowToolkitTutorial") != null) {
//					if (GameObject.Find ("ArrowToolkitTutorial").activeInHierarchy) {
//						return;
//					}
//				}
//			}

			ShowBGHighlight (false);

            ResumeGame();
            
			UnHighlightComponent ();

			ToolkitManager.Instance.HideToolkits();


//			exitButton.transform.DOMove (rootButtonPosition, actionTime).SetEase (Ease.OutSine);
//			exitButton.GetComponent<Image> ().DOFade (0, actionTime);
//
//
//			mapButton.transform.DOMove (rootButtonPosition, actionTime).SetEase (Ease.OutSine);
//			mapButton.GetComponent<Image> ().DOFade (0, actionTime);
//
////			exitButton.GetComponent<Button> ().interactable = false;
//			mapButton.GetComponent<Button> ().interactable = false;

			GameObject.Find ("MenuButton").GetComponent<Image> ().sprite = menuDefault;

			isMenuShowed = false;

			SoundManager.Instance.PlaySound (SoundID.ButtonClose);


		}
		else {
//			if (!Common.ShareInstance ().isCurrentMapTutorial) {
////				BG.SetActive(true);	
//				ShowBGHighlight(true);
//			}

			ShowBGHighlight(true);
            
            PauseGame();

			HighlightComponent ();

			ToolkitManager.Instance.ShowToolkits();

//			exitButton.transform.DOMove (exitButtonPosition, actionTime).SetEase (Ease.OutSine);
//			exitButton.GetComponent<Image> ().DOFade (1, actionTime)
//				.OnComplete(() => {
//					exitButton.GetComponent<Button>().interactable = true;
//				});
//
//			mapButton.transform.DOMove (mapButtonPosotion, actionTime).SetEase (Ease.OutSine);
//			mapButton.GetComponent<Image> ().DOFade (1, actionTime)
//				.OnComplete(() => {
//					mapButton.GetComponent<Button>().interactable = true;
//				});

			GameObject.Find ("MenuButton").GetComponent<Image> ().sprite = menuClicked;

			isMenuShowed = true;

			SoundManager.Instance.PlaySound (SoundID.ButtonOpen);

			CheckTutorial ();
		}


	}

	void ShowBGHighlight(bool enable) {
		if (enable) {
			if (GameObject.FindGameObjectWithTag ("BGCanvas")) {
				GameObject.FindGameObjectWithTag ("BGCanvas").transform.GetChild (0).GetComponent<Image> ().DOKill (true);
			}
			if (GameObject.FindGameObjectWithTag ("BGCanvas") == null) {
				
				GameObject bg = ObjectPool.instance.GetObjectForType (Settings.BGCanvasPool, false);
				bg.GetComponent<Canvas> ().worldCamera = Camera.main;
				bg.transform.GetChild (0).GetComponent<Image> ().SetAlpha (0);
				bg.transform.GetChild (0).GetComponent<Image> ().DOFade (.7f, .5f);
			}
		} else {
			
			GameObject bg = GameObject.FindGameObjectWithTag ("BGCanvas");

			bg.transform.GetChild (0).GetComponent<Image> ().DOFade (0, .5f).OnComplete (() => {
				ObjectPool.instance.PoolObject(bg);
			});
		}
	}

	void HighlightComponent() {
		var stars = GameObject.FindGameObjectsWithTag (Settings.StarTag);
		Camera camera = Camera.main;
		foreach (GameObject star in stars) {
//			starHighlight.Create (null, star.transform.position);
			ObjectPool.instance.GetObjectForType(Settings.StarHighlightPool, false).transform.position = star.transform.position;
		}

		var reverses = GameObject.FindGameObjectsWithTag (Settings.ReverseFoodTag);
		foreach (GameObject reverse in reverses) {
			if (!reverse.GetComponent<ReverseFoodController> ().isEaten) {
//				reverseHighlight.Create (null, reverse.transform.position);
				ObjectPool.instance.GetObjectForType(Settings.ReverseHighlightPool, false).transform.position = reverse.transform.position;
			}
		}

		var starFragments = GameObject.FindGameObjectsWithTag (Settings.StarFragmentTag);
		foreach (GameObject starFragment in starFragments) {
//			starfragmentHighlight.Create (null, starFragment.transform.position);
			ObjectPool.instance.GetObjectForType(Settings.StarFragmentHighlightPool, false).transform.position = starFragment.transform.position;
		}

		var shields = GameObject.FindGameObjectsWithTag (Settings.HelmetTag);
		foreach (GameObject shield in shields) {
//			shieldHighlight.Create (null, shield.transform.position);
			ObjectPool.instance.GetObjectForType(Settings.ShieldHighlightPool, false).transform.position = shield.transform.position;
		}
	}

	void UnHighlightComponent() {
		var starHighlights = GameObject.FindGameObjectsWithTag ("StarHighlight");
		foreach (GameObject star in starHighlights) {
//			Destroy (star);
			ObjectPool.instance.PoolObject(star);
		}

		var reverseHighlights = GameObject.FindGameObjectsWithTag ("ReverseHighlight");
		foreach (GameObject reverse in reverseHighlights) {
//			Destroy (reverse);
			ObjectPool.instance.PoolObject(reverse);
		}

		var starFragmentHighlights = GameObject.FindGameObjectsWithTag ("StarFragmentHighlight");
		foreach (GameObject starFragment in starFragmentHighlights) {
//			Destroy (starFragment);
			ObjectPool.instance.PoolObject(starFragment);
		}

		var shieldHighlights = GameObject.FindGameObjectsWithTag ("ShieldHighlight");
		foreach (GameObject shield in shieldHighlights) {
//			Destroy (shield);
			ObjectPool.instance.PoolObject(shield);
		}
	}

	public void RecheckHighlight() {
		UnHighlightComponent ();
		HighlightComponent ();
	}

	void CheckTutorial() {
		if (UserData.Instance.CompleteToolkitTutorial) {
			return;
		}

		var tutorialToolkit = GameObject.Find ("ToolkitTutorial");
		if (tutorialToolkit == null) {
			return;
		}

		if (GameObject.Find("ArrowToolkitTutorial") == null) {
			return;
		}

		if (!GameObject.Find ("ArrowToolkitTutorial").activeInHierarchy) {
			return;
		}

		GameObject.Find ("ArrowToolkitTutorial").Hide ();


		DOTween.To (() => 0, x => x = 0, 0, .2f).OnComplete(() => {
			tutorialToolkit.transform.Find ("ArrowToolkitTutorial2").Show();
		});
			
	}
    
    void PauseGame() {

        Clock clock = Timekeeper.instance.Clock("Root");
        clock.localTimeScale = 0;
        Common.ShareInstance().isGamePause = true;

    }
    
    void ResumeGame() {

        Clock clock = Timekeeper.instance.Clock("Root");
        clock.localTimeScale = 1;
        Common.ShareInstance().isGamePause = false;

    }
    


	public void Sprint() {
		if (!Common.ShareInstance ().isGameStart) {
			return;
		}

		if (Common.ShareInstance ().isCantMove) {
			return;
		}

		if (Common.ShareInstance ().isGateTurning) {
			return;
		}

		Clock clock = Timekeeper.instance.Clock("Snake");
		SoundManager.Instance.PlaySound(SoundID.RunFast);

		clock.LerpTimeScale(2f, 1.5f, true);

		Common.ShareInstance ().isHoldSprint = true;

		if (GameObject.FindGameObjectWithTag (Settings.SnakeHead)) {
			GameObject.FindGameObjectWithTag (Settings.SnakeHead).GetComponent<MySnake> ().ShowSprintEffect ();
		}
	}

	void UpdateScoreWhenSprint() {
		if (!Common.ShareInstance ().isGameStart) {
			return;
		}

		if (Common.ShareInstance ().isCantMove) {
			return;
		}

		if (Common.ShareInstance ().isGateTurning) {
			return;
		}

		Clock clock = Timekeeper.instance.Clock("Snake");

		Common.ShareInstance ().score += Mathf.RoundToInt (clock.timeScale * 10);
	}

	void FixedUpdate() {
		if (Common.ShareInstance ().isHoldSprint && !Common.ShareInstance().isCantMove && !Common.ShareInstance().isGamePause) {
			Common.ShareInstance ().score += Mathf.RoundToInt (Time.timeScale * 10);
		}
	}

	public void Slower() {

		Clock clock = Timekeeper.instance.Clock("Snake");

		clock.LerpTimeScale(1, 0.5f, true);

		Common.ShareInstance ().isHoldSprint = false;
		_updateScore.Stop ();

		if (GameObject.FindGameObjectWithTag (Settings.SnakeHead)) {
			GameObject.FindGameObjectWithTag (Settings.SnakeHead).GetComponent<MySnake> ().HideSprintEffect ();
		}
	}

	public void OpenPopupXcoin() {
		PopupManager.Instance.OpenPopupSuggestScoin (0, true, InsufficientType.None);
	}
}
