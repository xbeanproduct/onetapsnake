﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class SnakeSegment : TimeControlBehavior {

	public SnakeSegment prevSegment;
	public SnakeSegment nextSegment;
	public Vector3 nextPos;
	private float width, height;
    
    public bool isHead = true;	
	public bool isTail;
    
    public bool isCanTeleport = true;
    public GameObject nextTeleportDestination;

	public float snakeComponentSpace;
    
//	private Sequence sequence;

	enum SegmentDirection{
		Left, Right, Top, Down
	}

	// Use this for initialization
	void Start () {
//		sequence = DOTween.Sequence ();

		var sprite = GetComponent<SpriteRenderer>();
		if (sprite == null) {
			return;
		}

		width = Camera.main.WorldToScreenPoint(new Vector2(sprite.bounds.max.x, 0)).x - Camera.main.WorldToScreenPoint(new Vector2(sprite.bounds.min.x, 0)).x;
		height = Camera.main.WorldToScreenPoint(new Vector2(0, sprite.bounds.max.y)).y - Camera.main.WorldToScreenPoint(new Vector2(0, sprite.bounds.min.y)).y;
		
		if (width > height) {
			var temp = width;
			width = height;
			height = temp;
		}
        
        
	}

	public void ResetSegment() {
		StopAllCoroutines ();
		prevSegment = null;
		nextSegment = null;
		isTail = false;
	}

	public void PunchSegment() {

		if (!transform.name.Equals(Settings.SnakeHead)) {
			transform.GetChild (0).DOKill (true);
			transform.GetChild(0).DOPunchScale(new Vector3(.5f, .5f, .5f), .5f, 0, 0);
		}
			
		DOVirtual.DelayedCall (.3f, () => {
			if (prevSegment) {
				prevSegment.PunchSegment();
			}
		});
	}
    
    public void EnableTeleport() {
        StartCoroutine(DelayEnableTeleport());
    }
    
    IEnumerator DelayEnableTeleport() {
        yield return time.WaitForSeconds(.5f);
        isCanTeleport = true;
        nextTeleportDestination = null;
    }
    
    void TestTurnRight() {
        transform.Rotate(Vector3.forward, -90);	
    }
	
	// Update is called once per frame
    void Update() {
//        if (isTail) {
//            GetComponent<BoxCollider2D>().size = Settings.LargeSegmentCollider;
//        }
//        else {
//            GetComponent<BoxCollider2D>().size = Settings.SmallSegmentCollider;
//        }
        // if (nextSegment && isCanTeleport && !Common.ShareInstance().isCantMove && !Common.ShareInstance().isGamePause) {
        //     if (nextSegment.GetCurrentDirection() == GetCurrentDirection()) {
        //         FixPosition();
        //     }
        // }


		
        
    }
    
    
	void FixedUpdate () {
		// CheckSnakeLocation();
		
		if (IsSnakeCanMove()) {
			MoveToward();
		}
		else {
			CancelInvoke();
		}
        
        // if (prevSegment && prevSegment.isCanTeleport && !Common.ShareInstance().isCantMove && !Common.ShareInstance().isGamePause)
		// {
		// 	if (prevSegment.GetCurrentDirection() == GetCurrentDirection())
		// 	{
		// 		prevSegment.FixPosition();
		// 	}
		// }
		
	}
	
	private bool IsSnakeCanMove() {
		bool isCanMove = true;
		
		if (Common.ShareInstance().isGameOver) {
			isCanMove = false;
		}
		
		if (!Common.ShareInstance().isGameStart) {
			isCanMove = false;
		}
		
		if (Common.ShareInstance().isCantMove) {
			isCanMove = false;
		}
		
		if (Common.ShareInstance().isInTussock) {
			isCanMove = false;
		}
        
        if (Common.ShareInstance().isGamePause) {
            isCanMove = false;
        }
		
		if (!isCanMove) {
			GetComponent<Rigidbody2D>().velocity = Vector2.zero;
		}
		
		return isCanMove;
	}

	public void OnTurnRight()
	{
		transform.Rotate(Vector3.forward, Settings.RotateRightValue);

		if (prevSegment)
		{
            // if (!isCanTeleport){
            //     if (nextSegment.GetCurrentDirection() == GetCurrentDirection())
            //     {
            //         FixPosition();
            //     } 
            // }
            
			prevSegment.nextPos = transform.position;
			Vector2 deltaPos = transform.position - prevSegment.transform.position;
			float time2 = deltaPos.magnitude / (Common.ShareInstance().snakeSpeed) / time.fixedDeltaTime;
			// Invoke("TurnRightPreviousSegment", Mathf.Round(time * 10)/ 10);
            StartCoroutine(TurnRightPreviousSegment(time2));
		}

	}

	public void OnTurnLeft()
	{
		transform.Rotate(Vector3.forward, Settings.RotateLeftvalue);	

		if (prevSegment)
		{
            // if (!isCanTeleport){
            //     if (nextSegment.GetCurrentDirection() == GetCurrentDirection())
            //     {
            //         FixPosition();
            //     } 
            // }
            
			prevSegment.nextPos = transform.position;
			Vector2 deltaPos = transform.position - prevSegment.transform.position;
			float time2 = deltaPos.magnitude / Common.ShareInstance().snakeSpeed / time.fixedDeltaTime;
			// Invoke("TurnLeftPreviousSegment", Mathf.Round(time * 10)/ 10);
            StartCoroutine(TurnLeftPreviousSegment(time2));
		}
	}
	
	private Vector2 RoundVector(Vector2 vector) {
		return new Vector2(Mathf.Round(vector.x * 100f)/ 100f, Mathf.Round(vector.y * 100f) / 100f);
	}
	public IEnumerator TurnRightPreviousSegment(float delay)
	{
        
        yield return time.WaitForSeconds(delay);
        
		if (prevSegment && !Common.ShareInstance().isCantMove)
		{
			prevSegment.transform.position = prevSegment.nextPos;
			prevSegment.OnTurnRight();
			if (isCanTeleport) {
				prevSegment.FixPosition ();
			}
		}
	}

	public IEnumerator TurnLeftPreviousSegment(float delay)
	{
        yield return time.WaitForSeconds(delay);
        
		if (prevSegment && !Common.ShareInstance().isCantMove){
			prevSegment.transform.position = prevSegment.nextPos;
			prevSegment.OnTurnLeft();
			if (isCanTeleport) {
				prevSegment.FixPosition ();
			}
		}
		
	}

	private void MoveToward() {
		//  
        // GetComponent<Rigidbody2D>().MovePosition(nexPos);
		time.rigidbody2D.velocity = transform.up * Common.ShareInstance().snakeSpeed * time.fixedDeltaTime;
		// transform.Translate(Vector2.up * Common.ShareInstance().snakeSpeed);
	}


	void OnTriggerEnter2D(Collider2D obj) {
        if (isTail) {
            if (obj.tag.Equals(Settings.SnakeCorner)) {
                Common.ShareInstance().isTailReachedCorner = true;
                StartCoroutine(WaitToDestroyCorner(obj.gameObject));
            }
            
            if (obj.tag.Equals(Settings.FemaleSnakeCorner)) {
                Common.ShareInstance().isFemaleTailReachedCorner = true;
                StartCoroutine(WaitToDestroyCorner(obj.gameObject));
            }
        }
    }
    
    IEnumerator WaitToDestroyCorner(GameObject obj) {
        Animator anim = obj.GetComponent<Animator>();
        anim.SetTrigger(Settings.BeingDestroyTrigger);
        
        yield return time.WaitForSeconds(anim.GetCurrentAnimatorStateInfo(0).length * 2);
        
        if (!Common.ShareInstance().isCantMove) {
            Destroy(obj);
            Common.ShareInstance().isTailReachedCorner = false;
            Common.ShareInstance().isFemaleTailReachedCorner = false;
        }
    }
    
	
	private void CheckSnakeLocation() {
		
		if (transform.position.x < Mathf.Round(Camera.main.ScreenToWorldPoint(new Vector2(0 - height/2, 0)).x)) {
			transform.position = new Vector2(Mathf.Round(Camera.main.ScreenToWorldPoint(new Vector2(Screen.width + height/2, 0)).x), transform.position.y);
			
		}
		else if (transform.position.x > Mathf.Round(Camera.main.ScreenToWorldPoint(new Vector2(Screen.width + height/2, 0)).x)) {
			transform.position = new Vector2(Mathf.Round(Camera.main.ScreenToWorldPoint(new Vector2(0 - height/2,0)).x), transform.position.y);
			
		}
		
		if (transform.position.y < Mathf.Round(Camera.main.ScreenToWorldPoint(new Vector2(0, 0 - height/2)).y)) {
			transform.position = new Vector2(transform.position.x, Mathf.Round(Camera.main.ScreenToWorldPoint(new Vector2(0, Screen.height + height/2)).y));
		}
		else if (transform.position.y > Mathf.Round(Camera.main.ScreenToWorldPoint(new Vector2(0, Screen.height + height/2)).y)) {
			transform.position = new Vector2(transform.position.x, Mathf.Round(Camera.main.ScreenToWorldPoint(new Vector2(0, 0 - height/2)).y));
		}
	}

	public void FixPosition()
	{
		if (nextSegment)
		{
			transform.position = nextSegment.transform.position + GetParentDirection(transform.rotation);
		}
	}

	SegmentDirection GetCurrentDirection()
	{
		if (Mathf.Approximately(transform.rotation.eulerAngles.z, Settings.Direction.Up))
		{
			return SegmentDirection.Top;
		}
		else if (Mathf.Approximately(transform.rotation.eulerAngles.z, Settings.Direction.Down))
		{
			return SegmentDirection.Down;
		}
		else if (Mathf.Approximately(transform.rotation.eulerAngles.z, Settings.Direction.Left))
		{
			return SegmentDirection.Left;
		}
		else if (Mathf.Approximately(transform.rotation.eulerAngles.z, Settings.Direction.Right))
		{
			return SegmentDirection.Right;
		}

		return SegmentDirection.Top;
	}

	private Vector3 GetParentDirection(Quaternion parentRotation) {
		if (Mathf.Round(parentRotation.eulerAngles.z) == Settings.Direction.Up) {
			return new Vector2(0, -Settings.SnakeComponentSpace);
		}
		else if (Mathf.Round(parentRotation.eulerAngles.z) == Settings.Direction.Down) {
			return new Vector2(0, Settings.SnakeComponentSpace);
		}
		else if (Mathf.Round(parentRotation.eulerAngles.z) == Settings.Direction.Left) {
			return new Vector2(Settings.SnakeComponentSpace, 0);
		}
		else if (Mathf.Round(parentRotation.eulerAngles.z) == Settings.Direction.Right) {
			return new Vector2(-Settings.SnakeComponentSpace, 0);
		}
//		if (Mathf.Round(parentRotation.eulerAngles.z) == Settings.Direction.Up) {
//			return new Vector2(0, -snakeComponentSpace);
//		}
//		else if (Mathf.Round(parentRotation.eulerAngles.z) == Settings.Direction.Down) {
//			return new Vector2(0, snakeComponentSpace);
//		}
//		else if (Mathf.Round(parentRotation.eulerAngles.z) == Settings.Direction.Left) {
//			return new Vector2(snakeComponentSpace, 0);
//		}
//		else if (Mathf.Round(parentRotation.eulerAngles.z) == Settings.Direction.Right) {
//			return new Vector2(-snakeComponentSpace, 0);
//		}

		return Vector2.zero;
	}
}
