﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using Chronos;

public class GateCornerController : MonoBehaviour {
    
    public enum GateRotate {
        _90 = 90,
        _180 = 180,
    }
    public enum RotateDirection {
        Left = 1,
        Right = -1
    }
    
    public GateRotate rotateAmount;
    public RotateDirection rotateDirection;
    
    private Quaternion originalRotation;
    private Quaternion destinationRotation;
    private Quaternion nextRotation;


    public bool isCantRotate = false;
    
	// Use this for initialization
	void Start () {
        originalRotation = transform.rotation;
        destinationRotation = Quaternion.Euler(transform.eulerAngles.x, transform.eulerAngles.y, transform.eulerAngles.z + (int)rotateAmount*(int)rotateDirection);
        
        nextRotation = destinationRotation;

	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void ResetGate() {

		DOTween.Kill (transform);

		transform.rotation = originalRotation;

		nextRotation = destinationRotation;
		isCantRotate = false;
	}
    
    public void RotateGate() {
        if (isCantRotate) {
            return;
        }
        
        isCantRotate = true;


       
		if (Camera.main.GetComponent<CameraController> () != null && Camera.main.GetComponent<CameraController> ().numMap > 1) {

			float current = Camera.main.transform.position.x;
			float des = Mathf.Clamp (transform.position.x, Camera.main.GetComponent<CameraController> ().leftBound, Camera.main.GetComponent<CameraController> ().rightBound);

			Common.ShareInstance ().isGamePause = true;
			Common.ShareInstance ().isGateTurning = true;
			Clock clock = Timekeeper.instance.Clock ("Root");
			clock.localTimeScale = 0;


			DOTween.Sequence ()
				.Append (Camera.main.transform.DOMoveX (des, 1).SetEase (Ease.OutSine).OnComplete(() => {
					//Effect
					ObjectPool.instance.GetObjectForType(Settings.SmokeGateEffectPool, false, 3).transform.position = transform.position;
					SoundManager.Instance.PlaySound(SoundID.GateOpen);
					Debug.Log("Show Gate Effect");
				}))
				.Append (transform.DORotate (nextRotation.eulerAngles, .5f, RotateMode.Fast).SetEase (Ease.OutSine)
					.OnComplete (() => {
						isCantRotate = false;

						if (nextRotation == destinationRotation) {
							nextRotation = originalRotation;
						} else {
							nextRotation = destinationRotation;
						}

					}))
				.Append (Camera.main.transform.DOMoveX(current, 1).SetEase(Ease.OutSine))
				.OnComplete(() => {
					clock.localTimeScale = 1;
					Common.ShareInstance ().isGamePause = false;
					Common.ShareInstance().isGateTurning = false;

				});


		} else {

			Common.ShareInstance ().isGamePause = true;
			Common.ShareInstance ().isGateTurning = true;
			Clock clock = Timekeeper.instance.Clock ("Root");
			clock.localTimeScale = 0;


			//Effect
			ObjectPool.instance.GetObjectForType(Settings.SmokeGateEffectPool, false, 3).transform.position = transform.position;
			SoundManager.Instance.PlaySound(SoundID.GateOpen);


			transform.DORotate (nextRotation.eulerAngles, .5f, RotateMode.Fast).SetEase (Ease.OutSine)
			.OnComplete (() => {
				isCantRotate = false;

				if (nextRotation == destinationRotation) {
					nextRotation = originalRotation;
				} else {
					nextRotation = destinationRotation;
				}

				clock.localTimeScale = 1;
				Common.ShareInstance ().isGamePause = false;
				Common.ShareInstance().isGateTurning = false;
			});
		}


        
    }

}
