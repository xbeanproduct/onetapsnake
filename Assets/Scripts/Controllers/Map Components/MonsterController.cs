﻿using UnityEngine;
using System.Collections;

public class MonsterController : TimeControlBehavior {

    public enum PatrolDirection {
        Left, Right, Top, Down
    }
    
    public PatrolDirection direction;
    public float patrolRange = 1;
    
    Vector3 currentPos;
    Vector3 patrolPos;
    
    public float timeToMove = 1;
    
    bool _isLerping = false;
    bool _isTurning = false;
    
    Vector3 _startPosition;
    Vector3 _endPosition;
    
    Quaternion _nextRotation;
    IEnumerator turnCoroutine, moveCoroutine;
    

    Vector3 originalPosition;
    Quaternion originalRotation;
    
	// Use this for initialization
	void Start () {
       originalPosition = transform.position;
       originalRotation = transform.rotation;
       
       moveCoroutine = MoveToPosition();   
       turnCoroutine = TurnToBack();  
        
	   currentPos = transform.position;
       patrolPos = transform.position;
       
       switch(direction) {
			case PatrolDirection.Left:
				patrolPos+= new Vector3(-patrolRange, 0);
				break;
			case PatrolDirection.Right:
				patrolPos+= new Vector3(patrolRange, 0);
				break;
			case PatrolDirection.Top:
				patrolPos+= new Vector3(0, patrolRange);
				break;
			case PatrolDirection.Down:
				patrolPos+= new Vector3(0, -patrolRange);
				break;
			default:break;
		}
        
 
        //We set the start position to the current position, and the finish to 10 spaces in the 'forward' direction
        _startPosition = currentPos;
        _endPosition = patrolPos; 
        
        StartCoroutine(moveCoroutine);
	}
	
    void OnDrawGizmos() {
        Vector2 toPos = transform.position;
		switch(direction) {
			case PatrolDirection.Left:
				toPos+= new Vector2(-patrolRange, 0);
				break;
			case PatrolDirection.Right:
				toPos+= new Vector2(patrolRange, 0);
				break;
			case PatrolDirection.Top:
				toPos+= new Vector2(0, patrolRange);
				break;
			case PatrolDirection.Down:
				toPos+= new Vector2(0, -patrolRange);
				break;
			default:break;
		}
		
		Gizmos.color = Color.yellow;
		
		Gizmos.DrawLine(transform.position, toPos);
	}
    
    
    
	// Update is called once per frame
	void Update () {
	   
	}
    
    void FixedUpdate() {
        //  if(_isLerping && !Common.ShareInstance().isCantMove)
        // {
        //     //We want percentage = 0.0 when Time.time = _timeStartedLerping
        //     //and percentage = 1.0 when Time.time = _timeStartedLerping + timeTakenDuringLerp
        //     //In other words, we want to know what percentage of "timeTakenDuringLerp" the value
        //     //"Time.time - _timeStartedLerping" is.
        //     float timeSinceStarted = Time.time - _timeStartedLerping;
        //     float percentageComplete = timeSinceStarted / timeToMove;
 
        //     //Perform the actual lerping.  Notice that the first two parameters will always be the same
        //     //throughout a single lerp-processs (ie. they won't change until we hit the space-bar again
        //     //to start another lerp)
        //     transform.position = Vector3.Lerp (_startPosition, _endPosition, percentageComplete  );
        //     // transform.position = Vector3.SmoothDamp(_startPosition, _endPosition, ref _velocity, percentageComplete);
 
        //     //When we've completed the lerp, we set _isLerping to false
        //     if(percentageComplete >= 1.0f)
        //     {
        //         if (_startPosition == currentPos) {
        //             _startPosition = patrolPos;
        //         }
        //         else if (_startPosition == patrolPos) {
        //             _startPosition = currentPos;
        //         }                
                
        //         if (_endPosition == currentPos) {
        //             _endPosition = patrolPos;
        //         }
        //         else if (_endPosition == patrolPos) {
        //             _endPosition = currentPos;
        //         }           
                
        //         percentageComplete = 0;    
        //         _timeStartedLerping = Time.time;
        //     }
        // }
		if (!_isLerping && !Common.ShareInstance().isCantMove && Common.ShareInstance().isSnakeGoOut) {
            if (_startPosition == currentPos) {
                _startPosition = patrolPos;
            }
            else if (_startPosition == patrolPos) {
                _startPosition = currentPos;
            }                
            
            if (_endPosition == currentPos) {
                _endPosition = patrolPos;
            }
            else if (_endPosition == patrolPos) {
                _endPosition = currentPos;
            }           
                
            moveCoroutine = MoveToPosition();                       
            StartCoroutine(moveCoroutine);

			SoundManager.Instance.PlaySound (SoundID.Bee);
        }

        // if (Common.ShareInstance().isCantMove) {
        //     StopCoroutine(moveCoroutine);
        //     StopCoroutine(turnCoroutine);
        // }
        
    }
    
    public void ResetMonster() {
        StopCoroutine(moveCoroutine);
        StopCoroutine(turnCoroutine);
        
        transform.position = originalPosition;
        transform.rotation = originalRotation;
        
        _startPosition = patrolPos;
        _endPosition = currentPos;
        _nextRotation = originalRotation;
        
        _isLerping = false;
        
    }
    
    IEnumerator TurnToBack() {

		while (transform.rotation != _nextRotation && !Common.ShareInstance().isCantMove) {
            transform.rotation = Quaternion.Slerp(transform.rotation, _nextRotation, time.fixedDeltaTime * 5);
            yield return null;
        }
        
    }
    
    IEnumerator MoveToPosition() {
		
        _isLerping = true;
        float ratio = 0;
        float multiplier = 1 / timeToMove;
        
		while (transform.position != _endPosition && !Common.ShareInstance().isCantMove) {

			if (Common.ShareInstance ().isSnakeGoOut) {
				ratio += time.fixedDeltaTime * multiplier;
            
				transform.position = Vector3.Lerp (_startPosition, _endPosition, Mathf.SmoothStep (0.0f, 1.0f, ratio));
			}
            yield return null;
        }
        
		Debug.Log ("TURN TURN");
        _isLerping = false;
		if (!Common.ShareInstance().isCantMove && Common.ShareInstance().isSnakeGoOut) {
            StopCoroutine(turnCoroutine);
            _nextRotation = Quaternion.Euler(transform.eulerAngles.x, transform.eulerAngles.y, transform.eulerAngles.z + 180);
            turnCoroutine = TurnToBack();
            StartCoroutine(turnCoroutine);

        }
    }
    
    
    void OnTriggerEnter2D(Collider2D obj) {
        if (obj.name.Equals(Settings.SnakeHead)) {
            if (Common.ShareInstance().isImmortal) {
                return;
            }
            
            obj.GetComponent<MySnake>().AddHit();
                        
            GameManager.Instance.ReduceLife();
        }
    }
    
}
