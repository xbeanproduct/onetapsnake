﻿using UnityEngine;
using System.Collections;

public enum RockTypeChapter {
	Chapter1Small,
	Chapter1Big,
	Chapter2Small,
	Chapter2Big
}

public class RockController : MonoBehaviour {
    
//    public GameObject explorePref;
	public RockTypeChapter type;

	public StartPointController startPoint;
	// Use this for initialization
	void Start () {
		if (!startPoint) {
			startPoint = GameObject.FindGameObjectWithTag(Settings.StartPointTag).GetComponent<StartPointController>();
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	void OnTriggerEnter2D(Collider2D obj) {
		if (obj.name.Equals(Settings.SnakeHead)) {
			if (Common.ShareInstance().isHaveHelmet) {
				gameObject.SetActive(false);
				PlayEffect();
				obj.GetComponent<MySnake>().DestroyHelmet();	
				SoundManager.Instance.PlaySound(SoundID.BreakShield);
			}
			else {
				PlayReturnEffect();
				
				GameManager.Instance.ReduceLife();
			}
		}
	}
	
	private void PlayReturnEffect() {
		
	}
	
	void PlayEffect() {
//        if (!explorePref) {
//            return;
//        }
//		Instantiate(explorePref, transform.position, Quaternion.identity);
		switch (type) {
		case RockTypeChapter.Chapter1Small:
			ObjectPool.instance.GetObjectForType (Settings.Lv1BreakingRockEffectSmallPool, false, 3).transform.position = transform.position;
			break;
		case RockTypeChapter.Chapter1Big:
			ObjectPool.instance.GetObjectForType (Settings.Lv1BreakingRockEffectBigPool, false, 3).transform.position = transform.position;
			break;
		case RockTypeChapter.Chapter2Small:
			ObjectPool.instance.GetObjectForType (Settings.Lv2BreakingRockEffectSmallPool, false, 3).transform.position = transform.position;
			break;
		case RockTypeChapter.Chapter2Big:
			ObjectPool.instance.GetObjectForType (Settings.Lv2BreakingRockEffectBigPool, false, 3).transform.position = transform.position;
			break;
		}
    }
}
