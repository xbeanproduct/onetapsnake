﻿using UnityEngine;
using System.Collections;

public class SurvivalFoodController : MonoBehaviour {

	public float horizontalBound = Screen.width/10;
	public float verticalBound = Screen.width/10;
	
	public bool isTurnOther;
	
	public Sprite food, reverse;
	
	public int turnRedCount;
	void Start () {
		transform.position = RandomLocation();
		isTurnOther = false;
		turnRedCount = Random.Range(2, 5);
		GetComponent<SpriteRenderer>().sprite = food;
	}
	
	// // Update is called once per frame
	void Update () {
		if (isTurnOther) {
			GetComponent<SpriteRenderer>().sprite = reverse;
		}
		else {
			GetComponent<SpriteRenderer>().sprite = food;
		}
	}
	
	private Vector2 RandomLocation() {
		var x = Random.Range(horizontalBound, Screen.width - horizontalBound);
		var y = Random.Range(verticalBound, Screen.height - verticalBound);
		
		return Camera.main.ScreenToWorldPoint(new Vector2(x, y));
	}
	
	void OnTriggerEnter2D(Collider2D obj) {
		if (obj.name.Equals(Settings.SnakeHead)) {
            
            PlaySnakeEffect(obj.gameObject);
			obj.GetComponent<MySnake>().AddChild();
			// AddEatEffect(obj.gameObject);
			//Add score
			Common.ShareInstance().score+= Settings.FoodScore;
    
            do {
                transform.position = RandomLocation();    
            }
            while(Physics2D.OverlapCircle(transform.position, 0.5f));
            
            if (isTurnOther) {
                Common.ShareInstance().isTurnRight = !Common.ShareInstance().isTurnRight;
                isTurnOther = false;
            }
            else {
                turnRedCount--;
                if (turnRedCount <= 0) {
                    turnRedCount = Random.Range(2, 5);
                    isTurnOther = true;
                    
                }
            }			
		}
	}
    
    void PlaySnakeEffect(GameObject snake) {
        snake.GetComponent<Animator>().SetTrigger(Settings.SnakeEatTrigger);
    }
    
    // void AddEatEffect(GameObject obj) {
    //     float width = 15;
    //     float height = 15;
        
    //     Vector3 position = Camera.main.WorldToScreenPoint(obj.transform.position);
        
    //     GameObject magnifyGlass = (GameObject)Instantiate(magnifyGlassPref, new Vector3(obj.transform.position.x, obj.transform.position.y, -10), Quaternion.identity);
    //     magnifyGlass.GetComponent<Camera>().pixelRect = new Rect(position.x - width/2, position.y - height/2, width, height); 
        
    // }
    
    // public Vector3 getWorldPosition(Vector3 screenPos)
	// {
	// 	Vector3 worldPos;
	// 	if(Camera.main.orthographic)
	// 	{
	// 		worldPos = Camera.main.ScreenToWorldPoint (screenPos);
	// 		worldPos.z = Camera.main.transform.position.z;
	// 	}
	// 	else
	// 	{
	// 		worldPos = Camera.main.ScreenToWorldPoint (new Vector3 (screenPos.x, screenPos.y, Camera.main.transform.position.z));
	// 		worldPos.x *= -1;
	// 		worldPos.y *= -1;
	// 	}
	// 	return worldPos;
	// }
}
