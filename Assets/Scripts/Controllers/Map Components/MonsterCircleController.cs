﻿using UnityEngine;
using System.Collections;

public class MonsterCircleController : TimeControlBehavior {

    public enum PatrolDirection {
        Left, Right, Top, Down
    }
    
    public PatrolDirection direction;
	Vector3 center;
    public float degreesPerSecond = -65.0f;
    public float radius = 1;

    private Vector3 v;
    
    Vector3 originalPosition;
    Quaternion originalRotation;

    void Start() {
        originalPosition = transform.position;
        originalRotation = transform.rotation;
        
        center = transform.position;
       
        switch(direction) {
                case PatrolDirection.Left:
                    center+= new Vector3(-radius, 0);
                    break;
                case PatrolDirection.Right:
                    center+= new Vector3(radius, 0);
                    break;
                case PatrolDirection.Top:
                    center+= new Vector3(0, radius);
                    break;
                case PatrolDirection.Down:
                    center+= new Vector3(0, -radius);
                    break;
                default:break;
            }
            
        
        v = transform.position - center;
    }
        
    void Update () {
		if (!Common.ShareInstance().isCantMove && Common.ShareInstance().isSnakeGoOut){
            v = Quaternion.AngleAxis (degreesPerSecond * time.deltaTime, Vector3.forward) * v;
            transform.position = center + v;
            transform.RotateAround(center, Vector3.forward, (degreesPerSecond * time.deltaTime));
        }
    }
    
    public void ResetMonster() {
        transform.position = originalPosition;
        transform.rotation = originalRotation;
        center = transform.position;
        
        switch(direction) {
                case PatrolDirection.Left:
                    center+= new Vector3(-radius, 0);
                    break;
                case PatrolDirection.Right:
                    center+= new Vector3(radius, 0);
                    break;
                case PatrolDirection.Top:
                    center+= new Vector3(0, radius);
                    break;
                case PatrolDirection.Down:
                    center+= new Vector3(0, -radius);
                    break;
                default:break;
            }
            
        
        v = transform.position - center;
    }
    
    void OnDrawGizmos() {
        Vector2 toPos = transform.position;
		switch(direction) {
			case PatrolDirection.Left:
				toPos+= new Vector2(-radius, 0);
				break;
			case PatrolDirection.Right:
				toPos+= new Vector2(radius, 0);
				break;
			case PatrolDirection.Top:
				toPos+= new Vector2(0, radius);
				break;
			case PatrolDirection.Down:
				toPos+= new Vector2(0, -radius);
				break;
			default:break;
		}
		
		Gizmos.color = Color.yellow;
		
		Gizmos.DrawLine(transform.position, toPos);
        
	}
    
    void OnTriggerEnter2D(Collider2D obj) {
        if (obj.name.Equals(Settings.SnakeHead)) {
            if (Common.ShareInstance().isImmortal) {
                return;
            }
            
            obj.GetComponent<MySnake>().AddHit();
                        
            GameManager.Instance.ReduceLife();

			SoundManager.Instance.PlaySound (SoundID.Bee);
        }
    }
    
}
