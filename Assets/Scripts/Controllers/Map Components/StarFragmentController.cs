﻿using UnityEngine;
using System.Collections;

public class StarFragmentController : MonoBehaviour {
    
    public GameObject starFragmentEffectPref;
	// Use this for initialization
	void Start () {
		if (GameObject.FindGameObjectsWithTag(Settings.StarTag).Length > Settings.NumStarFragmentLimit) {
			Debug.LogError(" Only have 9 star fragments on map");
		}
        
        
	}
	
	// Update is called once per frame
	void Update () {
	   
	}
	
	void OnTriggerEnter2D(Collider2D obj) {
		if (obj.name.Equals(Settings.SnakeHead)) {
            
            PlaySnakeEffect(obj.gameObject);
            EatStar();
		}
	}
    
    void PlaySnakeEffect(GameObject snake) {
        snake.GetComponent<Animator>().SetTrigger(Settings.SnakeEatTrigger);
    }
    
    public void EatStar() {
        Common.ShareInstance().starfragments++;
        PlayEffect();
        
        CheckStar();

		SoundManager.Instance.PlaySound(SoundID.GetStar);
        
        // Add score
        // Common.ShareInstance().score+= Settings.StarScore;
        
        gameObject.SetActive(false);
    }
    
    private void CheckStar() {
        if (Common.ShareInstance().starfragments >= 3) {
            Common.ShareInstance().starfragments = 0;
            Common.ShareInstance().star++;
        }
    }
    
    private void PlayEffect() {
        GameObject starFragmentEffect = (GameObject)Instantiate(starFragmentEffectPref, transform.position, Quaternion.identity);
        starFragmentEffect.GetComponent<StarFragmentEffect>().starIndex = Common.ShareInstance().starfragments;
    }
    
}
