﻿using UnityEngine;
using System.Collections;

public class HelmetController : MonoBehaviour {

    public GameObject shieldEffectPref;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnTriggerEnter2D(Collider2D obj) {
		if (obj.name.Equals(Settings.SnakeHead)) {
			if (Common.ShareInstance().isHaveHelmet) {
				return;
			}
			else {
                AddEffect();
                
                PlaySnakeEffect(obj.gameObject);
                if (GameObject.FindObjectOfType(typeof(SoundManager)) != null) {
                      
                }
                
                				
				Common.ShareInstance().isHaveHelmet = true;
				
				gameObject.SetActive(false);
                
                UserData.Instance.ShieldEat++;

				SoundManager.Instance.PlaySound (SoundID.GetShield);
			}
		}
	}
    
    void AddEffect() {
//        Instantiate(shieldEffectPref, transform.position, Quaternion.identity);
		ObjectPool.instance.GetObjectForType(Settings.ShieldParticleEffectPool, false, 3).transform.position = transform.position;
    }
    void PlaySnakeEffect(GameObject snake) {
        snake.GetComponent<Animator>().SetTrigger(Settings.SnakeEatTrigger);
    }
	
}
