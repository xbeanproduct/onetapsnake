﻿using UnityEngine;
using System.Collections;

public class ReverseFoodController : MonoBehaviour {

    public bool isEaten = false;
    
    private Vector2 originalPos;
    private Vector3 originalScale;
    
    public ParticleSystem reverseFoodEffect;
	// Use this for initialization
	void Start () {
	   originalPos = transform.position;
       originalScale = transform.localScale;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnTriggerEnter2D(Collider2D obj) {
		if (obj.name.Equals(Settings.SnakeHead) && !isEaten) {
            
            StartCoroutine(AddEatEffect(obj.gameObject));
            
            AddFoodEffect();
            
            obj.GetComponent<MySnake>().PlaySnakeEffect();

            
			Common.ShareInstance().isTurnRight = !Common.ShareInstance().isTurnRight;
			obj.GetComponent<MySnake> ().ShowDirection ();

			obj.GetComponent<MySnake> ().AddChild ();
			obj.GetComponent<SnakeSegment> ().PunchSegment ();
			
			// Add score
			Common.ShareInstance().score+= Settings.ReverseFoodScore;
            
            // Add Stats
            UserData.Instance.ReverseFoodEat++;

			SoundManager.Instance.PlaySound (SoundID.GetRevert);
		}
	}
    
    IEnumerator AddEatEffect(GameObject obj) {
        isEaten = true;
        
        // Vector2 snakePos = obj.transform.position + obj.transform.up * 0.3f;
        
        while (transform.localScale != Vector3.zero && !Common.ShareInstance().isCantMove) 
        {
            Vector2 snakePos = obj.transform.position + obj.transform.up * 0.2f;
            transform.localScale = Vector3.Lerp(transform.localScale, Vector3.zero, Time.fixedDeltaTime * 4);
            transform.position = Vector2.Lerp(transform.position, snakePos, Time.fixedDeltaTime * 4);
            
            yield return null;
        }
        
        gameObject.SetActive(false);
    }
    
    void AddFoodEffect() {
        if (!reverseFoodEffect) {
            return;
        }
//        Instantiate(reverseFoodEffect, transform.position, Quaternion.identity);
		ObjectPool.instance.GetObjectForType(Settings.ReverseFoodEffectPool, false, 3).transform.position = transform.position;
    }
  
    public void ResetFood() {
        isEaten = false;
        transform.localScale = originalScale;
        transform.position = originalPos;
        gameObject.SetActive(true);
    }
}
