﻿using UnityEngine;
using System.Collections;

public class MonsterFishController : TimeControlBehavior {

    public float timeToMove = 2;
    public float timeRestPerPath = 2;
        
    public Vector3[] listPath;
    
    Vector3 originalPos;
    Vector3 _currentPos;
    Vector3 _nextPos;
    
    int posIndex = 0;
    
    Quaternion _nextRotation;
    
    IEnumerator moveCoroutine;
    IEnumerator turnCoroutine;
    
    bool _isLerping = false;
    
	// Use this for initialization
	void Start () {
        // Get AB
        _currentPos = transform.position;
        originalPos = transform.position;
       
       if (listPath.Length != 0) {
            _nextPos = listPath[posIndex];
            
            Vector2 newDir = _currentPos - _nextPos;
            float angle = Mathf.Atan2(newDir.y, newDir.x) * Mathf.Rad2Deg;
            _nextRotation = Quaternion.AngleAxis(angle + 90, Vector3.forward);
            
            turnCoroutine = TurnRotation();
            StartCoroutine(turnCoroutine);
            // moveCoroutine = MoveToPosition();
            // StartCoroutine(moveCoroutine);
       } 
	   
    }
    
    void Update() {
        
    }
    
    IEnumerator MoveToPosition() {
//        GetComponent<Animator>().SetTrigger("ChimStart");
//      
//        yield return time.WaitForSeconds(4.5f);
//        
//        float ratio = 0;
//        float multiplier = 1 / timeToMove;
//        
//        Vector3 nearPoint = Vector2.MoveTowards(_nextPos, _currentPos, 2);
//        var isLand = true;
//       
//        while (Vector2.Distance(transform.position, nearPoint) > .5f && !Common.ShareInstance().isCantMove) {
//         
//            ratio += time.fixedDeltaTime * multiplier;
//            
//            if (Vector2.Distance(transform.position, nearPoint)   < 1 && isLand) {
//                GetComponent<Animator>().SetTrigger("ChimLand");
//                isLand = false;
//            }
//            
//            transform.position = Vector3.Lerp(_currentPos, _nextPos, Mathf.SmoothStep(0.0f, 1.0f, ratio));
//            yield return null;
//        }
//        
//        
//        
//        yield return time.WaitForSeconds(4f);
//        
//        
//        
//        if (!Common.ShareInstance().isCantMove) {
//            _currentPos = _nextPos;
//            _nextPos = FindNextPos(_currentPos);
//            
//            Vector2 newDir = _currentPos - _nextPos;
//            float angle = Mathf.Atan2(newDir.y, newDir.x) * Mathf.Rad2Deg;
//            _nextRotation = Quaternion.AngleAxis(angle + 90, Vector3.forward);
//            
//            turnCoroutine = TurnRotation();
//            StartCoroutine(turnCoroutine);
//        }
		while (!Common.ShareInstance ().isSnakeGoOut) {
			yield return null;
		}

		GetComponent<Animator>().SetTrigger("SharkStart");
		SoundManager.Instance.PlaySound(SoundID.Whale);

		yield return time.WaitForSeconds(4);

		float ratio = 0;
		float multiplier = 1 / timeToMove;

		while (transform.position != _nextPos && !Common.ShareInstance().isCantMove) {
			ratio += time.fixedDeltaTime * multiplier;

			transform.position = Vector3.Lerp(_currentPos, _nextPos, Mathf.SmoothStep(0.0f, 1.0f, ratio));
			yield return null;
		}

		if (!Common.ShareInstance ().isCantMove) {
			GetComponent<Animator> ().SetTrigger ("SharkLand");
			SoundManager.Instance.PlaySound(SoundID.Whale);
		}

		yield return time.WaitForSeconds(4f);

		if (!Common.ShareInstance().isCantMove) {
			_currentPos = _nextPos;
			_nextPos = FindNextPos(_currentPos);

			Vector2 newDir = _currentPos - _nextPos;
			float angle = Mathf.Atan2(newDir.y, newDir.x) * Mathf.Rad2Deg;
			_nextRotation = Quaternion.AngleAxis(angle + 90, Vector3.forward);

			turnCoroutine = TurnRotation();
			StartCoroutine(turnCoroutine);
		}
        
    }
    
    Vector3 FindNextPos(Vector3 current) {
        posIndex++;
        if (posIndex == listPath.Length) {
            posIndex = -1;
            return originalPos;
        }
        
        return listPath[posIndex];
    }
    
    IEnumerator TurnRotation() {
		while (!Common.ShareInstance ().isSnakeGoOut) {
			yield return null;
		}

        yield return time.WaitForSeconds(timeRestPerPath);
        
        while (Quaternion.Angle(transform.rotation, _nextRotation) > 5 && !Common.ShareInstance().isCantMove) {
            transform.rotation = Quaternion.Slerp(transform.rotation, _nextRotation, time.fixedDeltaTime * 5);
           
            yield return null;
        }
        
        if (!Common.ShareInstance().isCantMove) {

            moveCoroutine = MoveToPosition();
            StartCoroutine(moveCoroutine);
            
        }
    }
	
    void OnDrawGizmos() {
        Gizmos.color = Color.red;
        
        
        if (listPath.Length > 0) {
            Gizmos.DrawLine(transform.position, listPath[0]);
            for(int i = 0; i< listPath.Length - 1; i++) {
                Gizmos.DrawLine(listPath[i], listPath[i+1]);
            }
            
            Gizmos.DrawLine(listPath[listPath.Length - 1], transform.position);
        }
        
        
	}

    public void ResetMonster() {
		if (moveCoroutine != null) {
			StopCoroutine (moveCoroutine);
		}
		if (turnCoroutine != null) {
			StopCoroutine (turnCoroutine);
		}
        
		GetComponent<Animator> ().SetTrigger ("Reset");
        posIndex = 0;
        _currentPos = originalPos;
        _nextPos = listPath[posIndex];
        transform.position = originalPos;
        Vector2 newDir = _currentPos - _nextPos;
        float angle = Mathf.Atan2(newDir.y, newDir.x) * Mathf.Rad2Deg;
        _nextRotation = Quaternion.AngleAxis(angle + 90, Vector3.forward);
        
        turnCoroutine = TurnRotation();
        StartCoroutine(turnCoroutine);
        
    }

    void OnTriggerEnter2D(Collider2D obj) {
        if (obj.name.Equals(Settings.SnakeHead)) {
            if (Common.ShareInstance().isImmortal) {
                return;
            }

            obj.GetComponent<MySnake>().AddHit();           
            GameManager.Instance.ReduceLife();
        }
    }
}
