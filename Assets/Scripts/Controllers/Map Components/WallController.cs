﻿using UnityEngine;
using System.Collections;

public class WallController : MonoBehaviour {

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnTriggerEnter2D(Collider2D obj) {
		if (obj.name.Equals(Settings.SnakeHead)) {
			if (MapManager.Instance.currentLevelPlayed > 50) {
                obj.GetComponent<MySnake>().AddWater();             
            }
            else {
				obj.GetComponent<MySnake>().AddSmoke();
            }
				
            // gameManager.ReduceLife();
			if (Common.ShareInstance ().isFreeStyleTutorial) {
				GameManagerTutorial.Instance.ReduceLife ();
			} else {
				GameManager.Instance.ReduceLife ();
			}
			
		}
	}
	
}
