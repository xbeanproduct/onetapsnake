﻿using UnityEngine;
using System.Collections;

public class StarController : MonoBehaviour {
    
    public GameObject starEffectPref;
	// Use this for initialization
	void Start () {
		if (GameObject.FindGameObjectsWithTag(Settings.StarTag).Length > Settings.NumStarLimit) {
			Debug.LogError(" Only have 3 stars on map");
		}
        
        
	}
	
	// Update is called once per frame
	void Update () {
	   
	}
	
	void OnTriggerEnter2D(Collider2D obj) {
		if (obj.name.Equals(Settings.SnakeHead)) {
            
            PlaySnakeEffect(obj.gameObject);
            EatStar();

			SoundManager.Instance.PlaySound (SoundID.GetStar);
		}
	}
    
    void PlaySnakeEffect(GameObject snake) {
        snake.GetComponent<Animator>().SetTrigger(Settings.SnakeEatTrigger);
    }
    
    public void EatStar() {
        Common.ShareInstance().star++;
        
        PlayEffect();
        
        
        // Add score
        Common.ShareInstance().score+= Settings.StarScore;
        
        gameObject.SetActive(false);
    }
    
    private void PlayEffect() {
        GameObject starEffect =  (GameObject)Instantiate(starEffectPref, transform.position, Quaternion.identity);
        starEffect.GetComponent<StarEffect>().starIndex = Common.ShareInstance().star;
    }
    
}
