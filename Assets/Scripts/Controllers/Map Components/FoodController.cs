﻿using UnityEngine;
using System.Collections;

public class FoodController : MonoBehaviour {

	public float horizontalBound = Screen.width/10;
	public float verticalBound = Screen.width/10;
	
	public bool isTurnOther;
    public bool isEaten = false;
    
    private Vector3 originalScale;
    private Vector2 originalPos;
    
    public ParticleSystem foodEffect;
    void Start() {
        originalScale = transform.localScale;
        originalPos = transform.position;
    }
	
	// public Sprite green, red;
	
	// public int turnRedCount;
	// Use this for initialization
	// void Start () {
	// 	transform.position = RandomLocation();
	// 	isTurnOther = false;
	// 	turnRedCount = Random.Range(2, 5);
	// 	GetComponent<SpriteRenderer>().sprite = green;
	// }
	
	// // Update is called once per frame
	// void Update () {
	// 	if (isTurnOther) {
	// 		GetComponent<SpriteRenderer>().sprite = red;
	// 	}
	// 	else {
	// 		GetComponent<SpriteRenderer>().sprite = green;
	// 	}
	// }
	
	// private Vector2 RandomLocation() {
	// 	var x = Random.Range(horizontalBound, Screen.width - horizontalBound);
	// 	var y = Random.Range(verticalBound, Screen.height - verticalBound);
		
	// 	return Camera.main.ScreenToWorldPoint(new Vector2(x, y));
	// }
	
	void OnTriggerEnter2D(Collider2D obj) {
		if (obj.name.Equals(Settings.SnakeHead) && !isEaten) {
            StartCoroutine(AddEatEffect(obj.gameObject));
            
            AddFoodEffect(obj.gameObject);
            
            obj.GetComponent<MySnake>().PlaySnakeEffect();
            

			obj.GetComponent<MySnake> ().AddChild ();
			obj.GetComponent<SnakeSegment> ().PunchSegment ();
            
			 
			//Add score
			Common.ShareInstance().score+= Settings.FoodScore;
			Common.ShareInstance ().foodEat++;
            // Add Stats
            UserData.Instance.FoodEat++;
			
			SoundManager.Instance.PlaySound (SoundID.GetFood);
		}
	}

    void AddFoodEffect(GameObject obj) {
        if (!foodEffect) {
            return;
        }
//		Instantiate(foodEffect, new Vector3(transform.position.x, transform.position.y, foodEffect.transform.position.z), Quaternion.Euler(-90, 0, 0));
		ObjectPool.instance.GetObjectForType(Settings.FoodEffectPool, false, 3).transform.position = new Vector3(transform.position.x, transform.position.y, foodEffect.transform.position.z);
    }
    
    IEnumerator AddEatEffect(GameObject obj) {
        isEaten = true;
        
        // Vector2 snakePos = obj.transform.position + obj.transform.up * 0.3f;
        
        while (transform.localScale != Vector3.zero && !Common.ShareInstance().isCantMove) 
        {
            Vector2 snakePos = obj.transform.position + obj.transform.up * 0.2f;
            transform.localScale = Vector3.Lerp(transform.localScale, Vector3.zero, Time.fixedDeltaTime * 2);
            transform.position = Vector2.Lerp(transform.position, snakePos, Time.fixedDeltaTime * 2);
            
            yield return null;
        }
        
        gameObject.SetActive(false);
    }
    
    public void ResetFood() {
        isEaten = false;
        transform.localScale = originalScale;
        transform.position = originalPos;
        gameObject.SetActive(true);
    }
    
    public Vector3 getWorldPosition(Vector3 screenPos)
	{
		Vector3 worldPos;
		if(Camera.main.orthographic)
		{
			worldPos = Camera.main.ScreenToWorldPoint (screenPos);
			worldPos.z = Camera.main.transform.position.z;
		}
		else
		{
			worldPos = Camera.main.ScreenToWorldPoint (new Vector3 (screenPos.x, screenPos.y, Camera.main.transform.position.z));
			worldPos.x *= -1;
			worldPos.y *= -1;
		}
		return worldPos;
	}
}
