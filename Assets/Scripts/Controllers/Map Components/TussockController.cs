﻿using UnityEngine;
using System.Collections;

public class TussockController : MonoBehaviour {

	private float minDistance = 0.0025f;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnTriggerEnter2D(Collider2D obj) {
		if (obj.name.Equals(Settings.SnakeHead)) {
			Common.ShareInstance().isInTussock = true;
		}
	}
	
	private bool GetSnakeDiriection(Transform snake) {
		if (Mathf.Round(snake.eulerAngles.z) == Settings.Direction.Up) {
			return false;
		}
		else if (Mathf.Round(snake.eulerAngles.z) == Settings.Direction.Down) {
			return false;
		}
		else if (Mathf.Round(snake.eulerAngles.z) == Settings.Direction.Left) {
			return true;
		}
		else if (Mathf.Round(snake.eulerAngles.z) == Settings.Direction.Right) {
			return true;
		}
		
		return true;
	}
}
