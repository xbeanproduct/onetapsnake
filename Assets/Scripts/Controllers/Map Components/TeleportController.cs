﻿using UnityEngine;
using System.Collections;

public class TeleportController : TimeControlBehavior {
    
    
    public enum TeleportDirection {
        Left, Right, Top, Down
    }
    // public Transform destination;
    public TeleportDirection direction;
    public int teleportGroup = 0;
    
    public GameObject destination;
    public bool isDisabled = false;
    static Transform last;
    
    [SerializeField]
    GameObject teleportEffect;
	// Use this for initialization
	void Start () {
        // if(transform.tag.Equals("TeleportA")) {
        //     if (GameObject.FindGameObjectWithTag("TeleportB")) {
        //         destination = GameObject.FindGameObjectWithTag("TeleportB").transform;
        //     }
        // }	
        // else if (transform.tag.Equals("TeleportB")) {
        //     if (GameObject.FindGameObjectWithTag("TeleportA")) {
        //         destination = GameObject.FindGameObjectWithTag("TeleportA").transform;
        //     }
        // }
        GameObject[] destinations = null;
        if (transform.tag.Equals(Settings.TeleportA)) {
            destinations = GameObject.FindGameObjectsWithTag(Settings.TeleportB);
            foreach(GameObject des in destinations) {
                if (des.GetComponent<TeleportController>().teleportGroup == teleportGroup) {
                    destination = des;
                }
            }
            // Teleport(obj, destination);
        }
        else if (transform.tag.Equals(Settings.TeleportB)) {
            destinations = GameObject.FindGameObjectsWithTag(Settings.TeleportA);
            // Teleport(obj, destination);
            foreach(GameObject des in destinations) {
                if (des.GetComponent<TeleportController>().teleportGroup == teleportGroup) {
                    destination = des;
                }
            }
        }
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    
    void OnDrawGizmos() {
		Vector2 toPos = transform.position;
		switch(direction) {
			case TeleportDirection.Left:
				toPos+= new Vector2(-1, 0);
				break;
			case TeleportDirection.Right:
				toPos+= new Vector2(1, 0);
				break;
			case TeleportDirection.Top:
				toPos+= new Vector2(0, 1);
				break;
			case TeleportDirection.Down:
				toPos+= new Vector2(0, -1);
				break;
			default:break;	
		}
		
		Gizmos.color = Color.yellow;
		
		Gizmos.DrawLine(transform.position, toPos);
	}
    
    void OnTriggerEnter2D(Collider2D obj) {
        if (obj.name.Equals(Settings.SnakeHead) || obj.tag.Equals(Settings.SnakeTail)) {
            if (obj.GetComponent<SnakeSegment>().isCanTeleport) {        
                if (obj.name.Equals(Settings.SnakeHead)) {
					Common.ShareInstance ().teleportCount++;
                    EnableEffect();
                    destination.GetComponent<TeleportController>().EnableEffect();
					Teleport (obj, destination);
                    obj.GetComponent<SnakeSegment>().isCanTeleport = false; 
                    if (obj.GetComponent<SnakeSegment>().prevSegment) {
                        obj.GetComponent<SnakeSegment>().prevSegment.GetComponent<SnakeSegment>().nextTeleportDestination = destination;
                    }

					SoundManager.Instance.PlaySound (SoundID.Teleport);
                }
                else {
					if (obj.GetComponent<SnakeSegment> ().nextTeleportDestination == null) {
						Debug.Log ("NULL DES");
						return;
					}

					 
					Teleport (obj, destination);
                    obj.GetComponent<SnakeSegment>().isCanTeleport = false;
                    if (obj.GetComponent<SnakeSegment>().prevSegment) {
                        obj.GetComponent<SnakeSegment>().prevSegment.GetComponent<SnakeSegment>().nextTeleportDestination = destination;
                    }
                }
                
                
                if (obj.GetComponent<SnakeSegment>().isTail) {
                    DisableEffect();
                    destination.GetComponent<TeleportController>().DisableEffect();
                }
            }
            
            
        }

		// Disable few time when Snake head just teleport
		if (obj.name.Equals(Settings.SnakeHead))
		{
			obj.gameObject.GetComponent<InputController>().lastDoActionTime = Time.time;
		}
    }
    
    public void EnableEffect() {
        var em = teleportEffect.GetComponent<ParticleSystem>().emission;
        em.enabled = true;
        
        var em2 = teleportEffect.transform.GetChild(0).GetComponent<ParticleSystem>().emission;
        em2.enabled = true;
    }
    
    public void DisableEffect() {
        var em = teleportEffect.GetComponent<ParticleSystem>().emission;
        em.enabled = false;
        
        var em2 = teleportEffect.transform.GetChild(0).GetComponent<ParticleSystem>().emission;
        em2.enabled = false;
    }
    
    void ChangeSnakeDirection(TeleportDirection direction, Transform snake) {
		float teleportDirection = 0;
		switch(direction) {
			case TeleportDirection.Left: 
				teleportDirection = Settings.Direction.Left;
				break;
			case TeleportDirection.Right:
				teleportDirection = Settings.Direction.Right;
				break;
			case TeleportDirection.Top:
				teleportDirection = Settings.Direction.Up;
				break;
			case TeleportDirection.Down:
				teleportDirection = Settings.Direction.Down;
				break;
			default: break;
		}
        
        if (!Mathf.Approximately(snake.eulerAngles.z, teleportDirection)) {
            snake.rotation = Quaternion.Euler(snake.eulerAngles.x, snake.eulerAngles.y, teleportDirection);
        }

    }
    
    void Teleport(Collider2D obj, GameObject destination) {
        
        last = transform;
        ChangeSnakeDirection(destination.GetComponent<TeleportController>().direction, obj.transform);
        obj.transform.position = destination.transform.position;
        if (!obj.tag.Equals(Settings.SnakeHead)) {
            obj.GetComponent<SnakeSegment>().FixPosition();

			if (obj.GetComponent<SnakeSegment> ().isTail) {
				Invoke ("EndTeleport", 0.5f);
			}
        }



        obj.GetComponent<SnakeSegment>().EnableTeleport();

    }

	void EndTeleport() {
		if (Common.ShareInstance ().teleportCount > 0) {
			Common.ShareInstance ().teleportCount--;
		}
	}
   
}
