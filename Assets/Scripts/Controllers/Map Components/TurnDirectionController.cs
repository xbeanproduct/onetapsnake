﻿using UnityEngine;
using System.Collections;

public class TurnDirectionController : MonoBehaviour {

    public bool isRight = true;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    
    void OnTriggerEnter2D(Collider2D obj) {
        if (obj.name.Equals(Settings.SnakeHead)) {
            obj.GetComponent<InputController>().TurnDirection(isRight);
         
        }
    }
}
