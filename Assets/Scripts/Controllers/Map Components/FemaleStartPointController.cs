﻿using UnityEngine;
using System.Collections;

public class FemaleStartPointController : MonoBehaviour {
	
	public enum StartPointDirection {
		Left, Right, Top, Down
	}
	
	public StartPointDirection direction; 
	
	private GameObject snake, midSegment, lastSegment;
    
	// Use this for initialization
	void Start () {
		
	}
	
	public void SettingCollider() {
		BoxCollider2D col = GetComponent<BoxCollider2D>();
		
		switch(direction) {
			case StartPointDirection.Left: 
				col.offset = new Vector2(0.4f, 0);
				col.size = new Vector2(0.2f, 1);
				break;
			case StartPointDirection.Right:
				col.offset = new Vector2(-0.4f, 0);
				col.size = new Vector2(0.2f, 1);
				break;
			case StartPointDirection.Top:
				col.offset = new Vector2(0, -0.4f);
				col.size = new Vector2(1, 0.2f);
				break;
			case StartPointDirection.Down:
				col.offset = new Vector2(0, 0.4f);
				col.size = new Vector2(1, 0.2f);
				break;
			default:break;
		}
		
	}

	public void FindSnakeElement()
	{
        GameObject snakeHolder = GameObject.FindGameObjectWithTag(Settings.FemaleSnakeHolderTag);
		snake = snakeHolder.FindInChildren(Settings.SnakeHead);
		midSegment = snakeHolder.FindInChildren(Settings.SnakeMidSegment);
		lastSegment = snakeHolder.FindInChildren(Settings.SnakeLastSegment);
	}
	
	public void ReturnPosition() {
		float startPointDirection = 0;
		switch(direction) {
			case StartPointDirection.Left: 
				startPointDirection = Settings.Direction.Left;
				break;
			case StartPointDirection.Right:
				startPointDirection = Settings.Direction.Right;
				break;
			case StartPointDirection.Top:
				startPointDirection = Settings.Direction.Up;
				break;
			case StartPointDirection.Down:
				startPointDirection = Settings.Direction.Down;
				break;
			default: break;
		}
		
		snake.transform.rotation = Quaternion.AngleAxis(startPointDirection, Vector3.forward);
		snake.transform.localPosition = new Vector3(transform.position.x, transform.position.y, snake.transform.localPosition.z) + snake.transform.up * 0.3f;
        midSegment.transform.rotation = snake.transform.rotation;
        midSegment.transform.localPosition = snake.transform.position + GetParentDirection(snake.transform.rotation);
        lastSegment.transform.rotation = snake.transform.rotation;
        lastSegment.transform.localPosition = midSegment.transform.position + GetParentDirection(snake.transform.rotation);
	}
    
    private Vector3 GetParentDirection(Quaternion parentRotation) {
		if (Mathf.Round(parentRotation.eulerAngles.z) == Settings.Direction.Up) {
			return new Vector2(0, -Settings.SnakeComponentSpace);
		}
		else if (Mathf.Round(parentRotation.eulerAngles.z) == Settings.Direction.Down) {
			return new Vector2(0, Settings.SnakeComponentSpace);
		}
		else if (Mathf.Round(parentRotation.eulerAngles.z) == Settings.Direction.Left) {
			return new Vector2(Settings.SnakeComponentSpace, 0);
		}
		else if (Mathf.Round(parentRotation.eulerAngles.z) == Settings.Direction.Right) {
			return new Vector2(-Settings.SnakeComponentSpace, 0);
		}
		
		return Vector2.zero;
	}
	
	public void ReturnObjectPosition(GameObject obj) {
		float startPointDirection = 0;
		switch(direction) {
			case StartPointDirection.Left: 
				startPointDirection = Settings.Direction.Left;
				break;
			case StartPointDirection.Right:
				startPointDirection = Settings.Direction.Right;
				break;
			case StartPointDirection.Top:
				startPointDirection = Settings.Direction.Up;
				break;
			case StartPointDirection.Down:
				startPointDirection = Settings.Direction.Down;
				break;
			default: break;
		}
		
		obj.transform.rotation = Quaternion.AngleAxis(startPointDirection, Vector3.forward);
		obj.transform.localPosition = new Vector3(transform.position.x, transform.position.y, obj.transform.localPosition.z);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnDrawGizmos() {
		Vector2 toPos = transform.position;
		switch(direction) {
			case StartPointDirection.Left:
				toPos+= new Vector2(-1, 0);
				break;
			case StartPointDirection.Right:
				toPos+= new Vector2(1, 0);
				break;
			case StartPointDirection.Top:
				toPos+= new Vector2(0, 1);
				break;
			case StartPointDirection.Down:
				toPos+= new Vector2(0, -1);
				break;
			default:break;	
		}
		
		Gizmos.color = Color.yellow;
		
		Gizmos.DrawLine(transform.position, toPos);
	}
	
	void OnTriggerEnter2D(Collider2D obj) {
		if (obj.name.Equals(Settings.SnakeHead)) {
			if (Common.ShareInstance().isSnakeGoOut) {
				Debug.Log("HIT");
				
				PlayEffect();
			
				GameManager.Instance.ReduceLife();
			}
		}
	}
	
	
	void OnTriggerExit2D(Collider2D obj) {
		if (obj.name.Equals(Settings.SnakeHead)) {
			Common.ShareInstance().isSnakeGoOut = true;
			Invoke("GoOutEffect", Settings.SnakeGoOutTime);
		}
	}
    
    private void GoOutEffect() {
        gameObject.FindInChildren(Settings.StartPointCover).SetActive(true);
    }
	
	private void PlayEffect() {
		
	}
}
