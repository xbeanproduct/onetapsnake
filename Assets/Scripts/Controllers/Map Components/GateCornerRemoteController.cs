﻿using UnityEngine;
using System.Collections;

public class GateCornerRemoteController : MonoBehaviour {
    
    public GameObject remoteEffect;
    public GameObject gateCorner;
    
    private bool isTurnGateCorner = false;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    
    void OnTriggerEnter2D(Collider2D obj) {
        if (gateCorner == null) {
            return;
        }
        if (obj.name.Equals(Settings.SnakeHead)) {
			Debug.Log ("Vao roi");
            if(!isTurnGateCorner) {
				Debug.Log ("Effect ne");
                AddEffect();
                
                isTurnGateCorner = true;
                gateCorner.GetComponent<GateCornerController>().RotateGate();
            }   
        }
    }
    
    void OnTriggerExit2D(Collider2D obj) {
        if (obj.tag.Equals(Settings.SnakeTail)) {
            if (obj.GetComponent<SnakeSegment>().isTail) {
                isTurnGateCorner = false;
            }
         }
    }

	public void ResetRemote() {
		isTurnGateCorner = false;
	}
    
    public void AddEffect() {
        if (!remoteEffect) {
            return;
        }
//        Instantiate(remoteEffect, transform.position, Quaternion.identity);
		ObjectPool.instance.GetObjectForType ("RemoteEffect", false, 3).transform.position = transform.position;
			
        
    }
}
