﻿using UnityEngine;
using System.Collections;

public class DiamondController : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnTriggerEnter2D(Collider2D obj) {
		if (obj.name.Equals(Settings.SnakeHead)) {
            
            
            PlaySnakeEffect(obj.gameObject);
			Common.diamond++;
			
			Destroy(gameObject);
		}
	}
    
    void PlaySnakeEffect(GameObject snake) {
        snake.GetComponent<Animator>().SetTrigger(Settings.SnakeEatTrigger);
    }
}
