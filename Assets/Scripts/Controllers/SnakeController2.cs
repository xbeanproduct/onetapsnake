﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SnakeController2 : MonoBehaviour {

	// Use this for initialization
	public Transform head;
    public Transform[] segments;
    List<Vector3> breadcrumbs;
 
 	
    Vector3 prevHeadPosition;
    float headDisplacement;
    public float segmentSpacing; //set controls the spacing between the segments,which is always constant.
 
    void Start()
    {
        //populate the first set of crumbs by the initial positions of the segments.
        breadcrumbs = new List<Vector3>();
        breadcrumbs.Add(head.position); //add head first, because that's where the segments will be going.
        for (int i = 0; i < segments.Length - 1; i++) // we don't want to the last segment to have a crumb.
            breadcrumbs.Add(segments[i].position);
        prevHeadPosition = head.position;
    }
 
    void Update()
    {
 
        Vector3 headMovement = head.position - prevHeadPosition;
        float headMovementMagnitude = headMovement.magnitude;
		
		print(headMovementMagnitude);
        headDisplacement += headMovementMagnitude;
 
        if (headDisplacement >= segmentSpacing)
        {
            breadcrumbs.RemoveAt(breadcrumbs.Count - 1); //remove the last breadcrumb
            breadcrumbs.Insert(0, head.position); // add a new one where head is.
            headDisplacement = 0;
        }
        if (headMovementMagnitude > 0)
        {
            for (int i = 0; i < segments.Length; i++)
            {
                if ((breadcrumbs[i] - segments[i].position).sqrMagnitude > 0.01){
                    segments[i].position += headMovementMagnitude * (breadcrumbs[i] - segments[i].position).normalized;
					//  Vector2 newPos = segments[i].position + headMovementMagnitude * (breadcrumbs[i] - segments[i].position).normalized;
					//  segments[i].position = Vector2.Lerp(segments[i].position, newPos, Time.time);
				}
            }
        }
 
        prevHeadPosition = head.position;
 
    }
}
