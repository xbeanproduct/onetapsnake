﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

public class CameraController : MonoBehaviour {
    
    public int numMap = 1;
    private GameObject snakeHead;
    
	public float leftBound;
	public float rightBound;
    
    public float dragSensitivity = 1;
    
	// Use this for initialization
	void Start () {
		leftBound = -.5f;
        rightBound = 1.5f + (6.5f * (numMap - 1));
	}
	
	// Update is called once per frame
	void Update () {
        if (!snakeHead) {
            if (GameObject.Find(Settings.SnakeHead)) {
                snakeHead = GameObject.Find(Settings.SnakeHead);
            }
        }	   
        
       
        
        if (Common.ShareInstance().isGamePause) {
            if (numMap >= 2) {

				if (Common.ShareInstance ().isGateTurning) {
					return;
				}

				if (EventSystem.current != null && EventSystem.current.IsPointerOverGameObject()) {
					return;	
				}

                Vector2 newPos = transform.position;

				#if UNITY_EDITOR
                // Editor
                if (Input.GetMouseButton(0)) {
                    newPos = transform.position - new Vector3(Input.GetAxis("Mouse X"), 0);
                }
				#elif UNITY_ANDROID || UNITY_IOS
                // Touch Device
                if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Moved) {
                    newPos = transform.position - new Vector3(Input.GetTouch(0).deltaPosition.x, 0);
                }
				#endif

                if (transform.position.x <= rightBound && transform.position.x >= leftBound) {
                    if (newPos.x > rightBound) {
                        newPos = new Vector2(rightBound, newPos.y);
                    }
                    
                    if (newPos.x < leftBound) {
                        newPos = new Vector2(leftBound, newPos.y);
                    }
                    transform.position = Vector2.Lerp(transform.position, newPos, Time.deltaTime * 5);    
                }
            }
        }
        else {
             if (snakeHead.transform.position.x - transform.position.x > 1.5f) {
                if (transform.position.x <= rightBound) {
                    Vector2 newPos = new Vector2(snakeHead.transform.position.x - 1.5f, transform.position.y);
                    if (newPos.x > rightBound) {
                        newPos = new Vector2(rightBound, newPos.y);
                    }
                    transform.position = Vector2.Lerp(transform.position, newPos, Time.deltaTime * 8);
                }
            }
            else {
                if (snakeHead.transform.position.x - transform.position.x < -1.5f) {
                    if (transform.position.x >= leftBound) {
                        Vector2 newPos = new Vector2(snakeHead.transform.position.x + 1.5f, transform.position.y);
                        if (newPos.x < leftBound) {
                            newPos = new Vector2(leftBound, newPos.y);
                        }
                        transform.position = Vector2.Lerp(transform.position, newPos, Time.deltaTime * 8);
                    }
                }
            }
        }
        
	}
}
