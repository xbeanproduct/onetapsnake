﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class AutoPlayGame : MonoBehaviour {

	public InputController userInput;
	// Use this for initialization
	void Start () {
		userInput = GameObject.FindObjectOfType<InputController> ();

		AutoClick ();
	}

	void AutoClick() {
		Sequence sequence = DOTween.Sequence ();
		sequence.AppendInterval (2).OnComplete(() => {
			if (userInput) {
				userInput.SnakeTurn();
			}
			else {
				userInput = GameObject.FindObjectOfType<InputController> ();
			}

			AutoClick();
		});
	}
	// Update is called once per frame
	void Update () {
		
	}
}
