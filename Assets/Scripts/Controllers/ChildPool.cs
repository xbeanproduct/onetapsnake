﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ChildPool : MonoBehaviour {

	public static ChildPool instance;
	public GameObject prefab;
	
	private List<GameObject> arrayChild;
	
	void Awake() {
		if (instance == null) {
			instance = this;
		}
	}
	// Use this for initialization
	void Start () {
		arrayChild = new List<GameObject>();
	}
	
	public GameObject GetChild() {
		foreach(GameObject obj in arrayChild) {
			if (!obj.activeInHierarchy) {
				return obj;
			}
		}
		
		GameObject newObj = (GameObject)Instantiate(prefab, prefab.transform.position, prefab.transform.rotation);
		arrayChild.Add(newObj);
		return newObj;
		
	}
	
	public GameObject GetChildAtPos(Vector2 position) {
		foreach(GameObject obj in arrayChild) {
			if (!obj.activeInHierarchy) {
				obj.transform.position = position;
				return obj;
			}
		}
		
		GameObject newObj = (GameObject)Instantiate(prefab, position, prefab.transform.rotation);
		arrayChild.Add(newObj);
		return newObj;
		
	}
	
	public void ClearPool() {
		arrayChild.Clear();
	}
	
	
	
	
	
	// Update is called once per frame
	void Update () {
	
	}
}
