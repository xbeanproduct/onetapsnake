﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FemaleSnake : MonoBehaviour {

	SnakeSegment segment;
	
    public GameObject snakeHelmet;
	public List<GameObject> childList;
    private int pendingAddChildCount;
	
	public GameObject lastChild;
	
	[SerializeField] GameObject snakeHolder;
	[SerializeField] GameObject snakeCornerPref;
	[SerializeField] GameObject snakeCornerFlipPref;
	[SerializeField] GameObject snakeSegmentPref;
    
    
    public Sprite tailSprite, segmentSprite;
    
    private GameObject midSegment, lastSegment;
    
    private bool isNearFood = false;
	
    public GameObject smokePref;
    public GameObject waterPref;
    public GameObject hitPref;
    
    private ParticleSystem helmetStarPS, helmetRingPS;
    
    public bool isStart = false;
    public bool isHaveHelmet = false; 
    public bool isTailReachedCorner = false;
    public bool isTurnRight = true;
    
	Vector2 turnPos;
	// Use this for initialization
	void Start () {
		
		segment = GetComponent<SnakeSegment>();
		childList = new List<GameObject>();
		
        midSegment = snakeHolder.FindInChildren(Settings.SnakeMidSegment);
        lastSegment = snakeHolder.FindInChildren(Settings.SnakeLastSegment);
		childList.Add(midSegment);
        childList.Add(lastSegment);
		
		if (childList.Count != 0) {
			lastChild = childList[childList.Count - 1];
		}
       
       
        helmetStarPS = snakeHelmet.GetComponent<ParticleSystem>();
        helmetRingPS = snakeHelmet.transform.GetChild(0).GetComponent<ParticleSystem>();
	}
	
	// Update is called once per frame
	void Update () {
        // Tail
        lastChild.GetComponent<SpriteRenderer>().sprite = tailSprite;
        
        
		if (!Common.ShareInstance().isFemaleTailReachedCorner && !Common.ShareInstance().isCantMove) {
            while (pendingAddChildCount > 0) {
                AddChild();
                pendingAddChildCount--;
            }
        }
        
        if (isHaveHelmet) {
           
            if (!helmetStarPS.emission.enabled) {
                var em = helmetStarPS.emission;
                var em1 = helmetRingPS.emission;
                em.enabled = true;
                em1.enabled = true;
            }
          
            
        }
        else {
            if (helmetStarPS.emission.enabled) {
                var em = helmetStarPS.emission;
                var em1 = helmetRingPS.emission;
                em.enabled = false;
                em1.enabled = false;
            }
        }
        
        
        bool isHaveFood = false;
        
        Collider2D[] cols = Physics2D.OverlapCircleAll(transform.position, 0.35f);
        foreach(Collider2D col in cols) {
            if ((col.tag.Equals(Settings.FoodTag) && !col.GetComponent<FoodController>().isEaten)  || 
                (col.tag.Equals(Settings.ReverseFoodTag) && !col.GetComponent<ReverseFoodController>().isEaten) &&
                !Common.ShareInstance().isCantMove) {
                if(!isNearFood) {
                    isNearFood = true;
                    GetComponent<Animator>().SetBool("SnakeSeeFood", isNearFood);
                    isHaveFood = true;
                    
                }
                else {
                    isHaveFood = true;
                }
                break;
            }
        }
        
        if (isNearFood && !isHaveFood) {
            isNearFood = false;
            GetComponent<Animator>().SetBool("SnakeSeeFood", isNearFood);
        }
        
	}
	
	void FixedUpdate() {
        
	}
	
	public void TurnRight()
	{
		turnPos = transform.position;
		if (childList.Count > 0) {
			AddCorner(snakeCornerPref);
		}
		segment.OnTurnRight();
	}
	
	public void TurnLeft()
	{
		turnPos = transform.position;
		if (childList.Count > 0) {
			AddCorner(snakeCornerFlipPref);
		}
		segment.OnTurnLeft();
	}
	
	void AddCorner(GameObject pref) {
		Instantiate(pref, turnPos, transform.rotation);
	}
	
	private Vector3 GetParentDirection(Quaternion parentRotation) {
		if (Mathf.Round(parentRotation.eulerAngles.z) == Settings.Direction.Up) {
			return new Vector2(0, -Settings.SnakeComponentSpace);
		}
		else if (Mathf.Round(parentRotation.eulerAngles.z) == Settings.Direction.Down) {
			return new Vector2(0, Settings.SnakeComponentSpace);
		}
		else if (Mathf.Round(parentRotation.eulerAngles.z) == Settings.Direction.Left) {
			return new Vector2(Settings.SnakeComponentSpace, 0);
		}
		else if (Mathf.Round(parentRotation.eulerAngles.z) == Settings.Direction.Right) {
			return new Vector2(-Settings.SnakeComponentSpace, 0);
		}

		return Vector2.zero;
	}
	
	public void AddChild() {
        if (Common.ShareInstance().isFemaleTailReachedCorner) {
            pendingAddChildCount++;
            return;
        }
        
		GameObject child;
		if (childList.Count == 0) {
			child = Instantiate(snakeSegmentPref, transform.position + GetParentDirection(transform.rotation), Quaternion.identity) as GameObject;
			//child = ChildPool.instance.GetChildAtPos(new Vector2(transform.position.x, transform.position.y) + GetParentDirection(transform.rotation));
			child.transform.rotation = transform.rotation;
		}
		else {
			child = Instantiate(snakeSegmentPref, lastChild.transform.position + GetParentDirection(lastChild.transform.rotation), Quaternion.identity) as GameObject;
			//child = ChildPool.instance.GetChildAtPos(new Vector2(lastChild.transform.position.x, lastChild.transform.position.y) + GetParentDirection(lastChild.transform.rotation));
			child.transform.rotation = lastChild.transform.rotation;	
		}
		
		
		SnakeSegment childComponent = child.GetComponent<SnakeSegment>();
		childComponent.isTail = true;
		if (childList.Count == 0) {
			segment.prevSegment = childComponent;
			childComponent.nextSegment = segment;
            if (segment.nextTeleportDestination) {
                childComponent.nextTeleportDestination = segment.nextTeleportDestination;
            }
		}
		else {
			lastChild.GetComponent<SnakeSegment>().prevSegment = childComponent;
			childComponent.nextSegment = lastChild.GetComponent<SnakeSegment>();
			lastChild.GetComponent<SnakeSegment>().isTail = false;
            lastChild.GetComponent<SpriteRenderer>().sprite = segmentSprite;
            if (lastChild.GetComponent<SnakeSegment>().nextTeleportDestination) {
                childComponent.nextTeleportDestination = lastChild.GetComponent<SnakeSegment>().nextTeleportDestination;
            }
		}
		
        // childComponent.FixPosition();
        
		childList.Add(child);
		
		lastChild = child;
		
		child.transform.parent = snakeHolder.transform;
	}
	
	public void ResetSnake() {
		foreach(GameObject child in childList) {
            if (!child.name.Equals(Settings.SnakeMidSegment) && !child.name.Equals(Settings.SnakeLastSegment)){
                Destroy(child);
            }
		}


		
		childList.Clear();
        childList.Add(midSegment);
        childList.Add(lastSegment);
        lastSegment.GetComponent<SpriteRenderer>().sprite = tailSprite;
        lastSegment.GetComponent<SnakeSegment>().isTail = true;
        
		ChildPool.instance.ClearPool();
		lastChild = lastSegment;

		GetComponent<InputController>().isWaitToTurn = false;
	}

	public void DestroyHelmet() {
		isHaveHelmet = false;
	}
    
    public void PlaySnakeEffect() {
        GetComponent<Animator>().SetTrigger(Settings.SnakeEatTrigger);
    }
    
    public void AddSmoke() {
        Instantiate(smokePref, transform.position, Quaternion.identity);
    }
    
    public void AddWater() {
        Instantiate(waterPref, transform.position, Quaternion.identity);
    }
    
    public void AddHit() {
        Instantiate(hitPref, transform.position, Quaternion.identity);
    }

	void OnTriggerEnter2D(Collider2D obj) {
		if (obj.tag.Equals(Settings.SnakeTail)) {
			GameManager.Instance.ReduceLife();
		}
	}
}
