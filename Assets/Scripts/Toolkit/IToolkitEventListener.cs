﻿using UnityEngine;

public interface IToolkitEventListener
{
	void OnToolkitPressed(Toolkit toolkit);

	void OnToolkitReleased(Toolkit toolkit);

	void OnToolkitTouchBegan(Toolkit toolkit, Vector3 position);

	void OnToolkitTouchMoved(Vector3 position);

	void OnToolkitTouchEnded(Toolkit toolkit, Vector3 position);
}
