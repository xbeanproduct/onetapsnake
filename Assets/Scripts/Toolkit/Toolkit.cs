﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Toolkit : MonoBehaviour
{
	/// <summary>
	/// The type of toolkit.
	/// </summary>
	public ToolkitType type;
	public int cost;
	public Text price;


	void OnEnable() {
		price.text = cost.ToString ();
	}

	public void OnClick()
	{
		ToolkitManager.Instance.OnToolkitClick(type, cost);

		SoundManager.Instance.PlaySound (SoundID.ButtonOpen);
	}
}
