﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Chronos;
using DG.Tweening;
using UnityEngine.EventSystems;

public class ToolkitManager : SimpleSingleton<ToolkitManager>
{
	public RectTransform toolkitHolder;

	[SerializeField]
	ToolkitType[] listToolkitType;
	List<Toolkit> listToolkit;

	public ToolkitSetting setting;
	public bool isShow = false;
    
    private Vector3 rootPos;
	private Vector3 targetPos;

	// Use this for initialization
	void Start () {

		// Set toolkit manager at Menu button position

		rootPos = transform.position;
		targetPos = transform.position - new Vector3(0, Screen.height, 0);

		transform.position = targetPos;
        
//        rootPos = transform.position + new Vector3(-8, 0);



		listToolkit = new List<Toolkit>();


		// Init list of toolkit
		foreach(ToolkitType type in listToolkitType)
		{			
			GameObject toolkit = Instantiate(setting.toolkitList[(int)type]);
		
			toolkit.transform.SetParent(toolkitHolder.transform);
//			toolkit.transform.position = rootPos;
			toolkit.GetComponent<RectTransform>().SetScale(1f);
//			toolkit.SetAlpha(0f, true);
			listToolkit.Add(toolkit.GetComponent<Toolkit>());

		}

    
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void ShowToolkits()
	{
		isShow = true;
		transform.DOMove (rootPos, .5f).SetEase (Ease.OutSine);
	}


	public void HideToolkits()
	{
		isShow = false;

		transform.DOMove (targetPos, .5f).SetEase (Ease.OutSine);
	}

	#region Toolkit event callback

	public void OnToolkitClick(ToolkitType type, int cost)
	{
		Debug.Log(string.Format("Use tookit: {0} with Cost: {1}", type, cost));
        
        switch (type)
        {
            case ToolkitType.TOOLKIT_REVERSE_FOOD:
                ReverseFoodTC(cost);
                break;
            case ToolkitType.TOOLKIT_SHIELD:
                ShieldTC(cost);    
                break;
            case ToolkitType.TOOLKIT_TIME_FREEZE:
                TimeFreezeTC(cost);
                break;
            case ToolkitType.TOOLKIT_STAR_EATER:
                StarEaterTC(cost);
                break;
            case ToolkitType.TOOLKIT_TIME_SLOW:
                TimeSlowTC(cost);
                break;
            case ToolkitType.TOOLKIT_IMMORTAL_SNAKE:
                ImmortalSnakeTC(cost);
                break;
            case ToolkitType.TOOLKIT_SURVIVAL:
                SurvivalTC(cost);
                break;
            case ToolkitType.TOOLKIT_MONSTER_HUNTER:
                MonsterHunterTC(cost);
                break;
			case ToolkitType.TOOLKIT_STAR_FRAGMENT_EATER:
				StarFragmentEaterTC (cost);
				break;
            default:
                break;
        }
        
	}

	public void OnToolkitDraging(ToolkitType type, Vector3 position)
	{
		Debug.Log(string.Format("Draging tookit: {0} at {1}", type, position));
        
	}

	public void OnToolkitFinishDrag(ToolkitType type, Vector3 position)
	{
		Debug.Log(string.Format("Place a tookit: {0} at {1}", type, position));
	}

	#endregion
    
    #region Toolkit Manager func
    public void ResetToolkitLife() {
        // Reset Time
        Clock clock = Timekeeper.instance.Clock("Monster");
        clock.localTimeScale = 1;
        
        // Reset Immortal
        Common.ShareInstance().isImmortal = false;
    }
    
    #endregion
    
    #region Toolkit Component
    
	void ReverseFoodTC(int cost) {
        if (!CheckSCoin(cost)) {
            return;
        }
        
		PopupManager.Instance.OpenPopupToolkitConfirm(cost, ToolkitType.TOOLKIT_REVERSE_FOOD, () => {
			DOVirtual.DelayedCall(.5f, () => {
				Common.ShareInstance().isTurnRight = !Common.ShareInstance().isTurnRight;
				if (GameObject.FindGameObjectWithTag(Settings.SnakeHead)) {
					ObjectPool.instance.GetObjectForType(Settings.ReverseFoodEffectPool, false, 3).transform.position = GameObject.FindGameObjectWithTag(Settings.SnakeHead).transform.position;
				}
				SoundManager.Instance.PlaySound (SoundID.GetRevert);
			});

			Analytics.Instance.LogEvent(Analytics.Purchase, Analytics.PurchaseTypeToolkit, "Reverse Elixir", cost);

			SoundManager.Instance.PlaySound(SoundID.GetRevert);
		});
			
    }
    
   
    
	void ShieldTC(int cost) {
        if (Common.ShareInstance().isHaveHelmet) {
            // Show Popup Cant Use
            ShowPopupUseFail("Shield", "You already have shield.");
            return;
        }
        
        if(!CheckSCoin(cost)) {
            return;
        }
        
		PopupManager.Instance.OpenPopupToolkitConfirm (cost, ToolkitType.TOOLKIT_SHIELD, () => {
			if (GameObject.FindObjectOfType(typeof(SoundManager)) != null) {
				//SoundManager.Instance.PlayShield();    
			}
			DOVirtual.DelayedCall(.5f, () => {
				Common.ShareInstance().isHaveHelmet = true;
				if (GameObject.FindGameObjectWithTag(Settings.SnakeHead)) {
					ObjectPool.instance.GetObjectForType(Settings.ShieldParticleEffectPool, false, 3).transform.position = GameObject.FindGameObjectWithTag(Settings.SnakeHead).transform.position;
				}
				SoundManager.Instance.PlaySound (SoundID.GetShield);
			});

			Analytics.Instance.LogEvent(Analytics.Purchase, Analytics.PurchaseTypeToolkit, "Helmet", cost);

			SoundManager.Instance.PlaySound(SoundID.GetShield);
		});

    }
    
    void TimeFreezeTC(int cost) {
        if(!CheckSCoin(cost)) {
            return;
        }

		PopupManager.Instance.OpenPopupToolkitConfirm (cost, ToolkitType.TOOLKIT_TIME_FREEZE, () => {
			Clock clock = Timekeeper.instance.Clock("Monster");
			clock.LerpTimeScale(0, 0.5f);

			// Show Effect
			if (GameObject.Find("MainGameCanvas")) {
				GameObject freeze = GameObject.Find("MainGameCanvas").FindInChildren("FreezeEffect");
				if (freeze) {
					freeze.Show();
					freeze.transform.DOScale(new Vector2(1, 1), 1);
					SoundManager.Instance.PlaySound(SoundID.Success);
				}
			}

			StartCoroutine(ReturnToDefaultTime(clock, Settings.TimeFreezeDuration));

			Analytics.Instance.LogEvent(Analytics.Purchase, Analytics.PurchaseTypeToolkit, "Time freeze", cost);
		});
    }
    
    IEnumerator ReturnToDefaultTime(Clock clock, float delay) {
        yield return new WaitForSeconds(delay);

		if (GameObject.Find("MainGameCanvas")) {
			if (GameObject.Find ("MainGameCanvas").FindInChildren ("FreezeEffect")) {
				GameObject freeze = GameObject.Find ("MainGameCanvas").FindInChildren ("FreezeEffect");
				if (freeze) {
					freeze.transform.DOScale(new Vector2(2, 2), 1).OnComplete(() => {
						freeze.Hide();
					});
				}
			}

			if (GameObject.Find ("MainGameCanvas").FindInChildren ("SlowEffect")) {
				GameObject slow = GameObject.Find ("MainGameCanvas").FindInChildren ("SlowEffect");
				if (slow) {
					slow.transform.DOScale(new Vector2(2, 2), 1).OnComplete(() => {
						slow.Hide();
					});
				}
			}
				
		}


        clock.LerpTimeScale(1, 0.5f);
    }
    
	void TimeSlowTC(int cost) {
        if (!CheckSCoin(cost)) {
            return;
        }
        
		PopupManager.Instance.OpenPopupToolkitConfirm (cost, ToolkitType.TOOLKIT_TIME_SLOW, () => {
			Clock clock = Timekeeper.instance.Clock("Monster");
			clock.LerpTimeScale(0.3f, 0.5f);

			if (GameObject.Find("MainGameCanvas")) {
				GameObject freeze = GameObject.Find("MainGameCanvas").FindInChildren("SlowEffect");
				if (freeze) {
					freeze.Show();
					freeze.transform.DOScale(new Vector2(1, 1), 1);
				}
			}

			StartCoroutine(ReturnToDefaultTime(clock, Settings.TimeSlowDuration));

			Analytics.Instance.LogEvent(Analytics.Purchase, Analytics.PurchaseTypeToolkit, "Time slow", cost);

			SoundManager.Instance.PlaySound(SoundID.Success);
		});
    }
    
   
    
	void StarEaterTC(int cost) {
        if(Common.ShareInstance().star > 0) {
			ShowPopupUseFail("Star Eater", "Can only use if you don't have any Golden Star.");
            return;
        }
        
        if(!CheckSCoin(cost)) {
            return;
        }  
        
		PopupManager.Instance.OpenPopupToolkitConfirm (cost, ToolkitType.TOOLKIT_STAR_EATER, () => {
			GameObject[] stars = GameObject.FindGameObjectsWithTag(Settings.StarTag);
			int rand = Random.Range(0, stars.Length);
			stars[rand].GetComponent<StarController>().EatStar();

//			if (GameObject.FindObjectOfType<GUIController>() != null) {
//				GameObject.FindObjectOfType<GUIController>().RecheckHighlight();
//			}

			Analytics.Instance.LogEvent(Analytics.Purchase, Analytics.PurchaseTypeToolkit, "Star Eater", cost);
			SoundManager.Instance.PlaySound(SoundID.GetStar);
		});
    }

	void StarFragmentEaterTC(int cost) {
		if (Common.ShareInstance ().starfragments > 3) {
			ShowPopupUseFail("Star Fragment Eater", "Can only use if you don't have any Golden Star.");
			return;
		}

		if(!CheckSCoin(cost)) {
			return;
		}  

		PopupManager.Instance.OpenPopupToolkitConfirm (cost, ToolkitType.TOOLKIT_STAR_FRAGMENT_EATER, () => {
			GameObject[] stars = GameObject.FindGameObjectsWithTag(Settings.StarFragmentTag);
			int rand = Random.Range(0, stars.Length);
			stars[rand].GetComponent<StarFragmentController>().EatStar();

//			if (GameObject.FindObjectOfType<GUIController>() != null) {
//				GameObject.FindObjectOfType<GUIController>().RecheckHighlight();
//			}

			Analytics.Instance.LogEvent(Analytics.Purchase, Analytics.PurchaseTypeToolkit, "Star Fragment Eater", cost);
			SoundManager.Instance.PlaySound(SoundID.GetStar);
		});
	}
    
    
	void ImmortalSnakeTC(int cost) {
		if (Common.ShareInstance ().isImmortal) {
			ShowPopupUseFail ("Immortal Snake", "You already use this.");
			return;
		}

        if (!CheckSCoin(cost)) {
            return;
        }
        
		PopupManager.Instance.OpenPopupToolkitConfirm (cost, ToolkitType.TOOLKIT_IMMORTAL_SNAKE, () => {
			Common.ShareInstance().isImmortal = true;

			if (GameObject.Find("MainGameCanvas")) {
				GameObject immortal = GameObject.Find("MainGameCanvas").FindInChildren("ImmortalEffect");
				if (immortal) {
					immortal.Show();
					immortal.transform.DOScale(new Vector2(1, 1), 1);
				}
			}

			StartCoroutine(ResetImmortal());

			SoundManager.Instance.PlaySound(SoundID.Success);
			Analytics.Instance.LogEvent(Analytics.Purchase, Analytics.PurchaseTypeToolkit, "Immortal", cost);
		});
 
    }
    
    IEnumerator ResetImmortal() {
        yield return new WaitForSeconds(Settings.ImmortalDuration);

		if (GameObject.Find ("MainGameCanvas").FindInChildren ("ImmortalEffect")) {
			GameObject immortal = GameObject.Find ("MainGameCanvas").FindInChildren ("ImmortalEffect");
			if (immortal) {
				immortal.transform.DOScale(new Vector2(2, 2), 1).OnComplete(() => {
					immortal.Hide();
				});
			}
		}

        Common.ShareInstance().isImmortal = false;
    }
    
	void SurvivalTC(int cost) {
		if (Common.ShareInstance ().isSurvivalSnake) {
			ShowPopupUseFail ("Survival Snake", "You already use this.");
			return;
		}

        if (!CheckSCoin(cost)) {
            return;
        }
        
		PopupManager.Instance.OpenPopupToolkitConfirm (cost, ToolkitType.TOOLKIT_SURVIVAL, () => {
			Common.ShareInstance().isSurvivalSnake = true;

			SoundManager.Instance.PlaySound(SoundID.Success);
			Analytics.Instance.LogEvent(Analytics.Purchase, Analytics.PurchaseTypeToolkit, "Survival", cost);
		});

    }
    
	void MonsterHunterTC(int cost) {
        if(!CheckSCoin(cost)) {
            return;
        }


        List<GameObject> monsters = new List<GameObject>();
        var monster = GameObject.FindGameObjectsWithTag(Settings.MonsterTag);
        foreach(GameObject obj in monster) {
            monsters.Add(obj);
        }
        
        var monsterCircle = GameObject.FindGameObjectsWithTag(Settings.MonsterCircleTag);
        foreach(GameObject obj in monsterCircle) {
            monsters.Add(obj);
        }
        
        var monsterPath = GameObject.FindGameObjectsWithTag(Settings.MonsterPathTag);
        foreach(GameObject obj in monsterPath) {
            monsters.Add(obj);
        }

		var monsterFish = GameObject.FindGameObjectsWithTag (Settings.MonsterFishTag);
		foreach (GameObject obj in monsterFish) {
			monsters.Add (obj);
		}
        
		if (monsters.Count > 0) {
			PopupManager.Instance.OpenPopupToolkitConfirm (cost, ToolkitType.TOOLKIT_MONSTER_HUNTER, () => {
				int rand = Random.Range (0, monsters.Count);

				Clock clock = Timekeeper.instance.Clock("Root");
				clock.LerpTimeScale(0, .3f);

				DOVirtual.DelayedCall(.5f, () => {
					ObjectPool.instance.GetObjectForType(Settings.KillMonsterEffectPool, false, 3).transform.position = monsters[rand].transform.position;
					DOVirtual.DelayedCall(1f, () => {
						monsters [rand].SetActive (false);

						clock.localTimeScale = 1;
					});
				});


				SoundManager.Instance.PlaySound(SoundID.MonsterDie);
				Analytics.Instance.LogEvent(Analytics.Purchase, Analytics.PurchaseTypeToolkit, "Monster Hunter", cost);
			});
		} else {
			ShowPopupUseFail ("Monster Hunter", "Don't have any Monster to kill.");
			return;
		}


		CheckTutorial ();
    }

	void CheckTutorial() {
		if (UserData.Instance.CompleteToolkitTutorial) {
			return;
		}

		if (GameObject.Find ("ToolkitTutorial") == null) {
			return;
		}

		GameObject.Find ("ToolkitTutorial").Hide();


		PopupManager.Instance.ShowToolkitConfirmTutorial (true);
	}
    
	void ScoreMultiplerTC(int cost) {
        if(!CheckSCoin(cost)) {
            return;
        }
    }
    
    bool CheckSCoin(int cost) {
		if (UserData.Instance.Scoin < cost) {
			// show popup
			PopupManager.Instance.OpenPopupSuggestScoin(cost - UserData.Instance.Scoin, false, InsufficientType.Toolkit);
			return false;
		}
        return true;
    }

	void UnPauseGame() {
		if (GameObject.FindGameObjectWithTag ("MenuButton")) {
			ExecuteEvents.Execute(GameObject.FindGameObjectWithTag ("MenuButton"), new PointerEventData(EventSystem.current), ExecuteEvents.pointerClickHandler);
		}

	}
    

    void ShowPopupUseFail(string title, string text) {
		PopupManager.Instance.OpenPopupNotification (title, text);
    }

    #endregion
}
