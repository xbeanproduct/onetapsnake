﻿using UnityEngine;

public enum ToolkitType
{
	TOOLKIT_REVERSE_FOOD = 0, // Done
	TOOLKIT_SHIELD, // Done
	TOOLKIT_MONSTER_HUNTER,
	TOOLKIT_TIME_FREEZE, // Done
    TOOLKIT_TIME_SLOW, // Done
//	TOOLKIT_SCORE_MULTIPLER,
//	TOOLKIT_STAR_MULTIPLER,
	TOOLKIT_SURVIVAL, // Done
	TOOLKIT_STAR_EATER, // Done
	TOOLKIT_IMMORTAL_SNAKE, // Done
	TOOLKIT_STAR_FRAGMENT_EATER
}

public static class BoosterTypeHelper
{	
	public static bool IsInstant(this ToolkitType type)
	{
		return !(type == ToolkitType.TOOLKIT_MONSTER_HUNTER);
	}
}