﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.Collections;

public class Grid : MonoBehaviour {

	public float width = 10;
    public float height = 10;
	
	public float gridSize = 1;
	public Color gridColor = Color.cyan;
	
	public bool showGrid = true;
	
	public char cloneGameObjectHotKey = 'a';
	
	public bool removeCanvas = true;
    
    public bool reGrid = true;
	
	public enum MapType {
		Normal, Scroll, Match
	}
	
	public MapType mapType;
    public int NumberOfMap = 1;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnEnable() {
		// EditorApplication.playmodeStateChanged += StateChange;
	}
	void StateChange() {
		// if (EditorApplication.isPlayingOrWillChangePlaymode && EditorApplication.isPlaying) {
		// 	Debug.Log("CHANGE");
		// 	if (!GameObject.Find("Canvas")) {
		// 		Debug.Log("Create Canvas Main UI");
		// 		EditorUtility.InstantiatePrefab(Resources.Load("Prefabs/GUI/Canvas"));
		// 	}
		// }	
		// else {
		// 	if (removeCanvas) {
		// 		if (GameObject.Find("Canvas")) {
		// 			DestroyImmediate(GameObject.Find("Canvas"));
		// 		}
		// 	}
		// 	else {
		// 		if (!GameObject.Find("Canvas")) {
		// 			Debug.Log("Create Canvas Main UI");
		// 			EditorUtility.InstantiatePrefab(Resources.Load("Prefabs/GUI/Canvas"));
		// 		}
		// 	}
		// }
	}
	void OnDrawGizmos()
	{
		if (!showGrid) {
			return;
		}
		Gizmos.color = gridColor;
		
		Vector3 pos = Camera.current.transform.position;
		
		for (float y = pos.y - height; y < pos.y + height; y+= gridSize)
		{
			Gizmos.DrawLine(new Vector3(-width, Mathf.Floor(y/gridSize) * gridSize, 0.0f),
							new Vector3(width, Mathf.Floor(y/gridSize) * gridSize, 0.0f));
		}
		
		for (float x = pos.x - width; x < pos.x + width; x+= gridSize)
		{
			Gizmos.DrawLine(new Vector3(Mathf.Floor(x/gridSize) * gridSize, -height, 0.0f),
							new Vector3(Mathf.Floor(x/gridSize) * gridSize, height, 0.0f));
		}
	}
}
