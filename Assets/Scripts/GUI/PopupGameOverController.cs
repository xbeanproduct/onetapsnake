﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections;

public class PopupGameOverController : PopupAnim {
		
        
    public Text gameOverText;    

	private bool isBtnClicked;
	// Use this for initialization
	void Start () {
        
	}
	
    void OnEnable() {
		OpenPopup (gameObject);

		isBtnClicked = false;
        int rand = Random.Range(0, StringConstrain.GameOverMsg.Length);
        gameOverText.text = StringConstrain.GameOverMsg[rand];

    }
    
	// Update is called once per frame
	void Update () {
	
	}
	
	public void PlayAgainBtn() {
		if (isBtnClicked)
			return;

		// Decrease Energy
//		if (UserData.Instance.Energy > 0) {
//			StatsManager.Instance.DescreaseEnergy();
//			FadePanel.Instance.LoadLevel(Application.loadedLevelName);
////			gameObject.SetActive(false);
//			DeactivePopupAnim(gameObject);
//
//			SoundManager.Instance.PlaySound (SoundID.ButtonOpen);
//
//			isBtnClicked = true;
//		}
//		else {
//			PopupManager.Instance.OpenPopupSuggestEnergy(false);	
//			SoundManager.Instance.PlaySound (SoundID.Wrong);
//		}

		FadePanel.Instance.LoadLevel(SceneManager.GetActiveScene().name);
		//			gameObject.SetActive(false);
		DeactivePopupAnim(gameObject);

		SoundManager.Instance.PlaySound (SoundID.ButtonOpen);

		isBtnClicked = true;
	}
	
	public void ReturnMapBtn() {
		if (isBtnClicked)
			return;

		isBtnClicked = true;

		FadePanel.Instance.LoadLevel(Settings.MapSelectScene);
//		GetComponent<Animator>().SetTrigger(Settings.ClosePopupTrigger);
		DeactivePopupAnim(gameObject);

		SoundManager.Instance.PlaySound (SoundID.ButtonChangeScene);
	}

}
