﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using DG.Tweening;

public class PopupToolkitHelp : PopupAnim {

	public GameObject reverseHelp,
	immortalHelp,
	monsterHunterHelp,
	shieldHelp,
	starEaterHelp,
	starFragmentEaterHelp,
	survivalHelp,
	timeFreezeHelp,
	timeSlowHelp;

	public Text title;
	public RectTransform tutorialImage;

	private GameObject currentHelp;

	private bool isBtnClicked;
	// Use this for initialization
	void Start () {

	}

	void OnEnable() {
		OpenPopup (gameObject);
		isBtnClicked = false;
	}

	public void SetText(ToolkitType type) {
		isBtnClicked = false;

		switch (type) {
		case ToolkitType.TOOLKIT_REVERSE_FOOD:
			title.text = "Reverse Elixir";
			ShowHelp (reverseHelp);
			break;
		case ToolkitType.TOOLKIT_IMMORTAL_SNAKE:
			title.text = "Immortal Snake";
			ShowHelp (immortalHelp);
			break;
		case ToolkitType.TOOLKIT_MONSTER_HUNTER:
			title.text = "Monster Hunter";
			ShowHelp (monsterHunterHelp);
			break;
		case ToolkitType.TOOLKIT_SHIELD:
			title.text = "Shield";
			ShowHelp (shieldHelp);
			break;
		case ToolkitType.TOOLKIT_STAR_EATER:
			title.text = "Star Eater";
			ShowHelp (starEaterHelp);
			break;
		case ToolkitType.TOOLKIT_STAR_FRAGMENT_EATER:
			title.text = "Star Fragment Eater";
			ShowHelp (starFragmentEaterHelp);
			break;
		case ToolkitType.TOOLKIT_SURVIVAL:
			title.text = "Survival";
			ShowHelp (survivalHelp);
			break;
		case ToolkitType.TOOLKIT_TIME_FREEZE:
			title.text = "Time Freeze";
			ShowHelp (timeFreezeHelp);
			break;
		case ToolkitType.TOOLKIT_TIME_SLOW:
			title.text = "Time Slow";
			ShowHelp (timeSlowHelp);
			break;
		}

	}

	void Update() {
		if (Input.GetKey (KeyCode.Escape)) {
			Cancel ();
		}
	}

	private void ShowHelp(GameObject pref) {
		currentHelp = pref.Create (tutorialImage.transform, tutorialImage.transform.position);
		currentHelp.GetComponent<CanvasGroup> ().alpha = 0;
		currentHelp.GetComponent<CanvasGroup> ().DOFade (1, 1f).SetEase (Ease.InSine);
	}

	private void HideHelp() {
		if (currentHelp != null) {
			currentHelp.GetComponent<CanvasGroup> ().DOFade (0, .5f).SetEase (Ease.OutSine)
				.OnComplete (() => {
					Destroy(currentHelp);
			});
		}
	}

	public void Cancel() {
		if (isBtnClicked)
			return;

		isBtnClicked = true;

//		GetComponent<Animator>().SetTrigger(Settings.ClosePopupTrigger);
		DeactivePopupAnim(gameObject);

		SoundManager.Instance.PlaySound (SoundID.ButtonOpen);

		HideHelp ();
	}
		
}
