﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System;
using System.Linq;

public class PopupChangeSkin : PopupAnim {

	public GameObject content;
	public GameObject buttonSkinPref;

	public RectTransform dau, than0, than1, than2;

	public List<ButtonSkin> buttonList;
	[SerializeField]int _selectedButtonIndex;
	[SerializeField] GameObject selectBtnText;
	[SerializeField] GameObject selectBtn;


	private bool isLoadSkin = false;

	private bool isPurchased = false;

	private Color chooseColor = new Color (255f / 255, 142f / 255, 73f / 255);
	private Color usedColor = new Color (203f / 255, 118f / 255, 1);
	private Color buyColor = new Color(143f / 255, 142f / 255, 141f / 255);

	private int[] cost = {
		0, 
		100,
		200,
		400,
		700,
		700,
		1500,
		2500,
		2500
	};
		

	private bool isBtnClicked = false;
	// Use this for initialization
	void Start () {
		
	}

	void OnEnable() {
		OpenPopup (gameObject);
		isBtnClicked = false;
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey (KeyCode.Escape)) {
			OnExit ();
		}
	}

	public void OnShowPopup()
	{
		LoadSkin ();
		gameObject.SetActive(true);

		_selectedButtonIndex = UserData.Instance.SelectedSkinIndex;
		OnSkinSelected(_selectedButtonIndex);

		Analytics.Instance.LogEvent (Analytics.Popup, Analytics.PopupTypeSkin, "Open", 0);
	}

	public void LoadSkin() {
	
		if (isLoadSkin) {
			return;
		}

		isLoadSkin = true;

		Debug.Log ("Load Skin");
		buttonList = new List<ButtonSkin>();

		var index = 0;
		while (Resources.Load (string.Format ("Snake/Skin{0}/dau", index)) != null) {
			GameObject newBtn = buttonSkinPref.CreateUI (content.transform);

			Sprite newSprite = Resources.Load<Sprite> (string.Format ("Snake/Skin{0}/skin_snake", index));
//			newBtn.GetComponent<Image> ().sprite = newSprite;
			newBtn.FindInChildren ("Image").GetComponent<Image> ().sprite = newSprite;
			newBtn.GetComponent<ButtonSkin> ().skinIndex = index;
			newBtn.GetComponent<ButtonSkin> ().price = cost [index];

			buttonList.Add (newBtn.GetComponent<ButtonSkin>());

			index++;
		}

		foreach (ButtonSkin btn in buttonList) {
			int thisIndex = btn.GetComponent<ButtonSkin> ().skinIndex;
			btn.GetComponent<Button> ().onClick.AddListener (() => {
				OnSkinSelected(thisIndex);
			});
		}
	}

	public void OnSkinSelected(int buttonIndex)
	{
		Sprite dauSprite = Resources.Load<Sprite> (string.Format ("Snake/Skin{0}/dau", buttonIndex));
		if (dauSprite) {
			if (dau) {
				dau.GetComponent<Image> ().sprite = dauSprite;
			}
		}
		Sprite thanSprite = Resources.Load<Sprite> (string.Format ("Snake/Skin{0}/than", buttonIndex));
		if (thanSprite) {
			if (than0) {
				than0.GetComponent<Image> ().sprite = thanSprite;
			}
			if (than1) {
				than1.GetComponent<Image> ().sprite = thanSprite;
			}
			if (than2) {
				than2.GetComponent<Image> ().sprite = thanSprite;
			}
		}

		isPurchased = false;
		foreach (int item in UserData.Instance.SkinPurchasedIndexList) {
			if (item == buttonIndex) {
				isPurchased = true;
				break;
			}
		}

		if (isPurchased) {
			if (buttonIndex == UserData.Instance.SelectedSkinIndex) {
				selectBtn.GetComponent<Image> ().color = usedColor;
				selectBtnText.SetText ("Used");
			} else {
				selectBtn.GetComponent<Image> ().color = chooseColor;
				selectBtnText.SetText ("Use");
			}
		} else {
			selectBtn.GetComponent<Image> ().color = buyColor;
			selectBtnText.SetText (buttonList [buttonIndex].price.ToString() + " Coins");
		}

		Debug.Log(string.Format("Button Press: {0} -- {1}",_selectedButtonIndex, buttonIndex));
		// Highlight selected button 

		buttonList[_selectedButtonIndex].SetSelected(false);

		_selectedButtonIndex = buttonIndex;
		buttonList[_selectedButtonIndex].SetSelected(true);

		// Update user data

		// Log
		Analytics.Instance.LogEvent(Analytics.Popup, Analytics.PopupTypeSkin, "Select Skin", _selectedButtonIndex);
	}

	public void OnButtonPress()
	{
		SoundManager.Instance.PlaySound (SoundID.ButtonOpen);
	}

	public void OnUsedSkin() {
		if (isPurchased) {
			UserData.Instance.SelectedSkinIndex = _selectedButtonIndex;
			UserData.Instance.Save();

			selectBtn.GetComponent<Image> ().color = usedColor;
			selectBtnText.SetText ("Used");

			Analytics.Instance.LogEvent (Analytics.Popup, Analytics.PopupTypeSkin, "Switch Skin", _selectedButtonIndex);

			SoundManager.Instance.PlaySound (SoundID.ButtonOpen);
		} else {
			if (!CheckSCoin(buttonList [_selectedButtonIndex].price)) {
				return;
			}
				
			int price = buttonList [_selectedButtonIndex].price;

			PopupManager.Instance.OpenPopupNotificationConfirm (StringConstrain.SkinConfirmTitle, StringConstrain.SkinConfirmMsgFormat(price), () => {

				StatsManager.Instance.DecreaseScoin(price);

				UserData.Instance.SelectedSkinIndex = _selectedButtonIndex;
				UserData.Instance.SkinPurchasedIndexList.Add(_selectedButtonIndex);
				UserData.Instance.Save();
				selectBtn.GetComponent<Image> ().color = usedColor;
				selectBtnText.SetText ("Used");
				isPurchased = true;

				Analytics.Instance.LogEvent(Analytics.Purchase, Analytics.PurchaseTypeSkin, string.Format("Skin index {0}", _selectedButtonIndex.ToString("00")), buttonList[_selectedButtonIndex].price);
			});

			Analytics.Instance.LogEvent (Analytics.Popup, Analytics.PopupTypeSkin, "Buy Skin", _selectedButtonIndex);

			SoundManager.Instance.PlaySound (SoundID.Wrong);
		}
	}

	bool CheckSCoin(int cost) {
		if (UserData.Instance.Scoin < cost) {
			// show popup
			PopupManager.Instance.OpenPopupSuggestScoin(cost - UserData.Instance.Scoin, false, InsufficientType.Skin);
			return false;
		}
		return true;
	}

	public void OnExit()
	{
		if (isBtnClicked) {
			return;
		}
		isBtnClicked = true;
//		gameObject.GetComponent<Animator>().SetTrigger(Settings.ClosePopupTrigger);
		buttonList[_selectedButtonIndex].SetSelected(false);
		DeactivePopupAnim(gameObject);
	

		SoundManager.Instance.PlaySound (SoundID.ButtonClose);
	}
    
    
}
