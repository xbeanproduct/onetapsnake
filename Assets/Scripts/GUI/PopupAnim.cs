﻿using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using System.Collections;
using UnityEngine.SceneManagement;

public class PopupAnim : MonoBehaviour {


	public void OpenPopup(GameObject parent) {
		if (SceneManager.GetActiveScene ().name.Equals (Settings.MapSelectScene)) { 
			GoogleAds.Instance.HideBanner ();
		}
		parent.SetActive (true);

		RectTransform bg = parent.FindInChildren ("BG").GetComponent<RectTransform> ();
		RectTransform popup = parent.FindInChildren ("Popup").GetComponent<RectTransform> ();
		RectTransform title = parent.FindInChildren ("Title").GetComponent<RectTransform> ();

		if (bg == null || popup == null || title == null) {
			return;
		}

		bg.GetComponent<Image> ().SetAlpha (0);
		popup.localScale = Vector3.zero;
		title.localScale = Vector3.zero;
		DOTween.Sequence ()
			.Append (popup.DOScale (Vector3.one, .4f).SetEase (Ease.OutBack))
			.Join (bg.GetComponent<Image> ().DOFade (.7f, .5f))
			.Join (title.DOScale (Vector3.one, .3f).SetEase (Ease.OutBack).SetDelay (.3f));
	}

	public void DestroyPopupAnim(GameObject parent) {
		RectTransform bg = parent.FindInChildren ("BG").GetComponent<RectTransform> ();
		RectTransform popup = parent.FindInChildren ("Popup").GetComponent<RectTransform> ();
		RectTransform title = parent.FindInChildren ("Title").GetComponent<RectTransform> ();

		DOTween.Sequence ()
			.Append (title.DOScale (Vector2.zero, .2f).SetEase (Ease.InBack))
			.Join (bg.GetComponent<Image> ().DOFade (0, .5f))
			.Join (popup.DOScale (Vector2.zero, .3f).SetEase (Ease.InBack).SetDelay (.2f))
			.OnComplete (() => {
				DestroyPopup(parent);
				if (SceneManager.GetActiveScene ().name.Equals (Settings.MapSelectScene)) { 
					GoogleAds.Instance.ShowBanner();
				}
			});
	}

	public void DeactivePopupAnim(GameObject parent, System.Action callback = null) {
		RectTransform bg = parent.FindInChildren ("BG").GetComponent<RectTransform> ();
		RectTransform popup = parent.FindInChildren ("Popup").GetComponent<RectTransform> ();
		RectTransform title = parent.FindInChildren ("Title").GetComponent<RectTransform> ();

		DOTween.Sequence ()
			.Append (title.DOScale (Vector3.zero, .2f).SetEase (Ease.InBack))
			.Join (bg.GetComponent<Image> ().DOFade (0, .5f))
			.Join (popup.DOScale (Vector3.zero, .3f).SetEase (Ease.InBack).SetDelay (.2f))
			.OnComplete (() => {
				DeactivePopup(parent);
				if (SceneManager.GetActiveScene ().name.Equals (Settings.MapSelectScene)) { 
					GoogleAds.Instance.ShowBanner();
				}
				if (callback != null) {
					callback();
				}
			});
	}



	private void DestroyPopup(GameObject obj) {
		Destroy(obj);
	}

	private void DeactivePopup(GameObject obj) {
		obj.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {

	}
}
