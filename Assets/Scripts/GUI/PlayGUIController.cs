﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Facebook.Unity;


public class PlayGUIController : MonoBehaviour {

    public GameObject tapToPlay;

	private bool isBtnClicked;
    void Awake() {
        Application.targetFrameRate = Settings.TargetFrameRate;
    }
	// Use this for initialization
	void Start () {
		isBtnClicked = false;
		SoundManager.Instance.PlayMusic (SoundID.MainMenu);


	}
	
	// Update is called once per frame
	void Update () {
	    if (Input.GetMouseButtonDown(0)) {
            // FadePanel.Instance.LoadLevel(Settings.MapSelectScene);
			if (isBtnClicked) return;
			isBtnClicked = true;

            Play();
        }
        
        if (Input.touchCount > 0) {
            Touch touch = Input.GetTouch(0);
            if (touch.phase == TouchPhase.Began) {
				if (isBtnClicked)
					return;

				isBtnClicked = true;
                Play();    
            }
            
        }


	}

	
	public void Play() {
		// TransitionManager.Instance.FadeTransitionScene(Settings.MapSelectScene);
		if (!UserData.Instance.CompleteBeginTutorial) {
			FadePanel.Instance.LoadLevel ("Tutorial1");
//			FadePanel.Instance.LoadLevel (Settings.MapSelectScene);
		}
		else {
	        tapToPlay.SetActive(false);
	        if (UserData.Instance.IsFBLogin) {
	            PopupManager.Instance.ShowActivityIndicator();
	            FacebookManager.Instance.InitFacebook(result => {
	                if (result) {
	                    Debug.Log("Init FB Successful");
	                    if (Helper.IsOnline()) {
	                        ApiManager.Instance.LoginSync(response => {
	                            PopupManager.Instance.HideActivityIndicator();
	                            if (response != null && response.Code == Settings.SuccessCode) {
	                                Debug.Log("Login Successful");
	                                ApiManager.Instance.isServerLogin = true;
	                            }
	                            FadePanel.Instance.LoadLevel(Settings.MapSelectScene);
	                        });  
	                    }
	                    else {
	                        PopupManager.Instance.HideActivityIndicator();
	                        FadePanel.Instance.LoadLevel(Settings.MapSelectScene);
	                    }
	               
	                } 
	            });
	        }
	        else {
	            FacebookManager.Instance.InitFacebook(result => {});
	            FadePanel.Instance.LoadLevel(Settings.MapSelectScene);
	        }
	        

		}

		SoundManager.Instance.PlaySound (SoundID.ButtonChangeScene);
		
	}
    
    public void ShowAds() {
        #if !UNITY_WEBGL
//		UnityAds.Instance.WatchVideoForEnergy(1);
        #endif
    }
}
