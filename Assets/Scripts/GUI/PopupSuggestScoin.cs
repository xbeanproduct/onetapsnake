﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public enum InsufficientType {
	Toolkit,
	Skin,
	UnlockMap,
	None
}

public class PopupSuggestScoin : PopupAnim {

	public Text messageText;
	public Text title;

	// Coin package text
	public Text package1Text;
	public Text package2Text;
	public Text package3Text;
	public Text package4Text;

	private bool _isBuy;
	private InsufficientType _type;

	private bool isBtnClicked = false;
	// Use this for initialization
	void Start () {
		if (package1Text != null)
		{
			package1Text.text = string.Format ("<color=#f47300>{0}</color> <color=#1f7acf>Coins</color>", CoinPackage.Package1.GetCoins ().ToString ());
		}

		if (package2Text != null)
		{
			package2Text.text = string.Format ("<color=#f47300>{0}</color> <color=#1f7acf>Coins</color>", CoinPackage.Package2.GetCoins ().ToString ());
		}

		if (package3Text != null)
		{
			package3Text.text = string.Format ("<color=#f47300>{0}</color> <color=#1f7acf>Coins</color>", CoinPackage.Package3.GetCoins ().ToString ());
		}

		if (package4Text != null)
		{
			package4Text.text = string.Format ("<color=#f47300>{0}</color> <color=#1f7acf>Coins</color>", CoinPackage.Package4.GetCoins ().ToString ());
		}

		Debug.Log("Start buy coins ...");
	}

	void OnEnable() {
		OpenPopup (gameObject);
		isBtnClicked = false;
	}

	public void SetText(int value, bool isBuy, InsufficientType type) {
		_isBuy = isBuy;
		_type = type;
		if (isBuy) {
			title.text = StringConstrain.BuyScoinTitle;
			int rand = UnityEngine.Random.Range(0, StringConstrain.BuyScoinMsg.Length);
			messageText.text = StringConstrain.BuyScoinMsg[rand];

			Analytics.Instance.LogEvent (Analytics.Popup, Analytics.PopupTypeBuyCoin, "Open", 0);
		} else {
			title.text = StringConstrain.InsufficientTitle;

			switch (type) {
			case InsufficientType.Toolkit:
				messageText.text = StringConstrain.ToolkitInsufficientMsgFormat (value);
				break;
			case InsufficientType.Skin:
				messageText.text = StringConstrain.SkinInsufficientMsgFormat (value);
				break;
			case InsufficientType.UnlockMap:
				messageText.text = StringConstrain.UnlockMapInsufficientMsgFormat (value);
				break;
			}

			Analytics.Instance.LogEvent (Analytics.Popup, string.Format ("{0} for {1}", Analytics.PopupTypeSuggestCoin, type.ToString ()), "Open", 0);
		}
	}


	// Update is called once per frame
	void Update () {
		if (Input.GetKey (KeyCode.Escape)) {
			ClosePopup ();
		}
	}

	public void BuyPackage1()
	{
		BuyPackage(CoinPackage.Package1);

	}

	public void BuyPackage2()
	{
		BuyPackage(CoinPackage.Package2);

	}

	public void BuyPackage3()
	{
		BuyPackage(CoinPackage.Package3);

	}

	public void BuyPackage4()
	{
		BuyPackage(CoinPackage.Package4);

	}

	void BuyPackage(CoinPackage package)
	{
		// Play sound
		SoundManager.Instance.PlaySound (SoundID.ButtonClose);

		if (!Helper.IsOnline())
		{
			
			return;
		}


		// Purchase
		Purchaser purchaser = GetComponent<Purchaser>();

		if (purchaser != null)
		{
			purchaser.BuyPackage(package, (purchasedPackage) => {
				StatsManager.Instance.IncreaseScoin (package.GetCoins());
			});
		}
	}

	public void FreeScoin() {
		UnityAds.Instance.WatchVideoForScoin (5);
		ClosePopup ();

		Analytics.Instance.LogEvent (Analytics.Popup, _isBuy ? 
			Analytics.PopupTypeBuyCoin : 
			string.Format ("{0} for {1}", Analytics.PopupTypeSuggestCoin, _type.ToString ()), "Watch Ads", 0);

		SoundManager.Instance.PlaySound (SoundID.ButtonOpen);
	}

	public void AskFriendBtn() {
		FacebookManager.Instance.RequestFriend (StringConstrain.InviteFriendTitle, StringConstrain.InviteFriendMsg, RequestObject.Coin);

		Analytics.Instance.LogEvent (Analytics.Popup, _isBuy ? Analytics.PopupTypeBuyEnergy : Analytics.PopupTypeSuggestEnergy, "Ask Friend", 0);
		SoundManager.Instance.PlaySound (SoundID.ButtonOpen);
	}

	public void ClosePopup() {
		if (isBtnClicked) {
			return;
		}
		isBtnClicked = true;
//		GetComponent<Animator>().SetTrigger(Settings.ClosePopupTrigger);
		DeactivePopupAnim(gameObject);

		SoundManager.Instance.PlaySound (SoundID.ButtonClose);
	}

}
