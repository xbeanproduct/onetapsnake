﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class StarFragmentEffect : MonoBehaviour {

    public int starIndex;
    GameObject star1, star2, star3;
    
//    Vector2 _startPoint;
    Vector3 _endPoint;
    
    float timeToDance = 2;
    float timeToMove = 2f;
    
//    float _timeStartedLerping;
    
	// Use this for initialization
	void Start () {
	   if (!star1) {
            if (GameObject.Find(Settings.StarUI1)) {
                star1 = GameObject.Find(Settings.StarFragmentUI1);
            }
            
            if (GameObject.Find(Settings.StarUI2)) {
                star2 = GameObject.Find(Settings.StarFragmentUI2);
            }
            
            if (GameObject.Find(Settings.StarUI3)) {
                star3 = GameObject.Find(Settings.StarFragmentUI3);
            }
        }
        
//        _startPoint = transform.position;
//        _timeStartedLerping = Time.time;
        
        Camera camera = Camera.main;
        switch(starIndex) {
            case 1:
                _endPoint = Camera.main.ScreenToWorldPoint(star1.transform.position);
                break;
            case 2:
                _endPoint = Camera.main.ScreenToWorldPoint(star2.transform.position);
                break;
            case 3:
                _endPoint = Camera.main.ScreenToWorldPoint(star3.transform.position);
                break;
        }
        
        StartCoroutine(Dancing());
	}
    
    IEnumerator Dancing() {
        Vector3 danceLocation = transform.position + new Vector3(0, 1);
        Vector3 newScale = new Vector3(0.4f, 0.4f);
        float ratio = 0;
        float multiplier = 1 / timeToDance;
        
        while(transform.position != danceLocation) {
            ratio += Time.fixedDeltaTime * multiplier;
            
            transform.position = Vector3.Slerp(transform.position, danceLocation, Mathf.SmoothStep(0.0f, 1.0f, ratio));
            transform.localScale = Vector3.Lerp(transform.localScale, newScale, Mathf.SmoothStep(0.0f, 1.0f, ratio));
            yield return null;
        }
        
        GetComponent<Animator>().enabled = true;
        GetComponent<Animator>().SetTrigger("RotateStar");
    }
    
    public void MoveStar() {
        StartCoroutine(MoveToUI());
    }
    
    IEnumerator MoveToUI() {
        float ratio = 0;
        float multiplier = 1 / timeToMove;
        
        while(Vector3.Distance(transform.position, _endPoint) > 0.5f) {
            ratio += Time.fixedDeltaTime * multiplier;
            
            switch(starIndex) {
                case 1:
                    _endPoint = Camera.main.ScreenToWorldPoint(star1.transform.position);
                    break;
                case 2:
                    _endPoint = Camera.main.ScreenToWorldPoint(star2.transform.position);
                    break;
                case 3:
                    _endPoint = Camera.main.ScreenToWorldPoint(star3.transform.position);
                    break;
            }
            
            transform.position = Vector3.Lerp(transform.position, _endPoint, Mathf.SmoothStep(0.0f, 1.0f, ratio));
            yield return null;
        }
        
        AddGUIStar();            
        Destroy(gameObject);
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        // float timeSinceStarted = Time.time - _timeStartedLerping;
        // float percentageComplete = timeSinceStarted / timeToMove;
        
        // transform.position = Vector2.Lerp(_startPoint, _endPoint, percentageComplete);
        
        // if (percentageComplete >= 1) {
        //     AddGUIStar();            
        //     Destroy(gameObject);
        // }
	}
    
    void AddGUIStar() {
        switch(starIndex) {
            case 1:
            star1.GetComponent<Image>().enabled = true;
//            Instantiate(starParticle, Camera.main.ScreenToWorldPoint(star1.transform.position), Quaternion.identity);
			ObjectPool.instance.GetObjectForType(Settings.StarFragmentParticleEffectPool, false, 3).transform.position = Camera.main.ScreenToWorldPoint(star1.transform.position);
            break;
            case 2:
            star2.GetComponent<Image>().enabled = true;
//            Instantiate(starParticle, Camera.main.ScreenToWorldPoint(star2.transform.position), Quaternion.identity);
			ObjectPool.instance.GetObjectForType(Settings.StarFragmentParticleEffectPool, false, 3).transform.position = Camera.main.ScreenToWorldPoint(star2.transform.position);
            break;
            case 3:
            star3.GetComponent<Image>().enabled = true;
//            Instantiate(starParticle, Camera.main.ScreenToWorldPoint(star3.transform.position), Quaternion.identity);
			ObjectPool.instance.GetObjectForType(Settings.StarFragmentParticleEffectPool, false, 3).transform.position = Camera.main.ScreenToWorldPoint(star3.transform.position);
            GameManager.Instance.CompileStarFragment();
            break;
        }
        
           
    }
    
    
}
