﻿using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class PopupScoreLeaderboard : PopupAnim {

	// Use this for initialization

	private bool isBtnClicked = false;

	void OnEnable() {
		OpenPopup (gameObject);
	}

	// Update is called once per frame
	void Update () {
		if (Input.GetKey (KeyCode.Escape)) {
			CloseBtn ();
		}
	}
		
    
    public void CloseBtn() {
		if (isBtnClicked) {
			return;
		}
		isBtnClicked = true;
//		GetComponent<Animator>().SetTrigger(Settings.ClosePopupTrigger);

		DestroyPopupAnim (gameObject);

		SoundManager.Instance.PlaySound (SoundID.ButtonClose);
	}

	public void OnButtonPress()
	{
		SoundManager.Instance.PlaySound (SoundID.ButtonOpen);
		Debug.Log ("Play button press");
	}
    
}
