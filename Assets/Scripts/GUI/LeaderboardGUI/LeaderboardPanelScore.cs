﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Facebook.MiniJSON;
using EnhancedUI.EnhancedScroller;

public class LeaderboardPanelScore : EnhancedScrollerCellView {

    public Text rank;
    public RawImage avatar;
    public Text username;
    public Text score;
	public Texture defaultImage;
    
	public void SetData(ScrollerData data) {
		this.username.text = data.name;
		this.score.text = data.score;
		this.rank.text = data.rank;

	}
    
	public void LoadImage(ScrollerData data) {
		StartCoroutine(LoadImage(data.url));
	}

	public void SetDefaultImage() {
		avatar.texture = defaultImage;
	}

	public void SetImage(Texture image) {
		avatar.texture = image;
	}

    public void SetAttribute(int rankID, string avatar, string username, int score) {
        this.username.text = username;
        this.score.text = score.ToString();
        this.rank.text = rankID.ToString();
        StartCoroutine(LoadImage(avatar));
    }
    
    IEnumerator LoadImage(string url) {
        url = url + "&redirect=false";
        WWW www = new WWW(url);
        yield return www;
		if (www.error == null) {
			Dictionary<string, object> dict = Json.Deserialize (www.text) as Dictionary<string, object>;
			Dictionary<string, object> dataDict = dict ["data"] as Dictionary<string,object>;
			string imageUrl = (string)dataDict ["url"];
			WWW www2 = new WWW (imageUrl);
			yield return www2;
			if (www2.error == null) {
				avatar.texture = www2.texture;
			}
            
            
		} else {
			avatar.texture = null;
		}
    }


    
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}

public class ScrollerData {
	public string rank;
	public string name;
	public string url;
	public string score;
	public Texture image;
	public bool isLoaded;
}
