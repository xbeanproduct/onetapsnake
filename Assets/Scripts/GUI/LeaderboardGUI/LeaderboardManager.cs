﻿using UnityEngine;
using System.Collections.Generic;

public class LeaderboardManager : MonoBehaviour {


    public Transform listGlobal;
    public Transform listFriend;
    
    public GameObject scorePanelPref;
	// Use this for initialization
	void Start () {
        LoadFriendScoreLeaderboard();
        LoadGlobalScoreLeaderboard();
        
	}
    
    public void LoadGlobalScoreLeaderboard() {
        ApiManager.Instance.GetScoresLeaderboard(1, response => {
            if (response.Code == Settings.SuccessCode) {
                List<ApiLeaderboardScore> list = response.Data;
                for(int i = 0; i < list.Count; i++ ) {
                    GameObject scorePanel = (GameObject)Instantiate(scorePanelPref, listGlobal.position, Quaternion.identity);
                    string name = list[i].Name;
                    string avatar = list[i].Image;
                    int score = list[i].Score;
                    
                    
                    scorePanel.GetComponent<LeaderboardPanelScore>().SetAttribute(i + 1, avatar, name, score);
                    scorePanel.GetComponent<RectTransform>().SetParent(listGlobal);
                    scorePanel.transform.localScale = new Vector3(1, 1, 1);
                }
            }
        });
    }
    
    public void LoadFriendScoreLeaderboard() {
        ApiManager.Instance.GetFriendScoresLeaderboard(1, response => {
            if (response != null && response.Code == Settings.SuccessCode) {
                List<ApiLeaderboardScore> list = response.Data;
                for(int i = 0; i < list.Count; i++ ) {
                    GameObject scorePanel = (GameObject)Instantiate(scorePanelPref, listGlobal.position, Quaternion.identity);
                    string name = list[i].Name;
                    string avatar = list[i].Image;
                    int score = list[i].Score;
                    
                    
                    scorePanel.GetComponent<LeaderboardPanelScore>().SetAttribute(i + 1, avatar, name, score);
                    scorePanel.GetComponent<RectTransform>().SetParent(listFriend);
                    scorePanel.transform.localScale = new Vector3(1, 1, 1);
                }
            }
        });
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
