﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Collections;
using EnhancedUI.EnhancedScroller;
using Facebook.MiniJSON;

public class GlobalStarsLeaderboardController : MonoBehaviour, IEnhancedScrollerDelegate {

    public bool isLoadNewData = false;
    public bool isOutOfData = false;
    public int currentPage = 0;
    
    ScrollRect scrollRect;

	private List<ScrollerData> _data;


	public EnhancedScroller scroller;
	public LeaderboardPanelScore panelScorePref;
    
    void Awake() {
        if(!scrollRect) {
            scrollRect = GetComponent<ScrollRect>();
        }
    }
    
	// Use this for initialization
	void Start () {
		_data = new List<ScrollerData>();
		scroller.Delegate = this;
		scroller.cellViewVisibilityChanged = CellViewVisibilityChanged;
	   LoadGlobalData();
	}
    
    
	// Update is called once per frame
	void Update () {
      
        if (scrollRect.verticalNormalizedPosition < 0) {
            if (!isLoadNewData && !isOutOfData) {
                isLoadNewData = true;
                LoadGlobalData();
            }
        }
     
        
	}

    
    void LoadGlobalData() {
        currentPage++;
        ApiManager.Instance.GetStarsLeaderboard(currentPage, response => {
            if (response != null && response.Code == Settings.SuccessCode) {
                if (scrollRect == null) {
                    isLoadNewData = false;
                    return;
                }
                
                List<ApiLeaderboardStar> list = response.Data;
                if (list.Count == 0) {
                    isOutOfData = true;
                    isLoadNewData = false;
                    return;
                }
                
				for(int i = 0; i < list.Count; i++) {

					ScrollerData data = new ScrollerData() {
						name = list[i].Name,
						score = list[i].Star.ToString(),
						rank = list[i].Rank.ToString(),
						url = list[i].Image,
						isLoaded = false
					};



					_data.Add(data);

				}
                
				scroller.ReloadData();
                isLoadNewData = false;
            }
        });
    }

	public int GetNumberOfCells(EnhancedScroller scroller)
	{
		return _data.Count;
	}

	public float GetCellViewSize(EnhancedScroller scroller, int dataIndex)
	{
		return 115f;
	}

	public EnhancedScrollerCellView GetCellView(EnhancedScroller scroller, int
		dataIndex, int cellIndex)
	{
		LeaderboardPanelScore cellView = scroller.GetCellView (panelScorePref) as
			LeaderboardPanelScore;
		cellView.SetData (_data [cellIndex]);
		_data [cellIndex].isLoaded = true;
		if (_data [cellIndex].image == null) {
			StartCoroutine (LoadImage (_data [dataIndex].url, (image) => {
				if (image != null) {
					_data [cellIndex].image = image;
					cellView.SetImage (image);
				}
			}));
		} else {
			cellView.SetImage (_data [cellIndex].image);
		}

		return cellView;
	}

	private void CellViewVisibilityChanged(EnhancedScrollerCellView cellView)
	{
		//		if (cellView.active)
		//			(cellView as LeaderboardPanelScore).LoadImage (_data [cellView.dataIndex]);
		//		else {
		//			if (!_data [cellView.dataIndex].isLoaded) {
		//				(cellView as LeaderboardPanelScore).SetDefaultImage ();
		//			} else {
		//				
		//			}
		//		}
		if (_data [cellView.dataIndex].image != null) {
			(cellView as LeaderboardPanelScore).SetImage (_data [cellView.dataIndex].image);
		} else {
			(cellView as LeaderboardPanelScore).SetDefaultImage ();
		}


	}

	IEnumerator LoadImage(string url, System.Action<Texture> callback) {
		url = url + "&redirect=false";
		WWW www = new WWW(url);
		yield return www;
		if (www.error == null) {
			Dictionary<string, object> dict = Json.Deserialize (www.text) as Dictionary<string, object>;
			Dictionary<string, object> dataDict = dict ["data"] as Dictionary<string,object>;
			string imageUrl = (string)dataDict ["url"];
			WWW www2 = new WWW (imageUrl);
			yield return www2;
			if (www2.error == null) {
				//				avatar.texture = www2.texture;
				callback (www2.texture);
			} else {
				callback (null);
			}


		} else {
			//			avatar.texture = null;
			callback(null);
		}
	}
}
