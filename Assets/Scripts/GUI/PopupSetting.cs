﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;
using GoogleMobileAds.Api;
using Chronos;

public class PopupSetting : PopupAnim {

	public RectTransform sfxToggle;
	public RectTransform bgmToggle;

	public RectTransform FBButton;

	public Sprite onSprite, offSprite;

	public Sprite loginSprite, logoutSprite;

	private GameObject mapSelection;

	private bool isBtnClicked = false;
	// Use this for initialization
	void Start () {
		
	}
		
	void OnEnable() {
		OpenPopup (gameObject);
		isBtnClicked = false;
		Clock clock = Timekeeper.instance.Clock("Root");
		clock.localTimeScale = 0;
		Common.ShareInstance().isGamePause = true;

	}

	private void LoginFB(System.Action callback = null) {
		if (mapSelection) {
			mapSelection.GetComponent<MapSelectionController> ().isSetAvatar = false;
		}

		PopupManager.Instance.ShowActivityIndicator();

		ApiManager.Instance.LoginSync(response => {
			PopupManager.Instance.HideActivityIndicator();

			if (response == null) {
				return;
			}

			if (response.Code != Settings.SuccessCode) {
				Debug.Log(response.Message);
				Debug.Log(response.ResultMessage);
				return;
			}
			else {
				Debug.Log(response.Message);
				if (mapSelection) {
					mapSelection.GetComponent<MapSelectionController>().SettingLevelBtn();
					mapSelection.GetComponent<MapSelectionController>().GetFriendHightestLevel();
				}
				StatsManager.Instance.UpdateStats();

				FBButton.GetComponent<Image>().sprite = logoutSprite;
				// username.GetComponent<Text>().text = response.Name;
				callback();
			}
		});
	}

	private void Logout() {
		PopupManager.Instance.ShowActivityIndicator();

		ApiManager.Instance.Logout(response => {
			PopupManager.Instance.HideActivityIndicator();
			if (response.Code == Settings.SuccessCode) {
				FBButton.GetComponent<Image>().sprite = loginSprite;
			} 

		});



	}
		
	// Update is called once per frame
	void Update () {
		if (Input.GetKey (KeyCode.Escape)) {
			ClosePopup ();
		}
	}

	public void SetButton() {
		if (sfxToggle) {
			sfxToggle.GetComponent<Image> ().sprite = UserData.Instance.SFXOn ? onSprite : offSprite;
		}

		if (bgmToggle) {
			bgmToggle.GetComponent<Image> ().sprite = UserData.Instance.BGMOn ? onSprite : offSprite;
		}

		if (FBButton) {

			mapSelection = GameObject.FindObjectOfType<MapSelectionController> ().gameObject;

			if (FacebookManager.Instance.IsLogin ()) {
				FBButton.GetComponent<Image> ().sprite = logoutSprite;
			} else {
				FBButton.GetComponent<Image> ().sprite = loginSprite;
			}
		}
	}

	public void ToggleSFX() {
		UserData.Instance.SFXOn = !UserData.Instance.SFXOn;
		UserData.Instance.Save ();
		SoundManager.Instance.SetFXEnable (UserData.Instance.SFXOn);
		SetButton ();
	}

	public void ToggleBGM() {
		UserData.Instance.BGMOn = !UserData.Instance.BGMOn;
		UserData.Instance.Save ();
		SoundManager.Instance.SetMusicEnable (UserData.Instance.BGMOn);
		SetButton ();
	}
	
	public void ClosePopup() {
		if (isBtnClicked) {
			return;
		}
		isBtnClicked = true;

//		GetComponent<Animator>().SetTrigger(Settings.ClosePopupTrigger);
		DeactivePopupAnim(gameObject, () => {
			if (GameObject.FindObjectOfType<GUIController>()) {
				if (!GameObject.FindObjectOfType<GUIController>().isMenuShowed) {
					if (!Common.ShareInstance().isShowingTutorial) {
						Clock clock = Timekeeper.instance.Clock("Root");
						clock.localTimeScale = 1;
						Common.ShareInstance().isGamePause = false;
					}
				}
			}
			else {
				Clock clock = Timekeeper.instance.Clock("Root");
				clock.localTimeScale = 1;
				Common.ShareInstance().isGamePause = false;
			}

		});

		SoundManager.Instance.PlaySound (SoundID.ButtonClose);

	}


	public void Replay() {

		string title = StringConstrain.ReplayGameTitles [Random.Range (0, StringConstrain.ReplayGameTitles.Length)];
		string message = StringConstrain.ReplayGameMsgs [Random.Range (0, StringConstrain.ReplayGameMsgs.Length)];
		PopupManager.Instance.OpenPopupNotificationConfirm (title, message, () => {
			// Decrease Energy
//			if (UserData.Instance.Energy > 0) {
//				PopupManager.Instance.HideTapToPlay();
//				StatsManager.Instance.DescreaseEnergy();
//				FadePanel.Instance.LoadLevel(Application.loadedLevelName);
//
//				SoundManager.Instance.PlaySound (SoundID.ButtonOpen);
//
//				isBtnClicked = true;
//				ClosePopup();
//			}
//			else {
//				PopupManager.Instance.OpenPopupSuggestEnergy(false);	
//				SoundManager.Instance.PlaySound (SoundID.Wrong);
//			}
			FadePanel.Instance.LoadLevel(SceneManager.GetActiveScene().name);

			SoundManager.Instance.PlaySound (SoundID.ButtonOpen);

			ClosePopup();

		});
	}
    
	public void Quit() {
		string title = StringConstrain.QuitGameTitles [Random.Range (0, StringConstrain.QuitGameTitles.Length)];
		string message = StringConstrain.QuitGameMsgs [Random.Range (0, StringConstrain.QuitGameMsgs.Length)];
		PopupManager.Instance.OpenPopupNotificationConfirm (title, message, () => {
			PopupManager.Instance.HideTapToPlay();
			PopupManager.Instance.HideTapToReturn();
			FadePanel.Instance.LoadLevel(Settings.MapSelectScene);

			Analytics.Instance.LogLevel(MapManager.Instance.currentLevelPlayed, LogLevelType.Quit);

			if (!UserData.Instance.CompleteBeginTutorial) {
				UserData.Instance.CompleteBeginTutorial = true;
			}

			ClosePopup();
		});
	}

	public void PlayTutorial() {
		PopupManager.Instance.OpenPopupNotificationConfirm ("Wait!", StringConstrain.PlayTutorialMsg, () => {
			PopupManager.Instance.HideTapToPlay();
			PopupManager.Instance.HideTapToReturn();
			FadePanel.Instance.LoadLevel("Tutorial1");

//			Analytics.Instance.LogScreen("Play Tutorial");
			ClosePopup();
		});
	}

	public void FBButtonClick() {
		if (FacebookManager.Instance.IsLogin ()) {
			Logout ();
		} else {
			LoginFB ();
		}
	}

}
