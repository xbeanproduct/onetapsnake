﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using DG.Tweening;

public class PopupPlayLevelController : PopupAnim {

	public Text _levelText;
    public GameObject starUIs;
    public Text scoreValue;

	private int currentStar;
    // public GameObject friendRect; 
	public int _currentLevel;

	private bool isBtnClicked = false;
	// Use this for initialization

	void OnEnable() {
		OpenPopup (gameObject);
	}

	public void SetLevelText(int level) {
        _levelText.text = "Level " + level;
        _currentLevel = level; 
        SetStarAndScore(level);
	}
    
    private void SetStarAndScore(int level) {
        int star = 0;
        int score = 0;
        foreach(Hashtable mapInfo in UserData.Instance.MapsData) {
            
            if (int.Parse(mapInfo[Settings.MapLevelKey].ToString()) == level) {
                star = int.Parse(mapInfo[Settings.MapStarKey].ToString());
                score = int.Parse(mapInfo[Settings.MapScoreKey].ToString());
                break;
            }
        }
        
		currentStar = star;
        switch (star) {
            case 1:
            starUIs.FindInChildren(Settings.StarUI1).GetComponent<Animator>().SetTrigger(Settings.ShowStarTrigger);
            break;
            case 2:
            starUIs.FindInChildren(Settings.StarUI1).GetComponent<Animator>().SetTrigger(Settings.ShowStarTrigger);
            starUIs.FindInChildren(Settings.StarUI2).GetComponent<Animator>().SetTrigger(Settings.ShowStarTrigger);
            break;
            case 3:
            starUIs.FindInChildren(Settings.StarUI1).GetComponent<Animator>().SetTrigger(Settings.ShowStarTrigger);
            starUIs.FindInChildren(Settings.StarUI2).GetComponent<Animator>().SetTrigger(Settings.ShowStarTrigger);
            starUIs.FindInChildren(Settings.StarUI3).GetComponent<Animator>().SetTrigger(Settings.ShowStarTrigger);
            break;
        }  
        
        scoreValue.text = score.ToString();      
    }
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey (KeyCode.Escape)) {
			CloseBtn ();
		}
	}
	
    void ResetList() {
        
    }
	public void CloseBtn() {
		if (isBtnClicked) {
			return;
		}
		isBtnClicked = true;
//		GetComponent<Animator>().SetTrigger(Settings.ClosePopupTrigger);
		DestroyPopupAnim(gameObject);

		SoundManager.Instance.PlaySound (SoundID.ButtonClose);
	}

	public void OnButtonPressed()
	{
		SoundManager.Instance.PlaySound (SoundID.ButtonOpen);
	}
	
	public void PlayBtn() {

//		if (UserData.Instance.Energy > 0) {
//
//			if (_currentLevel > 8) {
//				StatsManager.Instance.DescreaseEnergy ();
//			}
//			FadePanel.Instance.LoadLevel(Settings.LevelScene + _currentLevel);
//			MapManager.Instance.currentLevelPlayed = _currentLevel;
//			MapManager.Instance.currentLevelStar = currentStar;
//			Destroy(gameObject);
//
//			SoundManager.Instance.PlaySound (SoundID.ButtonChangeScene);
//		}
//		else{
//			PopupManager.Instance.OpenPopupSuggestEnergy(false);
//			SoundManager.Instance.PlaySound (SoundID.Wrong);
//		}

		FadePanel.Instance.LoadLevel(Settings.LevelScene + _currentLevel);
		MapManager.Instance.currentLevelPlayed = _currentLevel;
		MapManager.Instance.currentLevelStar = currentStar;
		Destroy(gameObject);

		SoundManager.Instance.PlaySound (SoundID.ButtonChangeScene);
	}
    
}
