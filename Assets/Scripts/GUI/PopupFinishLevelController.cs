﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections;
using DG.Tweening;

public class PopupFinishLevelController : PopupAnim {

	public GameObject starUIs;
    public Text scoreValue;

	private bool isBtnClicked;

	private RectTransform bg;
	private RectTransform popup;
	private RectTransform title;

	public RectTransform replay, next;
	public Text food;
	public Text scoins;

	public RectTransform foodIcon;
	public Sprite foodChap1, foodChap2, foodChap3;

	private int foodNum = 0;
	private int currentChapter = 1;
	// Use this for initialization
    
    void OnEnable() {
        switch (Common.ShareInstance().star) {
            case 1: 
            starUIs.FindInChildren(Settings.StarUI1).GetComponent<Animator>().SetTrigger(Settings.ShowStarTrigger);
            break;
            case 2:
            starUIs.FindInChildren(Settings.StarUI1).GetComponent<Animator>().SetTrigger(Settings.ShowStarTrigger);
            starUIs.FindInChildren(Settings.StarUI2).GetComponent<Animator>().SetTrigger(Settings.ShowStarTrigger);
            break;
            case 3:
            starUIs.FindInChildren(Settings.StarUI1).GetComponent<Animator>().SetTrigger(Settings.ShowStarTrigger);
            starUIs.FindInChildren(Settings.StarUI2).GetComponent<Animator>().SetTrigger(Settings.ShowStarTrigger);
            starUIs.FindInChildren(Settings.StarUI3).GetComponent<Animator>().SetTrigger(Settings.ShowStarTrigger);
            break;
        }
        
		OpenPopup (gameObject);

		CheckChapter ();
		ConvertCoin ();
    }
	
	// Update is called once per frame
	void Update () {
		// if (_starText) {
		// 	string text = "";
		// 	if (Common.ShareInstance().star == 1) {
		// 		text = "You earned " + Common.ShareInstance().star + " star";
		// 	}
		// 	else {
		// 		text = "You earned " + Common.ShareInstance().star + " stars";
		// 	}
		// 	_starText.text = text;
		// }
		// else {
		// 	_starText = gameObject.FindInChildren("StarEarn").GetComponent<Text>();
		// }
        if (Common.ShareInstance().isMapCompleted) {
            scoreValue.text = Common.ShareInstance().score.ToString();
			food.text = foodNum.ToString ();
        }

	}

	void CheckChapter() {
		currentChapter = 1;
		if (MapManager.Instance.currentLevelPlayed > Settings.Chapter2Level) {
			currentChapter = 2;
		}

		if (MapManager.Instance.currentLevelPlayed > Settings.Chapter3Level) {
			currentChapter = 3;
		}

		switch (currentChapter) {
		case 1:
			foodIcon.GetComponent<Image> ().sprite = foodChap1;
			break;
		case 2:
			foodIcon.GetComponent<Image> ().sprite = foodChap2;
			break;
		case 3:
			foodIcon.GetComponent<Image> ().sprite = foodChap3;
			break;
		}

	}

	void ConvertCoin() {
		foodNum = 0;
		int temp = Common.ShareInstance ().foodEat;
		int currentCoin = UserData.Instance.EarnScoin;
		int desCoin = currentCoin + temp;

		Transform xcoinText = GameObject.Find ("XcoinText").transform;

		DOTween.Sequence ()
			.AppendInterval(1.5f)
			.Append(
				DOTween.To((value) => {
					foodNum = Mathf.RoundToInt(value);
				}, 0, temp, 2).SetEase(Ease.OutSine))
			.AppendInterval(.5f)
			.Append (
				DOTween.To ((value) => {
					foodNum = Mathf.RoundToInt(value);
				}, temp, 0, 1.5f).SetEase (Ease.OutSine))
			.Join (
				DOTween.To ((value) => {
					scoins.text = Mathf.RoundToInt(value).ToString();
				}, 0, temp * currentChapter, 1.5f).SetEase (Ease.OutSine))
			.Join(
				DOTween.To((value) => {
					StatsManager.Instance.SetEarnScoin(Mathf.RoundToInt(value));
				}, currentCoin, desCoin, 1.5f).SetEase(Ease.OutSine))
			.Join(
				xcoinText.DOScale(new Vector2(1.5f, 1.5f), .5f)
			)
			.OnComplete (() => {
				xcoinText.DOScale(Vector2.one, .5f);
				EnableButton();
		});
		
	}

	public void EnableButton() {
		replay.SetScale (0);
		next.SetScale (0);
		replay.Show ();
		next.Show ();

		DOTween.Sequence ()
			.Append (replay.DOScale (1, .5f).SetEase (Ease.OutBack))
			.Join (next.DOScale (1, .5f).SetEase (Ease.OutBack));
	}
	
	public void NextLevelBtn() {
		if (isBtnClicked)
			return;

		isBtnClicked = true;

		FadePanel.Instance.LoadLevel(Settings.MapSelectScene);

		DestroyPopupAnim(gameObject);

		SoundManager.Instance.PlaySound (SoundID.ButtonChangeScene);
	}
    
	
	public void PlayAgainBtn() {
		if (isBtnClicked)
			return;
		
		// Decrease Energy
//		if (UserData.Instance.Energy > 0) {
//			StatsManager.Instance.DescreaseEnergy();
//			FadePanel.Instance.LoadLevel(Application.loadedLevelName);
//
//			DestroyPopupAnim (gameObject);
//
//			SoundManager.Instance.PlaySound (SoundID.ButtonOpen);
//
//			isBtnClicked = true;
//		}
//		else {
//			PopupManager.Instance.OpenPopupSuggestEnergy(false);
//
//			SoundManager.Instance.PlaySound (SoundID.Wrong);
//		}
		FadePanel.Instance.LoadLevel(SceneManager.GetActiveScene().name);

		DestroyPopupAnim (gameObject);

		SoundManager.Instance.PlaySound (SoundID.ButtonOpen);

		isBtnClicked = true;

	}

	public void ShareFB() {
		if (FacebookManager.Instance.IsLogin ()) {
			BeginShare ();
		} else {
			FacebookManager.Instance.Login (result => {
				if (FacebookManager.Instance.IsLogin()) {
					BeginShare();
				}
			});
		}
	}

	private void BeginShare() {
		string contentUrl = "http://google.com.vn";
		string imageUrl = "http://google.com.vn";

		FacebookManager.Instance.Share(contentUrl, string.Format("I won level {0} with {1} scores and {2} {3}", 
			MapManager.Instance.currentLevelPlayed, 
			Common.ShareInstance().score,
			Common.ShareInstance().star,
			Common.ShareInstance().star > 1 ? "stars" : "star"),
			imageUrl, result => {
				Debug.Log("Share completed!");
			});
	}
}
