﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PopupShowAdsComplete : MonoBehaviour {

	public Text messageText;
	// Use this for initialization
	void Start () {

	}

	public void SetText(int value, string type) {
		messageText.text = StringConstrain.ShowAdsRewardMsgFormat (value, type);

		Analytics.Instance.LogEvent (Analytics.Advertisement, Analytics.AdsTypeUnity, string.Format("View ads for {0}", type), 0);
	}


	// Update is called once per frame
	void Update () {

	}

	public void Cancel() {
		GetComponent<Animator>().SetTrigger(Settings.ClosePopupTrigger);

		SoundManager.Instance.PlaySound (SoundID.ButtonClose);
	}

	public void DeactivePopup() {
		gameObject.SetActive(false);
	}

}