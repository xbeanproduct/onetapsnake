﻿using UnityEngine;
using System.Collections;

public class ButtonSkin : MonoBehaviour {

	[SerializeField] GameObject selectedBG;
	public  bool isSelected;
	public int skinIndex;
	public int price;


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

	}

	public void SetSelected(bool selected)
	{

		isSelected = selected;
		selectedBG.SetActive(isSelected);

		Debug.Log(string.Format("Selected: {0}", selected));
	}
		
		
}
