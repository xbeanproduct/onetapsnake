﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class PopupNotification : PopupAnim {

	public Text title;
	public Text messageText;

	public Action cancelCallback;

	private bool isBtnClicked;
	// Use this for initialization
	void Start () {

	}

	void OnEnable() {
		OpenPopup (gameObject);
	}

	public void SetText(string title, string message) {
		isBtnClicked = false;
		this.title.text = title;
		this.messageText.text = message;

	}


	// Update is called once per frame
	void Update () {
		if (Input.GetKey (KeyCode.Escape)) {
			Cancel ();
		}
	}

	public void Cancel() {
		if (isBtnClicked)
			return;

		isBtnClicked = true;

		if (cancelCallback != null) {
			cancelCallback ();
		}

//		GetComponent<Animator>().SetTrigger(Settings.ClosePopupTrigger);
		DeactivePopupAnim(gameObject);
	}


}