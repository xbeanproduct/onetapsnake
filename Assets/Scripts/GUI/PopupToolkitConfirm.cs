﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections;
using System;
using Chronos;

public class PopupToolkitConfirm : PopupAnim {

	public Text title;
	public Text messageText;
	public Action _callback;

	public GameObject tutorialToolkit;

	private int cost = 0;

	private ToolkitType _type;

	private bool isBtnClicked;

	// Use this for initialization
	void Start () {

	}

	void OnEnable() {
		OpenPopup (gameObject);
		isBtnClicked = false;
	}

	public void ShowTutorial(bool enable) {
		tutorialToolkit.SetActive (enable);
	}

	public void SetText(int value, ToolkitType type) {
		isBtnClicked = false;
		_type = type;
		messageText.text = StringConstrain.ToolkitConfirmMsgFormat (value);
		title.text = StringConstrain.ToolkitConfirmTitle;

		cost = value;


	}


	// Update is called once per frame
	void Update () {
		if (Input.GetKey (KeyCode.Escape)) {
			Cancel ();
		}
	}

	public void Confirm() {
		if (isBtnClicked)
			return;

		isBtnClicked = true;

		// Decrease Energy

		StatsManager.Instance.DecreaseScoin(cost);

		// Tutorial
		if (!UserData.Instance.CompleteToolkitTutorial) {
			if (Common.ShareInstance ().isCurrentMapTutorial) {
				PopupManager.Instance.ShowToolkitConfirmTutorial (false);
				UserData.Instance.CompleteToolkitTutorial = true;
				Common.ShareInstance ().isCurrentMapTutorial = false;
				UserData.Instance.Save ();

//				var menuBtn = GameObject.FindGameObjectWithTag ("MenuButton");
//				ExecuteEvents.Execute(menuBtn, new PointerEventData(EventSystem.current), ExecuteEvents.pointerClickHandler);
			}
		}


//		Cancel ();
		DeactivePopupAnim(gameObject, () => {
			if (GameObject.FindGameObjectWithTag ("MenuButton")) {
				ExecuteEvents.Execute(GameObject.FindGameObjectWithTag ("MenuButton"), new PointerEventData(EventSystem.current), ExecuteEvents.pointerClickHandler);
			}
			if (_callback != null) {
				_callback ();
			}
		});

		Analytics.Instance.LogEvent(Analytics.Toolkit, Analytics.ToolkitUseConfirm, _type.ToString(), 0);
	}

	public void Help() {
		PopupManager.Instance.OpenPopupToolkitHelp (_type);

		Analytics.Instance.LogEvent (Analytics.Toolkit, Analytics.ToolkitViewHelp, _type.ToString (), 0);
	}

	public void Cancel() {
		if (isBtnClicked) {
			return;
		}
		isBtnClicked = true;
		// Tutorial
		if (!UserData.Instance.CompleteToolkitTutorial) {
			if (Common.ShareInstance ().isCurrentMapTutorial) {
				return;
			}
		}

		DeactivePopupAnim (gameObject);
//		GetComponent<Animator>().SetTrigger(Settings.ClosePopupTrigger);
//		DeactivePopupAnim(gameObject, () => {
//			if (GameObject.FindGameObjectWithTag ("MenuButton")) {
//				ExecuteEvents.Execute(GameObject.FindGameObjectWithTag ("MenuButton"), new PointerEventData(EventSystem.current), ExecuteEvents.pointerClickHandler);
//			}
//		});

		Analytics.Instance.LogEvent (Analytics.Toolkit, Analytics.ToolkitUseCancel, _type.ToString (), 0);

		SoundManager.Instance.PlaySound (SoundID.ButtonOpen);
	}
		
}
