﻿using UnityEngine;
using System.Collections;

public class PopupManager : Singleton<PopupManager> {
	
    private GameObject _tapToPlay;
    public GameObject tapToPlay;

	private GameObject _tapToReturn;
    
    private GameObject _popupPlayLevel;
    public GameObject popupPlayLevelPref;
    
    private GameObject _popupScoreLeaderboard;
    public GameObject popupScoreLeaderboardPref;
    
    private GameObject _popupStarLeaderboard;
    public GameObject popupStarLeaderboardPref;
    
    private GameObject _popupFinishLevel;
    public GameObject popupFinishLevelPref;
    
    private GameObject _activityIndicator;
    private GameObject _popupChangeSkin;
	private GameObject _popupSuggestEnergy;
    private GameObject _popupUserStats;
	private GameObject _popupToolkitConfirm;
	private GameObject _popupToolkitHelp;
	private GameObject _popupSuggestScoin;
	private GameObject _popupNotification;
	private GameObject _popupNotificationConfirm;
	private GameObject _popupSetting;
	private GameObject _popupMapSetting;

	private GameObject _popupGameOver;

	// Use this for initialization
	void Start () {
		if(!_popupSuggestEnergy) {
			_popupSuggestEnergy = gameObject.FindInChildren(Settings.PopupSuggestEnergy);
		}
		_popupSuggestEnergy.SetActive(false);

		if (!_popupSuggestScoin) {
			_popupSuggestScoin = gameObject.FindInChildren (Settings.PopupSuggestScoin);
		}
		_popupSuggestScoin.SetActive (false);

		if (!_popupGameOver) {
			_popupGameOver = gameObject.FindInChildren(Settings.PopupGameOver);
		}
		_popupGameOver.SetActive(false);
        
        if (!_popupChangeSkin) {
            _popupChangeSkin = gameObject.FindInChildren(Settings.PopupChangeSkin);
        }
        _popupChangeSkin.SetActive(false);
        
        if (!_popupUserStats) {
            _popupUserStats = gameObject.FindInChildren(Settings.PopupUserStats);
        }
        _popupUserStats.SetActive(false);

		if (!_popupNotificationConfirm) {
			_popupNotificationConfirm = gameObject.FindInChildren (Settings.PopupNotificationConfirm);
		}
		_popupNotificationConfirm.SetActive (false);

		if (!_popupToolkitConfirm) {
			_popupToolkitConfirm = gameObject.FindInChildren (Settings.PopupToolkitConfirm);
		}
		_popupToolkitConfirm.SetActive (false);

		if (!_popupToolkitHelp) {
			_popupToolkitHelp = gameObject.FindInChildren (Settings.PopupToolkitHelp);
		}
		_popupToolkitHelp.SetActive (false);

		if (!_popupNotification) {
			_popupNotification = gameObject.FindInChildren (Settings.PopupNotification);
		}
		_popupNotification.SetActive (false);
        
		if (!_popupSetting) {
			_popupSetting = gameObject.FindInChildren (Settings.PopupSetting);
		}
		_popupSetting.SetActive (false);

        if (!_activityIndicator) {
            _activityIndicator = gameObject.FindInChildren(Settings.ActivityIndicator);
        }
        _activityIndicator.SetActive(false);
        
        if (!_tapToPlay) {
            _tapToPlay = gameObject.FindInChildren(Settings.TapToPlay);
        }
        _tapToPlay.SetActive(false);

		if (!_tapToReturn) {
			_tapToReturn = gameObject.FindInChildren ("TapToReturn");
		}
		_tapToReturn.SetActive (false);

		if (!_popupMapSetting) {
			_popupMapSetting = gameObject.FindInChildren ("MenuMapSetting");
		}
		_popupMapSetting.SetActive (false);
        
    }
	
	// Update is called once per frame
	void Update () {
	
	}
    
    public int GetCurrentLevel() {
		return _popupPlayLevel.GetComponent<PopupPlayLevelController>()._currentLevel;

    }
	
	public void OpenPopupSuggestEnergy(bool isBuy) {
		_popupSuggestEnergy.GetComponent<PopupSuggestEnergyController> ().SetText (isBuy);
		if (_popupSuggestEnergy.activeInHierarchy) {
			return;
		}
		_popupSuggestEnergy.SetActive(true);

		SoundManager.Instance.PlaySound (SoundID.ButtonOpen);
	}
	
	public void OpenPopupFinishLevel() {
		_popupFinishLevel = (GameObject)Instantiate(popupFinishLevelPref, popupFinishLevelPref.transform.position, Quaternion.identity);
        
        _popupFinishLevel.GetComponent<RectTransform>().SetParent(transform, false);

		SoundManager.Instance.PlaySound (SoundID.ButtonOpen);
	}

	public void OpenPopupPlayLevel(int level) {
		if (_popupPlayLevel != null) {
			return;
		}

		GameObject[] menus = GameObject.FindGameObjectsWithTag ("MenuPopup");
		foreach (GameObject menu in menus) {
			if (menu.activeInHierarchy) {
				return;
			}
		}

		_popupPlayLevel = (GameObject)Instantiate(popupPlayLevelPref, popupPlayLevelPref.transform.position, Quaternion.identity);

		_popupPlayLevel.GetComponent<RectTransform>().SetParent(transform, false);
		_popupPlayLevel.GetComponent<PopupPlayLevelController>().SetLevelText(level);
		// _popupPlayLevel.SetActive(true);

		SoundManager.Instance.PlaySound (SoundID.ButtonOpen);
	}
	
	public void OpenPopupGameOver() {
		if (_popupGameOver && _popupGameOver.activeInHierarchy) {
			return;
		}
		_popupGameOver.SetActive(true);

		SoundManager.Instance.PlaySound (SoundID.ButtonOpen);
	}
    
    public void OpenPopupScoreLeaderboard() {
        _popupScoreLeaderboard = (GameObject)Instantiate(popupScoreLeaderboardPref, popupScoreLeaderboardPref.transform.position, Quaternion.identity);
        
        _popupScoreLeaderboard.GetComponent<RectTransform>().SetParent(transform, false);

		SoundManager.Instance.PlaySound (SoundID.ButtonOpen);
    }
    
    public void OpenPopupStarLeaderboard() {
        _popupStarLeaderboard = (GameObject)Instantiate(popupStarLeaderboardPref, popupStarLeaderboardPref.transform.position, Quaternion.identity);
        
        _popupStarLeaderboard.GetComponent<RectTransform>().SetParent(transform, false);

		SoundManager.Instance.PlaySound (SoundID.ButtonOpen);
    }
    
    public void OpenPopupChangeSkin() {
        if (_popupChangeSkin.activeInHierarchy) {
            return;
        }
        _popupChangeSkin.GetComponent<PopupChangeSkin>().OnShowPopup();

		SoundManager.Instance.PlaySound (SoundID.ButtonOpen);
    }
    
    public void OpenPopupUserStats() {
        if (_popupUserStats.activeInHierarchy) {
            return;
        }
        _popupUserStats.SetActive(true);

		SoundManager.Instance.PlaySound (SoundID.ButtonOpen);
    }

	public void OpenPopupNotificationConfirm(string title, string message, System.Action confirmCallback = null, System.Action cancelCallback = null) {
		_popupNotificationConfirm.GetComponent<PopupNotificationConfirm> ().SetText (title, message);
		_popupNotificationConfirm.GetComponent<PopupNotificationConfirm> ().confirmCallback = confirmCallback;
		_popupNotificationConfirm.GetComponent<PopupNotificationConfirm> ().cancelCallback = cancelCallback;
		if (_popupNotificationConfirm.activeInHierarchy) {
			return;
		}
		_popupNotificationConfirm.SetActive (true);

		SoundManager.Instance.PlaySound (SoundID.ButtonOpen);
	}

	public void OpenPopupToolkitConfirm(int value, ToolkitType type, System.Action buyCallback = null) {
		_popupToolkitConfirm.GetComponent<PopupToolkitConfirm> ().SetText (value, type);
		_popupToolkitConfirm.GetComponent<PopupToolkitConfirm> ()._callback = buyCallback;
		if (_popupToolkitConfirm.activeInHierarchy) {
			return;
		}
		_popupToolkitConfirm.SetActive (true);

		SoundManager.Instance.PlaySound (SoundID.ButtonOpen);
	}

	public void ShowToolkitConfirmTutorial(bool enable) {
		if (_popupToolkitConfirm.activeInHierarchy) {
			_popupToolkitConfirm.GetComponent<PopupToolkitConfirm> ().ShowTutorial (enable);
		}
	}

	public void OpenPopupToolkitHelp(ToolkitType type) {
		_popupToolkitHelp.GetComponent<PopupToolkitHelp> ().SetText (type);
		if (_popupToolkitHelp.activeInHierarchy) {
			return;
		}
		_popupToolkitHelp.SetActive (true);

		SoundManager.Instance.PlaySound (SoundID.ButtonOpen);
	}
    
	public void OpenPopupSuggestScoin(int value, bool isBuy, InsufficientType type) {
		_popupSuggestScoin.GetComponent<PopupSuggestScoin> ().SetText (value, isBuy, type);
		if (_popupSuggestScoin.activeInHierarchy) {
			return;
		}
		_popupSuggestScoin.SetActive (true);

		SoundManager.Instance.PlaySound (SoundID.ButtonOpen);
	}

	public void OpenPopupNotification(string title, string text, System.Action cancelCallback = null) {
		_popupNotification.GetComponent<PopupNotification> ().SetText (title, text);
		_popupNotification.GetComponent<PopupNotification> ().cancelCallback = cancelCallback;
		if (_popupNotification.activeInHierarchy) {
			return;
		}
		_popupNotification.SetActive (true);

		SoundManager.Instance.PlaySound (SoundID.ButtonOpen);
	}

	public void OpenPopupSetting() {
		_popupSetting.GetComponent<PopupSetting> ().SetButton ();
		if (_popupSetting.activeInHierarchy) {
			return;
		}

		_popupSetting.SetActive (true);
		SoundManager.Instance.PlaySound (SoundID.ButtonOpen);
	}

	public void OpenPopupMapSetting() {
		_popupMapSetting.GetComponent<PopupMapSetting> ().SetButton ();
		if (_popupMapSetting.activeInHierarchy) {
			return;
		}

		_popupMapSetting.SetActive (true);
		SoundManager.Instance.PlaySound (SoundID.ButtonOpen);
	}

    public void ShowActivityIndicator() {
        if (_activityIndicator.activeInHierarchy) {
            return;
        }
        _activityIndicator.SetActive(true);
    }
    
    public void HideActivityIndicator() {
        if (!_activityIndicator.activeInHierarchy) {
            return;
        }
        _activityIndicator.GetComponent<Animator>().SetTrigger(Settings.PopupLoadingDisappear);

    }
    
    public void ShowTapToPlay() {
        if (_tapToPlay && _tapToPlay.activeInHierarchy) {
            return;
        }
        _tapToPlay.SetActive(true);
    }
    
    public void HideTapToPlay() {
        if (_tapToPlay && !_tapToPlay.activeInHierarchy) {
            return;
        }
        _tapToPlay.SetActive(false);
    }

	public void ShowTapToReturn() {
		if (_tapToReturn && _tapToReturn.activeInHierarchy) {
			return;
		}
		_tapToReturn.SetActive (true);
	}

	public void HideTapToReturn() {
		if (_tapToReturn && !_tapToReturn.activeInHierarchy) {
			return;
		}
		_tapToReturn.SetActive(false);
	}
}
