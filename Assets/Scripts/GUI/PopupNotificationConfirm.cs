﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;


public class PopupNotificationConfirm : PopupAnim {

	public Text title;
	public Text messageText;
	public Action confirmCallback;
	public Action cancelCallback;

	private bool isBtnClicked;
	// Use this for initialization
	void Start () {
		
	}

	void OnEnable() {
		OpenPopup (gameObject);
	}

	public void SetText(string title, string message) {
		isBtnClicked = false;
		this.messageText.text = message;
		this.title.text = title;

	}


	// Update is called once per frame
	void Update () {
		if (Input.GetKey (KeyCode.Escape)) {
			Cancel ();
		}
	}

	public void Confirm() {
		if (isBtnClicked)
			return;

		isBtnClicked = true;

		// Decrease Energy
		if (confirmCallback != null) {
			confirmCallback ();
		}

		CancelWithouCallback ();

		SoundManager.Instance.PlaySound (SoundID.Coin);
	}

	private void CancelWithouCallback() {
//		GetComponent<Animator>().SetTrigger(Settings.ClosePopupTrigger);
		DeactivePopupAnim(gameObject);
	}

	public void Cancel() {
		if (isBtnClicked)
			return;

		isBtnClicked = true;

		if (cancelCallback != null) {
			cancelCallback ();
		}

//		GetComponent<Animator>().SetTrigger(Settings.ClosePopupTrigger);
		DeactivePopupAnim(gameObject);

		SoundManager.Instance.PlaySound (SoundID.ButtonClose);
	}


}
