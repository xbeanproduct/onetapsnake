﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using GoogleMobileAds.Api;

public class PopupSuggestEnergyController : PopupAnim {

    public Text suggestEnergyText;
	public Text title;


	private bool _isBuy;

	private bool isBtnClicked;
	// Use this for initialization
	void Start () {
		
	}

	void OnEnable() {
		OpenPopup (gameObject);
	}

	void RewardBasedVideoAd_Instance_OnAdLoaded (object sender, System.EventArgs e)
	{
		Debug.Log ("On Ads Loded");
	}
    
	public void SetText(bool isBuy) {
		isBtnClicked = false;
		_isBuy = isBuy;
		if (isBuy) {
			title.text = StringConstrain.BuyEnergyTitle;
			int rand = Random.Range (0, StringConstrain.BuyEnergyMsg.Length);
			suggestEnergyText.text = StringConstrain.BuyEnergyMsg [rand];

			Analytics.Instance.LogEvent (Analytics.Popup, Analytics.PopupTypeBuyEnergy, "Open", 0);
		} else {
			title.text = StringConstrain.SuggestEnergyTitle;
			int rand = Random.Range (0, StringConstrain.SuggestEnergyMsg.Length);
			suggestEnergyText.text = StringConstrain.SuggestEnergyMsg [rand];

			Analytics.Instance.LogEvent (Analytics.Popup, Analytics.PopupTypeSuggestEnergy, "Open", 0);
		}

//		Debug.Log ("Load Ads");
//		RewardBasedVideoAd.Instance.LoadAd (new AdRequest.Builder ().Build (), "ca-app-pub-2689619263921185/1964008355");
//		RewardBasedVideoAd.Instance.OnAdRewarded += OnAdRewarded;
//		RewardBasedVideoAd.Instance.OnAdLoaded += RewardBasedVideoAd_Instance_OnAdLoaded;
//		RewardBasedVideoAd.Instance.OnAdFailedToLoad += RewardBasedVideoAd_Instance_OnAdFailedToLoad;
	
    }

	void RewardBasedVideoAd_Instance_OnAdFailedToLoad (object sender, AdFailedToLoadEventArgs e)
	{
		Debug.Log ("Ads Load fail: " + e.Message);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public void ClosePopup() {
//		GetComponent<Animator>().SetTrigger(Settings.ClosePopupTrigger);
		DeactivePopupAnim(gameObject);

		SoundManager.Instance.PlaySound (SoundID.ButtonClose);
	}
    
	public void AskFriendBtn() {
		FacebookManager.Instance.RequestFriend (StringConstrain.InviteFriendTitle, StringConstrain.InviteFriendMsg, RequestObject.Energy);

		Analytics.Instance.LogEvent (Analytics.Popup, _isBuy ? Analytics.PopupTypeBuyEnergy : Analytics.PopupTypeSuggestEnergy, "Ask Friend", 0);
		SoundManager.Instance.PlaySound (SoundID.ButtonOpen);
	}
    
    public void WatchVideo() {
		if (isBtnClicked)
			return;

		isBtnClicked = true;

//		UnityAds.Instance.WatchVideoForEnergy (1);
        ClosePopup();

		Analytics.Instance.LogEvent (Analytics.Popup, _isBuy ? Analytics.PopupTypeBuyEnergy : Analytics.PopupTypeSuggestEnergy, "Watch Ads", 0);

//		WatchAdmobVideo ();
    }

	void WatchAdmobVideo() {
		if (RewardBasedVideoAd.Instance.IsLoaded ()) {
			Debug.Log ("Show Ads");
			RewardBasedVideoAd.Instance.Show ();

		}
	}

	void OnAdRewarded(object sender, Reward args) {
		Debug.Log (args.Type + " " + args.Amount);
	}
    
	public void BuyBtn() {
		if (isBtnClicked)
			return;

		isBtnClicked = true;

		StatsManager.Instance.SetFullEnergy();
        ClosePopup();

		SoundManager.Instance.PlaySound (SoundID.ButtonOpen);
	}
}
