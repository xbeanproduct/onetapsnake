﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections;

public class FadePanel : Singleton<FadePanel> {

	private GameObject _panel;
	private Image _image;
	private Animator _anim;
	private string _sceneName;
	private int _scene;
	
	
	// Use this for initialization
	void Start () {
		
		if (!_panel) {
			_panel = transform.GetChild(0).gameObject;
		}
		
		if (!_image) {
			_image = _panel.GetComponent<Image>();
		}
		
		if (!_anim) {
			_anim = _panel.GetComponent<Animator>();
		}
		
		_anim.SetTrigger(Settings.ScreenFadeOutTrigger);
	}
	
	void OnLevelWasLoaded() {
		
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public void LoadLevel(string levelName) {
		_scene = -1;
		_sceneName = levelName;

	    _anim.SetTrigger(Settings.ScreenFadeInTrigger);
		Invoke("LoadScene", .5f);
	} 
	
	public void LoadLevel(int level) {
		_scene = level;
		_sceneName = "";

	    _anim.SetTrigger(Settings.ScreenFadeInTrigger);
		Invoke("LoadScene", .5f);
	}
	
	private void LoadScene() {

//		SceneManager.UnloadScene (SceneManager.GetActiveScene ().name);

		if (_scene > 0) {
            SceneManager.LoadScene(_scene);
		}
		else {
			SceneManager.LoadScene(_sceneName);
		}
	 	_anim.SetTrigger(Settings.ScreenFadeOutTrigger);


	}
}
