﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Facebook.MiniJSON;

public class PopupUserStats : PopupAnim {

//    public RectTransform logoutFBBtn;
    public RectTransform avatar;
    public RectTransform username;
    
    public Text scores, stars, food, reverseFood, shield, died;
    
	private bool isBtnClicked = false;
	// Use this for initialization
	void OnEnable () {

		OpenPopup (gameObject);
		isBtnClicked = false;
	    if (UserData.Instance.IsFBLogin) {
           
//            logoutFBBtn.gameObject.SetActive(true);


            
            StartCoroutine(LoadImage(UserData.Instance.AvatarUrl));
            if (!string.IsNullOrEmpty(UserData.Instance.Username)) {
                username.GetComponent<Text>().text = UserData.Instance.Username;
                username.gameObject.SetActive(true);
            }
            
        }
        else {
          
//            logoutFBBtn.gameObject.SetActive(false);
            
            
        }
        
        int scoresValue = 0;
        int starsValue = 0;
        for(int i = 0; i < UserData.Instance.MapsData.Count; i++) {
            Hashtable mapData = (Hashtable)UserData.Instance.MapsData[i];
            scoresValue+= int.Parse(mapData[Settings.MapScoreKey].ToString());
            starsValue+= int.Parse(mapData[Settings.MapStarKey].ToString());
        }
        
        if (scores) {
            scores.text = scoresValue.ToString();
        }
        
        if (stars) {
            stars.text = starsValue.ToString();
        }
        
        if (food) {
            food.text = UserData.Instance.FoodEat.ToString();
        }
        
        if (reverseFood) {
            reverseFood.text = UserData.Instance.ReverseFoodEat.ToString();
        }
        
        if (shield) {
            shield.text = UserData.Instance.ShieldEat.ToString();
        }
        
        if (died) {
            died.text = UserData.Instance.Died.ToString();
        }
        
		Analytics.Instance.LogEvent (Analytics.Popup, Analytics.PopupUserStats, "Open", 0);
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey (KeyCode.Escape)) {
			ClosePopup ();
		}
	}
    
    IEnumerator LoadImage(string url) {
    
        
        url = url + "&redirect=false";
        WWW www = new WWW(url);
        yield return www;
        if (www.error == null) {
            Dictionary<string, object> dict = Json.Deserialize(www.text) as Dictionary<string, object>;
            Dictionary<string, object> dataDict = dict["data"] as Dictionary<string,object>;
            string imageUrl = (string)dataDict["url"];
            WWW www2 = new WWW(imageUrl);
            yield return www2;
            if (www2.error == null) {
                avatar.GetComponent<Image>().sprite = www2.texture.ToSprite();
            }
            
            
        }
    }
    
    public void ClosePopup() {
		if (isBtnClicked) {
			return;
		}
		isBtnClicked = true;
//		GetComponent<Animator>().SetTrigger(Settings.ClosePopupTrigger);
		DeactivePopupAnim(gameObject);

		SoundManager.Instance.PlaySound (SoundID.ButtonClose);
	}

    
    public void LogoutFB() {
        PopupManager.Instance.ShowActivityIndicator();
        
        ApiManager.Instance.Logout(response => {
            PopupManager.Instance.HideActivityIndicator();
           if (response.Code == Settings.SuccessCode) {
               
           } 
           
        });

		if (UserData.Instance.IsFBLogin) {

//			logoutFBBtn.gameObject.SetActive(true);



			StartCoroutine(LoadImage(UserData.Instance.AvatarUrl));
			if (!string.IsNullOrEmpty(UserData.Instance.Username)) {
				username.GetComponent<Text>().text = UserData.Instance.Username;
				username.gameObject.SetActive(true);
			}

		}
		else {

//			logoutFBBtn.gameObject.SetActive(false);


		}

		Analytics.Instance.LogEvent (Analytics.Popup, Analytics.PopupUserStats, "Logout Facebook", 0);
    }
}
