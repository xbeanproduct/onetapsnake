﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;


// Deriving the Purchaser class from IStoreListener enables it to receive messages from Unity Purchasing.
public class Purchaser : MonoBehaviour, IStoreListener
{
	private static IStoreController m_StoreController;                                                                  // Reference to the Purchasing system.
	private static IExtensionProvider m_StoreExtensionProvider;                                                         // Reference to store-specific Purchasing subsystems.

	// The purchase callback
	private Action<CoinPackage> _purchaseCallback;

	private static string kProductIDConsumable1 =    "consumable1";
	private static string kProductIDConsumable2 =    "consumable2";
	private static string kProductIDConsumable3 =    "consumable3";
	private static string kProductIDConsumable4 =    "consumable4";

	private static string kProductNameAppleSmallPackage =    "com.xbeangame.smallpackage";             // Apple App Store identifier for the consumable product.
	private static string kProductNameAppleMediumPackage = "com.xbeangame.mediumpackage";      // Apple App Store identifier for the non-consumable product.
	private static string kProductNameAppleBigPackage =  "com.xbeangame.bigpackage";       // Apple App Store identifier for the subscription product.
	private static string kProductNameAppleLargePackage =  "com.xbeangame.largepackage";

	private static string kProductNameGooglePlaySmallPackage =    "com.xbeangame.onetapsnake.smallpackage";        // Google Play Store identifier for the consumable product.
	private static string kProductNameGooglePlayMediumPackage = "com.xbeangame.onetapsnake.mediumpackage";     // Google Play Store identifier for the non-consumable product.
	private static string kProductNameGooglePlayBigPackage =  "com.xbeangame.onetapsnake.bigpackage";  // Google Play Store identifier for the subscription product.
	private static string kProductNameGooglePlayLargePackage =  "com.xbeangame.onetapsnake.largepackage";

	void Start()
	{
		// If we haven't set up the Unity Purchasing reference
		if (m_StoreController == null)
		{
			// Begin to configure our connection to Purchasing
			InitializePurchasing();
		}
	}

	public void InitializePurchasing() 
	{
		// If we have already connected to Purchasing ...
		if (IsInitialized())
		{
			// ... we are done here.
			return;
		}

		// Create a builder, first passing in a suite of Unity provided stores.
		var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());

		// Add a product to sell / restore by way of its identifier, associating the general identifier with its store-specific identifiers.
		builder.AddProduct(kProductIDConsumable1, ProductType.Consumable, new IDs(){{ kProductNameAppleSmallPackage,       AppleAppStore.Name },{ kProductNameGooglePlaySmallPackage,  GooglePlay.Name },});// Continue adding the non-consumable product.
		builder.AddProduct(kProductIDConsumable2, ProductType.Consumable, new IDs(){{ kProductNameAppleMediumPackage,       AppleAppStore.Name },{ kProductNameGooglePlayMediumPackage,  GooglePlay.Name },});// Continue adding the non-consumable product.
		builder.AddProduct(kProductIDConsumable3, ProductType.Consumable, new IDs(){{ kProductNameAppleBigPackage,       AppleAppStore.Name },{ kProductNameGooglePlayBigPackage,  GooglePlay.Name },});// Continue adding the non-consumable product.
		builder.AddProduct(kProductIDConsumable4, ProductType.Consumable, new IDs(){{ kProductNameAppleLargePackage,       AppleAppStore.Name },{ kProductNameGooglePlayLargePackage,  GooglePlay.Name },});// Continue adding the non-consumable product.
		//builder.AddProduct(kProductIDNonConsumable, ProductType.NonConsumable, new IDs(){{ kProductNameAppleNonConsumable,       AppleAppStore.Name },{ kProductNameGooglePlayNonConsumable,  GooglePlay.Name },});// And finish adding the subscription product.
		//builder.AddProduct(kProductIDSubscription, ProductType.Subscription, new IDs(){{ kProductNameAppleSubscription,       AppleAppStore.Name },{ kProductNameGooglePlaySubscription,  GooglePlay.Name },});// Kick off the remainder of the set-up with an asynchrounous call, passing the configuration and this class' instance. Expect a response either in OnInitialized or OnInitializeFailed.
		UnityPurchasing.Initialize(this, builder);
	}


	private bool IsInitialized()
	{
		// Only say we are initialized if both the Purchasing references are set.
		return m_StoreController != null && m_StoreExtensionProvider != null;
	}


	public void BuyPackage(CoinPackage package, Action<CoinPackage> callback)
	{
		Debug.Log("Build package: " + package.ToString());
		_purchaseCallback = callback;

		if (package == CoinPackage.Package1)
		{
			BuyProductID(kProductIDConsumable1);
		}
		else if (package == CoinPackage.Package2)
		{
			BuyProductID(kProductIDConsumable2);
		}
		else if (package == CoinPackage.Package3)
		{
			BuyProductID(kProductIDConsumable3);
		}
		else if (package == CoinPackage.Package4)
		{
			BuyProductID(kProductIDConsumable4);
		}
	}

	void BuyProductID(string productId)
	{
		// If the stores throw an unexpected exception, use try..catch to protect my logic here.
		try
		{
			// If Purchasing has been initialized ...
			if (IsInitialized())
			{
				// ... look up the Product reference with the general product identifier and the Purchasing system's products collection.
				Product product = m_StoreController.products.WithID(productId);

				// If the look up found a product for this device's store and that product is ready to be sold ... 
				if (product != null && product.availableToPurchase)
				{
					//Debug.Log (string.Format("Purchasing product asychronously: '{0}'", product.definition.id));// ... buy the product. Expect a response either through ProcessPurchase or OnPurchaseFailed asynchronously.
					m_StoreController.InitiatePurchase(product);
				}
				// Otherwise ...
				else
				{
					// ... report the product look-up failure situation  
					//Debug.Log ("BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");
				}
			}
			// Otherwise ...
			else
			{
				// ... report the fact Purchasing has not succeeded initializing yet. Consider waiting longer or retrying initiailization.
				//Debug.Log("BuyProductID FAIL. Not initialized.");
			}
		}
		// Complete the unexpected exception handling ...
		catch (Exception e)
		{
			// ... by reporting any unexpected exception for later diagnosis.
			//Debug.Log ("BuyProductID: FAIL. Exception during purchase. " + e);
		}
	}


	// Restore purchases previously made by this customer. Some platforms automatically restore purchases. Apple currently requires explicit purchase restoration for IAP.
	public void RestorePurchases()
	{
		// If Purchasing has not yet been set up ...
		if (!IsInitialized())
		{
			// ... report the situation and stop restoring. Consider either waiting longer, or retrying initialization.
			//Debug.Log("RestorePurchases FAIL. Not initialized.");
			return;
		}

		// If we are running on an Apple device ... 
		if (Application.platform == RuntimePlatform.IPhonePlayer || 
			Application.platform == RuntimePlatform.OSXPlayer)
		{
			// ... begin restoring purchases
			//Debug.Log("RestorePurchases started ...");

			// Fetch the Apple store-specific subsystem.
			var apple = m_StoreExtensionProvider.GetExtension<IAppleExtensions>();
			// Begin the asynchronous process of restoring purchases. Expect a confirmation response in the Action<bool> below, and ProcessPurchase if there are previously purchased products to restore.
			apple.RestoreTransactions((result) => {
				// The first phase of restoration. If no more responses are received on ProcessPurchase then no purchases are available to be restored.
				//Debug.Log("RestorePurchases continuing: " + result + ". If no further messages, no purchases available to restore.");
			});
		}
		// Otherwise ...
		else
		{
			// We are not running on an Apple device. No work is necessary to restore purchases.
			//Debug.Log("RestorePurchases FAIL. Not supported on this platform. Current = " + Application.platform);
		}
	}


	//  
	// --- IStoreListener
	//

	public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
	{
		// Purchasing has succeeded initializing. Collect our Purchasing references.
		//Debug.Log("OnInitialized: PASS");

		// Overall Purchasing system, configured with products for this application.
		m_StoreController = controller;
		// Store specific subsystem, for accessing device-specific store features.
		m_StoreExtensionProvider = extensions;
	}


	public void OnInitializeFailed(InitializationFailureReason error)
	{
		// Purchasing set-up has not succeeded. Check error for reason. Consider sharing this reason with the user.
		//Debug.Log("OnInitializeFailed InitializationFailureReason:" + error);
	}


	public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args) 
	{
		//Debug.Log("ID: " + args.purchasedProduct.definition.id + "...");
		// A consumable product has been purchased by this user.
		if (String.Equals(args.purchasedProduct.definition.id, kProductIDConsumable1, StringComparison.Ordinal))
		{
			// Add small coin here
			if (_purchaseCallback != null)
			{
				_purchaseCallback(CoinPackage.Package1);
			}
		}

		else if (String.Equals(args.purchasedProduct.definition.id, kProductIDConsumable2, StringComparison.Ordinal))
		{
			// Add medium coin here
			if (_purchaseCallback != null)
			{
				_purchaseCallback(CoinPackage.Package2);
			}
		}

		else if (String.Equals(args.purchasedProduct.definition.id, kProductIDConsumable3, StringComparison.Ordinal))
		{
			// Add large coin here
			if (_purchaseCallback != null)
			{
				_purchaseCallback(CoinPackage.Package3);
			}
		}

		else if (String.Equals(args.purchasedProduct.definition.id, kProductIDConsumable4, StringComparison.Ordinal))
		{
			// Add large coin here
			if (_purchaseCallback != null)
			{
				_purchaseCallback(CoinPackage.Package4);
			}
		}

		return PurchaseProcessingResult.Complete;
	}


	public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
	{
		// A product purchase attempt did not succeed. Check failureReason for more detail. Consider sharing this reason with the user.
		//Debug.Log(string.Format("OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}",product.definition.storeSpecificId, failureReason));
	}
}