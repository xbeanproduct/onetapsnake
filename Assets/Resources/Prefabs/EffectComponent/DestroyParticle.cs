﻿using UnityEngine;
using System.Collections;

public class DestroyParticle : MonoBehaviour {
    
    public float timeToDestoy = 1;
	// Use this for initialization
	void Start () {
	   Invoke("Destroy", timeToDestoy);
	}
	
    void Destroy() {
        Destroy(gameObject);
    }
    
	// Update is called once per frame
	void Update () {
	
	}
}
