﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class SkinEffect : ScriptableObject {

	public GameObject[] skinEffectList;
}
